import React from "react";

export const COLORS = {
    RED: {
      FIRE_ENGINE_RED: '#C11C22',
      DARK_RED: '#b61016'
    },
    WHITE: {
      WHITE: '#FFFFFF',
      WHITE_SMOKE: '#F3F3F3'
    },
    GREY: {
     LIGHT_GREY: '#D4D4D4',
     NIGHT_RIDER: '#333333',
     NERO: '#1C1C1C',
     DARK_GRAY: '#A9A9A9'
    },
    BLACK: {
      BLACK: '#000000'
    },
    YELLOW: {
      PARIS_DAISY: '#FDED4F'
    },
    GREEN:{
      GREEN: '#458b00'
    },
    BLUE:{
      ALICE_BLUE: '#D7ECFF'
    }
};
