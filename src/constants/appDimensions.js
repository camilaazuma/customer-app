import React from "react";
import {  Dimensions, } from "react-native";

let screenWidth = Dimensions.get('window').width,
    screenHeight = Dimensions.get('window').height,
    screenRatio = screenHeight/screenWidth,
    REFERENCE_RATIO = 1.777

const APP_DIMENSIONS = {
  SCREEN_WIDTH: screenWidth,
  SCREEN_HEIGHT: screenHeight,
  HEADER_HEIGHT: screenHeight*0.077,
  TITLE_HEIGHT: screenHeight*0.09,
  SCREEN_RATIO: screenRatio,
  REFERENCE_RATIO: REFERENCE_RATIO,
  SCREEN_WIDTH_SIZE_CORRECTION: screenRatio/REFERENCE_RATIO,
  SCREEN_HEIGHT_SIZE_CORRECTION: REFERENCE_RATIO/screenRatio,
  BOTTOM_TAB: {
    HEIGHT: screenHeight*0.1,
    HOME_HEIGHT: screenWidth*0.333,
    TEXT_SIZE: screenWidth*0.039
  },
};

export default APP_DIMENSIONS;