import HttpRequests from "./../services/http-requests";
import Queries from "./../services/sql-queries";
import * as Actions from ".";

export const FETCH_PRODUCTS_QUERY = "FETCH_PRODUCTS_QUERY";
export const FETCH_PRODUCTS_LIST_SUCCESS = "FETCH_PRODUCTS_LIST_SUCCESS";
export const FETCH_PRODUCTS_ERROR = "FETCH_PRODUCTS_ERROR";
export const FETCH_PRODUCTS_SUCCESS = "FETCH_PRODUCTS_SUCCESS";
export const CLEAR_PRODUCTS_LIST = "CLEAR_PRODUCTS_LIST";
export const FETCH_CATEGORIES_QUERY = "FETCH_CATEGORIES_QUERY";
export const FETCH_CATEGORIES_SUCCESS = "FETCH_CATEGORIES_SUCCESS";
export const FETCH_CATEGORIES_ERROR = "FETCH_CATEGORIES_ERROR";
export const FETCH_ALL_PRODUCTS_QUERY = "FETCH_ALL_PRODUCTS_QUERY";
export const FETCH_ALL_PRODUCTS_ERROR = "FETCH_ALL_PRODUCTS_ERROR";
export const FETCH_ALL_PRODUCTS_SUCCESS = "FETCH_ALL_PRODUCTS_SUCCESS";
export const FETCH_PRODUCTS_BY_NAME_REQUEST = "FETCH_PRODUCTS_BY_NAME_REQUEST";
export const FETCH_PRODUCTS_BY_NAME_ERROR = "FETCH_PRODUCTS_BY_NAME_ERROR";
export const FETCH_PRODUCTS_BY_NAME_SUCCESS = "FETCH_PRODUCTS_BY_NAME_SUCCESS";
export const SET_PRODUCTS_FOUND_FLAG = "SET_PRODUCTS_FOUND_FLAG";
export const SET_PRODUCT_LIST = "SET_PRODUCT_LIST";

// ACTIONS
const fetchProductsQuery = () => ({
  type: FETCH_PRODUCTS_QUERY
});

const fetchProductsError = () => ({
  type: FETCH_PRODUCTS_ERROR
});

const fetchProductsSuccess = data => ({
  type: FETCH_PRODUCTS_SUCCESS,
  data
});

const fetchProductListSuccess = data => ({
  type: FETCH_PRODUCTS_LIST_SUCCESS,
  data
});

const clearAllProducts = () => ({
  type: CLEAR_PRODUCTS_LIST,
});

const fetchCategoriesQuery = () => ({
  type: FETCH_CATEGORIES_QUERY
});

const fetchCategoriesError = () => ({
  type: FETCH_CATEGORIES_ERROR
});

const fetchCategoriesSuccess = data => ({
  type: FETCH_CATEGORIES_SUCCESS,
  data
});

const fetchAllProductsQuery = () => ({
  type: FETCH_ALL_PRODUCTS_QUERY
});

const fetchAllProductsError = () => ({
  type: FETCH_ALL_PRODUCTS_ERROR
});

const fetchAllProductsSuccess = data => ({
  type: FETCH_ALL_PRODUCTS_SUCCESS,
  data
});

const fetchProductsByNameRequest = () => ({
  type: FETCH_PRODUCTS_BY_NAME_REQUEST
});

const fetchProductsByNameError = () => ({
  type: FETCH_PRODUCTS_BY_NAME_ERROR
});

const fetchProductsByNameSuccess = data => ({
  type: FETCH_PRODUCTS_BY_NAME_SUCCESS,
  data
});

const setProductFoundFlag = (data) => ({
  type: SET_PRODUCTS_FOUND_FLAG,
  data
});

const setProductList = (data) => ({
  type: SET_PRODUCT_LIST,
  data
});

// UTIL
export function getProductsFromList(shoppingList) {
  return async (dispatch, getState) => {
    try {
      dispatch(fetchProductsQuery());
      shoppingList.shop_list_products.map(async (product) => {
        let result = await Queries.getProduct(product.art_no);
        product.description = result.DESCRIPTION;
      })

      return dispatch(fetchProductListSuccess(shoppingList));
    }
    catch(error) {
      dispatch(fetchProductsError());
    }
  };
}

export function getAllProducts() {
  return async (dispatch, getState) => {
    try {
      dispatch(fetchAllProductsQuery());
      let result = await Queries.getAllProducts();
      result = result.sort((a, b) => a.DESCRIPTION.localeCompare(b.DESCRIPTION));
      return dispatch(fetchAllProductsSuccess(result));
    }
    catch(error) {
      dispatch(fetchAllProductsError());
    }
  };
}

export function getCategories() {
  return async (dispatch, getState) => {
    try {
      dispatch(fetchCategoriesQuery());
      let result = await Queries.getCategories();
      result = result.sort((a, b) => a.DESCRIPTION.localeCompare(b.DESCRIPTION));
      return dispatch(fetchCategoriesSuccess(result));
    }
    catch(error) {
      dispatch(fetchCategoriesError());
    }
  };
}

export function getProductsByName(user, searchKey, store) {
  return async (dispatch, getState) => {
    try {
      dispatch(setProductFoundFlag(null));
      dispatch(fetchProductsByNameRequest());
      let response = await Actions.requestWithAuthentication(HttpRequests.searchProductByName, user, searchKey, store);
      dispatch(fetchProductsByNameSuccess());
      if (response === 600110)
        dispatch(setProductFoundFlag(false));
      else if (response === 600930) {
        dispatch({ type: 'SET_TOO_MANY_REQUESTS_ERROR' });
      }
      else if (response.length) {
        dispatch(setProductFoundFlag(true));
        dispatch(setProductList(response));

      }
    }
    catch (error) {
      console.log('error', error);
      dispatch(fetchProductsByNameError());
    }
  };
}

export function resetProductSearchByName(user, searchKey, store) {
  return async (dispatch, getState) => {
      dispatch({ type: 'RESET_PRODUCT_SEARCH_BY_NAME' });
  };
}
