import HttpRequests from "./../services/http-requests";
import * as Actions from "./";
import * as Layout from "./../components/layout";

export const FETCH_HOME_CAROUSEL_REQUEST = "FETCH_HOME_CAROUSEL_REQUEST";
export const FETCH_HOME_CAROUSEL_ERROR = "FETCH_HOME_CAROUSEL_ERROR";
export const FETCH_HOME_CAROUSEL_SUCCESS = "FETCH_HOME_CAROUSEL_SUCCESS";

// ACTIONS
const fetchHomeCarouselRequest = () => ({
  type: FETCH_HOME_CAROUSEL_REQUEST
});

const fetchHomeCarouselError = () => ({
  type: FETCH_HOME_CAROUSEL_ERROR
});

const fetchHomeCarouselSuccess = data => ({
  type: FETCH_HOME_CAROUSEL_SUCCESS,
  data
});

export function fetchHomeCarousel(cluster) {
  return async (dispatch, getState) => {
    dispatch(fetchHomeCarouselRequest());
    try {
      let response = await Actions.requestWithTimeout(HttpRequests.getHomeCarousel, cluster);
      if (response === 100050)
        return dispatch(fetchHomeCarouselError());
      return dispatch(fetchHomeCarouselSuccess(response));
    } catch(error) {
      dispatch(fetchHomeCarouselError());
    } 
  };
};

