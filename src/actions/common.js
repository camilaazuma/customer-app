import HttpRequests from "./../services/http-requests";
import LocationHelper from "../helpers/LocationHelper";
import AsyncStorageHelper, { KEYS } from "../helpers/AsyncStorageHelper";
import { Constants } from "expo";
import { guidGenerator, timeoutPromise } from "../utils";
import { Alert } from "react-native";
import { language } from "../config/i18n";
import _ from "lodash";
import * as Actions from "./";

// Action Names
export const SET_LOADING = "SET_LOADING";
export const STOP_LOADING = "STOP_LOADING";
export const SET_MODAL_VISIBLE = "SET_MODAL_VISIBLE";
export const SET_MODAL_INVISIBLE = "SET_MODAL_INVISIBLE";
export const SET_FONT_LOADED_FLAG = "SET_FONT_LOADED_FLAG";
export const FETCH_STORE_LIST_ERROR = "FETCH_STORE_LIST_ERROR";
export const FETCH_STORE_LIST_REQUEST = "FETCH_STORE_LIST_REQUEST";
export const FETCH_MORE_STORE_LIST_REQUEST = "FETCH_MORE_STORE_LIST_REQUEST";
export const FETCH_STORE_LIST_SUCCESS = "FETCH_STORE_LIST_SUCCESS";
export const SORT_STORE_LIST = "SORT_STORE_LIST";
export const SET_CURRENT_STORE = "SET_CURRENT_STORE";
export const SET_PREFERRED_STORE = "SET_PREFERRED_STORE";
export const CLEAR_CURRENT_STORE = "CLEAR_CURRENT_STORE";
export const CLEAR_PREFERRED_STORE = "CLEAR_PREFERRED_STORE";
export const SET_FIREBASE_STORE_VERSION = "SET_FIREBASE_STORE_VERSION";

export const GET_LOCATION_REQUEST = "GET_LOCATION_REQUEST";
export const GET_LOCATION_SUCCESS = "GET_LOCATION_SUCCESS";
export const GET_LOCATION_TURNED_OFF_ERROR = "GET_LOCATION_TURNED_OFF_ERROR";
export const GET_LOCATION_TIME_OUT_ERROR = "GET_LOCATION_TIME_OUT_ERROR";
export const GET_LOCATION_NO_PERMISSION_ERROR = "GET_LOCATION_NO_PERMISSION_ERROR";

export const FETCH_TOKEN_ERROR = "FETCH_TOKEN_ERROR";
export const FETCH_TOKEN_REQUEST = "FETCH_TOKEN_REQUEST";
export const FETCH_TOKEN_SUCCESS = "FETCH_TOKEN_SUCCESS";
export const GET_INSTALLATION_ID = "GET_INSTALLATION_ID";

export const SET_USER_INFO = "SET_USER_INFO";
export const RESET_USER_INFO = "RESET_USER_INFO";

// User Actions
export function setUserInfo({passportNo, passportRaw, fullName, email, deviceId, personalIdentifier, phone, cep, street, addressNumber, complement, city, stateName, contactUuid}) {
  return {
    type: SET_USER_INFO,
    passportNo: passportNo,
    passportRaw: passportRaw,
    fullName: fullName,
    email: email,
    deviceId: deviceId,
    personalIdentifier: personalIdentifier,
    phone: phone,
    cep: cep,
    street: street,
    addressNumber: addressNumber,
    complement: complement,
    city: city,
    stateName: stateName,
    contactUuid: contactUuid
  };
}

export function resetUserInfo() {
  return {
    type: RESET_USER_INFO,
  };
}

// current store
export const setCurrentStoreSuccess = (currentStore) => ({
  type: SET_CURRENT_STORE,
  currentStore
});
export const setPreferredStoreSuccess = (preferredStore) => ({
  type: SET_PREFERRED_STORE,
  preferredStore
});

export function setCurrentStore(currentStore) {
  return async (dispatch, getState) => {
    try {
      dispatch(setCurrentStoreSuccess(currentStore));
      await AsyncStorageHelper.setItem(KEYS.SET_STORE, currentStore);
    }
    catch(error) {
      console.log("ERRO: ", error);
      throw error;
    }
  };
}
export function fetchPreferredStore(user) {
  return async (dispatch, getState) => {
    try {
      let preferredStore = null;
      //search and set storeID
      let rec = await Actions.requestWithSapAuthentication(HttpRequests.getCustomerRecommendations, user.contactUuid, "TOPSTORE_SELL");
      if(rec.offers.length){
        preferredStore = rec.offers[0].hasOwnProperty("ProductOfferLocation") ? rec.offers[0].ProductOfferLocation : null;
        dispatch(setPreferredStoreSuccess(preferredStore));
        await AsyncStorageHelper.setItem(KEYS.SET_PREFERRED_STORE, preferredStore);
      }
    }
    catch(error) {
      console.log("ERRO: ", error);
      throw error;
    }
  };
}

export function setPreferredStore(store) {
  return async (dispatch, getState) => {
    try {
      dispatch(setPreferredStoreSuccess(store));
      await AsyncStorageHelper.setItem(KEYS.SET_PREFERRED_STORE, store);
    }
    catch(error) {
      console.log("ERRO: ", error);
      throw error;
    }
  };
}

export function clearPreferredStore() {
  return async (dispatch, getState) => {
    try {
      dispatch({type: CLEAR_PREFERRED_STORE});
      await AsyncStorageHelper.removeItem(KEYS.SET_PREFERRED_STORE);
    }
    catch(error) {
      console.log("ERRO: ", error);
      throw error;
    }
  };
}

export function clearCurrentStore() {
  return async (dispatch, getState) => {
    try {
      dispatch({type: CLEAR_CURRENT_STORE});
      await AsyncStorageHelper.removeItem(KEYS.SET_STORE);
    }
    catch(error) {
      console.log("ERRO: ", error);
      throw error;
    }
  };
}

// General Actions
export const delay = ms => new Promise(resolve => setTimeout(resolve, ms));

export function setLoading() {
  return {
    type: SET_LOADING,
    payload: true
  };
}

export function stopLoading() {
  return {
    type: STOP_LOADING,
    payload: false
  };
}

export function setModalVisible(modalKey) {
  return {
    type: SET_MODAL_VISIBLE,
    payload: true,
    modalKey: modalKey
  };
}

export function setModalInvisible(modalKey) {
  return {
    type: SET_MODAL_INVISIBLE,
    payload: false,
    modalKey: modalKey
  };
}

export function setFontLoadedFlag() {
  return {
    type: SET_FONT_LOADED_FLAG
  };
}


// Store List Actions
export const fetchStoreListError = () => ({
  type: FETCH_STORE_LIST_ERROR
});

export const fetchStoreListRequest = () => ({
  type: FETCH_STORE_LIST_REQUEST
});

export const fetchMoreStoreListRequest = () => ({
  type: FETCH_MORE_STORE_LIST_REQUEST
});

export const fetchStoreListSuccess = data => ({
  type: FETCH_STORE_LIST_SUCCESS,
  data
});

export function fetchStoreList() {
  return async (dispatch, getState) => {
    dispatch(fetchStoreListRequest());
    try {
      const state = getState();
      let response = await requestWithTimeout(HttpRequests.getStoreList);        
      dispatch(fetchStoreListSuccess(response));
      if (state.location.location && response.length) dispatch(sortStoreList());
    }
    catch(error) {
      dispatch(fetchStoreListError());
    }
  };
}

export function fetchStoreListByCountry() {
  return async (dispatch, getState) => {
    dispatch(fetchStoreListRequest());
    try {
      let response = await requestWithTimeout(HttpRequests.getStoreListByCountry);
      dispatch(fetchStoreListSuccess(response));
    }
    catch(error) {
      dispatch(fetchStoreListError());
    }
  };
}

export function sortStoreList() {
  return (dispatch, getState) => {
    const state = getState();
    let storesArray  = _.cloneDeep(state.storeListFetch.storeList);

    let location  = state.location.location
    let userLatitud = parseFloat(location.coords.latitude)
    let userLongitud = parseFloat(location.coords.longitude)
    storesArray.forEach(function (arrayItem) {
      if(language === 'es-CO'){
        arrayItem.distance = LocationHelper.getDistance(
          userLatitud,
          userLongitud,
          parseFloat(arrayItem.latitud), // ORIGINAL_COLOMBIA: latitude
          parseFloat(arrayItem.longitud) // ORIGINAL_COLOMBIA: longitude
        )
      }else if (language === 'pt-BR') {
        arrayItem.distance = LocationHelper.getDistance(
          userLatitud,
          userLongitud,
          parseFloat(arrayItem.latitud),
          parseFloat(arrayItem.longitud)
        )
      }
    });
    storesArray.sort((a,b)=>(a.distance-b.distance))
    dispatch({
      type: SORT_STORE_LIST,
      data: storesArray
    });
  };
}

export const setFirebaseStoreVersion = (version) => ({
  type: SET_FIREBASE_STORE_VERSION,
  version
})


const TIMEOUT_TIME = 45000;
// Autenticacao no BE
export function requestWithAuthentication(request, user, ...params) {
  return (
    AsyncStorageHelper.getItem(KEYS.AUTH_TOKEN_KEY).then((token) => {
      return timeoutPromise(TIMEOUT_TIME, request(user, ...params, token))
        .then((response) => {
          if(response == "100090") {
            return requestWithTimeout(HttpRequests.getToken, user.personalIdentifier,  user.deviceId, user.phone, user.email).then(async function(newToken) {
              await AsyncStorageHelper.setItem(KEYS.AUTH_TOKEN_KEY, newToken["access_token"]);
              return request(user, ...params, newToken["access_token"]);
            })
          }
          else return response;
        }).catch(function(error) {
          console.log("ERRO: ", error);
          throw error;
      });
    })
  );
}

// Autenticacao no SAP
export function requestWithSapAuthentication(request, ...params) {
  return (
    AsyncStorageHelper.getItem(KEYS.AUTH_SAP_TOKEN_KEY).then((token) => {
      return timeoutPromise(TIMEOUT_TIME, request(...params, token))
        .then((response) => {
          if(response == "100090" || !response) {
            return requestWithTimeout(HttpRequests.getBearerToken).then(async function(newToken) {
              await AsyncStorageHelper.setItem(KEYS.AUTH_SAP_TOKEN_KEY, newToken["access_token"]);
              return request(...params, newToken["access_token"]);
            })
          }
          else return response;
        }).catch(function(error) {
          console.log("ERRO: ", error);
          throw error;
      });
    })
  );
}

/*
  Para requisições sem autenticação e timeout
*/
export function requestWithTimeout(request, ...params) {
  return (
    timeoutPromise(TIMEOUT_TIME, request(...params))
    .then(function(response) {
      return response;
    }).catch(function(error) {
      console.log("ERRO: ", error);
      throw error;
    })
  );
}

// get installation ID
export const getInstallationId = () => ({
  type: GET_INSTALLATION_ID,
  installationId: Constants.installationId
});

//Location Actions
export function getLocation(forceAsk) {
  return async(dispatch, getState) => {
    dispatch({type: GET_LOCATION_REQUEST});
    try {
      let location = await LocationHelper.getLocationAsync(forceAsk);
      dispatch({
        type: GET_LOCATION_SUCCESS,
        location
      })
    }
    catch(error) {
      console.log(error);
      if(error.code === "E_LOCATION_SERVICES_DISABLED") {
        dispatch({
          type: GET_LOCATION_TURNED_OFF_ERROR
        })
      }
      else if(error.message === "Request timed out") {
        dispatch({
          type: GET_LOCATION_TIME_OUT_ERROR
        })
      }
      else if(error.code === "E_LOCATION_UNAUTHORIZED") {
        dispatch({
          type: GET_LOCATION_NO_PERMISSION_ERROR
        })
      }
    }
  }
}

// Authentication Token Actions
export const fetchTokenError = () => ({
  type: FETCH_TOKEN_ERROR
});

export const fetchTokenRequest = () => ({
  type: FETCH_TOKEN_REQUEST
});

export const fetchTokenSuccess = data => ({
  type: FETCH_TOKEN_SUCCESS,
  data
});

export function fetchAuthToken(personalIdentifier, deviceId, phone, email) {
  return async (dispatch, getState) => {
    dispatch(fetchTokenRequest());
    try {
      let token = await requestWithTimeout(HttpRequests.getToken, personalIdentifier, deviceId, phone, email);
      await AsyncStorageHelper.setItem(KEYS.AUTH_TOKEN_KEY, token["access_token"]);
      dispatch(fetchTokenSuccess());
    }
    catch(error) {
      dispatch(fetchTokenError());
    }
  };
}
