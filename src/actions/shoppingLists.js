import HttpRequests from "./../services/http-requests";
import * as Actions from "./";
import { Share } from 'react-native';
import { getDateTime } from "./../utils";

export const CRUD_SHOPLISTS_REQUEST = "CRUD_SHOPLISTS_REQUEST";
export const FETCH_MORE_SHOPLISTS_REQUEST = "FETCH_MORE_SHOPLISTS_REQUEST";
export const CRUD_SHOPLISTS_ERROR = "CRUD_SHOPLISTS_ERROR";
export const CRUD_SHOPLISTS_SUCCESS = "CRUD_SHOPLISTS_SUCCESS";
export const SHARED_SHOPLISTS_REQUEST = "SHARED_SHOPLISTS_REQUEST";
export const SHARED_SHOPLISTS_ERROR = "SHARED_SHOPLISTS_ERROR";
export const SHARED_SHOPLISTS_SUCCESS = "SHARED_SHOPLISTS_SUCCESS";
export const CLEAR_SHOPLISTS_LIST = "CLEAR_SHOPLISTS_LIST";

export const FETCH_PRODUCTS_ARE_AVAILABLE_REQUEST = "FETCH_PRODUCTS_ARE_AVAILABLE_REQUEST";
export const FETCH_PRODUCTS_ARE_AVAILABLE_ERROR = "FETCH_PRODUCTS_ARE_AVAILABLE_ERROR";
export const FETCH_PRODUCTS_ARE_AVAILABLE_SUCCESS = "FETCH_PRODUCTS_ARE_AVAILABLE_SUCCESS";
export const LIST_SHARING_SUCCESS = "LIST_SHARING_SUCCESS";
export const LIST_SHARING_ERROR = "LIST_SHARING_ERROR";

export const CREATE_SHOP_LIST_REQUEST = "CREATE_SHOP_LIST_REQUEST";
export const CREATE_SHOP_LIST_ERROR = "CREATE_SHOP_LIST_ERROR";
export const CREATE_SHOP_LIST_SUCCESS = "CREATE_SHOP_LIST_SUCCESS";

export const APPRAISE_SHOP_LIST_REQUEST = "APPRAISE_SHOP_LIST_REQUEST";
export const APPRAISE_SHOP_LIST_ERROR = "APPRAISE_SHOP_LIST_ERROR";
export const APPRAISE_SHOP_LIST_SUCCESS = "APPRAISE_SHOP_LIST_SUCCESS";
export const RESET_APPRAISE_SHOP_LIST_RESPONSE = "RESET_APPRAISE_SHOP_LIST_RESPONSE";

// ACTIONS
const CRUDShopListsRequest = () => ({
  type: CRUD_SHOPLISTS_REQUEST
});

const fetchMoreShopListsRequest = () => ({
  type: FETCH_MORE_SHOPLISTS_REQUEST
});

const CRUDShopListsError = () => ({
  type: CRUD_SHOPLISTS_ERROR
});

const CRUDShopListsSuccess = data => ({
  type: CRUD_SHOPLISTS_SUCCESS,
  data
});

const SharedShopListError = () => ({
  type: SHARED_SHOPLISTS_ERROR
});

const SharedShopListRequest = () => ({
  type: SHARED_SHOPLISTS_REQUEST
});

const SharedShopListSuccess = data => ({
  type: SHARED_SHOPLISTS_SUCCESS,
  data
});

const fetchAllShopListsSuccess = () => ({
  type: FETCH_ALL_SHOPLISTS_SUCCESS,
});

const clearAllShopLists = () => ({
  type: CLEAR_SHOPLISTS_LIST,
});

const fetchProductsAreAvailableRequest = () => ({
  type: FETCH_PRODUCTS_ARE_AVAILABLE_REQUEST,
});

const fetchProductsAreAvailableSuccess = data => ({
  type: FETCH_PRODUCTS_ARE_AVAILABLE_SUCCESS,
  data
});

const fetchProductsAreAvailableError = () => ({
  type: FETCH_PRODUCTS_ARE_AVAILABLE_ERROR,
});

const listSharingSuccess = data => ({
  type: LIST_SHARING_SUCCESS,
  data
});

const listSharingError = () => ({
  type: LIST_SHARING_ERROR,
});

const createShopListRequest = () => ({
  type: CREATE_SHOP_LIST_REQUEST,
});

const createShopListSuccess = data => ({
  type: CREATE_SHOP_LIST_SUCCESS,
  data
});

const createShopListError = () => ({
  type: CREATE_SHOP_LIST_ERROR,
});

const appraiseShopListRequest = () => ({
  type: APPRAISE_SHOP_LIST_REQUEST,
});

const appraiseShopListSuccess = response => ({
  type: APPRAISE_SHOP_LIST_SUCCESS,
  response
});

const appraiseShopListError = () => ({
  type: APPRAISE_SHOP_LIST_ERROR,
});

export const resetAppraiseShopListResponse = () => ({
  type: RESET_APPRAISE_SHOP_LIST_RESPONSE,
});

// UTIL
export function getShopList(user, updated) {
  return async (dispatch, getState) => {
    try {
      dispatch(CRUDShopListsRequest());
      dispatch(clearAllShopLists());
      let response = await Actions.requestWithAuthentication(HttpRequests.getShopList, user, updated);

      let shopListsArray = response["shop_lists"];
      shopListsArray = shopListsArray.sort(function(a,b){
        return new Date(b.created) - new Date(a.created);
      });
      dispatch(CRUDShopListsSuccess(shopListsArray));
    }
    catch(error) {
      dispatch(CRUDShopListsError());
    }
  };
}

// UTIL
export function getSharedShopList(user, listId, props) {
  return async (dispatch, getState) => {
    try {
      dispatch(SharedShopListRequest());

      function guid() {
        function s4() {
          return Math.floor((1 + Math.random()) * 0x10000)
            .toString(16)
            .substring(1);
        }
        return s4() + s4() + s4() + s4() + s4() + s4() + s4() + s4();
      }
      let responseCreate = await Actions.requestWithAuthentication(HttpRequests.createSharedShopList, user, listId);
      let newListId = guid();
      let responseGet = await Actions.requestWithAuthentication(HttpRequests.getSharedShopList, user, listId, newListId);

      var shoppingList = responseGet.shop_lists[0];
      shoppingList['created'] = getDateTime();
      dispatch(saveShopList(user, "2018-11-01 19:22:02", shoppingList))
      await props.getListDetails({productsList: shoppingList});
      await props.navigation.navigate("ListDetail");
      dispatch(SharedShopListSuccess(shoppingList));
    }
    catch(error) {
      console.log(error);
      dispatch(SharedShopListError());
    }
  };
}



// UTIL
export function getProductsAvailability(user, articles) {
  return async (dispatch, getState) => {
    try {
      dispatch(fetchProductsAreAvailableRequest());

      let response = await Actions.requestWithAuthentication(HttpRequests.fetchProductsArticles, user, articles);
      let productListStatus = response;
      dispatch(fetchProductsAreAvailableSuccess(productListStatus));
    }
    catch(error) {
      dispatch(fetchProductsAreAvailableError());
    }
  };
}
// UTIL
export function saveShopList(user, updated, shopList) {
  return async (dispatch, getState) => {
    try {
      dispatch(Actions.getProductsFromList(shopList));
      dispatch(CRUDShopListsRequest());
      var shoppingList = {
        shop_list_id: shopList.uuid,
        name: shopList.name,
        creation_date: shopList.created,
        deleted: shopList.deleted,
        shop_list_products: shopList.shop_list_products
      }
      let response = await Actions.requestWithAuthentication(HttpRequests.saveShopList, user, updated, shoppingList);
      let shopListsArray = response["shop_lists"];
      shopListsArray = shopListsArray.sort(function(a,b){
        return new Date(b.created) - new Date(a.created);
      });
      return dispatch(CRUDShopListsSuccess(shopListsArray));
    }
    catch(error) {
      dispatch(CRUDShopListsError());
    }
  };
}

// UTIL
export function showListSharing(shopList) {
  return async (dispatch, getState) => {
    try {
       Share.share({
        message: 'Segue a minha lista de compras "'+ shopList.name + '": ' + HttpRequests.getSharingListUrl(shopList),
        title: 'Lista de compras'
      }, {
        // Android only:
        dialogTitle: 'Compartilhar lista de compras'
      });
      return dispatch(listSharingSuccess());
    }
    catch(error) {
      dispatch(listSharingError());
    }
  };
}

// UTIL
export function appraiseShopList(passport, personalIdentifier, uuid) {
  return async (dispatch, getState) => {
    try {
      dispatch(appraiseShopListRequest());
      let response = await Actions.requestWithAuthentication(HttpRequests.appraiseShopList, passport, personalIdentifier, uuid);
      dispatch(appraiseShopListSuccess(response));
    }
    catch(error) {
      dispatch(appraiseShopListError());
    }
  };
}
