import * as Actions from "./";
import HttpRequests from "./../services/http-requests";
import { language } from "../config/i18n";

export const CHECK_SIGN_UP_STATUS_REQUEST = "CHECK_SIGN_UP_STATUS_REQUEST";
export const CHECK_SIGN_UP_STATUS_ERROR = "CHECK_SIGN_UP_STATUS_ERROR";
export const CHECK_SIGN_UP_STATUS_SUCCESS = "CHECK_SIGN_UP_STATUS_SUCCESS";
export const SET_CPF_INPUT_RESPONSE = "SET_CPF_INPUT_RESPONSE";
export const RESET_CPF_INPUT_RESPONSE = "RESET_CPF_INPUT_RESPONSE";
export const SET_CPF_OR_CNPJ = "SET_CPF_OR_CNPJ";

export const SEND_TOKEN_FOR_PASSWORD_REGISTRATION_REQUEST = "SEND_TOKEN_FOR_PASSWORD_REGISTRATION_REQUEST";
export const SEND_TOKEN_FOR_PASSWORD_REGISTRATION_ERROR = "SEND_TOKEN_FOR_PASSWORD_REGISTRATION_ERROR";
export const SEND_TOKEN_FOR_PASSWORD_REGISTRATION_SUCCESS = "SEND_TOKEN_FOR_PASSWORD_REGISTRATION_SUCCESS";
export const SET_SEND_TOKEN_FOR_PASSWORD_REGISTRATION_RESPONSE = "SET_SEND_TOKEN_FOR_PASSWORD_REGISTRATION_RESPONSE";
export const RESET_SEND_TOKEN_FOR_PASSWORD_REGISTRATION_RESPONSE = "RESET_SEND_TOKEN_FOR_PASSWORD_REGISTRATION_RESPONSE";

export const CREATE_NEW_PASSWORD_REQUEST = "CREATE_NEW_PASSWORD_REQUEST";
export const CREATE_NEW_PASSWORD_ERROR = "CREATE_NEW_PASSWORD_ERROR";
export const CREATE_NEW_PASSWORD_SUCCESS = "CREATE_NEW_PASSWORD_SUCCESS";
export const SET_CREATE_PASSWORD_RESPONSE = "SET_CREATE_PASSWORD_RESPONSE";
export const RESET_CREATE_PASSWORD_RESPONSE = "RESET_CREATE_PASSWORD_RESPONSE";

export const LOGIN_REQUEST = "LOGIN_REQUEST";
export const LOGIN_ERROR = "LOGIN_ERROR";
export const LOGIN_SUCCESS = "LOGIN_SUCCESS";
export const SET_LOGIN_RESPONSE = "SET_LOGIN_RESPONSE";
export const RESET_LOGIN_RESPONSE = "RESET_LOGIN_RESPONSE";

export const SIGN_UP_REQUEST = "SIGN_UP_REQUEST";
export const SIGN_UP_ERROR = "SIGN_UP_ERROR";
export const SIGN_UP_SUCCESS = "SIGN_UP_SUCCESS";
export const SET_SIGN_UP_RESPONSE = "SET_SIGN_UP_RESPONSE";
export const RESET_SIGN_UP_RESPONSE = "RESET_SIGN_UP_RESPONSE";

export const VALIDATE_CONFIRMATION_CODE_REQUEST = "VALIDATE_CONFIRMATION_CODE_REQUEST";
export const VALIDATE_CONFIRMATION_CODE_ERROR = "VALIDATE_CONFIRMATION_CODE_ERROR";
export const VALIDATE_CONFIRMATION_CODE_SUCCESS = "VALIDATE_CONFIRMATION_CODE_SUCCESS";
export const SET_VALIDATE_CONFIRMATION_CODE_RESPONSE = "SET_VALIDATE_CONFIRMATION_CODE_RESPONSE";
export const RESET_VALIDATE_CONFIRMATION_CODE_RESPONSE = "RESET_VALIDATE_CONFIRMATION_CODE_RESPONSE";




// ACTIONS
const checkSignUpStatusRequest = () => ({
  type: CHECK_SIGN_UP_STATUS_REQUEST
});

const checkSignUpStatusError = () => ({
  type: CHECK_SIGN_UP_STATUS_ERROR
});

const checkSignUpStatusSuccess = () => ({
  type: CHECK_SIGN_UP_STATUS_SUCCESS,
});

export const setCpfOrCnpj = (cpfOrCnpj) => ({
  type: SET_CPF_OR_CNPJ,
  cpfOrCnpj
});

export const setCpfInputResponse = (response) => ({
  type: SET_CPF_INPUT_RESPONSE,
  response
})

export const resetCpfInputResponse = () => ({
  type: RESET_CPF_INPUT_RESPONSE,
})

export function checkSignUpStatus(personalIdentifier, documentName) {
  let docName = documentName;
  if (language==='pt-BR'){
    docName = personalIdentifier.length == 11 ? documentName : "ZDOC_ID_COMP";
  }

  return async (dispatch, getState) => {
    dispatch(checkSignUpStatusRequest());
    dispatch(setCpfOrCnpj(personalIdentifier));
    try {
      let response = await Actions.requestWithSapAuthentication(HttpRequests.checkSignUpStatus, personalIdentifier, docName);
      dispatch(checkSignUpStatusSuccess());
      dispatch(setCpfInputResponse(response));
    } catch(error) {
      dispatch(checkSignUpStatusError());
    }
  };
};


const createNewPasswordRequest = () => ({
  type: CREATE_NEW_PASSWORD_REQUEST
});

const createNewPasswordError = () => ({
  type: CREATE_NEW_PASSWORD_ERROR
});

const createNewPasswordSuccess = () => ({
  type: CREATE_NEW_PASSWORD_SUCCESS,
});

export const setCreatePasswordResponse = (response) => ({
  type: SET_CREATE_PASSWORD_RESPONSE,
  response
});

export const resetCreatePasswordResponse = () => ({
  type: RESET_CREATE_PASSWORD_RESPONSE,
});


export function createNewPassword(personalIdentifier, password) {
  return async (dispatch, getState) => {
    dispatch(createNewPasswordRequest());
    try {
      let response = await Actions.requestWithTimeout(HttpRequests.createPassword, personalIdentifier, password);
      dispatch(createNewPasswordSuccess());
      dispatch(setCreatePasswordResponse(response));
    } catch(error) {
      dispatch(createNewPasswordError());
    } 
  };
};

const sendTokenForPasswordRegistrationRequest = () => ({
  type: SEND_TOKEN_FOR_PASSWORD_REGISTRATION_REQUEST
});

const sendTokenForPasswordRegistrationError = () => ({
  type: SEND_TOKEN_FOR_PASSWORD_REGISTRATION_ERROR
});

const sendTokenForPasswordRegistrationSuccess = () => ({
  type: SEND_TOKEN_FOR_PASSWORD_REGISTRATION_SUCCESS,
});

export const setSendTokenForPasswordRegistrationResponse = (response) => ({
  type: SET_SEND_TOKEN_FOR_PASSWORD_REGISTRATION_RESPONSE,
  response
});

export const resetSendTokenForPasswordRegistrationResponse = () => ({
  type: RESET_SEND_TOKEN_FOR_PASSWORD_REGISTRATION_RESPONSE,
});

export function sendTokenForPasswordRegistration(personalIdentifier, validationType) {
  return async (dispatch, getState) => {
    dispatch(sendTokenForPasswordRegistrationRequest());
    try {
      response = await Actions.requestWithTimeout(HttpRequests.sendTokenForPasswordRegistration, personalIdentifier, validationType);
      dispatch(sendTokenForPasswordRegistrationSuccess());
      dispatch(setSendTokenForPasswordRegistrationResponse(response));
    } catch(error) {
      dispatch(sendTokenForPasswordRegistrationError());
    } 
  };
};



const LoginRequest = () => ({
  type: LOGIN_REQUEST
});

const LoginError = () => ({
  type: LOGIN_ERROR
});

const LoginSuccess = () => ({
  type: LOGIN_SUCCESS,
});

export const setLoginResponse = (response) => ({
  type: SET_LOGIN_RESPONSE,
  response
});

export const resetLoginResponse = () => ({
  type: RESET_LOGIN_RESPONSE,
});

export function login(personalIdentifier, password) {
  return async (dispatch, getState) => {
    dispatch(LoginRequest());
    try {
      let response = await Actions.requestWithTimeout(HttpRequests.login, personalIdentifier, password);
      dispatch(LoginSuccess());
      dispatch(setLoginResponse(response));
    } catch(error) {
      dispatch(LoginError());
    } 
  };
};

const signUpRequest = () => ({
  type: SIGN_UP_REQUEST
});

const signUpError = () => ({
  type: SIGN_UP_ERROR
});

const signUpSuccess = () => ({
  type: SIGN_UP_SUCCESS,
});

export const setSignUpResponse = (response) => ({
  type: SET_SIGN_UP_RESPONSE,
  response
});

export const resetSignUpResponse = () => ({
  type: RESET_SIGN_UP_RESPONSE,
});

export function signUp(full_name, personalIdentifier, phone, email, password, validationType) {
  return async (dispatch, getState) => {
    dispatch(signUpRequest());
    try {
      let response = await Actions.requestWithTimeout(HttpRequests.signUp, full_name, personalIdentifier, phone, email, password, validationType);
      dispatch(signUpSuccess());
      dispatch(setSignUpResponse(response));
    } catch(error) {
      dispatch(signUpError());
    } 
  };
};

const validateConfirmationCodeRequest = () => ({
  type: VALIDATE_CONFIRMATION_CODE_REQUEST
});

const validateConfirmationCodeError = () => ({
  type: VALIDATE_CONFIRMATION_CODE_ERROR
});

const validateConfirmationCodeSuccess = () => ({
  type: VALIDATE_CONFIRMATION_CODE_SUCCESS,
});

export const setValidateConfirmationResponse = (response) => ({
  type: SET_VALIDATE_CONFIRMATION_CODE_RESPONSE,
  response
});

export const resetValidateConfirmationResponse = () => ({
  type: RESET_VALIDATE_CONFIRMATION_CODE_RESPONSE,
});

export function validateConfimationCode(personalIdentifier, tokenpass) {
  return async (dispatch, getState) => {
    dispatch(validateConfirmationCodeRequest());
    try {
      let response = await Actions.requestWithTimeout(HttpRequests.validateTokenpass, personalIdentifier, tokenpass);
      dispatch(validateConfirmationCodeSuccess());
      dispatch(setValidateConfirmationResponse(response));
    } catch(error) {
      dispatch(validateConfirmationCodeError());
    } 
  };
};
