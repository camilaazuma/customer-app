import HttpRequests from "./../services/http-requests";
import * as Actions from "./";

export const SEND_CONTACT_US_MESSAGE_REQUEST = "SEND_CONTACT_US_MESSAGE_REQUEST";
export const SEND_CONTACT_US_MESSAGE_ERROR = "SEND_CONTACT_US_MESSAGE_ERROR";
export const SEND_CONTACT_US_MESSAGE_SUCCESS = "SEND_CONTACT_US_MESSAGE_SUCCESS";

// ACTIONS
const sendContactUsMessageRequest = () => ({
  type: SEND_CONTACT_US_MESSAGE_REQUEST
});

const sendContactUsMessageError = () => ({
  type: SEND_CONTACT_US_MESSAGE_ERROR
});

const sendContactUsMessageSuccess = data => ({
  type: SEND_CONTACT_US_MESSAGE_SUCCESS,
  data
});

export function sendContactUsMessage(name, phoneNumber, email, message) {
  return async (dispatch, getState) => {
    dispatch(
      sendContactUsMessageRequest()
    );
    try {
      let response = await Actions.requestWithTimeout(HttpRequests.sendContactUsMessage, name, phoneNumber, email, message);
      return response;
    } catch(error) {
      dispatch(sendContactUsMessageError());
    } 
  };
};

