import * as Actions from "./";
import HttpRequests from "./../services/http-requests";

export const UPDATE_USER_REQUEST = "UPDATE_USER_REQUEST";
export const UPDATE_USER_ERROR = "UPDATE_USER_ERROR";
export const UPDATE_USER_SUCCESS = "UPDATE_USER_SUCCESS";
export const SET_UPDATE_USER_RESPONSE = "SET_UPDATE_USER_RESPONSE";
export const RESET_UPDATE_USER_RESPONSE = "RESET_UPDATE_USER_RESPONSE";

export const UPDATE_EMAIL_OR_PHONE_REQUEST = "UPDATE_EMAIL_OR_PHONE_REQUEST";
export const UPDATE_EMAIL_OR_PHONE_ERROR = "UPDATE_EMAIL_OR_PHONE_ERROR";
export const UPDATE_EMAIL_OR_PHONE_SUCCESS = "UPDATE_EMAIL_OR_PHONE_SUCCESS";
export const SET_UPDATE_EMAIL_OR_PHONE_RESPONSE = "SET_UPDATE_EMAIL_OR_PHONE_RESPONSE";
export const RESET_UPDATE_EMAIL_OR_PHONE_RESPONSE = "RESET_UPDATE_EMAIL_OR_PHONE_RESPONSE";

export const SEND_TOKEN_FOR_PHONE_OR_EMAIL_REQUEST = "SEND_TOKEN_FOR_PHONE_OR_EMAIL_REQUEST";
export const SEND_TOKEN_FOR_PHONE_OR_EMAIL_ERROR = "SEND_TOKEN_FOR_PHONE_OR_EMAIL_ERROR";
export const SEND_TOKEN_FOR_PHONE_OR_EMAIL_SUCCESS = "SEND_TOKEN_FOR_PHONE_OR_EMAIL_SUCCESS";
export const SET_SEND_TOKEN_FOR_PHONE_OR_EMAIL_RESPONSE = "SET_SEND_TOKEN_FOR_PHONE_OR_EMAIL_RESPONSE";
export const RESET_SEND_TOKEN_FOR_PHONE_OR_EMAIL_RESPONSE = "RESET_SEND_TOKEN_FOR_PHONE_OR_EMAIL_RESPONSE";

const updateUserRequest = () => ({
  type: UPDATE_USER_REQUEST
});

const updateUserError = () => ({
  type: UPDATE_USER_ERROR
});

const updateUserSuccess = () => ({
  type: UPDATE_USER_SUCCESS
});

export const setUpdateUserResponse = response => ({
  type: SET_UPDATE_USER_RESPONSE,
  response
});

export const resetUpdateUserResponse = () => ({
  type: RESET_UPDATE_USER_RESPONSE
});

export function updateUser(
  personalIdentifier,
  fullName,
  cep,
  street,
  addressNumber,
  complement,
  city,
  stateName
) {
  return async (dispatch, getState) => {
    dispatch(updateUserRequest());
    try {
      let response = await Actions.requestWithTimeout(HttpRequests.updateUser, 
        personalIdentifier,
        fullName,
        cep,
        street,
        addressNumber,
        complement,
        city,
        stateName
      );
      dispatch(updateUserSuccess());
      dispatch(setUpdateUserResponse(response));
    } catch (error) {
      dispatch(updateUserError());
    }
  };
}


const updateEmailOrPhoneRequest = () => ({
  type: UPDATE_EMAIL_OR_PHONE_REQUEST
});

const updateEmailOrPhoneError = () => ({
  type: UPDATE_EMAIL_OR_PHONE_ERROR
});

const updateEmailOrPhoneSuccess = () => ({
  type: UPDATE_EMAIL_OR_PHONE_SUCCESS
});

export const setUpdateEmailOrPhoneResponse = response => ({
  type: SET_UPDATE_EMAIL_OR_PHONE_RESPONSE,
  response
});

export const resetUpdateEmailOrPhoneResponse = () => ({
  type: RESET_UPDATE_EMAIL_OR_PHONE_RESPONSE
});

export function updateEmailOrPhone({personalIdentifier, email, phone}) {
  return async (dispatch, getState) => {
    dispatch(updateEmailOrPhoneRequest());
    try {
      let response = await Actions.requestWithTimeout(HttpRequests.updateEmailOrPhone, 
        personalIdentifier, email, phone
      );
      dispatch(updateEmailOrPhoneSuccess());
      dispatch(setUpdateEmailOrPhoneResponse(response));
    } catch (error) {
      dispatch(updateEmailOrPhoneError());
    }
  };
}


const sendTokenForPhoneOrEmailRequest = () => ({
  type: SEND_TOKEN_FOR_PHONE_OR_EMAIL_REQUEST
});

const sendTokenForPhoneOrEmailError = () => ({
  type: SEND_TOKEN_FOR_PHONE_OR_EMAIL_ERROR
});

const sendTokenForPhoneOrEmailSuccess = () => ({
  type: SEND_TOKEN_FOR_PHONE_OR_EMAIL_SUCCESS,
});

export const setSendTokenForPhoneOrEmailResponse = (response) => ({
  type: SET_SEND_TOKEN_FOR_PHONE_OR_EMAIL_RESPONSE,
  response
});

export const resetSendTokenForPhoneOrEmailResponse = () => ({
  type: RESET_SEND_TOKEN_FOR_PHONE_OR_EMAIL_RESPONSE,
});

export function sendTokenForPhoneOrEmail({personalIdentifier, validationType, phone, email}) {
  return async (dispatch, getState) => {
    dispatch(sendTokenForPhoneOrEmailRequest());
    try {
      response = await Actions.requestWithTimeout(HttpRequests.sendTokenForPhoneOrEmail, {personalIdentifier: personalIdentifier, validationType: validationType, phone: phone, email: email});
      dispatch(sendTokenForPhoneOrEmailSuccess());
      dispatch(setSendTokenForPhoneOrEmailResponse(response));
    } catch(error) {
      dispatch(sendTokenForPhoneOrEmailError());
    } 
  };
};