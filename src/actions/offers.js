import HttpRequests from "./../services/http-requests";
import * as Actions from "./";
import { delay } from "./common";
import { removeDuplicatesbyAttribute } from "../utils";

export const FETCH_OFFERS_REQUEST = "FETCH_OFFERS_REQUEST";
export const FETCH_MORE_OFFERS_REQUEST = "FETCH_MORE_OFFERS_REQUEST";
export const FETCH_OFFERS_ERROR = "FETCH_OFFERS_ERROR";
export const FETCH_OFFERS_SUCCESS = "FETCH_OFFERS_SUCCESS";
export const CLEAR_OFFERS_LIST = "CLEAR_OFFERS_LIST";
export const FETCH_ALL_OFFERS_SUCCESS = "FETCH_ALL_OFFERS_SUCCESS";
export const SET_TOTAL_PAGE_NUMBER = "SET_TOTAL_PAGE_NUMBER";
export const INCREMENT_PAGE = "INCREMENT_PAGE";
export const RESET_PAGE_NUMBER = "RESET_PAGE_NUMBER";
export const FILTER_CATEGORY_SUCCESS ="FILTER_CATEGORY_SUCCESS"
export const SET_OFFERS ="SET_OFFERS"

export const FETCH_RECOMMENDATIONS_SUCCESS = "FETCH_RECOMMENDATIONS_SUCCESS"
export const SET_RECOMMENDATIONS = "SET_RECOMMENDATIONS"
export const FETCH_RECOMMENDATIONS_REQUEST = "FETCH_RECOMMENDATIONS_REQUEST"
export const FETCH_RECOMMENDATIONS_ERROR = "FETCH_RECOMMENDATIONS_ERROR"
export const CLEAR_RECOMMENDATIONS_LIST = "CLEAR_RECOMMENDATIONS_LIST"

// ACTIONS

export const fetchOffersRequest = () => ({
  type: FETCH_OFFERS_REQUEST
});
export const fetchOffersError = () => ({
  type: FETCH_OFFERS_ERROR
});

export const fetchOffersSuccess = data => ({
  type: FETCH_OFFERS_SUCCESS,
  data
});

const fetchAllOffersSuccess = () => ({
  type: FETCH_ALL_OFFERS_SUCCESS,
});

export const filterCategorySuccess = (category, group) => ({
  type: FILTER_CATEGORY_SUCCESS,
  category,
  group
});

export const clearAllOffers = () => ({
  type: CLEAR_OFFERS_LIST,
});

const setTotalPageNumber = (totalPageNumber) => ({
  type: SET_TOTAL_PAGE_NUMBER,
  totalPageNumber
});

export const setOffers = data => ({
  type: SET_OFFERS,
  data
});


const fetchRecommendationsSuccess = () => ({
  type: FETCH_RECOMMENDATIONS_SUCCESS
});
const setRecommendations = (data, offerType) => ({
  type: SET_RECOMMENDATIONS,
  data, offerType
});
const fetchRecommendationsRequest = () => ({
  type: FETCH_RECOMMENDATIONS_REQUEST
});
const fetchRecommendationsError = () => ({
  type: FETCH_RECOMMENDATIONS_ERROR
});
export const clearRecommendationsList = () => ({
  type: CLEAR_RECOMMENDATIONS_LIST
});

export function fetchOffersByStore(storeId) {
  return async (dispatch, getState) => {
    try {
      await dispatch(clearAllOffers());
      await dispatch(fetchOffersRequest());

      offers = await Actions.requestWithSapAuthentication(HttpRequests.getOffersByStore, storeId);
      // filtra ofertas sem descritivo
      if(offers.length){
        offers = offers.filter(offer => (offer.ProductName && offer.ProductName !== "") && (offer.ProductImages && offer.ProductImages.ImageURL !== ""));
      }
      offers = removeDuplicatesbyAttribute(offers, "ProductSKU");

      await dispatch(setOffers(offers));
      return dispatch(fetchAllOffersSuccess());
    } catch(error) {
      dispatch(fetchOffersError());
    }
  };
}

export function fetchCustomerRecommendations(customerId) {
  return async (dispatch, getState) => {
    try {
      let merge = {};
      await dispatch(clearRecommendationsList());
      await dispatch(fetchRecommendationsRequest());

      let reqs = [Actions.requestWithSapAuthentication(HttpRequests.getCustomerRecommendations, customerId, "TOPSTORE_SELL"),
                  Actions.requestWithSapAuthentication(HttpRequests.getCustomerRecommendations, customerId, "UP_SELL"),
                  Actions.requestWithSapAuthentication(HttpRequests.getCustomerRecommendations, customerId, "CROSS_SELL")]
      Promise.all(reqs).then((responses) => {
        let ts = responses.filter(item => item.scenario == "TOPSTORE_SELL");
        ts = removeDuplicatesbyAttribute(ts[0].offers, "ProductSKU");
        dispatch(setRecommendations(ts, 'topSell'));

        let us = responses.filter(item => item.scenario == "UP_SELL");
        us = removeDuplicatesbyAttribute(us[0].offers, "ProductSKU");
        us = us.filter(val => !ts.some(e => e.ProductSKU === val.ProductSKU));
        dispatch(setRecommendations(us, 'upSell'));

        let cs = responses.filter(item => item.scenario == "CROSS_SELL");
        cs = removeDuplicatesbyAttribute(cs[0].offers, "ProductSKU");
        cs = cs.filter(val => !ts.some(e => e.ProductSKU === val.ProductSKU) && !us.some(e => e.ProductSKU === val.ProductSKU));
        dispatch(setRecommendations(cs, 'crossSell'));
        
        return dispatch(fetchRecommendationsSuccess());
      });
    } catch(error) {
      dispatch(fetchRecommendationsError());
    }
  };
}
