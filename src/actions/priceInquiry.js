import HttpRequests from "./../services/http-requests";
import * as Actions from "./";
import { language } from "../config/i18n";

export const FETCH_PRODUCT_BY_BARCODE_REQUEST = "FETCH_PRODUCT_BY_BARCODE_REQUEST";
export const FETCH_PRODUCT_BY_BARCODE_ERROR = "FETCH_PRODUCT_BY_BARCODE_ERROR";
export const FETCH_PRODUCT_BY_BARCODE_SUCCESS = "FETCH_PRODUCT_BY_BARCODE_SUCCESS";
export const SET_BARCODE = "SET_BARCODE";
export const SET_PRODUCT_FOUND_FLAG = "SET_PRODUCT_FOUND_FLAG";
export const SET_PRODUCT = "SET_PRODUCT";

export const FETCH_PRODUCT_BY_SKU_REQUEST = "FETCH_PRODUCT_BY_SKU_REQUEST";
export const FETCH_PRODUCT_BY_SKU_ERROR = "FETCH_PRODUCT_BY_SKU_ERROR";
export const FETCH_PRODUCT_BY_SKU_SUCCESS = "FETCH_PRODUCT_BY_SKU_SUCCESS";
export const SET_PRODUCT_FOUND_BY_SKU_FLAG = "SET_PRODUCT_FOUND_BY_SKU_FLAG";
export const SET_PRODUCT_FOUND_BY_SKU = "SET_PRODUCT_FOUND_BY_SKU";

// ACTIONS
export const setBarcode = (data) => ({
  type: SET_BARCODE,
  data
});

const fetchProductByBarcodeRequest = () => ({
  type: FETCH_PRODUCT_BY_BARCODE_REQUEST
});

const fetchProductByBarcodeError = () => ({
  type: FETCH_PRODUCT_BY_BARCODE_ERROR
});

const fetchProductByBarcodeSuccess = () => ({
  type: FETCH_PRODUCT_BY_BARCODE_SUCCESS,
});

export const setProductFoundFlag = (value) => ({
  type: SET_PRODUCT_FOUND_FLAG,
  value
});

export const setProduct = (data) => ({
  type: SET_PRODUCT,
  data
});

const fetchProductBySKURequest = () => ({
  type: FETCH_PRODUCT_BY_SKU_REQUEST
});

const fetchProductBySKUError = () => ({
  type: FETCH_PRODUCT_BY_SKU_ERROR
});

const fetchProductBySKUSuccess = data => ({
  type: FETCH_PRODUCT_BY_SKU_SUCCESS,
  data
});

export const setProductFoundBySKUFlag = (data) => ({
  type: SET_PRODUCT_FOUND_BY_SKU_FLAG,
  data
});

export const setProductFoundBySKU = (data) => ({
  type: SET_PRODUCT_FOUND_BY_SKU,
  data
});

export function fetchProductByBarcode(user, barcode, storeNo) {
  return async (dispatch, getState) => {
    try {
      dispatch(setProductFoundFlag(null));
      dispatch(setBarcode(barcode));
      dispatch(fetchProductByBarcodeRequest());
      if (language==="pt-BR" || language==="es-PE") {
        var response = await Actions.requestWithAuthentication(HttpRequests.fetchProductByBarcode, user, barcode, storeNo);
      }
      else if (language==="es-CO") {
        var response = await Actions.requestWithSapAuthentication(HttpRequests.searchProductByBarcode, barcode, storeNo);
      }
      dispatch(fetchProductByBarcodeSuccess());
      if (response === 600110 || response === 400 || response === 404)
        dispatch(setProductFoundFlag(false));
      else if (response === 600930) {
        dispatch({ type: 'SET_TOO_MANY_REQUESTS_ERROR' });
      }
      else if (
        (language==="pt-BR" || language === "es-PE") && typeof response.art_no !== "undefined" ||
        language==="es-CO" && typeof response.ProductSKU !== "undefined"
      ) {
        dispatch(setProductFoundFlag(true));
        dispatch(setProduct(response));
      }
      else throw response;
    }
    catch (error) {
      dispatch(fetchProductByBarcodeError());
    }
  };
}

export function fetchProductBySKU(sku, store) {
  return async (dispatch, getState) => {
    try {
      dispatch(fetchProductBySKURequest());
      let response = await Actions.requestWithSapAuthentication(HttpRequests.searchProductBySKU, sku, store);
      dispatch(fetchProductBySKUSuccess());
      if (response === 600110)
        dispatch(setProductFoundBySKU(false));
      else if (response === 600930) {
        dispatch({ type: 'SET_TOO_MANY_REQUESTS_ERROR' });
      }
      else if (response) {
        dispatch(setProductFoundBySKUFlag(true));
        dispatch(setProductFoundBySKU(response));
      }
    }
    catch (error) {
      console.log('error', error);
      dispatch(fetchProductBySKUError());
    }
  };
}
