import { MaskService } from 'react-native-masked-text';
import { Asset } from 'expo';
import { Image, Platform } from 'react-native';
import Snackbar from "react-native-snackbar";

export function titleCase(str) {
  if (typeof str === 'string')
    return str.toLowerCase().replace(/(^[a-z]| [a-z])/g, function(match) {
      return match.toUpperCase();
    });
  else 
    return '';
}

function numberWithDots(x) {
  return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
}

export const formatStringForPrice = (price, language = null) => {
  try {
    var auxPrice = price;
    if (price.indexOf(',') > -1)
      auxPrice = auxPrice.replace(",", ".")    
    var priceFloat = parseFloat(auxPrice);
    const decimalPart = (priceFloat % 1).toFixed(2).substring(2); 
    const wholePart = numberWithDots(Math.floor(priceFloat));
    if (language==="es-CO") return wholePart; 
    return wholePart + "," + decimalPart; 
  }
  catch(e) {
    return price
  }
}

export const formatNumberForPrice = (price) => {
  try {
    return price.toFixed(2).replace(".", ","); 
  }
  catch(e) {
    return price
  }
}

export const capitalizeFirstLetter = (string) => {
  if (typeof string === 'string')
    return string.replace(/^\w/, c => c.toUpperCase());
  else
    return '';
}

export const capitalizeEachFirstLetter = (string) => {
  if (typeof string === 'string'){
    let result = '';
    string.split(' ').map(item => {
      result = result + ' ' + item.replace(/^\w/, c => c.toUpperCase());
    });
    return result;
  }else{
    return '';
  }
}

export const getSubtraction = (a, b) => {
  return (parseFloat(a) - parseFloat(b));
}

export const nullFunction = () => (null);

export function guidGenerator() {
  var S4 = function() {
     return (((1+Math.random())*0x10000)|0).toString(16).substring(1);
  };
  return (S4()+S4()+"-"+S4()+"-"+S4()+"-"+S4()+"-"+S4()+S4()+S4());
}

export const global = {};

export function timeoutPromise(timeout, promise) {
  let expire = new Promise((resolve, reject) => {
    let timer = setTimeout(() => {
      clearTimeout(timer);
      reject( new Error('Request timed out') )
    }, timeout)
  })

  return Promise.race([promise, expire])
}

export function getDateTime() {
  var date = new Date();
  var month = date.getMonth() + 1;
  var day = date.getDate();
  var now =  date.getFullYear() + "/";

  if(month.length == 1) {
    now = now.concat("0", month, "/");
  }
  else {
    now = now.concat(month, "/");
  }

  if(day.length == 1) {
    now = now.concat("0", day);
  }
  else {
    now = now.concat(day);
  }

  return now.concat(" ", date.getHours(), ":", date.getMinutes(), ":", date.getSeconds());
}

export function validateEmail(email) {
  var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(String(email).toLowerCase());
}

export function containsOnlyWhiteSpace(string) {
  return (!string.replace(/\s/g, '').length)
}

export const validatePersonalIdentifier = (language, personalIdentifier) => {
  let validation = {
    "pt-BR": () => {
      let validCpf = (personalIdentifier.length===11 && MaskService.isValid('cpf', personalIdentifier))
      let validCnpj = (personalIdentifier.length===14 && MaskService.isValid('cnpj', personalIdentifier))
      return validCpf || validCnpj
    }, 
    "es-CO": () => MaskService.isValid('only-numbers', personalIdentifier),
    "es-PE": () => MaskService.isValid('only-numbers', personalIdentifier),
    "default": () => MaskService.isValid('only-numbers', personalIdentifier),
  }
  return (typeof validation[language]==='undefined' ? validation['default']() : validation[language]())
}

export const validatePhone= (language, phone) => {
  let validation = {
    "pt-BR": () => MaskService.toRawValue(getPhoneMask(language), phone).length > 9, 
    "es-CO": () => MaskService.toRawValue(getPhoneMask(language), phone).length > 9, 
    "es-PE": () => MaskService.toRawValue(getPhoneMask(language), phone).length > 9,
    "default": () => true
  }
  return (typeof validation[language]==='undefined' ? validation['default']() : validation[language]())
}

export const formatPhone = (language, phone) => {
  let validation = {
    "pt-BR": () => 
      phone.length === 13 && phone.substr(0, 2) === "55"
      ? phone.substr(2)
      : phone, 
    "es-CO": () => phone, 
    "es-PE": () => phone, 
    "default": () => phone
  }
  return (typeof validation[language]==='undefined' ? validation['default']() : validation[language]())
}

export const getPersonalIdentifierMask = (language, personalIdentifier) => {
  let length = personalIdentifier.length;
  let mask = {
    "pt-BR": () => length === 11 ? 'cpf' : length === 14 ? 'cnpj' : 'only-numbers',
    "es-CO": () => 'only-numbers',
    "es-PE": () => 'only-numbers',
    "default": () => 'only-numbers'
  }
  return (typeof mask[language]==='undefined' ? mask['default']() : mask[language]())
}

export const getPhoneMask = (language) => {
  let mask = {
    "pt-BR": () => 'cel-phone',
    "es-CO": () => 'only-numbers',
    "es-PE": () => 'only-numbers',
    "default": () => 'only-numbers'
  }
  return (typeof mask[language]==='undefined' ? mask['default']() : mask[language]())
}


export const cacheImages = (images) => {
  return images.map(image => {
    if (typeof image === 'string') {
      return Image.prefetch(image);
    } else {
      return Asset.fromModule(image).downloadAsync();
    }
  });
}

export const getLanguage = (bundleIdentifier) => {
  let language = {
    "br.com.makrocapp.app": () => "pt-BR",
    "br.com.makrocapp.app.debug": () => "pt-BR",
    "co.makro.cap": () => "es-CO",
    "co.makro.cap.debug": () => "es-CO",
    "pe.com.makrocapp.app": () => "es-PE",
    "pe.com.makrocapp.app.debug": () => "es-PE",
    "default": () => "pt-BR"
  }
  return (typeof language[bundleIdentifier]==='undefined' ? language['default']() : language[bundleIdentifier]());
}

export const showSnackbar = (message, duration = Snackbar.LENGTH_INDEFINITE) => Snackbar.show({
  title: message,
  duration: duration,
  action: {
    title: 'OK',
    color: 'white',
  }
});

export const getCustomerType = (personalIdentifier) => {
  try {
    if (MaskService.isValid('cpf', personalIdentifier)) 
      return "consumidor final";
    else if (MaskService.isValid('cnpj', personalIdentifier)) 
      return "pessoa jurídica";
    return "indefinido";
  }
  catch(e) {
    return "indefinido";
  }
}

export const removeDuplicatesbyAttribute = (array, attribute) => {
  return array.map(e => e[attribute])
              .map((e, i, final) => final.indexOf(e) === i && i)
              .filter(e => array[e]).map(e => array[e]);
}
