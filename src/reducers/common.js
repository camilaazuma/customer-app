import { titleCase } from "../utils";

userInitialState = {
  passportNo: "",
  passportRaw: "",
  fullName: "",
  email: "",
  deviceId: "",
  personalIdentifier: "",
  phone: "",
  cep: "",
  street: "",
  addressNumber: "",
  complement: "",
  city: "",
  stateName: "",
  contactUuid: ""
}

export const userReducer = (state = userInitialState, action) => {
  switch(action.type) {
    case 'SET_USER_INFO':
      return {
        passportNo: action.passportNo,
        passportRaw: action.passportRaw,
        fullName: action.fullName,
        email: action.email,
        deviceId: action.deviceId,
        personalIdentifier: action.personalIdentifier,
        phone: action.phone,
        cep: action.cep,
        street: action.street,
        addressNumber: action.addressNumber,
        complement: action.complement,
        city: action.city,
        stateName: action.stateName,
        contactUuid: action.contactUuid
      };
    case 'RESET_USER_INFO':
      return { userInitialState };
    default:
      return state;
  }
}

export const currentStoreReducer = (state = {object:null, preferred:null}, action) => {
  switch(action.type) {
    case 'SET_CURRENT_STORE': {
      return {
        ...state,
        object: action.currentStore
      }
    }
    case 'SET_PREFERRED_STORE': {
      return {
        ...state,
        preferred: action.preferredStore
      }
    }
    case 'CLEAR_PREFERRED_STORE': {
      return {
        ...state,
        preferred: null
      }
    }
    case 'CLEAR_CURRENT_STORE': {
      return {
        ...state,
        object: null
      }
    }
    default:
      return state;
  }
}

export const appLoadingStateReducer = (state = { loading: false }, action) => {
    switch(action.type) {
      case 'SET_LOADING':
        return { loading: action.payload };
      case 'STOP_LOADING':
        return { loading: action.payload };
      default:
        return state;
    }
}

export function createModalReducer(initialState) {
  return modalReducer = (state = initialState, action) => {
    switch(action.type) {
      case 'SET_MODAL_VISIBLE':
        return {
          ...state,
          [action.modalKey] : true
        };
      case 'SET_MODAL_INVISIBLE':
        return {
          ...state,
          [action.modalKey] : false
        };
      default:
        return state;
    }
  }
}

export const fontLoaderReducer = (state = { fontLoaded: false }, action) => {
  switch(action.type) {
    case 'SET_FONT_LOADED_FLAG':
      return { fontLoaded: true };
    default:
      return state
  }
}

export const fetchStoreListReducer = (state = {loading: false, fetching: false, error: false, storeList: [], isSorted: false, nearestStore: null, firebaseStoreVersion: null }, action) => {
  switch(action.type) {
    case 'FETCH_STORE_LIST_SUCCESS': {
      return {
        ...state,
        loading: false,
        fetching: false,
        error: false,
        storeList: action.data,
      };
    }
    case 'FETCH_STORE_LIST_REQUEST': {
      return {
        ...state,
        loading: true,
        fetching: true,
        error: false,
      };
    }
    case 'FETCH_MORE_STORE_LIST_REQUEST': {
      return {
        ...state,
        loading: false,
        fetching: true,
        error: false,
      };
    }
    case 'FETCH_STORE_LIST_ERROR': {
      return {
        ...state,
        loading: false,
        fetching: false,
        error: true,
      };
    }
    case 'SORT_STORE_LIST': {
      return {
        ...state,
        storeList: action.data,
        isSorted: true,
        nearestStore: action.data[0]
      };
    }
    case 'SET_FIREBASE_STORE_VERSION': {
      return {
        ...state,
        firebaseStoreVersion: action.version
      };
    }
    default:
      return state;
  }
}

export const locationReducer = (state = { location: null, turnedOffError: false, timedOutError: false, accessDeniedError: false }, action) => {
  switch(action.type) {
    case 'GET_LOCATION_REQUEST':
      return {
        ...state,
        fetching: true,
        turnedOffError: false,
        timedOutError: false,
        accessDeniedError: false
      };
    case 'GET_LOCATION_SUCCESS':
      return {
        ...state,
        fetching: false,
        location: action.location,
        turnedOffError: false,
        timedOutError: false,
        accessDeniedError: false
      };
    case 'GET_LOCATION_TURNED_OFF_ERROR':
      return {
        ...state,
        fetching: false,
        turnedOffError: true,
        timedOutError: false,
        accessDeniedError: false
      };
    case 'GET_LOCATION_TIME_OUT_ERROR':
      return {
        ...state,
        fetching: false,
        turnedOffError: false,
        timedOutError: true,
        accessDeniedError: false
      };
    case 'GET_LOCATION_NO_PERMISSION_ERROR':
      return {
        ...state,
        fetching: false,
        turnedOffError: false,
        timedOutError: false,
        accessDeniedError: true
      }
    default:
      return state
  }
}

export const fetchTokenReducer = (state = {fetching: false, error: false}, action) => {
  switch(action.type) {
    case 'FETCH_TOKEN_SUCCESS': {
      return {
        ...state,
        fetching: false,
        error: false,
      };
    }
    case 'FETCH_TOKEN_REQUEST': {
      return {
        ...state,
        fetching: true,
        error: false,
      };
    }
    case 'FETCH_TOKEN_ERROR': {
      return {
        ...state,
        fetching: false,
        error: true,
      };
    }
    default:
      return state;
  }
}

export const getInstallationIdReducer = (state = { installationId: "" }, action) => {
  switch(action.type) {
    case 'GET_INSTALLATION_ID':
      return { installationId: action.installationId };
    default:
      return state
  }
}
