import { combineReducers } from "redux";
import { createModalReducer } from "./common";

const infoModalReducer = createModalReducer({
  infoModal: false,
  connectionError: false,
});

export const storesReducer = combineReducers({
  storeList: infoModalReducer,
});
