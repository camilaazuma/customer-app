import { combineReducers } from "redux";
import { createModalReducer } from "./common"

const successMessageModal = createModalReducer({
  updateInfo: false,
  loggedOutSuccessfully: false
});

const updateUserInfoInitialState = {
  updateUserResponse: "",
  loading: false,
  fetching: false,
  error: false
}

const updateUserReducer = (state = updateUserInfoInitialState, action) => {
  switch(action.type) {
    case 'UPDATE_USER_SUCCESS': {
      return {
        ...state,
        loading: false,
        fetching: false,
        error: false,
      };
    }
    case 'UPDATE_USER_REQUEST': {
      return {
        ...state,
        loading: true,
        fetching: true,
        error: false,
      };
    }
    case 'UPDATE_USER_ERROR': {
      return {
        ...state,
        loading: false,
        fetching: false,
        error: true,
      };
    }
    case 'SET_UPDATE_USER_RESPONSE': {
      return {
        ...state, 
        updateUserResponse: action.response,
      }
    }
    case 'RESET_UPDATE_USER_RESPONSE': {
      return {
        ...state, 
        updateUserResponse: "",
      }
    }
    default:
      return state;
  }
}

const updateEmailOrPhoneInfoInitialState = {
  updateEmailOrPhoneResponse: "",
  loading: false,
  fetching: false,
  error: false
}

const updateEmailOrPhoneReducer = (state = updateEmailOrPhoneInfoInitialState, action) => {
  switch(action.type) {
    case 'UPDATE_EMAIL_OR_PHONE_SUCCESS': {
      return {
        ...state,
        loading: false,
        fetching: false,
        error: false,
      };
    }
    case 'UPDATE_EMAIL_OR_PHONE_REQUEST': {
      return {
        ...state,
        loading: true,
        fetching: true,
        error: false,
      };
    }
    case 'UPDATE_EMAIL_OR_PHONE_ERROR': {
      return {
        ...state,
        loading: false,
        fetching: false,
        error: true,
      };
    }
    case 'SET_UPDATE_EMAIL_OR_PHONE_RESPONSE': {
      return {
        ...state, 
        updateEmailOrPhoneResponse: action.response,
      }
    }
    case 'RESET_UPDATE_EMAIL_OR_PHONE_RESPONSE': {
      return {
        ...state, 
        updateEmailOrPhoneResponse: "",
      }
    }
    default:
      return state;
  }
}


const sendTokenForPhoneOrEmailInitialState = {
  sendTokenForPhoneOrEmailResponse: "",
  loading: false,
  fetching: false,
  error: false
}

const sendTokenForPhoneOrEmailReducer = (state = sendTokenForPhoneOrEmailInitialState, action) => {
  switch(action.type) {
    case 'SEND_TOKEN_FOR_PHONE_OR_EMAIL_SUCCESS': {
      return {
        ...state,
        loading: false,
        fetching: false,
        error: false,
      };
    }
    case 'SEND_TOKEN_FOR_PHONE_OR_EMAIL_REQUEST': {
      return {
        ...state,
        loading: true,
        fetching: true,
        error: false,
      };
    }
    case 'SEND_TOKEN_FOR_PHONE_OR_EMAIL_ERROR': {
      return {
        ...state,
        loading: false,
        fetching: false,
        error: true,
      };
    }
    case 'SET_SEND_TOKEN_FOR_PHONE_OR_EMAIL_RESPONSE': {
      return {
        ...state, 
        sendTokenForPhoneOrEmailResponse: action.response,
      }
    }
    case 'RESET_SEND_TOKEN_FOR_PHONE_OR_EMAIL_RESPONSE': {
      return {
        ...state, 
        sendTokenForPhoneOrEmailResponse: "",
      }
    }
    default:
      return state;
  }
}




export const settingsReducer = combineReducers({
  settingsModal: successMessageModal,
  updateUser: updateUserReducer,
  updateEmailOrPhone: updateEmailOrPhoneReducer,
  sendTokenForPhoneOrEmail: sendTokenForPhoneOrEmailReducer
});
