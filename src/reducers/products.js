import { combineReducers } from "redux";

const productState = {
  productFetched: false,
  allProductsFetched: false,
  loading: false,
  error: false,
  fetching: false,
  product: {},
  productList: {},
  allProductsList: []
};

const categoryState = {
  categoriesFetched: false,
  loading: false,
  error: false,
  fetching: false,
  categoryList: []
};

const productSearchState = {
    productList: [],
    productFetched: false,
    fetching: false,
    loading: false,
    error: false,
    productFound: null,
    tooManyRequestsError: false
};

const fetchProductsReducer = (state = productState, action) => {
  switch(action.type) {
    case 'FETCH_PRODUCTS_QUERY': {
      return {
        ...state,
        loading: true,
        fetching: true,
        error: false,
      };
    }
    case 'FETCH_PRODUCTS_SUCCESS': {
      return {
        ...state,
        loading: false,
        fetching: false,
        error: false,
        product: action.data,
      };
    }
    case 'FETCH_PRODUCTS_ERROR': {
      return {
        ...state,
        loading: false,
        fetching: false,
        error: true,
      };
    }
    case 'CLEAR_PRODUCTS_LIST': {
      return {
        ...state,
        product: {},
        productList: []
      }
    }
    case 'FETCH_PRODUCTS_LIST_SUCCESS': {
      return {
        ...state,
        loading: false,
        fetching: false,
        error: false,
        productList: action.data,
      };
    }
    case 'FETCH_ALL_PRODUCTS_QUERY': {
      return {
        ...state,
        loading: false,
        fetching: true,
        error: false,
        allProductsFetched: false
      };
    }
    case 'FETCH_ALL_PRODUCTS_SUCCESS': {
      return {
        ...state,
        loading: false,
        fetching: false,
        error: false,
        allProductsList: action.data,
        allProductsFetched: true
      };
    }
    case 'FETCH_ALL_PRODUCTS_ERROR': {
      return {
        ...state,
        loading: false,
        fetching: false,
        error: true,
      };
    }
    default:
      return state;
  }
}
const fetchCategoriesReducer = (state = categoryState, action) => {
  switch(action.type) {
    case 'FETCH_CATEGORIES_QUERY': {
      return {
        ...state,
        loading: true,
        fetching: true,
        error: false,
        categoriesFetched: false
      };
    }
    case 'FETCH_CATEGORIES_SUCCESS': {
      return {
        ...state,
        loading: false,
        fetching: false,
        error: false,
        categoryList: action.data,
        categoriesFetched: true
      };
    }
    case 'FETCH_CATEGORIES_ERROR': {
      return {
        ...state,
        loading: false,
        fetching: false,
        error: true,
      };
    }
    default:
      return state;
  }
}

const searchProductByNameReducer = (state = productSearchState, action) => {
  switch(action.type) {
    case 'FETCH_PRODUCTS_BY_NAME_SUCCESS': {
      return {
        ...state,
        loading: false,
        fetching: false,
        error: false,
      };
    }
    case 'FETCH_PRODUCTS_BY_NAME_REQUEST': {
      return {
        ...state,
        loading: true,
        fetching: true,
        error: false,
      };
    }
    case 'FETCH_PRODUCTS_BY_NAME_ERROR': {
      return {
        ...state,
        loading: false,
        fetching: false,
        error: true,
      };
    }
    case 'SET_PRODUCT_LIST': {
      return {
        ...state,
        productList: action.data,
      };
    }
    case 'SET_PRODUCTS_FOUND_FLAG': {
      return {
        ...state,
        productFound: action.value,
      };
    }
    case 'SET_TOO_MANY_REQUESTS_ERROR': {
      return {
        ...state,
        tooManyRequestsError: true,
      };
    }
    case 'RESET_TOO_MANY_REQUESTS_ERROR': {
      return {
        ...state,
        tooManyRequestsError: false,
      };
    }
    case 'RESET_PRODUCT_SEARCH_BY_NAME': {
      return {
        ...state,
        productList: [],
        productFetched: false,
        fetching: false,
        loading: false,
        error: false,
        productFound: null,
        tooManyRequestsError: false
      };
    }
    default:
      return state;
  }
}


export const productsReducer = combineReducers({
  fetchProduct: fetchProductsReducer,
  fetchCategories: fetchCategoriesReducer,
  searchProductByName: searchProductByNameReducer
});
