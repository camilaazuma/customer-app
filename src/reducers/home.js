import { combineReducers } from "redux";

const initialState = {
  carouselImageList: []
};

const fetchHomeCarouselReducer = (state = initialState, action) => {
  switch(action.type) {
    case 'FETCH_HOME_CAROUSEL_SUCCESS': {
      return {
        ...state,
        loading: false,
        fetching: false,
        error: false,
        carouselImageList: action.data,
      };
    }
    case 'FETCH_HOME_CAROUSEL_REQUEST': {
      return {
        ...state,
        loading: true, 
        fetching: true,
        error: false,
      };
    }
    case 'FETCH_HOME_CAROUSEL_ERROR': {
      return {
        ...state,
        loading: false,
        fetching: false,
        error: true,
      };
    }
    default: 
      return state;
  }
}


export const homeReducer = combineReducers({
  fetchHomeCarousel: fetchHomeCarouselReducer,
});
