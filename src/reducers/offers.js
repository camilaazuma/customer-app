import { combineReducers } from "redux";
import { createModalReducer } from "./common";

const offersListModalReducer = createModalReducer({
  activateOffer: false,
  addedToShoppingListSuccess: false,
  addToShoppingList: false,
  connectionError: false,
});

const initialState = {
  allOffersFetched: false,
  offerList: [],
  storeList: [],
  fetching: false,
  loading: false,
  error: false,
  activateOffer: false,
  addedToShoppingListSuccess: false,
  addToShoppingList: false,
  offerListPageNumber: 1,
  totalPageNumber: 0,
  categoryList: [{code: "", name: "TODOS"}],
  groupList: [{code: "", name: "TODOS"}]
};

const recommendationsState = {
  recommendationsFetched: false,
  recommendationList: {
    topSell: [],
    crossSell: [],
    upSell: []
  },
  fetching: false,
  loading: false,
  error: false,
  categoryList: [{code: "", name: "TODOS"}],
  groupList: [{code: "", name: "TODOS"}]
};

const fetchOfferListReducer = (state = initialState, action) => {
  switch(action.type) {
    case 'FETCH_OFFERS_SUCCESS': {
      // TODO: Melhorar. Iguala o código de categorias unificadas
      var offers = state.offerList.concat(action.data);
      offers = offers.map(offer => {
        if (offer.cod_sub_area == "021"){
          offer.cod_sub_area = "020";
        }
        if(offer.cod_sub_area == "031"){
          offer.cod_sub_area = "030";
        }
        return offer
      })

      return {
        ...state,
        loading: false,
        fetching: false,
        error: false,
        offerList: offers,
      };
    }
    case 'SET_OFFERS': {
      return {
        ...state,
        loading: false,
        fetching: false,
        error: false,
        offerList: action.data,
      };
    }
    case 'FETCH_OFFERS_REQUEST': {
      return {
        ...state,
        loading: true,
        fetching: true,
        error: false,
        allOffersFetched: false,
      };
    }
    case 'FETCH_MORE_OFFERS_REQUEST': {
      return {
        ...state,
        loading: false,
        fetching: true,
        error: false,
      };
    }
    case 'FETCH_OFFERS_ERROR': {
      return {
        ...state,
        loading: false,
        fetching: false,
        error: true,
      };
    }
    case 'FETCH_ALL_OFFERS_SUCCESS': {
      return {
        ...state,
        allOffersFetched: true,
      };
    }
    case 'CLEAR_OFFERS_LIST': {
      return {
        ...state,
        offerList: [],
      }
    }
    case 'SET_TOTAL_PAGE_NUMBER': {
      return {
        ...state,
        totalPageNumber: action.totalPageNumber,
      }
    }
    case 'INCREMENT_PAGE': {
      return {
        ...state,
        offerListPageNumber: state.offerListPageNumber + 1,
      }
    }
    case 'RESET_PAGE_NUMBER': {
      return {
        ...state,
        offerListPageNumber: 1,
      }
    }
    case 'FILTER_CATEGORY_SUCCESS': {
      // TODO: Melhorar. Troca os nomes de categorias e remove duplicatas
      var categories = action.category;
      categories.map(category => {
        category.name = /(horeca)/gi.test(category.name) ? "Utilidades" : category.name;
        category.name = /(bar)/gi.test(category.name) ? "Bebidas" : category.name;
        category.name = /(merc.gerais)/gi.test(category.name) ? "Bazar e utilizades" : category.name;
        category.name = /(basicos)/gi.test(category.name) || /(seca)/gi.test(category.name) ? "Mercearia" : category.name
        category.name = /(carnes e hortifruti)/gi.test(category.name) || /(frios e laticinios)/gi.test(category.name) ? "Perecíveis" : category.name
      });
      categories = categories.filter((category, index, self) =>
        index === self.findIndex((t) => (
          t.name === category.name || t.code === category.code
        ))
      )

      return {
        ...state,
        categoryList: categories,
        groupList: action.group
      }
    }
    default:
      return state;
  }
}

const fetchRecommendationListReducer = (state = recommendationsState, action) => {
  switch(action.type) {
    case 'FETCH_RECOMMENDATIONS_SUCCESS': {
      return {
        ...state,
        loading: false,
        fetching: false,
        error: false, 
        recommendationsFetched: true
      };
    }
    case 'SET_RECOMMENDATIONS': {
      return {
        ...state,
        loading: false,
        fetching: false,
        error: false,
        recommendationList: {
          ...state.recommendationList,
          [action.offerType]: action.data
        } ,
      };
    }
    case 'FETCH_RECOMMENDATIONS_REQUEST': {
      return {
        ...state,
        loading: true,
        fetching: true,
        error: false,
      };
    }
    case 'FETCH_RECOMMENDATIONS_ERROR': {
      return {
        ...state,
        loading: false,
        fetching: false,
        error: true,
      };
    }
    case 'CLEAR_RECOMMENDATIONS_LIST': {
      return recommendationsState;
    }
    default:
      return state;
  }
}


export const offersReducer = combineReducers({
  productsList: offersListModalReducer,
  fetchOfferList: fetchOfferListReducer,
  fetchCustomerRecommendations: fetchRecommendationListReducer
});
