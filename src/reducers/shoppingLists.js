import { combineReducers } from "redux";
import { createModalReducer } from "./common";

const createShoppingListModalReducer = createModalReducer({
  createList: false,
  editList: false,
  deleteProduct: false,
  connectionError: false,
});

const initialState = {
  allShopListFetched: false,
  shopListList: [],
  loading: false,
  error: false,
  activateShopList: false,
  addedToShoppingListSuccess: false,
  addToShoppingList: false,
  productListStatus: [],
  getStatusError: false
};

const CRUDShopListListReducer = (state = initialState, action) => {
  switch(action.type) {
    case 'CRUD_SHOPLISTS_SUCCESS': {
      return {
        ...state,
        loading: false,
        fetching: false,
        error: false,
        shopListList: action.data,
        allShopListFetched: true
      };
    }
    case 'CRUD_SHOPLISTS_REQUEST': {
      return {
        ...state,
        loading: true,
        fetching: true,
        error: false,
      };
    }
    case 'FETCH_MORE_SHOPLISTS_REQUEST': {
      return {
        ...state,
        loading: false,
        fetching: true,
        error: false,
      };
    }
    case 'CRUD_SHOPLISTS_ERROR': {
      return {
        ...state,
        loading: false,
        fetching: false,
        error: true,
      };
    }
    case 'FETCH_ALL_SHOPLISTS_SUCCESS': {
      return {
        ...state,
        allShopListFetched: true,
      };
    }
    case 'CLEAR_SHOPLISTS_LIST': {
      return {
        ...state,
        shopListList: [],
      }
    }

    default:
      return state;
  }
}

const fetchProductListStatusReducer = (state = initialState, action) => {
  switch(action.type) {

  case 'FETCH_PRODUCTS_ARE_AVAILABLE_REQUEST': {
    return {
      ...state,
      loading: true,
      fetching: true,
      getStatusError: false,
    };
  }
  case 'FETCH_PRODUCTS_ARE_AVAILABLE_ERROR': {
    return {
      ...state,
      loading: false,
      fetching: false,
      getStatusError: true,
    };
  }
  case 'FETCH_PRODUCTS_ARE_AVAILABLE_SUCCESS': {
    return {
      ...state,
      loading: false,
      fetching: false,
      productListStatus: action.data,
    }
  }
    default:
      return state;
  }
}

const createShoppingListReducer = (state = initialState, action) => {
  switch(action.type) {
    case 'CREATE_SHOP_LIST_SUCCESS': {
      return {
        ...state,
        loading: false,
        fetching: false,
        error: false,
        shopListList: action.data
      };
    }
    case 'CREATE_SHOP_LIST_REQUEST': {
      return {
        ...state,
        loading: true,
        fetching: true,
        error: false,
      };
    }
    case 'CREATE_SHOP_LIST_ERROR': {
      return {
        ...state,
        loading: false,
        fetching: false,
        error: true,
      };
    }

    default:
      return state;
  }
}

const appraiseShoppingListInitialState = {
  appraiseShoppingListResponse: "",
  loading: false,
  fetching: false,
  error: false
}

const appraiseShoppingListReducer = (state = appraiseShoppingListInitialState, action) => {
  switch(action.type) {
    case 'APPRAISE_SHOP_LIST_SUCCESS': {
      return {
        ...state,
        loading: false,
        fetching: false,
        error: false,
        appraiseShoppingListResponse: action.response
      };
    }
    case 'APPRAISE_SHOP_LIST_REQUEST': {
      return {
        ...state,
        loading: true,
        fetching: true,
        error: false,
      };
    }
    case 'APPRAISE_SHOP_LIST_ERROR': {
      return {
        ...state,
        loading: false,
        fetching: false,
        error: true,
      };
    }
    case 'RESET_APPRAISE_SHOP_LIST_RESPONSE': {
      return {
        ...state,
        appraiseShoppingListResponse: ""
      };
    }

    default:
      return state;
  }
}


export const shoppingListsReducer = combineReducers({
  createListModal: createShoppingListModalReducer,
  CRUDShoppingList: CRUDShopListListReducer,
  fetchProductListStatus: fetchProductListStatusReducer,
  createShoppingList: createShoppingListReducer,
  appraiseShoppingList: appraiseShoppingListReducer
});
