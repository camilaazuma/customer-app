import { combineReducers } from "redux";
import { createModalReducer } from "./common";

const modalReducer = createModalReducer({
  notFound: false,
  manualInquiry: false,
  selectQuantity: false,
  addToShoppingList: false,
  addedToShoppingListSuccess: false,
  connectionError: false,
});

const initialState = {
    currentCode: null,
    productFetched: false,
    product: null,
    fetching: false,
    loading: false,
    error: false,
    productFound: null,
    tooManyRequestsError: false
};

const productState = {
    product: null,
    productFetched: false,
    fetching: false,
    loading: false,
    error: false,
    productFound: null,
    tooManyRequestsError: false
};

const readBarcodeReducer = (state = initialState, action) => {
  switch(action.type) {
    case 'SET_BARCODE': {
      return {
        ...state,
        currentCode: action.data,
      };
    }
    case 'FETCH_PRODUCT_BY_BARCODE_SUCCESS': {
      return {
        ...state,
        loading: false,
        fetching: false,
        error: false,
      };
    }
    case 'FETCH_PRODUCT_BY_BARCODE_REQUEST': {
      return {
        ...state,
        loading: true,
        fetching: true,
        error: false,
      };
    }
    case 'FETCH_PRODUCT_BY_BARCODE_ERROR': {
      return {
        ...state,
        loading: false,
        fetching: false,
        error: true,
      };
    }
    case 'SET_PRODUCT': {
      return {
        ...state,
        product: action.data,
      };
    }
    case 'SET_PRODUCT_FOUND_FLAG': {
      return {
        ...state,
        productFound: action.value,
      };
    }
    case 'SET_TOO_MANY_REQUESTS_ERROR': {
      return {
        ...state,
        tooManyRequestsError: true,
      };
    }
    case 'RESET_TOO_MANY_REQUESTS_ERROR': {
      return {
        ...state,
        tooManyRequestsError: false,
      };
    }
    default:
      return state;
  }
}

const fetchProductBySKUReducer = (state = productState, action) => {
  switch(action.type) {
    case 'SET_BARCODE': {
      return {
        ...state,
        currentCode: action.data,
      };
    }
    case 'FETCH_PRODUCT_BY_SKU_SUCCESS': {
      return {
        ...state,
        loading: false,
        fetching: false,
        error: false,
        productFound: null
      };
    }
    case 'FETCH_PRODUCT_BY_SKU_REQUEST': {
      return {
        ...state,
        loading: true,
        fetching: true,
        error: false,
      };
    }
    case 'FETCH_PRODUCT_BY_SKU_ERROR': {
      return {
        ...state,
        loading: false,
        fetching: false,
        error: true,
      };
    }
    case 'SET_PRODUCT_FOUND_BY_SKU': {
      return {
        ...state,
        product: action.data,
      };
    }
    case 'SET_PRODUCT_FOUND_BY_SKU_FLAG': {
      return {
        ...state,
        productFound: action.data,
      };
    }
    case 'SET_TOO_MANY_REQUESTS_ERROR': {
      return {
        ...state,
        tooManyRequestsError: true,
      };
    }
    case 'RESET_TOO_MANY_REQUESTS_ERROR': {
      return {
        ...state,
        tooManyRequestsError: false,
      };
    }
    default:
      return state;
  }
}

export const priceInquiryReducer = combineReducers({
  readBarcode: readBarcodeReducer,
  modal: modalReducer,
  fetchProductBySKU: fetchProductBySKUReducer
});
