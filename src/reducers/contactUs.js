import { combineReducers } from "redux";
import { createModalReducer } from "./common"

const initialState = {
};

const sendContactUsMessageReducer = (state = initialState, action) => {
  switch(action.type) {
    case 'SEND_CONTACT_US_MESSAGE_SUCCESS': {
      return {
        ...state,
        loading: false,
        fetching: false,
        error: false,
        protocol: action.data,
      };
    }
    case 'SEND_CONTACT_US_MESSAGE_REQUEST': {
      return {
        ...state,
        loading: true, 
        fetching: true,
        error: false,
      };
    }
    case 'SEND_CONTACT_US_MESSAGE_ERROR': {
      return {
        ...state,
        loading: false,
        fetching: false,
        error: true,
      };
    }
    default: 
      return state;
  }
}


export const contactUsReducer = combineReducers({
  sendContactUsMessage: sendContactUsMessageReducer
});
