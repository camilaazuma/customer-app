import { combineReducers } from "redux";
import * as Common from "./common";
import { offersReducer } from "./offers";
import { signUpFlowReducer } from "./signUp";
import { shoppingListsReducer } from "./shoppingLists";
import { storesReducer } from "./stores";
import { contactUsReducer } from "./contactUs";
import { settingsReducer } from "./settings";
import { priceInquiryReducer } from "./priceInquiry";
import { homeReducer } from "./home";
import { productsReducer } from "./products";

// Combine all the reducers
const rootReducer = combineReducers({
  appLoadingState: Common.appLoadingStateReducer,
  user: Common.userReducer, 
  currentStore: Common.currentStoreReducer,
  fontLoader: Common.fontLoaderReducer,
  location: Common.locationReducer,
  storeListFetch: Common.fetchStoreListReducer,
  tokenFetch: Common.fetchTokenReducer,
  installationId: Common.getInstallationIdReducer,
  signUp: signUpFlowReducer,
  offers: offersReducer,
  shoppingLists: shoppingListsReducer,
  stores: storesReducer,
  contactUs: contactUsReducer,
  settings: settingsReducer,
  priceInquiry: priceInquiryReducer,
  products: productsReducer,
  home: homeReducer,
  // ,[ANOTHER REDUCER], [ANOTHER REDUCER] ....
});

export default rootReducer;
