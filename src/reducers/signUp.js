import { combineReducers } from "redux";
import { createModalReducer } from "./common"

const cpfInputInitialState = {
  cpfOrCnpj: "",
  statusCode: null,
  passportRaw: "",
  passportNo: "",
  cpfInputResponse: "",
  loading: false,
  fetching: false,
  error: false
};

const cpfInputReducer = (state = cpfInputInitialState, action) => {
  switch(action.type) {
    case 'CHECK_SIGN_UP_STATUS_SUCCESS': {
      return {
        ...state,
        loading: false,
        fetching: false,
        error: false,
      };
    }
    case 'CHECK_SIGN_UP_STATUS_REQUEST': {
      return {
        ...state,
        loading: true,
        fetching: true,
        error: false,
      };
    }
    case 'CHECK_SIGN_UP_STATUS_ERROR': {
      return {
        ...state,
        loading: false,
        fetching: false,
        error: true,
      };
    }
    case 'SET_CPF_INPUT_RESPONSE': {
      return {
        ...state, 
        cpfInputResponse: action.response
      }
    }
    case 'RESET_CPF_INPUT_RESPONSE': {
      return {
        ...state, 
        cpfInputResponse: ""
      }
    }
    case 'SET_CPF_OR_CNPJ': {
      return {
        ...state, 
        cpfOrCnpj: action.cpfOrCnpj
      }
    }
    case 'SET_STATUS_CODE': {
      return {
        ...state, 
        statusCode: action.statusCode
      }
    }
    case 'SET_PASSPORT_RAW': {
      return {
        ...state, 
        passportRaw: action.passportRaw
      }
    }
    case 'SET_PASSPORT_NO': {
      return {
        ...state, 
        passportNo: action.passportNo
      }
    }
    case 'RESET_STATUS': {
      return {
        ...state, 
        statusCode: null,
        passportRaw: "",
        passportNo: ""
      }
    }
    default:
      return state;
  }
}

const sendTokenForPasswordRegistrationInitialState = {
  sendTokenForPasswordRegistrationResponse: "",
  loading: false,
  fetching: false,
  error: false
}

const sendTokenForPasswordRegistrationReducer = (state = sendTokenForPasswordRegistrationInitialState, action) => {
  switch(action.type) {
    case 'SEND_TOKEN_FOR_PASSWORD_REGISTRATION_SUCCESS': {
      return {
        ...state,
        loading: false,
        fetching: false,
        error: false,
      };
    }
    case 'SEND_TOKEN_FOR_PASSWORD_REGISTRATION_REQUEST': {
      return {
        ...state,
        loading: true,
        fetching: true,
        error: false,
      };
    }
    case 'SEND_TOKEN_FOR_PASSWORD_REGISTRATION_ERROR': {
      return {
        ...state,
        loading: false,
        fetching: false,
        error: true,
      };
    }
    case 'SET_SEND_TOKEN_FOR_PASSWORD_REGISTRATION_RESPONSE': {
      return {
        ...state, 
        sendTokenForPasswordRegistrationResponse: action.response,
      }
    }
    case 'RESET_SEND_TOKEN_FOR_PASSWORD_REGISTRATION_RESPONSE': {
      return {
        ...state, 
        sendTokenForPasswordRegistrationResponse: "",
      }
    }
    default:
      return state;
  }
}



const createNewPasswordInitialState = {
  createPasswordResponse: "",
  loading: false,
  fetching: false,
  error: false
}

const createNewPasswordReducer = (state = createNewPasswordInitialState, action) => {
  switch(action.type) {
    case 'CREATE_NEW_PASSWORD_SUCCESS': {
      return {
        ...state,
        loading: false,
        fetching: false,
        error: false,
      };
    }
    case 'CREATE_NEW_PASSWORD_REQUEST': {
      return {
        ...state,
        loading: true,
        fetching: true,
        error: false,
      };
    }
    case 'CREATE_NEW_PASSWORD_ERROR': {
      return {
        ...state,
        loading: false,
        fetching: false,
        error: true,
      };
    }
    case 'SET_CREATE_PASSWORD_RESPONSE': {
      return {
        ...state, 
        createPasswordResponse: action.response,
      }
    }
    case 'RESET_CREATE_PASSWORD_RESPONSE': {
      return {
        ...state, 
        createPasswordResponse: "",
      }
    }
    default:
      return state;
  }
}

const createNewPasswordModalReducer = createModalReducer({
  modalMessage: false
});

const loginInitialState = {
  loginResponse: "",
  loading: false,
  fetching: false,
  error: false
}

const loginReducer = (state = loginInitialState, action) => {
  switch(action.type) {
    case 'LOGIN_SUCCESS': {
      return {
        ...state,
        loading: false,
        fetching: false,
        error: false,
      };
    }
    case 'LOGIN_REQUEST': {
      return {
        ...state,
        loading: true,
        fetching: true,
        error: false,
      };
    }
    case 'LOGIN_ERROR': {
      return {
        ...state,
        loading: false,
        fetching: false,
        error: true,
      };
    }
    case 'SET_LOGIN_RESPONSE': {
      return {
        ...state, 
        loginResponse: action.response,
      }
    }
    case 'RESET_LOGIN_RESPONSE': {
      return {
        ...state, 
        loginResponse: "",
      }
    }
    default:
      return state;
  }
}

const loginModalReducer = createModalReducer({
  modalMessage: false
});

const signUpInitialState = {
  signUpResponse: "",
  loading: false,
  fetching: false,
  error: false
}

const signUpReducer = (state = signUpInitialState, action) => {
  switch(action.type) {
    case 'SIGN_UP_SUCCESS': {
      return {
        ...state,
        loading: false,
        fetching: false,
        error: false,
      };
    }
    case 'SIGN_UP_REQUEST': {
      return {
        ...state,
        loading: true,
        fetching: true,
        error: false,
      };
    }
    case 'SIGN_UP_ERROR': {
      return {
        ...state,
        loading: false,
        fetching: false,
        error: true,
      };
    }
    case 'SET_SIGN_UP_RESPONSE': {
      return {
        ...state, 
        signUpResponse: action.response,
      }
    }
    case 'RESET_SIGN_UP_RESPONSE': {
      return {
        ...state, 
        signUpResponse: "",
      }
    }
    default:
      return state;
  }
}

const signUpModalReducer = createModalReducer({
  modalMessage: false
});

const validateConfirmationCodeInitialState = {
  validateConfirmationCodeResponse: "",
  loading: false,
  fetching: false,
  error: false
}

const validateConfirmationCodeReducer = (state = validateConfirmationCodeInitialState, action) => {
  switch(action.type) {
    case 'VALIDATE_CONFIRMATION_CODE_SUCCESS': {
      return {
        ...state,
        loading: false,
        fetching: false,
        error: false,
      };
    }
    case 'VALIDATE_CONFIRMATION_CODE_REQUEST': {
      return {
        ...state,
        loading: true,
        fetching: true,
        error: false,
      };
    }
    case 'VALIDATE_CONFIRMATION_CODE_ERROR': {
      return {
        ...state,
        loading: false,
        fetching: false,
        error: true,
      };
    }
    case 'SET_VALIDATE_CONFIRMATION_CODE_RESPONSE': {
      return {
        ...state, 
        validateConfirmationCodeResponse: action.response,
      }
    }
    case 'RESET_VALIDATE_CONFIRMATION_CODE_RESPONSE': {
      return {
        ...state, 
        validateConfirmationCodeResponse: "",
      }
    }
    default:
      return state;
  }
}

const validateConfirmationCodeModalReducer = createModalReducer({
  modalMessage: false
});


const confirmationCodeModalReducer = createModalReducer({
  successMessage: false
});


export const signUpFlowReducer = combineReducers({
  cpfInput: cpfInputReducer,
  sendTokenForPasswordRegistration: sendTokenForPasswordRegistrationReducer,
  createNewPassword: createNewPasswordReducer,
  createNewPasswordModal: createNewPasswordModalReducer,
  login: loginReducer,
  loginModal: loginModalReducer,
  signUp: signUpReducer,
  signUpModal: signUpModalReducer,
  validateConfirmationCode: validateConfirmationCodeReducer,
  validateConfirmationCodeModal: validateConfirmationCodeModalReducer,
  confirmationCode: confirmationCodeModalReducer
});
