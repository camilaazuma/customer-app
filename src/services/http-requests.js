import _ from "lodash";
import VersionNumber from 'react-native-version-number';

const SAP_API_BASE_URL = "https://makrogroup.apimanagement.us2.hana.ondemand.com:443/v1"
const FIREBASE_BASE_URL = "https://us-central1-br-makro-rj.cloudfunctions.net";

var MAKRO_BASE_URL = "http://cap.makro.com.br/br-cap/cap-be/v2.1/index.php";
const MAKRO_BASE_URL_HOMOLOG = "http://cap.makro.com.br/br-cap/cap-be-uat/v2.1/index.php";
const MAKRO_BASE_URL_COLOMBIA = "http://co-cap-be.azurewebsites.net/v2.1/index.php";
const MAKRO_BASE_URL_DEBUG = "http://192.168.1.148/br-cap-be/v2.1/index.php";
const MAKRO_BASE_URL_COLOMBIA_DEBUG = "http://192.168.1.134/co-cap-be/v2.1/index.php";
const MAKRO_BASE_URL_PERU = "https://pe-cap-be.azurewebsites.net/v2.1/index.php";
const MAKRO_BASE_URL_PERU_HOMOLOG = "https://pe-cap-be-hom.azurewebsites.net/v2.1/index.php";

const getBaseUrl = (bundleIdentifier) => {
    let urls = {
        "br.com.makrocapp.app": () => MAKRO_BASE_URL,
        "br.com.makrocapp.app.debug": () => MAKRO_BASE_URL_HOMOLOG,
        "co.makro.cap": () => MAKRO_BASE_URL_COLOMBIA,
        "co.makro.cap.debug": () => MAKRO_BASE_URL_PERU_HOMOLOG,
        "pe.com.makrocapp.app": () => MAKRO_BASE_URL_PERU_HOMOLOG,
        "pe.com.makrocapp.app.debug": () => MAKRO_BASE_URL_PERU_HOMOLOG,
        "default": () => MAKRO_BASE_URL
    }
    return (typeof urls[bundleIdentifier] === 'undefined' ? urls['default']() : urls[bundleIdentifier]())
}

const getCountryId = (bundleIdentifier) => {
    let ids = {
        "br.com.makrocapp.app": () => 'BR',
        "br.com.makrocapp.app.debug": () => 'BR',
        "co.makro.cap": () => 'CO',
        "co.makro.cap.debug": () => 'CO',
        "pe.com.makrocapp.app": () => 'PE',
        "pe.com.makrocapp.app.debug": () => 'PE',
        "default": () => 'BR'
    }
    return (typeof ids[bundleIdentifier] === 'undefined' ? ids['default']() : ids[bundleIdentifier]())
}

MAKRO_BASE_URL = getBaseUrl(VersionNumber.bundleIdentifier);

const URL_STORE_LIST = MAKRO_BASE_URL + "/store/allstores";
const URL_HOME_MARQUEE = MAKRO_BASE_URL + "/marquee/get_home_marquee";
const URL_AUTH_TOKEN = MAKRO_BASE_URL + "/oauth/token";
const URL_SHOP_LIST = MAKRO_BASE_URL + "/shop_list";
const URL_PRICECHECK = MAKRO_BASE_URL + "/price/pricecheck";
const URL_APPRAISELIST = MAKRO_BASE_URL + "/price/appraise_list";
const URL_ARTICLES = MAKRO_BASE_URL + "/article/info";
const URL_CHECK_SIGN_UP_STATUS = MAKRO_BASE_URL + "/customer/check_sign_up_status";
const URL_SEND_TOKEN_FOR_PASSWORD_REGISTRATION = MAKRO_BASE_URL + "/customer/send_token_for_password_registration";
const URL_SEND_TOKEN_FOR_PHONE_OR_EMAIL = MAKRO_BASE_URL + "/customer/send_token_for_phone_or_email";

const URL_CREATE_PASSWORD = MAKRO_BASE_URL + "/customer/create_password";
const URL_LOGIN = MAKRO_BASE_URL + "/customer/login";
const URL_SIGN_UP = MAKRO_BASE_URL + "/customer/sign_up";
const URL_VALIDATE_TOKENPASS = MAKRO_BASE_URL + "/customer/validate_tokenpass";
const URL_USER_UPDATE = MAKRO_BASE_URL + "/customer/update";
const URL_USER_UPDATE_PHONE_OR_EMAIL = MAKRO_BASE_URL + "/customer/update_phone_or_email";
const URL_SHOP_LIST_SHARING = MAKRO_BASE_URL + "/share/list/";
const URL_CREATE_SHARED_SHOP_LIST = MAKRO_BASE_URL + "/shop_list/get_shared/";
const URL_SHARED_SHOP_LIST = MAKRO_BASE_URL + "/shop_list/download_shared/";
const URL_ARTICLE_SEARCH = FIREBASE_BASE_URL + "/getProduct";
const URL_STORES_BY_COUNTRY = MAKRO_BASE_URL + "/store/get_stores_from_location";
const URL_GET_OFFERS_BY_STORE = FIREBASE_BASE_URL + "/getStoreOffers";
const URL_GET_CUSTOMER_RECOMMENDATIONS = FIREBASE_BASE_URL + "/getCustomerRecommendations";

const URL_SAP_AUTH_TOKEN = SAP_API_BASE_URL + "/OAuthService/v1/OAuthService/GenerateToken"
const CLIENT_ID = "WAN1xeTyq54IDjvBTNHSBIq4yKx3jI8o";
const CLIENT_SECRET = "8GVPcv8cCgh9Cc21";
const GRANT_TYPE = "client_credentials"

const HttpRequests = {
    getStoreList() {
        return (
            fetch(`${URL_STORE_LIST}`)
                .then((res) => res.json())
                .then((data) => data.d)
                .catch((err) => console.log(err))
        );
    },
    getHomeCarousel(cluster) {
        const params = JSON.stringify({ "cluster": cluster });
        return (
            fetch(`${URL_HOME_MARQUEE}`, {
                method: 'POST',
                body: params
            })
                .then((res) => res.json())
                .then((data) => { return data.d; })
                .catch((err) => console.log(err))
        );
    },
    getShopList(user, updated, token) {
        const params = JSON.stringify({ "passport": user.passportNo, "updated": updated, "personal_identifier": user.personalIdentifier });
        return (
            fetch(`${URL_SHOP_LIST}/get`, {
                method: 'POST',
                headers: {
                    'Authorization': token,
                },
                body: params
            })
                .then((res) => res.json())
                .then((res) => { return res })
                .then((data) => data.d)
                .catch((err) => console.log(err))
        );
    },
    createSharedShopList(user, listId, token) {
        return (
            fetch(`${URL_CREATE_SHARED_SHOP_LIST}`, {
                method: 'POST',
                headers: {
                    'Authorization': token,
                },
                body: JSON.stringify({ "personal_identifier": user.personalIdentifier, "shop_list_id": listId, "passport": user.passportRaw })
            })
                .then((res) => res.json())
                .then((data) => data.d)
                .catch((err) => console.log(err))
        );
    },
    getSharedShopList(user, listId, newListId, token) {
        return (
            fetch(`${URL_SHARED_SHOP_LIST}`, {
                method: 'POST',
                headers: {
                    'Authorization': token,
                },
                body: JSON.stringify({ "personal_identifier": user.personalIdentifier, "shop_list_id": listId, "new_shop_list_id": newListId, "created": true })
            })
                .then((res) => res.json())
                .then((data) => data.d)
                .catch((err) => console.log(err))
        );
    },
    fetchProductByBarcode(user, barcode, storeNo, token) {
        const params = JSON.stringify({ "passport": _.padStart(storeNo, 2, '0') + "0000000", "personal_identifier": user.personalIdentifier, "barcode": barcode });
        return (
            fetch(`${URL_PRICECHECK}`, {
                method: 'POST',
                headers: {
                    'Authorization': token,
                },
                body: params
            })
                .then((res) => res.json())
                .then((data) => data.d)
                .catch((err) => console.log(err))
        );
    },
    checkSignUpStatus(personalIdentifier, documentName, token) {
        const params = JSON.stringify({
            "personal_identifier": personalIdentifier,
            "country_id": getCountryId(VersionNumber.bundleIdentifier),
            "document_name": documentName,
            "token": 'Bearer '+ token
        });
        return (
            fetch(`${URL_CHECK_SIGN_UP_STATUS}`, {
                method: 'POST',
                body: params
            })
                .then((res) => {console.log(res);
                 return res.json()})
                .then((data) => { return data.d; })
                .catch((err) => console.log(err))
        );
    },
    sendTokenForPasswordRegistration(personalIdentifier, validationType, keepStatus) {
        const params = JSON.stringify({ "personal_identifier": personalIdentifier, "validation_type": validationType, "keep_status": keepStatus });
        return (
            fetch(`${URL_SEND_TOKEN_FOR_PASSWORD_REGISTRATION}`, {
                method: 'POST',
                body: params
            })
                .then((res) => res.json())
                .then((data) => { return data.d })
                .catch((err) => console.log(err))
        );
    },
    sendTokenForPhoneOrEmail({ personalIdentifier, validationType, phone, email }) {
        const params = JSON.stringify({ "personal_identifier": personalIdentifier, "validation_type": validationType, "phone": phone, "email": email });
        return (
            fetch(`${URL_SEND_TOKEN_FOR_PHONE_OR_EMAIL}`, {
                method: 'POST',
                body: params
            })
                .then((res) => res.json())
                .then((data) => { return data.d })
                .catch((err) => console.log(err))
        );
    },
    createPassword(personalIdentifier, password) {
        const params = JSON.stringify({ "personal_identifier": personalIdentifier, "password": password });
        return (
            fetch(`${URL_CREATE_PASSWORD}`, {
                method: 'POST',
                body: params
            })
                .then((res) => { return res.json() })
                .then((data) => data.d)
                .catch((err) => console.log(err))
        );
    },
    signUp(full_name, personalIdentifier, phone, email, password, validationType) {
        const params = JSON.stringify({
            "full_name": full_name,
            "personal_identifier": personalIdentifier,
            "phone": phone,
            "email": email,
            "password": password,
            "validation_type": validationType
        });
        return (
            fetch(`${URL_SIGN_UP}`, {
                method: 'POST',
                body: params
            })
                .then((res) => res.json())
                .then((data) => data.d)
                .catch((err) => console.log(err))
        );
    },
    login(personalIdentifier, password) {
        const params = JSON.stringify({ "personal_identifier": personalIdentifier, "password": password });
        return (
            fetch(`${URL_LOGIN}`, {
                method: 'POST',
                body: params
            })
                .then((res) => res.json())
                .then((data) => data.d)
                .catch((err) => console.log(err))
        );
    },
    validateTokenpass(personalIdentifier, tokenpass) {
        const params = JSON.stringify({
            "personal_identifier": personalIdentifier,
            "tokenpass": tokenpass,
        });
        return (
            fetch(`${URL_VALIDATE_TOKENPASS}`, {
                method: 'POST',
                body: params
            })
                .then((res) => res.json())
                .then((data) => { return data.d })
                .catch((err) => console.log(err))
        );
    },
    getToken(personalIdentifier, deviceId, phone, email) {
        const params = JSON.stringify({
            "personal_identifier": personalIdentifier,
            "device_id": deviceId,
            "phone": phone,
            "email": email
        })
        return (
            fetch(`${URL_AUTH_TOKEN}`, {
                method: 'POST',
                body: params
            })
                .then((res) => res.json())
                .then((data) => { return data.d })
                .catch((err) => console.log(err))
        );
    },
    getBearerToken() {
        const body = `client_id=${CLIENT_ID}&client_secret=${CLIENT_SECRET}&grant_type=${GRANT_TYPE}`;
        return (
            fetch(`${URL_SAP_AUTH_TOKEN}`, {
                method: 'POST',
                body: body,
                headers: {
                    "Content-Type": "application/x-www-form-urlencoded"
                }
            })
                .then((res) => res.json())
                .then((data) => { return data })
                .catch((err) => console.log(err))
        );
    },
    updateUser(personalIdentifier, fullName, cep, street, addressNumber, complement, city, stateName) {
        const params = JSON.stringify({
            "personal_identifier": personalIdentifier,
            "full_name": fullName,
            "cep": cep,
            "street": street,
            "addressNumber": addressNumber,
            "addressComplement": complement,
            "city": city,
            "state": stateName
        })
        return (
            fetch(`${URL_USER_UPDATE}`, {
                method: 'POST',
                body: params
            })
                .then((res) => res.json())
                .then((data) => { return data.d })
                .catch((err) => console.log(err))
        );
    },
    updateEmailOrPhone(personalIdentifier, email, phone) {
        const params = JSON.stringify({
            "personal_identifier": personalIdentifier,
            "email": email,
            "phone": phone,
        });
        return (
            fetch(`${URL_USER_UPDATE_PHONE_OR_EMAIL}`, {
                method: 'POST',
                body: params
            })
                .then((res) => res.json())
                .then((data) => { return data.d })
                .catch((err) => console.log(err))
        );
    },
    fetchProductsArticles(user, barcodes, token) {
        return (
            fetch(`${URL_ARTICLES}`, {
                method: 'POST',
                headers: {
                    'Authorization': token,
                },
                body: JSON.stringify({ "passport": user.passportNo, "articles": barcodes, "personal_identifier": user.personalIdentifier })
            })
                .then((res) => res.json())
                .then((data) => data.d)
                .catch((err) => console.log(err))
        );
    },
    saveShopList(user, updated, shopList, token) {
        const params = JSON.stringify({
            "passport": user.passportNo,
            "updated": updated,
            "personal_identifier": user.personalIdentifier,
            "shop_lists": [shopList]
        });
        return (
            fetch(`${URL_SHOP_LIST}/save`, {
                method: 'POST',
                headers: {
                    'Authorization': token,
                },
                body: params
            })
                .then((res) => res.json())
                .then((res) => { return res })
                .then((data) => data.d)
                .catch((err) => console.log(err))
        );
    },
    getSharingListUrl(list) {
        return `${URL_SHOP_LIST_SHARING + list.uuid + '/?toApp=' + Expo.Linking.makeUrl()}`;
    },
    appraiseShopList(storeNo, personalIdentifier, uuid, token) {
        const params = JSON.stringify({ "passport": _.padStart(storeNo, 2, '0') + "0000000", "personal_identifier": personalIdentifier, "uuid": uuid });
        return (
            fetch(`${URL_APPRAISELIST}`, {
                method: 'POST',
                headers: {
                    'Authorization': token,
                },
                body: params
            })
                .then((res) => res.json())
                .then((data) => data.d)
                .catch((err) => console.log(err))
        );
    },
    searchProductByName(user, searchKey, storeNo, token) {
        const params = JSON.stringify({ "personal_identifier": user.personalIdentifier, "productName": searchKey, "store": storeNo });
        return (
            fetch(`${URL_ARTICLE_SEARCH}`, {
                method: 'POST',
                headers: {
                    'Authorization': token,
                },
                body: params
            })
                .then((res) => res.json())
                .then((data) => data.d)
                .catch((err) => console.log(err))
        );
    },
    searchProductBySKU(sku, storeNo, token) {
        const params = JSON.stringify({ 
            "sku": sku, 
            "storeId": storeNo, 
            "countryId": getCountryId(VersionNumber.bundleIdentifier),
            "token": "Bearer " + token
        });
        return (
            fetch(`${URL_ARTICLE_SEARCH}`, {
                method: 'POST',
                body: params
            })
                .then((res) => { return res.json() })
                .then((data) => { return data })
                .catch((err) => console.log(err))
        );
    },
    searchProductByBarcode(barcode, storeNo, token) {
        const params = JSON.stringify({ 
            "barcode": barcode,
            "countryId": getCountryId(VersionNumber.bundleIdentifier),
            "storeId": storeNo,
            "token": "Bearer " + token
        });
        return (
            fetch(`${URL_ARTICLE_SEARCH}`, {
                method: 'POST',
                body: params
            })
                .then((res) => {
                    if (res.status === 200) return res.json();
                    return res.status;
                })
                .catch((err) => {
                    console.log(err);
                    throw err;
                })
        );
    },
    getStoreListByCountry() {
        const params = JSON.stringify({ "location": getCountryId(VersionNumber.bundleIdentifier) });
        return (
            fetch(`${URL_STORES_BY_COUNTRY}`, {
                method: 'POST',
                body: params
            })
                .then((res) => res.json())
                .then((data) => data.d)
                .catch((err) => console.log(err))
        );
    },
    getOffersByStore(storeNo, token) {
        const params = JSON.stringify({
        	"token": "Bearer " + token,
        	"countryId": getCountryId(VersionNumber.bundleIdentifier), //change to COUNTRY constant
        	"storeId": storeNo
        });
        return (
            fetch(`${URL_GET_OFFERS_BY_STORE}`, {
                method: 'POST',
                body: params
            })
                .then((res) => res.json())
                .then((data) => data.StoreOffers)
                .catch((err) => console.log(err))
        );
    },
    getCustomerRecommendations(customerId, scenarioId, token) {
        const params = JSON.stringify({
        	"token": "Bearer " + token,
        	"countryId": getCountryId(VersionNumber.bundleIdentifier), //change to COUNTRY constant
        	"contactUUID": customerId,
          "scenarioID": "Z_M" + getCountryId(VersionNumber.bundleIdentifier) + "_" + scenarioId
        });
        return (
            fetch(`${URL_GET_CUSTOMER_RECOMMENDATIONS}`, {
                method: 'POST',
                body: params
            })
                .then((res) => res.json())
                .then((data) => {return {scenario: scenarioId, offers: data.OffersRecommendations}})
                .catch((err) => console.log(err))
        );
    },
}

export default HttpRequests;
