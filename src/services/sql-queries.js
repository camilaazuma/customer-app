import { SQLite } from 'expo';

const db = SQLite.openDatabase('sql.db');

const Queries  = {
    getProduct(productId){
        return new Promise((resolve, reject) => {
            db.transaction(tx => {
                tx.executeSql(
                    'select description from product_catalog where id = ?',
                    [productId],
                    (_, { rows }) => {
                        // what you resolve here is what will be the result of
                        // await getProduct();
                        resolve(rows._array[0]);
                });
            }, null, null);
        });
    },
    getCategories()  {
        return new Promise((resolve, reject) => {
            db.transaction(tx => {
                tx.executeSql(
                    'select id, description from product_category',
                    [],
                    (_, { rows }) => {
                        // what you resolve here is what will be the result of
                        // await getCategories();
                        resolve(rows._array);
                });
            }, null, null);
        });
    },
    getAllProducts()  {
        return new Promise((resolve, reject) => {
            db.transaction(tx => {
                tx.executeSql(
                    'select id, description, category_id from product_catalog',
                    [],
                    (_, { rows }) => {
                        // what you resolve here is what will be the result of
                        // await getCategories();
                        resolve(rows._array);
                });
            }, null, null);
        });
    }
}

export default Queries;
