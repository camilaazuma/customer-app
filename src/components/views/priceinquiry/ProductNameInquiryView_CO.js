import React from "react";
import {
  View,
  Platform,
  LayoutAnimation,
  UIManager,
  ActivityIndicator,
  FlatList,
  NetInfo
} from "react-native";
import { ListItem } from "react-native-elements";
import { COLORS } from "../../../constants/styles";
import APP_DIMENSIONS from "../../../constants/appDimensions";
import * as Layout from "../../layout";
import * as Actions from "../../../actions";
import { connect } from "react-redux";
import { translate } from 'react-i18next';
import AsyncStorageHelper from "../../../helpers/AsyncStorageHelper";
import { showSnackbar } from "../../../utils";

const USER_SCAN_COUNT_KEY = "USER_SCAN_COUNT";
const USER_SCAN_DATE_KEY = "USER_SCAN_DATE";
const USER_SCAN_LIMIT_CPF = 35;
const USER_SCAN_LIMIT_CNPJ = 70;

@translate(['priceInquiry', 'common'], { wait: true })
class ProductNameInquiryView_CO extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      expanded: false,
      loading: false,
      successMessage: false,
      categories: [],
      products: [],
      endReached: false,
      productList: [],
      shopList: [],
      scanLimitReachedModal: false,
      modalText: "",
      modalMessage: false,
      productFound: null,
    };
    if (Platform.OS === "android") {
      UIManager.setLayoutAnimationEnabledExperimental(true);
    }
  }

  componentDidMount() {
    this.setState({
      productList: [],
      productFound: null,
    });

    this.props.resetProductSearchByName();

    if (this.props.fetchedStoreList.length===0)
      this.props.fetchStoreList();
    if (this.props.location === null)
      this.props.getLocation();
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    var obj = {};
    if(prevState.productList!== nextProps.productList ){
      obj.productList = nextProps.productList;
    }
    return Object.keys(obj).length? obj : null;
  }

  async componentDidUpdate(prevProps) {
    if (this.props.productDetailsFound===true  && prevProps.productDetailsFound===null) {
      this.props.navigation.navigate("ProductPriceDetail");
      await this._incrementScanCount();
    }
    else if (this.props.tooManyRequestsError===true && prevProps.tooManyRequestsError===false) {
      this.setState({scanLimitReachedModal: true});
      this.props.resetTooManyRequestsError();
    }

    if (prevProps.error===false && this.props.error===true) {
      let isConnected = await NetInfo.isConnected.fetch();
      if (!isConnected) {
        this.setState({ modalText: this.props.t("common:errors.connectionError"), modalMessage: true});
        showSnackbar(this.props.t("common:errors.noConnection"));
      }
      else
        this.setState({ modalText: this.props.t("common:errors.serverError"), modalMessage: true});
    }
  }

  changeLayout = () => {
    LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
    if (this.state.expanded === false)
      this.setState({
        expanded: true
      });
    else
      this.setState({
        expanded: false
      });
  };

  _getProductDetail =  (sku) => async () => {
    if (await this._canScan()) {
      await this.props.fetchProductBySKU(this.props.user, sku, this.props.currentStoreObject.store_no)
    }
    else this.setState({scanLimitReachedModal: true})
  };

  _canScan = async () => {
    var count = await AsyncStorageHelper.getItem(USER_SCAN_COUNT_KEY);
    if (count===null) count=0;
    var countDate = await AsyncStorageHelper.getItem(USER_SCAN_DATE_KEY);
    if (countDate === null) countDate =  (new Date(1,1,1,0,0,0,0)).valueOf();
    var currentDate = new Date().setHours(0, 0, 0, 0).valueOf();
    const scanLimit = this.props.user.personalIdentifier.length === 11 ? USER_SCAN_LIMIT_CPF : USER_SCAN_LIMIT_CNPJ;
    if (currentDate=== countDate && count < scanLimit) return true;
    else if (countDate < currentDate) {
      await AsyncStorageHelper.removeItem(USER_SCAN_COUNT_KEY);
      await AsyncStorageHelper.setItem(USER_SCAN_DATE_KEY, currentDate);
      return true
    }
    return false;
  }

  _incrementScanCount = async () => {
    var count = await AsyncStorageHelper.getItem(USER_SCAN_COUNT_KEY);
    if (count===null) count=0;
    await AsyncStorageHelper.setItem(USER_SCAN_COUNT_KEY, (count+1));
  }

  _renderFooter = () => {
    if (this.state.productList) {
      if (this.state.productList.length > 0 ) {
        return (
          <View >
            { this.state.endReached == false ?
              <ActivityIndicator size="large"/> : null
            }
          </View>
        )
      }else{
        if(this.state.productFound == false){
          return (
            <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
              <Layout.FontText style={{ color: COLORS.BLACK.BLACK, fontSize: APP_DIMENSIONS.SCREEN_WIDTH*0.053, fontFamily: 'open-sans' }}>
                {this.props.t("shoppingLists:addProducts.productsNotFound")}
              </Layout.FontText>
            </View>
          )
        }else if(this.state.productFound === null){
          return (
            <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
              <Layout.FontText style={{ color: COLORS.BLACK.BLACK, fontSize: APP_DIMENSIONS.SCREEN_WIDTH*0.053, fontFamily: 'open-sans' }}>
                {this.props.t("shoppingLists:addProducts.typeToSearch")}
              </Layout.FontText>
            </View>
          )
        }
      }
    }
  };

  _closeNotFoundModal = () => setTimeout(() => {
    this.props.closeModal('notFound');
  }, 1)

  _onEndReached = () => this.setState({ endReached: true });

  _setStore = async (store) =>
  {
    if  (this.props.currentStoreObject===null || store.store_no !== this.props.currentStoreObject.store_no || store.idlocation !== this.props.currentStoreObject.idlocation) {
      await this.props.setCurrentStore(store);
      this.changeLayout();
    }
  }

  _getStoreName = () => {
    const {currentStoreObject} = this.props;
    if (currentStoreObject!==null && typeof currentStoreObject!=='undefined') {
      if('name' in currentStoreObject){ // ORIGINAL_COLOMBIA: locationname
        return this.props.currentStoreObject.name.toUpperCase(); // ORIGINAL_COLOMBIA: locationname
      }
    }
    return "";
  }

  _searchProduct = async text => {
    this.props.resetProductSearchByName();
    await this.props.fetchProductByName(this.props.user, text, this.props.currentStoreObject.store_no)
  };

  _keyExtractor = (item, index) => index.toString();

  _renderProductItem = ({ item }) => {
    return (
      <Layout.ProductNamePriceInquiryListItem_CO
        code={item.productsku}
        productName={item.productname.toUpperCase()}
        productDescription={item.productsalespomopackingdescription.toUpperCase()}
        productImgSrc={item.productimages[0].imageurl}
        onPress={this._getProductDetail(item.productsku)}
      />
    )
  };

  render() {
    const { t, i18n } = this.props;
    return (
      <Layout.ViewContainer>
        <Layout.Loader loading={this.props.productListLoading || this.props.currentStoreObject===null} />
        <Layout.SuccessMessageModal
          t={t}
          modalVisible={this.props.modals.notFound}
          onOk={this._closeNotFoundModal}
          title={t("priceInquiry:productNotFound")}
          okText={'OK'}
        />
        <Layout.SuccessMessageModal
          t={t}
          modalVisible={this.state.scanLimitReachedModal}
          onOk={()=>this.setState({scanLimitReachedModal: false})}
          title={t("priceInquiry:barCodeInquiry.scanLimitReachedModal.title")}
          okText={'OK'}
        />
        <Layout.SuccessMessageModal
          t={t}
          modalVisible={this.state.modalMessage}
          onOk={()=>this.setState({modalMessage: false})}
          title={this.state.modalText}
          okText={'OK'}
        />
        <Layout.TouchableTitle
          onPress = {this.changeLayout}
          titleText = {t("common:store").toUpperCase() + " "  + (this._getStoreName().toUpperCase())}
          caretUp = { this.state.expanded }
        />

        <View style={{ flex: 1 }}>
          <Layout.StoreList
            expanded={this.state.expanded}
            expandedHeight={'100%'}
            hiddenHeight={0}
            storesArray={this.props.fetchedStoreList}
            component={Layout.StoreListItem}
            location={this.props.location}
            onPress={this._setStore}
            modalConnectionError={this.props.modals.connectionError}
            hasConnectionError={this.props.fetchStoreListError}
            openModal={this.props.openModal}
            closeModal={this.props.closeModal}
            t={t}
            language={i18n.language}
          />

          <View style={{ height: "100%" }}>

          <Layout.Title
            text={t("common:searchProductPlaceholder")}
          />

            <Layout.CustomSearchBar
              placeholder={t("common:searchProductPlaceholder")}
              onEndEditing={(e) => this._searchProduct(e.nativeEvent.text)}
            />

            <FlatList
              keyExtractor={this._keyExtractor}
              data={this.state.productList}
              renderItem={this._renderProductItem}
              ListFooterComponent={this._renderFooter}
              onEndReached = {()=> this.setState({endReached: true})}
            />
          </View>
        </View>
      </Layout.ViewContainer>
    );
  }
}


const mapStateToProps = (state) => ({
  modals: {
    activateOffer: state.offers.productsList.activateOffer,
    addToShoppingList: state.offers.productsList.addToShoppingList,
    addedToShoppingListSuccess: state.offers.productsList.addedToShoppingListSuccess,
    notFound: state.priceInquiry.modal.notFound,
    connectionError: state.priceInquiry.modal.connectionError,
  },
  storeListLoading: state.storeListFetch.loading,
  fetchedStoreList: state.storeListFetch.storeList,
  fetchStoreListError: state.storeListFetch.error,
  location: state.location.location,

  productList: state.products.searchProductByName.productList,
  productListLoading: state.products.searchProductByName.loading,
  error: state.products.searchProductByName.error,
  productFound: state.products.searchProductByName.productFound,

  productDetails: state.priceInquiry.fetchProductBySKU.product,
  productDetailsFound: state.priceInquiry.fetchProductBySKU.productFound,

  user: state.user,
  currentStoreObject: state.currentStore.object,
  tooManyRequestsError: state.products.searchProductByName.tooManyRequestsError,
});

const mapDispatchToProps = (dispatch) => {
  return {
    openModal: (modalKey) => {
      dispatch(Actions.setModalVisible(modalKey))
    },
    closeModal: (modalKey) => {
      dispatch(Actions.setModalInvisible(modalKey))
    },
    fetchStoreList: () => {
      dispatch(Actions.fetchStoreList()); // ORIGINAL_COLOMBIA: fetchStoreListByCountry
    },
    getLocation: (forceAsk) => {
      dispatch(Actions.getLocation(forceAsk));
    },
    fetchProductBySKU: (user, sku, storeNo) => {
      dispatch(Actions.fetchProductBySKU(user, sku, storeNo))
    },
    fetchProductByName: (user, searchKey, storeNo) => {
      dispatch(Actions.getProductsByName(user, searchKey, storeNo))
    },
    resetProductSearchByName: () => {
      dispatch(Actions.resetProductSearchByName())
    },
    setCurrentStore: (store) => {
      dispatch(Actions.setCurrentStore(store))
    },
    resetTooManyRequestsError: () => { dispatch({ type: 'RESET_TOO_MANY_REQUESTS_ERROR' }) }
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(ProductNameInquiryView_CO);
