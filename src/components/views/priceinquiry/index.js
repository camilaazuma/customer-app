export {default as InquiryTypeView} from  "./InquiryTypeView";
export {default as ProductNameInquiryView} from  "./ProductNameInquiryView";
export {default as ProductPriceDetailView} from  "./ProductPriceDetailView";
export {default as BarCodeInquiryView} from  "./BarCodeInquiryView";

export {default as ProductNameInquiryView_CO} from  "./ProductNameInquiryView_CO";
