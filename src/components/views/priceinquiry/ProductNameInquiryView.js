import React from "react";
import {
  View,
  Platform,
  LayoutAnimation,
  UIManager,
  ActivityIndicator, 
  FlatList,
  NetInfo
} from "react-native";
import { ListItem } from "react-native-elements";
import { COLORS } from "../../../constants/styles";
import APP_DIMENSIONS from "../../../constants/appDimensions";
import * as Layout from "../../layout";
import * as Actions from "../../../actions";
import { connect } from "react-redux";
import { translate } from 'react-i18next';
import AsyncStorageHelper from "../../../helpers/AsyncStorageHelper";
import { showSnackbar } from "../../../utils";

const USER_SCAN_COUNT_KEY = "USER_SCAN_COUNT";
const USER_SCAN_DATE_KEY = "USER_SCAN_DATE";
const USER_SCAN_LIMIT_CPF = 35;
const USER_SCAN_LIMIT_CNPJ = 70;

@translate(['priceInquiry', 'common'], { wait: true })
class ProductNameInquiryView extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      expanded: false,
      loading: false,
      successMessage: false,
      categories: [],
      products: [],
      endReached: false,
      productsOpened: false,
      productsToShow: [],
      baseForSearch: [],
      shopList: [],
      scanLimitReachedModal: false,
      modalText: "",
      modalMessage: false
    };
    if (Platform.OS === "android") {
      UIManager.setLayoutAnimationEnabledExperimental(true);
    }
  }

  componentDidMount() {
    if (this.props.categories.length===0)
      this.props.getCategories();
    if (this.props.products.length===0)
      this.props.getAllProducts();
    if (this.props.fetchedStoreList.length===0)
      this.props.fetchStoreList();
    if (this.props.location === null)
      this.props.getLocation();
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    var obj = {};
    // recebe os status dos produtos e inclui na lista de produtos
    if(prevState.products!== nextProps.products){
      obj.products = nextProps.products;
    }
    if(prevState.categories!== nextProps.categories){
      obj.categories = nextProps.categories;
    }

    return Object.keys(obj).length? obj : null;
  }

  async componentDidUpdate(prevProps) {
    if (this.props.productFound===true && prevProps.productFound===null) {
      this.props.navigation.navigate("ProductPriceDetail");
      await this._incrementScanCount();
    }
    else if (this.props.productFound===false && prevProps.productFound===null) {
      this.props.openModal("notFound");
    }
    else if (this.props.tooManyRequestsError===true && prevProps.tooManyRequestsError===false) {
      this.setState({scanLimitReachedModal: true});
      this.props.resetTooManyRequestsError();
    }

    if (prevProps.products.length!==this.props.products.length)
      this.setState({baseForSearch: this.props.products});

    if (prevProps.error===false && this.props.error===true) {
      let isConnected = await NetInfo.isConnected.fetch();
      if (!isConnected) {
        this.setState({ modalText: this.props.t("common:errors.connectionError"), modalMessage: true});
        showSnackbar(this.props.t("common:errors.noConnection"));        
      }
      else
        this.setState({ modalText: this.props.t("common:errors.serverError"), modalMessage: true});
    }
  }

  changeLayout = () => {
    LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
    if (this.state.expanded === false)
      this.setState({
        expanded: true
      });
    else
      this.setState({
        expanded: false
      });
  };

  _showProducts = (item) => {
    var filtered = this.state.products.filter((e)=>e["CATEGORY_ID"] == item);
    this.setState({productsOpened: true, productsToShow: filtered, baseForSearch: filtered});
  }

  _hideProducts = () => {
    this.setState({productsOpened: false, baseForSearch: this.state.products});
  }

  _getProductDetail =  (barcode) => async () => {
    if (await this._canScan()) {
      await this.props.fetchProductByBarcode(this.props.user, barcode, this.props.currentStoreObject.store_no)
    }
    else this.setState({scanLimitReachedModal: true})
  };

  _canScan = async () => {
    var count = await AsyncStorageHelper.getItem(USER_SCAN_COUNT_KEY); 
    if (count===null) count=0;
    var countDate = await AsyncStorageHelper.getItem(USER_SCAN_DATE_KEY);
    if (countDate === null) countDate =  (new Date(1,1,1,0,0,0,0)).valueOf();
    var currentDate = new Date().setHours(0, 0, 0, 0).valueOf(); 
    const scanLimit = this.props.user.personalIdentifier.length === 11 ? USER_SCAN_LIMIT_CPF : USER_SCAN_LIMIT_CNPJ;
    if (currentDate=== countDate && count < scanLimit) return true;
    else if (countDate < currentDate) {
      await AsyncStorageHelper.removeItem(USER_SCAN_COUNT_KEY);
      await AsyncStorageHelper.setItem(USER_SCAN_DATE_KEY, currentDate);
      return true
    }
    return false;
  }

  _incrementScanCount = async () => {
    var count = await AsyncStorageHelper.getItem(USER_SCAN_COUNT_KEY);
    if (count===null) count=0;
    await AsyncStorageHelper.setItem(USER_SCAN_COUNT_KEY, (count+1));
  }

  _keyExtractor = (item, index) => index.toString();

  _renderCategoryItem = ({ item }) => (
    <Layout.BorderedView>
      <Layout.TouchableContainer
        opacity
        onPress={() => this._showProducts(item.ID)}
      >
        <View
          style={{
            height: APP_DIMENSIONS.SCREEN_HEIGHT*0.1,
            justifyContent: 'center',
            alignItems: 'flex-start',
            padding: APP_DIMENSIONS.SCREEN_WIDTH*0.031,
          }}
        >
          <Layout.FontText style={{
            fontSize: APP_DIMENSIONS.SCREEN_WIDTH*0.045,
          }}>
            {item.DESCRIPTION}
          </Layout.FontText>
        </View>
      </Layout.TouchableContainer>
    </Layout.BorderedView>
  );
  _renderProductItem = ({ item }) => {
    return (
      <Layout.ProductNamePriceInquiryListItem
        code={item.ID}
        productName={item.DESCRIPTION}
        onPress={this._getProductDetail(item.ID)}
      />
    )
  };

  _renderFooter = () => {
    if (this.state.productsToShow.length===0 && this.state.productsOpened) {
      return (
        <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
          <Layout.FontText style={{ color: COLORS.BLACK.BLACK, fontSize: APP_DIMENSIONS.SCREEN_WIDTH*0.053, fontFamily: 'open-sans' }}>
            {this.props.t("shoppingLists:addProducts.productsNotFound")}
          </Layout.FontText>
        </View>
      )
    }

    return (
      <View >
        { this.state.endReached == false ?
          <ActivityIndicator size="large"/> : null
        }
      </View>
    )
  };

  _searchFilterFunction = text => {
    var newData = this.state.baseForSearch.filter(item => {
      const itemData = `${item["DESCRIPTION"].toUpperCase()}`;
      const textData = text.toUpperCase();
      return itemData.indexOf(textData) > -1;
    });

    var willReturnToList = text== "" ? true : false;
    this.setState({
      productsOpened: true,
      productsToShow: newData,
      endReached: !willReturnToList
    });
  };

  _closeNotFoundModal = () => setTimeout(() => {
    this.props.closeModal('notFound');
  }, 1)


  _onEndReached = () => this.setState({ endReached: true });

  _setStore = async (store) =>
  {
    if  (this.props.currentStoreObject===null || store.store_no !== this.props.currentStoreObject.store_no) {
      await this.props.setCurrentStore(store);
      this.changeLayout();
    }
  }

  _getStoreName = () => {
    const {currentStoreObject} = this.props;
    return (currentStoreObject!==null &&  typeof currentStoreObject.name!=='undefined' ?  this.props.currentStoreObject.name.toUpperCase() : "" )
  }

  render() {
    const { t } = this.props;
    if(!this.props.categoriesFetched){
      return(
        <Layout.ViewContainer>
          <Layout.Title
            text={t("shoppingLists:addProducts.category")}
          />
          <ActivityIndicator size="large" style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}/>
        </Layout.ViewContainer>
      )
    }
    return (
      <Layout.ViewContainer>
        <Layout.Loader loading={this.props.loading || this.props.currentStoreObject===null} />
        <Layout.SuccessMessageModal 
          t={t}
          modalVisible={this.props.modals.notFound}
          onOk={this._closeNotFoundModal}
          title={t("priceInquiry:productNotFound")}
          okText={'OK'}       
        />
        <Layout.SuccessMessageModal 
          t={t}
          modalVisible={this.state.scanLimitReachedModal}
          onOk={()=>this.setState({scanLimitReachedModal: false})}
          title={t("priceInquiry:barCodeInquiry.scanLimitReachedModal.title")}
          okText={'OK'}       
        />
        <Layout.SuccessMessageModal 
          t={t}
          modalVisible={this.state.modalMessage}
          onOk={()=>this.setState({modalMessage: false})}
          title={this.state.modalText}   
          okText={'OK'}       
        />
        <Layout.TouchableTitle   
          onPress = {this.changeLayout}
          titleText = {t("common:store").toUpperCase() + " "  + (this._getStoreName().toUpperCase())}
          caretUp = { this.state.expanded }
        /> 

        <View style={{ flex: 1 }}>
          <Layout.StoreList
            expanded={this.state.expanded}
            expandedHeight={'100%'}
            hiddenHeight={0}
            storesArray={this.props.fetchedStoreList}
            component={Layout.StoreListItem}
            location={this.props.location}
            onPress={this._setStore}
            modalConnectionError={this.props.modals.connectionError}
            hasConnectionError={this.props.fetchStoreListError}
            openModal={this.props.openModal}
            closeModal={this.props.closeModal}
            t={t}
          />

          <View style={{ height: "100%" }}>
          {this.state.productsOpened ?
          <Layout.TouchableTitle
            titleText={t("common:products").toUpperCase()}
            iconRight={false}
            caretRight={false}
            onPress={this._hideProducts}
          />
          :
          <Layout.Title
            text={t("shoppingLists:addProducts.category")}
          />
        }

            <Layout.CustomSearchBar
              placeholder={t("common:searchProductPlaceholder")}
              onChangeText={text => this._searchFilterFunction(text)}
            />
            <View style={{ flex: 1 }}>
              {this.state.productsOpened ?
                <FlatList
                  keyExtractor={this._keyExtractor}
                  data={this.state.productsToShow}
                  renderItem={this._renderProductItem}
                  ListFooterComponent={this._renderFooter}
                  onEndReached={this._onEndReached}
                />
                :
                <FlatList
                  keyExtractor={this._keyExtractor}
                  data={this.state.categories}
                  renderItem={this._renderCategoryItem}
                  ListFooterComponent={this._renderFooter}
                  onEndReached={this._onEndReached}
                />
              }
            </View>
          </View>
        </View>
      </Layout.ViewContainer>
    );
  }
}


const mapStateToProps = (state) => ({
  modals: {
    activateOffer: state.offers.productsList.activateOffer,
    addToShoppingList: state.offers.productsList.addToShoppingList,
    addedToShoppingListSuccess: state.offers.productsList.addedToShoppingListSuccess,
    notFound: state.priceInquiry.modal.notFound,
    connectionError: state.priceInquiry.modal.connectionError,
  },
  storeListLoading: state.storeListFetch.loading,
  fetchedStoreList: state.storeListFetch.storeList,
  fetchStoreListError: state.storeListFetch.error,
  location: state.location.location,
  categories: state.products.fetchCategories.categoryList,
  categoriesFetched: state.products.fetchCategories.categoriesFetched,
  products: state.products.fetchProduct.allProductsList,
  product: state.priceInquiry.readBarcode.product,
  loading: state.priceInquiry.readBarcode.loading,
  error: state.priceInquiry.readBarcode.error,
  productFound: state.priceInquiry.readBarcode.productFound,
  user: state.user,
  currentStoreObject: state.currentStore.object,
  tooManyRequestsError: state.priceInquiry.readBarcode.tooManyRequestsError,
});

const mapDispatchToProps = (dispatch) => {
  return {
    openModal: (modalKey) => {
      dispatch(Actions.setModalVisible(modalKey))
    },
    closeModal: (modalKey) => {
      dispatch(Actions.setModalInvisible(modalKey))
    },
    fetchStoreList: () => {
      dispatch(Actions.fetchStoreList());
    },
    getCategories: () => {
      dispatch(Actions.getCategories());
    },
    getAllProducts: () => {
      dispatch(Actions.getAllProducts());
    },
    getLocation: (forceAsk) => {
      dispatch(Actions.getLocation(forceAsk));
    },
    fetchProductByBarcode: (user, barcode, storeNo) => {
      dispatch(Actions.fetchProductByBarcode(user, barcode, storeNo))
    },
    setCurrentStore: (store) => {
      dispatch(Actions.setCurrentStore(store))
    },
    resetTooManyRequestsError: () => { dispatch({ type: 'RESET_TOO_MANY_REQUESTS_ERROR' }) }
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(ProductNameInquiryView);