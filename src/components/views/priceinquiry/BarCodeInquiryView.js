import React from "react";
import {
  View,
  Platform,
  UIManager,
  LayoutAnimation,
  TouchableOpacity,
  NetInfo
} from "react-native";
import { COLORS } from "../../../constants/styles";
import APP_DIMENSIONS from "../../../constants/appDimensions";
import * as Layout from "../../layout";
import * as Actions from "../../../actions";
import { connect } from "react-redux";
import { Camera, Permissions, Audio } from 'expo';
import { translate } from 'react-i18next';
import { withNavigationFocus } from 'react-navigation';
import { Icon } from "react-native-elements";
import firebase from 'react-native-firebase';
import AsyncStorageHelper from "../../../helpers/AsyncStorageHelper";
import { showSnackbar } from "../../../utils";
import { language } from "../../../config/i18n";

const USER_SCAN_COUNT_KEY = "USER_SCAN_COUNT";
const USER_SCAN_DATE_KEY = "USER_SCAN_DATE";
const USER_SCAN_LIMIT_CPF = 35;
const USER_SCAN_LIMIT_CNPJ = 70;

@translate(['priceInquiry', 'common'], { wait: true })
class BarCodeInquiryView extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      expanded: false,
      hasCameraPermission: null,
      type: Camera.Constants.Type.back,
      manualInquiryInput: "",
      error: false,
      flashMode: Camera.Constants.FlashMode.off,
      beepEnabled: true,
      scanLimitReachedModal: false,
      sameProduct: false,
      modalText: "",
      modalMessage: false
    };
    if (Platform.OS === "android") {
      UIManager.setLayoutAnimationEnabledExperimental(true);
    }
  }

  async componentDidMount(){
    // turn off flash on blur
    const onWillBlurSubscription  = this.props.navigation.addListener(
      "willBlur",
      payload => {
        this.setState({
          flashMode: Camera.Constants.FlashMode.off
        });
      }
    );

    this.props.openModal('infoModal');
    this.props.fetchStoreList();

    if (this.props.location===null)
      this.props.getLocation();

    const { status } = await Permissions.askAsync(Permissions.CAMERA);
    this.setState({ hasCameraPermission: status === 'granted' });
    await firebase.analytics().logEvent('barcode_scanner_opened', {
      personalIdentifier: this.props.user.personalIdentifier
    });
  }

  async componentDidUpdate(prevProps) {
    if (this.props.productFound===true && prevProps.productFound===null) {
      if ( !this.state.sameProduct) {
        await this._incrementScanCount();
      }
      this.props.navigation.navigate("ProductPriceDetail", {fromBarcode: true});
      await firebase.analytics().logEvent('barcode_get_product_success', {
        barcode: this.props.product.barcode
      });
    }
    else if (this.props.productFound===false && prevProps.productFound===null) {
      this.props.openModal("notFound");
      await firebase.analytics().logEvent('barcode_get_product_error', {
        barcode: this.props.currentCode
      });
    }
    else if (this.props.tooManyRequestsError===true && prevProps.tooManyRequestsError===false) {
      this.setState({scanLimitReachedModal: true});
      this.props.resetTooManyRequestsError();
    }

    if (prevProps.error===false && this.props.error===true) {
      let isConnected = await NetInfo.isConnected.fetch();
      if (!isConnected) {
        this.setState({ modalText: this.props.t("common:errors.connectionError"), modalMessage: true});
        showSnackbar(this.props.t("common:errors.noConnection"));
      }
      else
        this.setState({ modalText: this.props.t("common:errors.serverError"), modalMessage: true});
    }
  }

  static getDerivedStateFromProps(nextProps, prevState){
    if(!prevState.error && nextProps.connectionError) {
      nextProps.openModal("connectionError");
      return {
        error: true
      }
    }
    return null;
  }

  changeLayout = () => {
    LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
    if (this.state.expanded === false)
      this.setState({
        expanded: true
      });
    else
      this.setState({
        expanded: false
      });
  };

  _canScan = async () => {
    var count = await AsyncStorageHelper.getItem(USER_SCAN_COUNT_KEY);
    if (count===null) count=0;
    var countDate = await AsyncStorageHelper.getItem(USER_SCAN_DATE_KEY);
    if (countDate === null) countDate =  (new Date(1,1,1,0,0,0,0)).valueOf();
    var currentDate = new Date().setHours(0, 0, 0, 0).valueOf();

    const scanLimit = this.props.user.personalIdentifier.length === 11 ? USER_SCAN_LIMIT_CPF : USER_SCAN_LIMIT_CNPJ;
    if (currentDate=== countDate && count < scanLimit) return true;
    else if (countDate < currentDate) {
      await AsyncStorageHelper.removeItem(USER_SCAN_COUNT_KEY);
      await AsyncStorageHelper.setItem(USER_SCAN_DATE_KEY, currentDate);
      return true
    }
    return false;
  }

  _incrementScanCount = async () => {
    var count = await AsyncStorageHelper.getItem(USER_SCAN_COUNT_KEY);
    if (count===null) count=0;
    await AsyncStorageHelper.setItem(USER_SCAN_COUNT_KEY, (count+1));
  }

  _onBarCodeScanned = async ({ type, data }) => {

    if (this.state.beepEnabled)
      await Audio.Sound.createAsync(require("../../../../assets/beep.wav"), { shouldPlay: true } )
    if (!this.props.loading) {
      if (this.props.product!==null && this.props.product.barcode === data) {
        this.setState({sameProduct: true})
        this.props.setProductFoundFlag(null);
        this.props.setProductFoundFlag(true);
      }
      else if (this.props.productFound===false && data === this.props.currentCode) {
        this.props.setProductFoundFlag(null);
        this.props.setProductFoundFlag(false);
      }
      else {
        if (await this._canScan()) {
          this.setState({sameProduct: false})
          await this.props.fetchProductByBarcode(this.props.user, data, this.props.currentStoreObject.store_no);
        }
        else this.setState({scanLimitReachedModal: true});
      }
    }
    await firebase.analytics().logEvent('barcode_scanned', {
      barcode: data
    });

  }

  _getManualScan = async () =>  {
    this._closeManualInquiryModal();
    await this.props.fetchProductByBarcode(this.props.user, this.state.manualInquiryInput, this.props.currentStoreObject.store_no);
  }
  _closeNotFoundModal = () => setTimeout(() => {
    this.props.closeModal('notFound');
  }, 1)

  _closeManualInquiryModal = () => setTimeout(() => {
    this.props.closeModal('manualInquiry');
  }, 1)

  _openManualInquiryModal = () => setTimeout(() => {
    this.props.openModal('manualInquiry');
  }, 1)

  _onManualInputChange = (text) => {
    this.setState({manualInquiryInput: text})
  }

  _setStore = async (store) =>
  {
    if  (this.props.currentStoreObject===null || store.store_no !== this.props.currentStoreObject.store_no || store.idlocation !== this.props.currentStoreObject.idlocation) {
      await this.props.setCurrentStore(store);
      this.changeLayout();
    }
  }

  _getStoreName = () => {
    const {currentStoreObject} = this.props;
    if (currentStoreObject!==null && typeof currentStoreObject!=='undefined') {
      if('name' in currentStoreObject){
        return this.props.currentStoreObject.name.toUpperCase();
      }
      if('locationname' in currentStoreObject){
        return this.props.currentStoreObject.locationname.toUpperCase();
      }
    }
    return "";
  }

  _toggleFlash = () => {
    this.setState({
      flashMode: this.state.flashMode === Camera.Constants.FlashMode.off ? Camera.Constants.FlashMode.torch : Camera.Constants.FlashMode.off
    })
  }

  _toggleBeep = () => {
    this.setState({
      beepEnabled: this.state.beepEnabled ? false : true
    })
  }

  render() {
    const { loading, successMessage, t , i18n} = this.props;
    return (
      <Layout.ViewContainer>
        <Layout.Loader loading={loading || this.props.currentStoreObject===null} />
        <Layout.SuccessMessageModal
          t={t}
          modalVisible={this.props.modals.notFound}
          onOk={this._closeNotFoundModal}
          title={t("priceInquiry:productNotFound")}
          okText={'OK'}
        />
        <Layout.SuccessMessageModal
          t={t}
          modalVisible={this.props.modals.connectionError}
          onOk={() => this.props.closeModal('connectionError')}
          title={t('common:connectionErrorTitle')}
          contentText={t("common:connectionErrorMessage")}
          okText={'OK'}
        />
        <Layout.SuccessMessageModal
          t={t}
          modalVisible={this.state.scanLimitReachedModal}
          onOk={()=>this.setState({scanLimitReachedModal: false})}
          title={t("priceInquiry:barCodeInquiry.scanLimitReachedModal.title")}
          okText={'OK'}
        />
        <Layout.SuccessMessageModal
          t={t}
          modalVisible={this.state.modalMessage}
          onOk={()=>this.setState({modalMessage: false})}
          title={this.state.modalText}
          okText={'OK'}
        />
        <Layout.ManualInquiryModal
          t={t}
          modalVisible={this.props.modals.manualInquiry}
          onOk={this._getManualScan}
          onCancel={this._closeManualInquiryModal}
          inputValue = {this.state.manualInquiryInput}
          onChangeText = {this._onManualInputChange}
        />
        <Layout.TouchableTitle
          onPress = {this.changeLayout}
          titleText = {t("common:store").toUpperCase() + " " + this._getStoreName()}
          caretUp = { this.state.expanded }
        />
        <View style={{ flex: 1 }}>
          <Layout.StoreList
            expanded={this.state.expanded}
            expandedHeight={'100%'}
            hiddenHeight={0}
            storesArray={this.props.fetchedStoreList}
            component={Layout.StoreListItem}
            location = { this.props.location }
            onPress = {this._setStore}
            modalConnectionError={this.props.modals.connectionError}
            hasConnectionError={this.props.fetchStoreListError}
            openModal={this.props.openModal}
            closeModal={this.props.closeModal}
            t={t}
            language={i18n.language}
          />
          { this.state.hasCameraPermission ?
          <View style={{height: "100%"}}>
            <View style={{ flex: 1, backgroundColor: COLORS.WHITE.WHITE }}>
              <Camera style={{ flex: 1, }} type={this.state.type} onBarCodeScanned={
                (this.props.isFocused && !loading && !this.state.scanLimitReachedModal &&
                  !this.props.modals.connectionError && !this.props.modals.manualInquiry &&
                  !this.props.modals.notFound) ? this._onBarCodeScanned : undefined
              }
              flashMode={this.state.flashMode}>
                <View
                  style={{
                    flex: 1,
                    flexDirection: 'column',
                  }}
                >
                  <View style={{ flex: 0.2, backgroundColor: 'rgba(0,0,0,0.6)',}}/>

                  <View style={{ flex: 0.6, flexDirection: 'row' }}>
                    <View style={{ flex: 0.1, backgroundColor: 'rgba(0,0,0,0.6)', }}/>

                    <View style={{ flex: 0.8, backgroundColor: 'transparent', alignItems: "center", justifyContent: "center" }}>
                      <View style={{ position: "absolute", top: 0, left: 0, width: 30,  height: 30, borderColor: "white", borderLeftWidth: 2, borderTopWidth: 2 }}/>
                      <View style={{ position: "absolute", top: 0, right: 0, width: 30, height: 30, borderColor: "white", borderRightWidth: 2, borderTopWidth: 2  }}/>
                      <View style={{ position: "absolute", bottom: 0, left: 0, width: 30, height: 30, borderColor: "white", borderLeftWidth: 2, borderBottomWidth: 2   }}/>
                      <View style={{ position: "absolute", bottom: 0, right: 0, width: 30, height: 30, borderColor: "white", borderRightWidth: 2, borderBottomWidth: 2  }}/>
                    </View>
                    <View style={{ flex: 0.1, backgroundColor: 'rgba(0,0,0,0.6)' }}/>
                  </View>

                  <View style={{ flex: 0.2, flexDirection: 'row', backgroundColor: 'rgba(0,0,0,0.6)', borderTopColor: 'transparent', marginTop: -0.2 }}>
                    <TouchableOpacity
                      style={{
                        flex: 1,
                        alignSelf: 'flex-end',
                        alignItems: 'center',
                      }}
                      onPress={() => {
                        this.setState({
                          type: this.state.type === Camera.Constants.Type.back
                            ? Camera.Constants.Type.front
                            : Camera.Constants.Type.back,
                        });
                      }}>
                      <Layout.FontText
                        style={{ fontSize: APP_DIMENSIONS.SCREEN_WIDTH*0.043, marginBottom: 10, color: 'white', textAlign: 'center', fontFamily: 'open-sans', }}>
                        {' '}Flip{' '}
                      </Layout.FontText>
                    </TouchableOpacity>
                    <TouchableOpacity
                      style={{
                        flex: 1,
                        alignSelf: 'flex-end',
                        alignItems: 'center',
                      }}
                      onPress={this._openManualInquiryModal}
                      >
                      <Icon
                        color={'white'}
                        name='search'
                        type='font-awesome'
                        containerStyle={{
                          padding: 10
                        }}
                      />
                    </TouchableOpacity>
                    <TouchableOpacity
                      style={{
                        flex: 1,
                        alignSelf: 'flex-end',
                        alignItems: 'center',
                      }}
                      onPress={this._toggleFlash}
                      >
                      <Icon
                        color={'white'}
                        name='flash'
                        type='font-awesome'
                        containerStyle={{
                          padding: 10
                        }}
                      />
                    </TouchableOpacity>

                    <TouchableOpacity
                      style={{
                        flex: 1,
                        alignSelf: 'flex-end',
                        alignItems: 'center',
                      }}
                      onPress={this._toggleBeep}
                      >
                      <Icon
                        color={'white'}
                        name={this.state.beepEnabled ? 'volume-high' : 'volume-off'}
                        type='material-community'
                        containerStyle={{
                          padding: 10
                        }}
                      />
                    </TouchableOpacity>
                  </View>
                </View>
              </Camera>
            </View>
            <Layout.BottomTabContainer style={{height: APP_DIMENSIONS.SCREEN_HEIGHT*0.068, }}>
              <View style={{ justifyContent: 'center', alignItems:'center', flex: 1}}>
                <Layout.FontText style={{
                  textAlign: 'center',
                  fontFamily: 'open-sans',
                  fontSize: APP_DIMENSIONS.SCREEN_WIDTH*0.0375
                }}> {t("priceInquiry:barCodeInquiry.instructionsText")}</Layout.FontText>
              </View>
            </Layout.BottomTabContainer>
          </View>
          :
          <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
            <Layout.FontText style={{ color: COLORS.BLACK.BLACK, fontSize: APP_DIMENSIONS.SCREEN_WIDTH*0.053, fontFamily: 'open-sans' }}>
              {t("priceInquiry:barCodeInquiry.noCameraPermissionText")}
            </Layout.FontText>
          </View>
          }
        </View>
      </Layout.ViewContainer>
    );
  }
}

const mapStateToProps = state => ({
  modals: {
    notFound: state.priceInquiry.modal.notFound,
    manualInquiry: state.priceInquiry.modal.manualInquiry,
    connectionError: state.priceInquiry.modal.connectionError,
  },
  storeListLoading: state.storeListFetch.loading,
  fetchedStoreList: state.storeListFetch.storeList,
  fetchStoreListError: state.storeListFetch.error,
  location: state.location.location,
  product: state.priceInquiry.readBarcode.product,
  loading: state.priceInquiry.readBarcode.loading,
  fetching: state.priceInquiry.readBarcode.fetching,
  error: state.priceInquiry.readBarcode.error,
  currentCode: state.priceInquiry.readBarcode.currentCode,
  productFound: state.priceInquiry.readBarcode.productFound,
  tooManyRequestsError: state.priceInquiry.readBarcode.tooManyRequestsError,
  user: state.user,
  currentStoreObject: state.currentStore.object,
});

const mapDispatchToProps = dispatch => {
  return {
    openModal: modalKey => {
      dispatch(Actions.setModalVisible(modalKey));
    },
    closeModal: modalKey => {
      dispatch(Actions.setModalInvisible(modalKey));
    },
    fetchStoreList: () => {
        dispatch(Actions.fetchStoreList()); // ORIGINAL_COLOMBIA: fetchStoreListByCountry
    },
    fetchProductByBarcode: (user, barcode, storeNo) => {
      dispatch(Actions.fetchProductByBarcode(user, barcode, storeNo))
    },
    fetchProductBySKU: (sku, storeNo) => {
      dispatch(Actions.fetchProductBySKU(sku, storeNo))
    },
    setBarcode: (barcode) => {
      dispatch(Actions.setBarcode(barcode))
    },
    setProductFoundFlag: (value) => {
      dispatch(Actions.setProductFoundFlag(value));
    },
    openModal: modalKey => {
      dispatch(Actions.setModalVisible(modalKey));
    },
    closeModal: modalKey => {
      dispatch(Actions.setModalInvisible(modalKey));
    },
    getLocation: (forceAsk) => {
      dispatch(Actions.getLocation(forceAsk));
    },
    setCurrentStore: (store) => {
      dispatch(Actions.setCurrentStore(store))
    },
    resetTooManyRequestsError: () => { dispatch({ type: 'RESET_TOO_MANY_REQUESTS_ERROR' }) }
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withNavigationFocus(BarCodeInquiryView));

// export default BarCodeInquiryView;
