import React from "react";
import {
  View,
  Platform,
  UIManager,
  LayoutAnimation,
  StyleSheet,
  Alert
} from "react-native";
import { COLORS } from "../../../constants/styles";
import APP_DIMENSIONS from "../../../constants/appDimensions";
import * as Layout from "../../layout";
import * as Actions from "../../../actions";
import { connect } from "react-redux";
import { translate } from 'react-i18next';
import { nullFunction } from "../../../utils";
import { language } from "../../../config/i18n";

const SCREEN_HEIGHT = APP_DIMENSIONS.SCREEN_HEIGHT;
const SCREEN_WIDTH = APP_DIMENSIONS.SCREEN_WIDTH;

@translate(['priceInquiry', 'common'], { wait: true })
class InquiryTypeView extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      expanded: false,
      selectStoreModalVisible: false,
      modalSelectedStore: null,
    };
    if (Platform.OS === "android") {
      UIManager.setLayoutAnimationEnabledExperimental(true);
    }
  }

  componentDidMount() {
    this.props.fetchStoreList()
    if (this.props.location===null)
      this.props.getLocation(); 

    if (this.props.currentStoreObject === null) 
      this.setState({ selectStoreModalVisible: true });
  }

  async componentDidUpdate(prevProps, prevState) {
    if (this.props.currentStoreObject && !prevProps.currentStoreObject) {
      this.setState({selectStoreModalVisible: false});
    }
  }

  changeLayout = () => {
    LayoutAnimation.configureNext({
      duration: 200,
      update: {
        type: LayoutAnimation.Types.linear,
      },
    });
    if (this.state.expanded === false)
      this.setState({
        expanded: true
      });
    else
      this.setState({
        expanded: false
      });
  };

  _setStore = async (store) =>
  {
    if  (this.props.currentStoreObject===null || store.store_no !== this.props.currentStoreObject.store_no || store.idlocation !== this.props.currentStoreObject.idlocation) {
      await this.props.setCurrentStore(store);
      this.changeLayout();
    }
  }

  _getStoreName = () => {
    const {currentStoreObject} = this.props;
    if (currentStoreObject!==null && typeof currentStoreObject!=='undefined') {
      if('name' in currentStoreObject){
        return this.props.currentStoreObject.name.toUpperCase();
      }
      if('locationname' in currentStoreObject){
        return this.props.currentStoreObject.locationname.toUpperCase();
      }
    }
    return "";
  }

  _selectStoreFromModal = async () => {
    let {t} = this.props;
    if (this.state.modalSelectedStore===null) {
      Alert.alert(
        t("common:errors.error"),
        t("common:errors.storeSelect"),
        [
          {text: 'OK'},
        ],
        { cancelable: true }
      );
    }
    else {
      await this.props.setCurrentStore(this.state.modalSelectedStore);
      this.setState({ selectStoreModalVisible: false });
    }
  }

  render() {
    const { loading, successMessage, t , i18n} = this.props;
    return (
      <Layout.ViewContainer>
        {/* <Layout.Loader loading={loading} /> */}
        <Layout.SelectStoreModal
          t={t}
          language={i18n.language}
          modalVisible={this.state.selectStoreModalVisible}
          closeModal={()=>{this.setState({selectStoreModalVisible: false})}}
          onOk={  this._selectStoreFromModal   }
          lists = {this.props.fetchedStoreList}
          onValueChange={(itemValue, itemIndex) => { this.setState({modalSelectedStore: itemValue}) }}
          selectedValue={this.state.modalSelectedStore}
          storeListLoading={this.props.storeListLoading}
          onPressOutside={()=>{this.props.currentStoreObject!==null ? this.setState({selectStoreModalVisible: false}) : null }}
        />
        <Layout.TouchableTitle   
          onPress = {this.changeLayout}
          titleText = {t("common:store").toUpperCase() + " " + (this._getStoreName().toUpperCase())}
          caretUp = { this.state.expanded }
        />
        <View style={{ flex: 1, marginTop: StyleSheet.hairlineWidth, }}>
          <Layout.StoreList
            loading = {this.props.storeListLoading}
            expandedHeight={'100%'}
            hiddenHeight={0}
            expanded={this.state.expanded}
            storesArray={this.props.fetchedStoreList}
            component={Layout.StoreListItem}
            location = {this.props.location}
            onPress = {this._setStore}
            modalConnectionError={this.props.modals.connectionError}
            hasConnectionError={this.props.fetchStoreListError}
            openModal={this.props.openModal}
            closeModal={this.props.closeModal}
            t = {t}
            language={i18n.language}
          />
          <View style={{height: "100%"}}>
          { i18n.language !== "es-CO" && i18n.language !== "es-PE" &&
            <Layout.TouchableContainer onPress={()=>setTimeout(()=>this.props.navigation.navigate('ProductNameInquiry'),1)}>
              <Layout.ContentContainer
                style={{ backgroundColor: COLORS.RED.FIRE_ENGINE_RED }}
              >
                <Layout.BigIcon name="tags" color={COLORS.WHITE.WHITE} />
                <Layout.IconTitleText>
                  {t("priceInquiry:inquiryType.nameInquiryButton")}
                </Layout.IconTitleText>
              </Layout.ContentContainer>
            </Layout.TouchableContainer>
          }
            <Layout.TouchableContainer
              onPress={()=>setTimeout(()=>this.props.navigation.navigate('BarCodeInquiry'),1)}
            >
              <Layout.ContentContainer
                style={{ backgroundColor: COLORS.RED.FIRE_ENGINE_RED, borderTopColor: COLORS.WHITE.WHITE, borderTopWidth: StyleSheet.hairlineWidth, }}
              >
                <Layout.BigIcon name="barcode" color={COLORS.WHITE.WHITE} />
                <Layout.IconTitleText>
                {t("priceInquiry:inquiryType.barCodeInquiryButton")}
                </Layout.IconTitleText>
              </Layout.ContentContainer>
            </Layout.TouchableContainer>
          </View>
        </View>
      </Layout.ViewContainer>
    );
  }
}

const mapStateToProps = state => ({
  modals: {
    createList: state.shoppingLists.createListModal.createList,
    connectionError: state.shoppingLists.createListModal.connectionError,
  },
  storeListLoading: state.storeListFetch.loading,
  fetchedStoreList: state.storeListFetch.storeList,
  fetchStoreListError: state.storeListFetch.error,
  location: state.location.location,
  currentStoreObject: state.currentStore.object,
  isSorted: state.storeListFetch.isSorted,
});

const mapDispatchToProps = dispatch => {
  return {
    openModal: modalKey => {
      dispatch(Actions.setModalVisible(modalKey));
    },
    closeModal: modalKey => {
      dispatch(Actions.setModalInvisible(modalKey));
    },
    fetchStoreList: () => {
      dispatch(Actions.fetchStoreList()); // ORIGINAL_COLOMBIA: fetchStoreListByCountry
    },
    getLocation: (forceAsk) => {
      dispatch(Actions.getLocation(forceAsk));
    },
    setCurrentStore: (store) => {
      dispatch(Actions.setCurrentStore(store))
    },
    fetchProductByBarcode: (user, barcode, storeNo) => {
      dispatch(Actions.fetchProductByBarcode(user, barcode, storeNo))
    },
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(InquiryTypeView);

// export default InquiryTypeView;
