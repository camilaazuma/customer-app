import React from "react";
import {
  View,
  Text,
  ScrollView,
  Keyboard,
  Alert,
  Platform,
  UIManager,
  LayoutAnimation
} from "react-native";
import { Icon } from "react-native-elements";
import { COLORS } from "../../../constants/styles";
import APP_DIMENSIONS from "../../../constants/appDimensions";
import styled from "styled-components/native";
import * as Layout from "../../layout";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scrollview";
import * as Actions from "../../../actions";
import { connect } from "react-redux";
import { stores } from "../../../config/mockdata/storesList";
import { translate } from 'react-i18next';
import { nullFunction } from "../../../utils";
import cloneDeep from 'lodash/cloneDeep';
import { language } from "../../../config/i18n";

const SCREEN_HEIGHT = APP_DIMENSIONS.SCREEN_HEIGHT;
const SCREEN_WIDTH = APP_DIMENSIONS.SCREEN_WIDTH;

const params = {
  updated: "2018-11-01 19:22:02",
}

@translate(['priceInquiry', 'common'], { wait: true })
class ProductPriceDetailView extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      storeListStyle: { height: 0 },
      expanded: false,
      selectedList: null,
      productQuantity: "",
      productAdded: false,
      fetchError: false,
      language: props.i18n.language,
      product: {}
    };
    if (Platform.OS === "android") {
      UIManager.setLayoutAnimationEnabledExperimental(true);
    }
  }

  componentDidMount() {
    this.props.fetchStoreList();
    if (this.props.location===null)
      this.props.getLocation();
    if (this.props.fetchedShopListList.length===0) {
      this.props.getShopList({user: this.props.user, updated: params.updated});
    }
    this.setState({fetchError: false});
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    var obj = {};
    if(prevState.language === "es-CO" && !nextProps.navigation.getParam("fromBarcode", false)){
      if(prevState.productBySKU!== nextProps.productBySKU ){
        obj.product = nextProps.productBySKU;
      }
    }else{
      if(prevState.productByBarcode!== nextProps.productByBarcode ){
        obj.product = nextProps.productByBarcode;
      }
    }

    if(!prevState.fetchError && nextProps.fetchListError) {
      nextProps.openModal("connectionError");
        obj.fetchError = true;
    }
    return Object.keys(obj).length? obj : null;
  }

  changeLayout = () => {
    LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
    if (this.state.expanded === false)
      this.setState({
        storeListStyle: { height: "100%"},
        expanded: true
      });
    else
      this.setState({
        storeListStyle: { height: 0},
        expanded: false
      });
  };

  _goBack = () => {this.props.navigation.goBack()}

  _selectList = async () => {
    let {t} = this.props;
    if (this.state.selectedList===null) {
      Alert.alert(
        t("common:locationPermissionDeniedErrorTitle"),
        t("priceInquiry:productPriceDetail.noListSelectedError"),
        [
          {text: 'OK'},
        ],
        { cancelable: true }
      );
    }
    else {
      this.props.closeModal('addToShoppingList');
      this.props.openModal('selectQuantity');
    }
  }

  _addProduct = async () =>{
    this.setState({productAdded: false, productQuantity: ""});
    const list = cloneDeep(this.state.selectedList);
    var { productQuantity } = this.state;
    let {t} = this.props;
    if(productQuantity === "0" || productQuantity==="" || productQuantity===null || typeof productQuantity==='undefined'){
      return Alert.alert(
        t("common:locationPermissionDeniedErrorTitle"),
        t("priceInquiry:productPriceDetail.invalidInputError"),
        [
          {text: 'OK'},
        ],
        { cancelable: true }
      );
    }
    else
    if (list.shop_list_products!==undefined) {
      const index = list.shop_list_products.findIndex(x => x.art_no === this.state.product.art_no.toString());
      if(index !== -1){
        list.shop_list_products[index].quantity = productQuantity;
        list.shop_list_products[index].deleted = "0";
      }else{
        if (list.shop_list_products===undefined)
          list.shop_list_products = [];
        list.shop_list_products.push({
          "art_no": this.state.product.art_no.toString(),
          "deleted": "0",
          "description": this.state.product.descr.toString(),
          "quantity": productQuantity
        });
      }
    }
    await this.props.saveShopList({user: this.props.user, updated: params.updated, shopList: list});
    this.setState({selectedList: list});
    if (this.props.allListsFetched===true){
      this.setState({productAdded: true})
    }
  }


  componentDidUpdate(prevProps, prevState) {
    let {t} = this.props;
    if (prevState.productAdded===false && this.state.productAdded===true) {
      Keyboard.dismiss();
      Alert.alert(
        t("common:success"),
        t("priceInquiry:productPriceDetail.addedProductSuccess"),
        [
          {text: 'OK', onPress: () => this.props.closeModal('selectQuantity')},
        ],
        { cancelable: true }
      );
    }
  }

  _setStore = async (store) =>
  {
    if  (this.props.currentStoreObject===null || store.store_no !== this.props.currentStoreObject.store_no || store.idlocation !== this.props.currentStoreObject.idlocation) {
      await this.props.setCurrentStore(store);
      this.changeLayout();
    }
  }

  _getStoreName = () => {
    const {currentStoreObject} = this.props;
    if (currentStoreObject!==null && typeof currentStoreObject!=='undefined') {
      if('name' in currentStoreObject){
        return this.props.currentStoreObject.name.toUpperCase();
      }
      if('locationname' in currentStoreObject){
        return this.props.currentStoreObject.locationname.toUpperCase();
      }
    }
    return "";
  }

  _getProductDetail =  (barcode) => async () => {await this.props.fetchProductByBarcode(this.props.user, barcode, this.props.currentStoreObject.store_no)};

  _onPlusButtonPress = () => {
    if (this.state.productQuantity === "")
      this.setState({productQuantity: "1"})
    else
      this.setState({productQuantity: (parseInt(this.state.productQuantity)+1).toString()})
  }

  _onMinusButtonPress = () => {
    if (this.state.productQuantity !== "" && parseInt(this.state.productQuantity)>0)
      this.setState({productQuantity: (parseInt(this.state.productQuantity)-1).toString()})
  }

  _onChangeProductQuantity = async (value) => {
    if (/^[0-9]*$/.test(value)) {
      await this.setState({productQuantity: value});
    }
  }

  _renderPriceLabel = () => {
    if (this.state.language === "es-CO") {
      if(this.state.product !== null){
        return(
          <Layout.PriceLabel_CO
            product = {this.state.product}
            t = {this.props.t}
          />
        );
      }else{
        return(<Layout.ViewLoader loading={true} />);
      }
    }else{
      if(this.state.product !== null){
        return(
          <Layout.PriceLabel
            product = {this.state.product}
            t = {this.props.t}
          />
        );
      }else{
        return(<Layout.ViewLoader loading={true} />);
      }
    }
  }

  render() {
    const { loading, loadingList, successMessage, t, i18n } = this.props;
    return (
      <Layout.ViewContainer>
        <Layout.Loader loading={ this.props.currentStoreObject === null} />
        <Layout.AddToShoppingListModal
          t={t}
          modalVisible={this.props.modals.addToShoppingList}
          closeModal={this.props.closeModal}
          onOk={  this._selectList   }
          onCancel={() => setTimeout(() => {
              this.props.closeModal('addToShoppingList');
            }, 1)
          }
          lists = {this.props.fetchedShopListList}
          onValueChange={(itemValue, itemIndex) => { this.setState({selectedList: itemValue}) }}
          selectedValue={this.state.selectedList}
        />

        <Layout.SelectProductQuantityModal
          t={t}
          modalVisible={this.props.modals.selectQuantity}
          closeModal={this.props.closeModal}
          onOk={this._addProduct}
          onCancel={() => setTimeout(() => {
              this.props.closeModal('selectQuantity');
            }, 1)
          }
          value={this.state.productQuantity}
          onChangeText = {this._onChangeProductQuantity}
          onPlusButtonPress={this._onPlusButtonPress}
          onMinusButtonPress={this._onMinusButtonPress}
        />

        <Layout.SuccessMessageModal
          t={t}
          modalVisible={this.props.modals.addedToShoppingListSuccess}
          onOk={() => this.props.closeModal('addedToShoppingListSuccess')}
          title={t('priceInquiry:productPriceDetail.productAddedMessage')}
          okText={'OK'}
        />
        <Layout.SuccessMessageModal
          t={t}
          modalVisible={this.props.modals.connectionError}
          onOk={() => this.props.closeModal('connectionError')}
          title={t('common:connectionErrorTitle')}
          contentText={t("common:connectionErrorMessage")}
          okText={'OK'}
        />

        <Layout.Title
          text = {t("common:store").toUpperCase() + " "  + this._getStoreName()}
        />

        <View style={{ flex: 1 }}>
          <Layout.StoreList
            expanded={this.state.expanded}
            expandedHeight={'100%'}
            hiddenHeight={0}
            hideSearchBar={true}
            storesArray={this.props.fetchedStoreList}
            component={Layout.StoreListItem}
            location = {this.props.location}
            onPress = {this._setStore}
            modalConnectionError={this.props.modals.connectionError}
            hasConnectionError={this.props.fetchStoreListError}
            openModal={this.props.openModal}
            closeModal={this.props.closeModal}
            t={t}
            language={i18n.language}
          />
          <View style={{height: "100%"}}>

            <View style = {{flex: 1, marginTop: SCREEN_HEIGHT * 0.15, marginBottom: SCREEN_HEIGHT * 0.15}}>

            {this._renderPriceLabel()}

            </View>
            <Layout.BottomTabContainer>
              <Layout.TouchableContainer
                opacity
                onPress={this._goBack}
              >
                <Layout.BottomTabButtonContainer>
                  <Layout.BottomTabIcon
                    type="font-awesome"
                    name="dollar"
                    color={COLORS.RED.FIRE_ENGINE_RED}
                  />
                  <Layout.BottomTabButtonText>
                  {"  " + t('priceInquiry:productPriceDetail.nextInquiryButton')}
                  </Layout.BottomTabButtonText>
                </Layout.BottomTabButtonContainer>
              </Layout.TouchableContainer>
            </Layout.BottomTabContainer>
          </View>
        </View>
      </Layout.ViewContainer>
    );
  }
}

const mapStateToProps = state => ({
  modals: {
    addToShoppingList: state.priceInquiry.modal.addToShoppingList,
    addedToShoppingListSuccess: state.priceInquiry.modal.addedToShoppingListSuccess,
    selectQuantity: state.priceInquiry.modal.selectQuantity,
    connectionError: state.priceInquiry.modal.connectionError,
  },
  storeListLoading: state.storeListFetch.loading,
  fetchedStoreList: state.storeListFetch.storeList,
  fetchStoreListError: state.storeListFetch.error,
  location: state.location.location,

  productByBarcode: state.priceInquiry.readBarcode.product,
  loadingByBarcode: state.priceInquiry.readBarcode.loading,
  productBySKU: state.priceInquiry.fetchProductBySKU.product,
  loadingBySKU: state.priceInquiry.fetchProductBySKU.loading,

  fetchedShopListList: state.shoppingLists.CRUDShoppingList.shopListList,
  allListsFetched: state.shoppingLists.CRUDShoppingList.allShopListFetched,
  fetchListError: state.shoppingLists.CRUDShoppingList.error,
  loadingList: state.shoppingLists.CRUDShoppingList.loading,
  user: state.user,
  fetchedListDetails: state.products.fetchProduct.productList,
  currentStoreObject: state.currentStore.object,
});

const mapDispatchToProps = dispatch => {
  return {
    openModal: modalKey => {
      dispatch(Actions.setModalVisible(modalKey));
    },
    closeModal: modalKey => {
      dispatch(Actions.setModalInvisible(modalKey));
    },
    fetchStoreList: () => {
      dispatch(Actions.fetchStoreList()); // ORIGINAL_COLOMBIA: fetchStoreListByCountry
    },
    getLocation: (forceAsk) => {
      dispatch(Actions.getLocation(forceAsk));
    },
    getShopList: ({user, updated}={}) => {
      dispatch(Actions.getShopList(user, updated));
    },
    getListDetails: ({productsList}={}) => {
      dispatch(Actions.getProductsFromList(productsList));
    },
    saveShopList: ({user, updated, shopList}) => {
      dispatch(Actions.saveShopList(user, updated, shopList));
    },
    setCurrentStore: (store) => {
      dispatch(Actions.setCurrentStore(store))
    },
    fetchProductByBarcode: (user, barcode, storeNo) => {
      dispatch(Actions.fetchProductByBarcode(user, barcode, storeNo))
    },
    fetchProductBySKU: (user, sku, storeNo) => {
      dispatch(Actions.fetchProductBySKU(user, sku, storeNo))
    },
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ProductPriceDetailView);

// export default ProductPriceDetailView;
