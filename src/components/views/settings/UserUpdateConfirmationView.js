import React from "react";
import {
  View,
  Platform,
  StyleSheet,
  Alert,
  BackHandler
} from "react-native";
import { COLORS } from "../../../constants/styles";
import APP_DIMENSIONS from "../../../constants/appDimensions";
import styled from "styled-components/native";
import * as Layout from "../../layout";
import * as Actions from "../../../actions";
import {connect} from "react-redux";
import { NavigationActions } from 'react-navigation';
import { translate, Trans } from 'react-i18next';
import AsyncStorageHelper from "../../../helpers/AsyncStorageHelper";
import firebase from 'react-native-firebase';

const AUTH_TOKEN_KEY = "AUTH_TOKEN";
const LOGGED_IN_KEY = "LOGGED_IN";
const LOGGED_USER_DATA = "LOGGED_USER_DATA"
const SCREEN_HEIGHT = APP_DIMENSIONS.SCREEN_HEIGHT;
const SCREEN_WIDTH = APP_DIMENSIONS.SCREEN_WIDTH;

const BottomTabTimerText = styled(Layout.FontText)`
  fontSize: ${SCREEN_WIDTH*0.0375};
  marginRight: 6;
  textAlign: center;
  color: ${COLORS.WHITE.WHITE};
  fontFamily: open-sans-semibold;
  letterSpacing: ${SCREEN_WIDTH*0.0016};
`;

const CodeNumericInput = (props) => (
  <Layout.FontTextInput
    underlineColorAndroid={"#00000000"}
    placeholder="-"
    keyboardType={Platform.OS==='ios' ? "decimal-pad" : 'numeric'}
    maxLength={1}
    style={{
      height: SCREEN_WIDTH*0.182,
      width: SCREEN_WIDTH*0.182,
      backgroundColor: COLORS.WHITE.WHITE,
      textAlign: "center",
      borderWidth: 0,
      fontSize: SCREEN_HEIGHT*0.055,
      color: COLORS.RED.FIRE_ENGINE_RED
    }}
    placeholderTextColor= {COLORS.RED.FIRE_ENGINE_RED}
    {...props}
/>
);

@translate(['signUp', 'common'], { wait: true })
class UserUpdateConfirmationView extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      successMessage: false,
      firstDigit: "",
      secondDigit: "",
      thirdDigit: "",
      fourthDigit: "",
      modalText: "",
      emptyError: false,
      confirmationCodeSuccess: false,
      chooseValidationTypeModal: false,
      timerSeconds: 120,
      timeIsOver: false,
      error: false,
      fieldType: this.props.navigation.getParam('field', '')
    }
  }

  async componentDidUpdate(prevProps) {
    const { installationId, validateConfirmationCodeResponse, user, updateEmailOrPhoneResponse, t } = this.props;
    if (prevProps.validateConfirmationCodeResponse === "") {
      if (validateConfirmationCodeResponse.access_token !== undefined) {
        await AsyncStorageHelper.setItem(AUTH_TOKEN_KEY, validateConfirmationCodeResponse.access_token);
        this.setState({ modalText: t("common:successes.token"), confirmationCodeSuccess: true });
        this.props.openModal("modalMessage");

        await firebase.analytics().logEvent('confirmation_code_success', {
          personalIdentifier: user.personalIdentifier
        });

      } else if (validateConfirmationCodeResponse === '400111') {
        this.setState({ modalText: t("common:errors.invalidUserState") });
        this.props.openModal("modalMessage");

        await firebase.analytics().logEvent('confirmation_code_failed', {
          personalIdentifier: user.personalIdentifier
        });
      } else if (validateConfirmationCodeResponse === '400110') {
        this.setState({ modalText:  t("common:errors.invalidToken") });
        this.props.openModal("modalMessage");

        await firebase.analytics().logEvent('confirmation_code_failed', {
          personalIdentifier: user.personalIdentifier
        });
      }
    }

    if (prevProps.sendTokenForPhoneOrEmailResponse==="" && typeof this.props.sendTokenForPhoneOrEmailResponse!=="undefined" && 
    this.props.sendTokenForPhoneOrEmailResponse !== "") {
      if (this.props.sendTokenForPhoneOrEmailResponse.constructor===Array) {
        messageTitle=t("common:success");
        message=t("common:successes.resentToken");
        this.setState({timerSeconds: 120, timeIsOver: false, modalText: "Token reenviado."});
      }
      else
        this.setState({ modalText: t("common:errors.tryAgain")});
      this.props.openModal("modalMessage");
    }

    if (prevProps.updateEmailOrPhoneResponse==="") {
      if (updateEmailOrPhoneResponse!==undefined && updateEmailOrPhoneResponse.passport_no!==undefined) {
        userInfo = {
          passportNo: updateEmailOrPhoneResponse.passport_no,
          passportRaw: updateEmailOrPhoneResponse.passport_raw,
          fullName: updateEmailOrPhoneResponse.cust_name,
          email: updateEmailOrPhoneResponse.email,
          deviceId: installationId,
          personalIdentifier: user.personalIdentifier,
          phone: updateEmailOrPhoneResponse.phone,
          cep: updateEmailOrPhoneResponse.cep,
          street: updateEmailOrPhoneResponse.address_st,
          addressNumber: updateEmailOrPhoneResponse.address_no,
          complement: updateEmailOrPhoneResponse.address_comp,
          city: updateEmailOrPhoneResponse.city,
          stateName: updateEmailOrPhoneResponse.state,
          contactUuid: updateEmailOrPhoneResponse.contact_uuid
        }
        this.props.setUserInfo(userInfo);
        await AsyncStorageHelper.removeItem(LOGGED_USER_DATA)
        await AsyncStorageHelper.setItem(LOGGED_USER_DATA, userInfo)
        setTimeout(()=>this.props.navigation.navigate('Settings'), 1);
      }
      else if (updateEmailOrPhoneResponse===100110) {
        this.setState({modalText: t("common:errors.userNotFound")});
        this.props.openModal('modalMessage');
      }
      else if (updateEmailOrPhoneResponse === 100050) {
        this.setState({ modalText: t("common:errors.serverError") });
        this.props.openModal("modalMessage");
      } else if (updateEmailOrPhoneResponse === 100113) {
        this.setState({ modalText: t("common:errors.userBlocked") });
        this.props.openModal("modalMessage");
      } else if (updateEmailOrPhoneResponse === 1900110) {
        this.setState({ modalText: t("common:errors.tokenError") });
        this.props.openModal("modalMessage");
      } else if (updateEmailOrPhoneResponse === 100001) {
        this.setState({ modalText: t("common:errors.error") });
        this.props.openModal("modalMessage");
      }

      if (!prevProps.error && this.props.error) {
        this.setState({error: true})
      } 
    }
  }

  componentDidMount() {
    const didFocusSubscription = this.props.navigation.addListener(
      "didFocus",
      payload => {
        this.setState({  confirmationCodeSuccess: false });
        this.props.resetValidateConfirmationResponse();
      }
    );

    this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
      this.props.navigation.navigate('Settings', {updated: false}); // works best when the goBack is async
      return true;
    });
  }

  componentWillUnmount() {
    this.backHandler.remove();
  }

  _validateBeforeProceed = async () => {
    const { user } = this.props;
    const { firstDigit, secondDigit, thirdDigit, fourthDigit } = this.state;
    this.props.resetValidateConfirmationResponse();
    if (
      firstDigit === "" ||
      secondDigit === "" ||
      thirdDigit === "" ||
      fourthDigit === ""
    ) {
      await this.setState({ emptyError: true });
    } else {
      let tokenpass = firstDigit + secondDigit + thirdDigit + fourthDigit;
      await this.props.submitConfirmationCode(user.personalIdentifier, tokenpass)
    }
  };

  _onFourthDigitInputChange = text => {
    this.setState({ fourthDigit: text })
  };

  _onInputChange = (nextInputFocus, currentInput) => text => {
    this.setState({ [currentInput]: text })
    if (nextInputFocus)
      nextInputFocus.focus();
  }

  _didStartEditing = (key) => (e) => {
    if (key==="firstDigit") {
      this.setState({
        emptyError: false ,
        firstDigit: "",
        secondDigit: "",
        thirdDigit: "",
        fourthDigit:""
      });
    }
    else
      this.setState({
        emptyError: false ,
        [key]: "",
      });
  };

  navigateAction = NavigationActions.navigate({
    routeName: 'HomeSignedIn',
  });

  _closeModal = () =>
    setTimeout(async () => {
      var {user} = this.props;
      this.props.closeModal("modalMessage");
      if (this.state.confirmationCodeSuccess) {

        this.props.resetUpdateEmailOrPhoneResponse();

        if (this.state.fieldType==='PHONE')
          this.props.updateEmailOrPhone({personalIdentifier: user.personalIdentifier, phone:  this.props.navigation.getParam('phone', '')});
        else if (this.state.fieldType==='EMAIL')
          this.props.updateEmailOrPhone({personalIdentifier: user.personalIdentifier, email:  this.props.navigation.getParam('email', '')});
      }
  }, 1);

  _chooseValidationType = validationType => async () => {
    this.setState({chooseValidationTypeModal: false});
    this.props.resetSendTokenForPhoneOrEmailResponse();
    await this.props.sendTokenForPhoneOrEmail({
      personalIdentifier: this.props.user.personalIdentifier,
      validationType: validationType,
      phone:  this.props.navigation.getParam('phone', ''),
      email:  this.props.navigation.getParam('email', '')
    })
  }

  _onTimeIsOver = () => { this.setState({timeIsOver: true}) }

  _closeChooseValidationTypeModal = ()=>this.setState({chooseValidationTypeModal: false})

  render() {
    const { loading, modalMessage, t, tokenRequestLoading, updateLoading, i18n } = this.props;
    const { firstDigit, secondDigit, thirdDigit, fourthDigit } = this.state;
    return (
      <Layout.ViewContainer style={{backgroundColor: COLORS.RED.FIRE_ENGINE_RED}}>
        <Layout.Loader loading={loading || tokenRequestLoading || updateLoading } />
        <Layout.SuccessMessageModal
          t={t}
          modalVisible={modalMessage}
          title={this.state.modalText}
          onOk = {this._closeModal}
        />
        <Layout.SuccessMessageModal
          t={t}
          modalVisible={this.state.error}
          onOk={() => this.setState({error: false})}
          title={t('common:connectionErrorTitle')} 
          contentText={t("common:connectionErrorMessage")} 
          okText={'OK'}    
        />
        <Layout.Title text={ this.state.fieldType==="PHONE" ? t("settings:userUpdateConfirmation.phoneUpdateTitle") : t("settings:userUpdateConfirmation.emailUpdateTitle")  } />
        <Layout.KeyboardAvoidingContainer behavior='padding'
          style={{ backgroundColor: COLORS.RED.FIRE_ENGINE_RED, marginTop: StyleSheet.hairlineWidth,  }}
          keyboardVerticalOffset={SCREEN_HEIGHT*0.137}
        >
          <Layout.BigIcon name="envelope" color={COLORS.WHITE.WHITE} />
          <Layout.IconTitleText>
            {this.state.fieldType==="PHONE" ? t('settings:userUpdateConfirmation.mainMessagePhone') : t('settings:userUpdateConfirmation.mainMessageEmail')}
          </Layout.IconTitleText>
          <View style={{
            alignSelf: 'stretch',
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
            marginTop: SCREEN_HEIGHT*0.045
          }}>
             <CodeNumericInput
              onChangeText={this._onInputChange(this.passcode2, 'firstDigit')}
              onFocus={this._didStartEditing('firstDigit')}
              value={firstDigit}
              onSubmitEditing={this._validateBeforeProceed}

             />
            <CodeNumericInput
              inputRef={(r) => { this.passcode2 = r }}
              onChangeText={this._onInputChange(this.passcode3, 'secondDigit')}
              onFocus={this._didStartEditing('secondDigit')}
              value={secondDigit}
              onSubmitEditing={this._validateBeforeProceed}

            />
            <CodeNumericInput
              inputRef={(r) => { this.passcode3 = r }}
              onChangeText={this._onInputChange(this.passcode4, 'thirdDigit')}
              onFocus={this._didStartEditing('thirdDigit')}
              value={thirdDigit}
              onSubmitEditing={this._validateBeforeProceed}

            />
            <CodeNumericInput
              inputRef={(r) => { this.passcode4 = r }}
              onChangeText = {this._onInputChange(null, 'fourthDigit')}
              onFocus={this._didStartEditing('fourthDigit')}
              returnKeyType='done'
              value={fourthDigit}
              onSubmitEditing={this._validateBeforeProceed}
             />
          </View>
          {this.state.emptyError && (
              <Layout.FontText
                style={{
                  color: COLORS.WHITE.WHITE,
                  fontFamily: "open-sans-semibold"
                }}
              >
                {t("settings:userUpdateConfirmation.inputValidation")}
              </Layout.FontText>
            )}

        </Layout.KeyboardAvoidingContainer>
        <View
          style={{
            height: APP_DIMENSIONS.BOTTOM_TAB.HEIGHT,
            backgroundColor: COLORS.RED.FIRE_ENGINE_RED,
          }}
        >
          <View style={{ flex: 1, justifyContent: 'center' }}>
              { this.state.timeIsOver ?
              <Layout.TouchableContainer onPress={ this._chooseValidationType(this.state.fieldType==='PHONE'?'phone':'email')}>
                <Layout.BottomTabButtonText style={{color: COLORS.WHITE.WHITE}}>{t("settings:userUpdateConfirmation.resendToken")}</Layout.BottomTabButtonText>
              </Layout.TouchableContainer>
              :
              <BottomTabTimerText>
                <Trans i18nKey="signUp:confirmationCode.waitMessage">
                  <Layout.Timer seconds={this.state.timerSeconds} onTimeIsOver={this._onTimeIsOver} />
                </Trans>
              </BottomTabTimerText>
              }
          </View>
        </View>



        <Layout.BottomTabContainer>
          <Layout.TouchableContainer onPress={ this._validateBeforeProceed }>
            <Layout.BottomTabButtonContainer style={{backgroundColor: COLORS.RED.FIRE_ENGINE_RED}} >
              <Layout.BottomTabButtonText style={{color: COLORS.WHITE.WHITE}}>{t('common:next')}</Layout.BottomTabButtonText>
              <Layout.BottomTabIcon name='arrow-right' size={APP_DIMENSIONS.SCREEN_WIDTH*0.092}/>
            </Layout.BottomTabButtonContainer>
          </Layout.TouchableContainer>
        </Layout.BottomTabContainer>
      </Layout.ViewContainer>
    );
  }
}

const mapStateToProps = (state) => ({
  passportRaw: state.signUp.cpfInput.cpfInputResponse.passport_raw,
  passportNo: state.signUp.cpfInput.cpfInputResponse.passport_no,
  installationId: state.installationId.installationId,
  validateConfirmationCodeResponse: state.signUp.validateConfirmationCode.validateConfirmationCodeResponse,
  loading: state.signUp.validateConfirmationCode.loading,
  fetching: state.signUp.validateConfirmationCode.fetching,
  error: state.signUp.validateConfirmationCode.error,
  modalMessage: state.signUp.validateConfirmationCodeModal.modalMessage,
  cpfOrCnpj: state.signUp.cpfInput.cpfOrCnpj,
  cpfInputResponse: state.signUp.cpfInput.cpfInputResponse,
  loginResponse: state.signUp.login.loginResponse,
  tokenRequestLoading: state.signUp.sendTokenForPasswordRegistration.loading,
  loginResponse: state.signUp.login.loginResponse,
  loginLoading: state.signUp.login.loading,
  installationId: state.installationId.installationId,
  user: state.user,
  updateEmailOrPhoneResponse: state.settings.updateEmailOrPhone.updateEmailOrPhoneResponse,

  sendTokenForPhoneOrEmailResponse: state.settings.sendTokenForPhoneOrEmail.sendTokenForPhoneOrEmailResponse,

  updateLoading: state.settings.updateEmailOrPhone.loading,
  updateFetching: state.settings.updateEmailOrPhone.fetching,
  updateError: state.settings.updateEmailOrPhone.error,
});

const mapDispatchToProps = (dispatch) => {
  return {
    submitConfirmationCode: (personalIdentifier, tokenpass) => {
      dispatch(Actions.validateConfimationCode(personalIdentifier, tokenpass))
    },
    resetValidateConfirmationResponse: () => {
      dispatch(Actions.resetValidateConfirmationResponse());
    },
    closeModal: (modalKey) => {
      dispatch(Actions.setModalInvisible(modalKey))
    },
    openModal: (modalKey) => {
      dispatch(Actions.setModalVisible(modalKey))
    },
    fetchAuthToken: ({personalIdentifier, deviceId, phone, email}={}) => {
      dispatch(Actions.fetchAuthToken(personalIdentifier, deviceId, phone, email));
    },
    sendTokenForPhoneOrEmail: ({personalIdentifier, validationType, email, phone}) => {
      dispatch(Actions.sendTokenForPhoneOrEmail({personalIdentifier: personalIdentifier, validationType: validationType, email: email, phone: phone}));
    },
    resetSendTokenForPhoneOrEmailResponse: () => {
      dispatch(Actions.resetSendTokenForPhoneOrEmailResponse());
    },
    setUserInfo: ({passportNo, passportRaw, fullName, email, deviceId, personalIdentifier, phone, cep, street, addressNumber, complement, city, stateName, contactUuid}) => {
      dispatch(Actions.setUserInfo({passportNo, passportRaw, fullName, email, deviceId, personalIdentifier, phone, cep, street, addressNumber, complement, city, stateName, contactUuid}));
    },
    updateEmailOrPhone: ({personalIdentifier, email, phone}) => {
      dispatch(Actions.updateEmailOrPhone({personalIdentifier, email, phone}));
    },
    resetUpdateEmailOrPhoneResponse: () => {
      dispatch(Actions.resetUpdateEmailOrPhoneResponse());
    }
  }
};

export default connect(mapStateToProps, mapDispatchToProps) (UserUpdateConfirmationView);
