import React from "react";
import {
  View,
  Text,
  ScrollView,
  Keyboard,
  Platform,
  Alert
} from "react-native";
import { Icon } from "react-native-elements";
import { COLORS } from "../../../constants/styles";
import APP_DIMENSIONS from "../../../constants/appDimensions";
import styled, { ThemeProvider } from "styled-components/native";
import * as Layout from "../../layout";
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scrollview';
import { connect } from "react-redux";
import * as Actions from "../../../actions";
import { translate } from 'react-i18next';
import { titleCase, containsOnlyWhiteSpace, validateEmail, getPersonalIdentifierMask, getPhoneMask, validatePhone, formatPhone, nullFunction } from "../../../utils";
import { MaskService } from 'react-native-masked-text'
import AsyncStorageHelper, { KEYS } from "../../../helpers/AsyncStorageHelper";
import cep from "cep-promise";
import VersionNumber from 'react-native-version-number';

@translate(['settings', 'common'], { wait: true })
class SettingsView extends React.Component {
  constructor(props) {
    super(props);
    const { user } = props;
    // check ddi
    this.originalPhone = formatPhone(props.i18n.language, user.phone)
    this.state = {
      identificationInput: user.personalIdentifier || "",
      phone: this.originalPhone || "",
      email: user.email || "",
      fullName: titleCase(user.fullName) || "",
      cep: user.cep || "",
      street: titleCase(user.street) || "",
      addressNumber: user.addressNumber || "",
      complement: titleCase(user.complement) || "",
      city: titleCase(user.city) || "",
      stateName: typeof user.stateName === 'string' ? user.stateName.toUpperCase() : "",
      inputMask: getPersonalIdentifierMask(props.i18n.language, user.personalIdentifier),
      emptyError: false,
      cepError: false,
      confirmPhoneChangeModal: false,
      confirmEmailChangeModal: false,
      modalMessage: false,
      serverError: false
    };
  }

  componentDidMount() {
    const didFocusSubscription = this.props.navigation.addListener(
      "didFocus",
      payload => {
        const { user, t, i18n } = this.props;
        this.originalPhone = formatPhone(i18n.language, user.phone)
        this.props.resetUpdateUserResponse();
        this.setState({
          identificationInput: user.personalIdentifier || "",
          phone: this.originalPhone || "",
          email: user.email || "",
          fullName: titleCase(user.fullName) || "",
          cep: user.cep || "",
          street: titleCase(user.street) || "",
          addressNumber: user.addressNumber || "",
          complement: titleCase(user.complement) || "",
          city: titleCase(user.city) || "",
          stateName: typeof user.stateName === 'string' ? user.stateName.toUpperCase() : "",
          confirmPhoneChangeModal: false,
          confirmEmailChangeModal: false,
          modalMessage: false
        })
        if (this.props.navigation.getParam('updated', '') === false) {
          Alert.alert(
            t("settings:alertError.title"),
            t("settings:alertError.failedUpdate"),
            [
              { text: 'OK' },
            ],
            { cancelable: true }
          );
          this.props.navigation.setParams({ 'updated': null })
        }
      }
    );
  }

  async componentDidUpdate(prevProps) {
    const { updateUserResponse, installationId, user, sendTokenForPhoneOrEmailResponse, t, i18n } = this.props;
    if (prevProps.updateUserResponse === "") {
      if (updateUserResponse.cust_name !== undefined) {
        setTimeout(() => this.props.openModal('updateInfo'), 1)
        userInfo = {
          passportNo: updateUserResponse.passport_no,
          passportRaw: updateUserResponse.passport_raw,
          fullName: updateUserResponse.cust_name,
          email: updateUserResponse.email,
          deviceId: installationId,
          personalIdentifier: user.personalIdentifier,
          phone: updateUserResponse.phone,
          cep: updateUserResponse.cep,
          street: updateUserResponse.address_st,
          addressNumber: updateUserResponse.address_no,
          complement: updateUserResponse.address_comp,
          city: updateUserResponse.city,
          stateName: updateUserResponse.state,
          contactUuid: updateUserResponse.contact_uuid
        }
        await AsyncStorageHelper.setItem(KEYS.LOGGED_USER_DATA, userInfo);
        this.props.setUserInfo(userInfo);
      } else if (updateUserResponse === 100110) {
        this.setState({ modalText: t("common:errors.userNotFound"), modalMessage: true });
      } else if (updateUserResponse === 100050) {
        this.setState({ modalText: t("common:errors.serverError"), modalMessage: true });
      } else if (updateUserResponse === 100113) {
        this.setState({ modalText: t("common:errors.userBlocked"), modalMessage: true });
      } else if (updateUserResponse === 1900110) {
        this.setState({ modalText: t("common:errors.tokenError"), modalMessage: true });
      } else if (updateUserResponse === 100001) {
        this.setState({ modalText: t("common:errors.error"), modalMessage: true });
      }
    }

    if (prevProps.sendTokenForPhoneOrEmailResponse === "" && typeof this.props.sendTokenForPhoneOrEmailResponse !== "undefined" &&
      this.props.sendTokenForPhoneOrEmailResponse !== "") {
      if (this.props.sendTokenForPhoneOrEmailResponse.constructor === Array) {
        if (this.state.updateCandidate === "phone") {
          this.props.navigation.navigate('UserUpdateConfirmation', { field: 'PHONE', phone: MaskService.toRawValue(getPhoneMask(this.props.i18n.language), this.state.phone) });
          this.setState({ confirmPhoneChangeModal: false });
        }
        else if (this.state.updateCandidate === "email") {
          this.props.navigation.navigate('UserUpdateConfirmation', { field: 'EMAIL', email: this.state.email });
          this.setState({ confirmEmailChangeModal: false });
        }
      }
      else if (sendTokenForPhoneOrEmailResponse === 100110) {
        this.setState({ modalText: t("common:errors.userNotFound"), modalMessage: true });
      } else if (sendTokenForPhoneOrEmailResponse === 100050) {
        this.setState({ modalText: t("common:errors.serverError"), modalMessage: true });
      } else if (sendTokenForPhoneOrEmailResponse === 100113) {
        this.setState({ modalText: t("common:errors.userBlocked"), modalMessage: true });
      } else if (sendTokenForPhoneOrEmailResponse === 1900110) {
        this.setState({ modalText: t("common:errors.tokenError"), modalMessage: true });
      } else if (sendTokenForPhoneOrEmailResponse === 100001) {
        this.setState({ modalText: t("common:errors.error"), modalMessage: true });
      }
    }

    if (!prevProps.tokenError && this.props.tokenError) {
      this.originalPhone = formatPhone(i18n.language, user.phone)
      this.setState({
        serverError: true,
        phone: this.originalPhone || "",
        email: user.email || "",
      })
    }
    if (!prevProps.error && this.props.error)
      this.setState({ serverError: true })
  }

  _validateBeforeProceed = async () => {
    if (this.props.user.email !== this.state.email) {
      this._onEmailBlurOrEndEditing();
    }

    else if (this.originalPhone !== MaskService.toRawValue(getPhoneMask(this.props.i18n.language), this.state.phone)) {
      this._onPhoneBlurOrEndEditing();
    }
    else {
      const { passportNo, passportRaw, installationId, user, t, i18n } = this.props;
      this.props.resetUpdateUserResponse();
      if (
        this.state.fullName === "" || containsOnlyWhiteSpace(this.state.fullName) ||
        this.state.phone === "" || !validatePhone(i18n.language, this.state.phone) ||
        this.state.email === "" || !validateEmail(this.state.email)
      ) {
        await this.setState({ emptyError: true });
        Alert.alert(t("settings:alertError.title"),
          t("settings:alertError.emptyError"));
      } else if (
        this.state.cepError === true
      ) {
        await this.setState({ cepError: true });
        Alert.alert(t("settings:alertError.title"),
          t("settings:alertError.cepError"));
      } else {
        await this.props.updateUser(
          user.personalIdentifier,
          this.state.fullName,
          this.state.cep,
          this.state.street,
          this.state.addressNumber,
          this.state.complement,
          this.state.city,
          this.state.stateName,
        );
      }
    }
  };

  _logoutUser = async () => {
    await AsyncStorageHelper.removeItem(KEYS.LOGGED_IN_KEY);
    await AsyncStorageHelper.removeItem(KEYS.LOGGED_USER_DATA);
    this.props.clearCurrentStore();
    this.props.clearPreferredStore();
    this.props.resetUserInfo();
    this.props.openModal("loggedOutSuccessfully")
  };

  _didStartEditing = e => {
    this.setState({ emptyError: false });
  };

  _didFinishCepEditing = (e) => {
    const { t } = this.props;
    let value = e.nativeEvent.text;
    value = value.replace(/[^0-9]/g, '');

    if (!value) {
      this.setState({ cepError: false });
    } else if (
      value.length === 8 && this.props.i18n.language === "pt-BR" ||
      value.length > 5 && (this.props.i18n.language === "es-CO" || this.props.i18n.language === "es-PE")
    ) {
      this.setState({ cep: value, cepError: false });
      cep(value).then((res) => {
        this.setState({
          street: res.street,
          city: res.city,
          stateName: res.state
        });
      }).catch(console.log);
    } else {
      this.setState({ cepError: true });
      Alert.alert(t("settings:alertError.title"),
        t("settings:alertError.cepError"));
    }
  };

  _onFullNameInputChange = text => this.setState({ fullName: text });

  _onPhoneInputChange = text => this.setState({ phone: text });

  _onEmailInputChange = text => this.setState({ email: text });

  _onCepInputChange = text => this.setState({ cep: text });

  _onStreetInputChange = text => this.setState({ street: text });

  _onAddressNumberInputChange = text => this.setState({ addressNumber: text });

  _onComplementInputChange = text => this.setState({ complement: text });

  _onCityInputChange = text => this.setState({ city: text });

  _onStateNameInputChange = text => this.setState({ stateName: text });

  _onEmailBlurOrEndEditing = () => {
    let { t } = this.props;
    if (!validateEmail(this.state.email)) {
      Alert.alert(
        t("settings:alertError.title"),
        t("settings:alertError.invalidEmail"),
        [{ text: "OK" }],
        { cancelable: true }
      );
      this.setState({ email: this.props.user.email })
    }
    else if (this.props.user.email !== this.state.email) {
      this.setState({ confirmEmailChangeModal: true, updateCandidate: 'email' })
    }
  }

  _onPhoneBlurOrEndEditing = () => {
    let { t, i18n } = this.props;
    if (!validatePhone(i18n.language, this.state.phone)) {
      Alert.alert(
        t("settings:alertError.title"),
        t("settings:alertError.invalidPhone"),
        [{ text: "OK" }],
        { cancelable: true }
      );
      this.setState({ phone: this.originalPhone })
    }
    else if (this.originalPhone !== MaskService.toRawValue(getPhoneMask(this.props.i18n.language), this.state.phone)) {
      this.setState({ confirmPhoneChangeModal: true, updateCandidate: 'phone' })
    }
  }

  render() {
    const { t, loading, user, tokenLoading, i18n } = this.props;
    const { language } = i18n;
    return (
      <Layout.ViewContainer>
        <Layout.Loader loading={loading || tokenLoading} />

        <Layout.SuccessMessageModal
          t={t}
          modalVisible={this.state.modalMessage}
          onOk={() => this.setState({ modalMessage: false })}
          title={this.state.modalText}
          okText={'OK'}
        />

        <Layout.TwoButtonsModal
          t={t}
          modalVisible={this.state.confirmPhoneChangeModal}
          onOk={async () => {
            this.setState({ confirmPhoneChangeModal: false })
            await this.props.resetSendTokenForPhoneOrEmail();
            await this.props.sendTokenForPhoneOrEmail({
              personalIdentifier: user.personalIdentifier,
              validationType: 'phone',
              phone: MaskService.toRawValue(getPhoneMask(this.props.i18n.language), this.state.phone)
            });
          }}
          onCancel={() => this.setState({ confirmPhoneChangeModal: false, phone: this.originalPhone })}
          title={t && t("settings:modalChangePhone.title")}
          contentText={t && t("settings:modalChangePhone.content")}
          okText={t && t("common:next")}
          cancelText={t && t('common:cancel')}
          hidePicker={true}
        />

        <Layout.TwoButtonsModal
          t={t}
          modalVisible={this.state.confirmEmailChangeModal}
          onOk={async () => {
            this.setState({ confirmEmailChangeModal: false, updateCandidate: 'email' })
            await this.props.resetSendTokenForPhoneOrEmail();
            await this.props.sendTokenForPhoneOrEmail({
              personalIdentifier: user.personalIdentifier,
              validationType: 'email',
              email: this.state.email
            });
          }}
          onCancel={() => this.setState({ confirmEmailChangeModal: false, email: this.props.user.email })}
          title={t && t("settings:modalChangeEmail.title")}
          contentText={t && t("settings:modalChangeEmail.content")}
          okText={t && t("common:next")}
          cancelText={t && t('common:cancel')}
          hidePicker={true}
        />

        <Layout.SuccessMessageModal
          modalVisible={this.props.modals.updateInfo}
          okText='OK'
          title={t("settings:modal.title")}
          contentText={t("settings:modal.content")}
          onOk={() => setTimeout(() => {
            this.props.closeModal('updateInfo');
          }, 1)
          }
        />

        <Layout.SuccessMessageModal
          t={t}
          modalVisible={this.state.serverError}
          onOk={() => this.setState({ serverError: false })}
          title={t('common:connectionErrorTitle')}
          contentText={t("common:connectionErrorMessage")}
          okText={'OK'}
        />

        <Layout.SuccessMessageModal
          modalVisible={this.props.modals.loggedOutSuccessfully}
          okText='OK'
          title={t("settings:modal_logout.title")}
          contentText={t("settings:modal_logout.content")}
          onOk={() => setTimeout(() => {
            this.props.closeModal('loggedOutSuccessfully');
            this.props.navigation.navigate('TermsOfUse');
          }, 1)
          }
        />
        <Layout.Title text={t("settings:title")} />
        <Layout.KeyboardAvoidingContainer
          keyboardVerticalOffset={APP_DIMENSIONS.SCREEN_HEIGHT * 0.15}
          behavior={Platform.OS === 'ios' ? 'height' : 'padding'}
          style={{ justifyContent: 'flex-start', flex: 1 }}
        >
          <KeyboardAwareScrollView contentContainerStyle={{ flexGrow: 1 }} keyboardShouldPersistTaps='always'>
            <View style={{
              justifyContent: 'space-between',
              flex: 1,
              paddingTop: APP_DIMENSIONS.SCREEN_HEIGHT * 0.02,
              paddingBottom: APP_DIMENSIONS.SCREEN_HEIGHT * 0.03,
              alignContent: 'center'
            }}
            >
              <Layout.BorderedFormInput
                value={this.state.identificationInput}
                placeholder={t("settings:cpfInputPlaceHolder")}
                editable={false}
                maskActive={true}
                type={this.state.inputMask}
                label={t("settings:cpfInputLabel")}
              />
              <Layout.BorderedFormInput
                placeholder={t("settings:nameInputPlaceHolder")}
                inputRef={r => {
                  this.fullName = r;
                }}
                onSubmitEditing={event => {
                  event && this.cellPhone.focus();
                }}
                onChangeText={this._onFullNameInputChange}
                value={this.state.fullName}
                onFocus={this._didStartEditing}
                inputError={this.state.emptyError}
                label={t("settings:nameInputLabel")}
              />
              <Layout.BorderedFormInput
                placeholder={t("settings:cellPhoneInputPlaceHolder")}
                inputRef={r => {
                  this.cellPhone = r;
                }}
                onSubmitEditing={event => {
                  event && this.email.focus();
                  this._onPhoneBlurOrEndEditing();
                }}
                keyboardType={Platform.OS === 'ios' ? "decimal-pad" : 'numeric'}
                maskActive={true}
                type={getPhoneMask(this.props.i18n.language)}
                onChangeText={this._onPhoneInputChange}
                value={this.state.phone}
                onFocus={this._didStartEditing}
                returnKeyType='done'
                inputError={this.state.emptyError}
                onBlur={this._onPhoneBlurOrEndEditing}
                label={t("settings:cellPhoneInputLabel")}
              />
              <Layout.BorderedFormInput
                placeholder={t("settings:emailInputPlaceHolder")}
                inputRef={r => {
                  this.email = r;
                }}
                onSubmitEditing={event => {
                  event && this.cep.focus();
                  this._onEmailBlurOrEndEditing();
                }}
                onChangeText={this._onEmailInputChange}
                value={this.state.email}
                onFocus={this._didStartEditing}
                keyboardType={'email-address'}
                textContentType={'emailAddress'}
                inputError={this.state.emptyError}
                onBlur={this._onEmailBlurOrEndEditing}
                label={t("settings:emailInputLabel")}
              />
              <Layout.BorderedFormInput
                placeholder={t("settings:cepInputPlaceHolder")}
                inputRef={r => {
                  this.cep = r;
                }}
                onSubmitEditing={event => {
                  event && this.street.focus();
                }}
                onFocus={this._didStartEditing}
                value={this.state.cep}
                onEndEditing={this._didFinishCepEditing}
                keyboardType={Platform.OS === 'ios' ? "decimal-pad" : 'numeric'}
                onChangeText={this._onCepInputChange}
                inputError={this.state.cepError}
                returnKeyType={'done'}
                label={t("settings:cepInputLabel")}
              />
              <Layout.BorderedFormInput
                placeholder={t("settings:streetInputPlaceHolder")}
                inputRef={r => {
                  this.street = r;
                }}
                onFocus={this._didStartEditing}
                value={this.state.street}
                onSubmitEditing={event => {
                  event && this.addressNumber.focus();
                }}
                onChangeText={this._onStreetInputChange}
                label={t("settings:streetInputLabel")}
              />
              <Layout.BorderedFormInput
                placeholder={t("settings:addressNumberInputPlaceHolder")}
                inputRef={r => {
                  this.addressNumber = r;
                }}
                keyboardType={Platform.OS === 'ios' ? "decimal-pad" : 'numeric'}
                onFocus={this._didStartEditing}
                onSubmitEditing={event => {
                  event && this.complement.focus();
                }}
                value={this.state.addressNumber}
                onChangeText={this._onAddressNumberInputChange}
                returnKeyType={'done'}
                label={t("settings:addressNumberInputLabel")}
              />
              <Layout.BorderedFormInput
                placeholder={t("settings:complementInputPlaceHolder")}
                inputRef={r => {
                  this.complement = r;
                }}
                onSubmitEditing={event => {
                  event && this.city.focus();
                }}
                onFocus={this._didStartEditing}
                value={this.state.complement}
                onChangeText={this._onComplementInputChange}
                label={t("settings:complementInputLabel")}
              />
              <Layout.BorderedFormInput
                placeholder={t("settings:cityInputPlaceHolder")}
                inputRef={r => {
                  this.city = r;
                }}
                onFocus={this._didStartEditing}
                onSubmitEditing={event => {
                  event && this.stateName && this.stateName.focus();
                }}
                value={this.state.city}
                onChangeText={this._onCityInputChange}
                onSubmitEditing={i18n.language === "es-CO" ? this._validateBeforeProceed : nullFunction}
                label={t("settings:cityInputPlaceHolder")}
              />
              {this.props.i18n.language === "es-CO" &&

                <Layout.BorderedFormInput
                  placeholder={t("settings:stateInputPlaceHolder")}
                  inputRef={r => {
                    this.stateName = r;
                  }}
                  onFocus={this._didStartEditing}
                  value={this.state.stateName}
                  onChangeText={this._onStateNameInputChange}
                  onSubmitEditing={this._validateBeforeProceed}
                  label={t("settings:stateInputLabel")}
                />
              }
            </View>
            <View style={{ height: APP_DIMENSIONS.SCREEN_HEIGHT * 0.0625, marginBottom: 7, width: APP_DIMENSIONS.SCREEN_WIDTH * 0.87, alignSelf: "center" }}>
              <Layout.TouchableContainer onPress={this._validateBeforeProceed} opacity>
                <Layout.BottomTabButtonContainer style={{ backgroundColor: COLORS.RED.FIRE_ENGINE_RED, borderRadius: 14 }} >
                  <Layout.BottomTabButtonText style={{ color: COLORS.WHITE.WHITE }}>{t("settings:updateUserDataButton")}</Layout.BottomTabButtonText>
                  <Layout.BottomTabIcon name='arrow-right' />
                </Layout.BottomTabButtonContainer>
              </Layout.TouchableContainer>
            </View>
          </KeyboardAwareScrollView>
        </Layout.KeyboardAvoidingContainer>

        <View style={{ height: APP_DIMENSIONS.SCREEN_HEIGHT * 0.125, }}>
          <View style={{ flex: 1, paddingRight: APP_DIMENSIONS.SCREEN_WIDTH * 0.06, paddingLeft: APP_DIMENSIONS.SCREEN_WIDTH * 0.06 }}>
            <Layout.TouchableContainer onPress={this._logoutUser} opacity>
              <Layout.BottomTabButtonContainer style={{ backgroundColor: COLORS.GREY.LIGHT_GREY, borderRadius: 14 }} >
                <Layout.BottomTabButtonText style={{ color: COLORS.BLACK.BLACK }}>{t("settings:logoutButton")}</Layout.BottomTabButtonText>
              </Layout.BottomTabButtonContainer>
            </Layout.TouchableContainer>
          </View>
          <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
            <Text style={{ fontSize: APP_DIMENSIONS.SCREEN_WIDTH * 0.024 }}>
              {
                t("settings:appVersion", {
                  version: VersionNumber.appVersion,
                  buildNumber: VersionNumber.buildVersion
                })
              }
            </Text>
          </View>
        </View>
      </Layout.ViewContainer>
    );
  }
}

const mapStateToProps = state => ({
  modals: {
    updateInfo: state.settings.settingsModal.updateInfo,
    loggedOutSuccessfully: state.settings.settingsModal.loggedOutSuccessfully
  },
  updateUserResponse: state.settings.updateUser.updateUserResponse,
  sendTokenForPhoneOrEmailResponse: state.settings.sendTokenForPhoneOrEmail.sendTokenForPhoneOrEmailResponse,
  tokenLoading: state.settings.sendTokenForPhoneOrEmail.loading,
  tokenError: state.settings.sendTokenForPhoneOrEmail.error,
  loading: state.settings.updateUser.loading,
  fetching: state.settings.updateUser.fetching,
  error: state.settings.updateUser.error,
  installationId: state.installationId.installationId,
  loginResponse: state.signUp.login.loginResponse,
  cpfOrCnpj: state.signUp.cpfInput.cpfOrCnpj,
  user: state.user,
});

const mapDispatchToProps = dispatch => {
  return {
    updateUser: (personalIdentifier, fullName, cep, street, addressNumber, complement, city, stateName) => {
      dispatch(Actions.updateUser(personalIdentifier, fullName, cep, street, addressNumber, complement, city, stateName));
    },
    resetUpdateUserResponse: () => {
      dispatch(Actions.resetUpdateUserResponse());
    },
    openModal: modalKey => {
      dispatch(Actions.setModalVisible(modalKey));
    },
    closeModal: modalKey => {
      dispatch(Actions.setModalInvisible(modalKey));
    },
    setUserInfo: ({ passportNo, passportRaw, fullName, email, deviceId, personalIdentifier, phone, cep, street, addressNumber, complement, city, stateName, contactUuid }) => {
      dispatch(Actions.setUserInfo({ passportNo, passportRaw, fullName, email, deviceId, personalIdentifier, phone, cep, street, addressNumber, complement, city, stateName, contactUuid }));
    },
    resetUserInfo: () => {
      dispatch(Actions.resetUserInfo());
    },
    clearPreferredStore: () => { dispatch(Actions.clearPreferredStore()) },
    clearCurrentStore: () => { dispatch(Actions.clearCurrentStore()) },
    sendTokenForPhoneOrEmail: ({ personalIdentifier, validationType, email, phone }) => {
      dispatch(Actions.sendTokenForPhoneOrEmail({ personalIdentifier: personalIdentifier, validationType: validationType, email: email, phone: phone }));
    },
    resetSendTokenForPhoneOrEmail: () => {
      dispatch(Actions.resetSendTokenForPhoneOrEmailResponse());
    },
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SettingsView);

// export default SettingsView;
