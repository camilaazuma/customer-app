import React from "react";
import {
  StatusBar,
  Button,
  View,
  Text,
  Image,
  StyleSheet,
  TouchableWithoutFeedback,
  TouchableNativeFeedback,
  Dimensions, 
  Platform
} from "react-native";
import { Icon } from "react-native-elements";
import { COLORS } from "../../constants/styles";
import styled, { ThemeProvider } from "styled-components/native";
import Carousel, { Pagination } from "react-native-snap-carousel";
import * as Layout from "../layout";

const CarouselImagesArray = [
  <Layout.CarouselImage source={require("../../../assets/homenaologada.png")} /> , 
  <Layout.CarouselImage source={require("../../../assets/makro-random.jpg")} />
]

class HomeView extends React.Component {
  render() {
    return (
      <Layout.ViewContainer >
        <Layout.StatusBarContainer /> 
        <Layout.HeaderContainer>
          <Layout.MenuIcon onPress={ this.props.navigation.openDrawer }/>
          <Layout.HeaderLogo navigation={this.props.navigation}/>
          <Layout.LocationIcon />
        </Layout.HeaderContainer>
        <Layout.CarouselContainer>
          <Layout.ImageCarousel images={CarouselImagesArray} />
        </Layout.CarouselContainer>
        <Layout.HomeBottomTabContainer>
          <Layout.TouchableContainer opacity onPress={() => { console.log('')}}>
            <Layout.HomeBottomTabButtonContainer>
              <Layout.HomeBottomTabIcon name='user'/>
              <Layout.BottomTabButtonText>ACESSAR{'\n'}CONTA</Layout.BottomTabButtonText>
            </Layout.HomeBottomTabButtonContainer>
          </Layout.TouchableContainer>
          <Layout.TouchableContainer opacity onPress={()=>setTimeout(()=>this.props.navigation.navigate('SignUp'),1)}>
            <Layout.HomeBottomTabButtonContainer>
              <Layout.HomeBottomTabIcon name='sign-in'/>
              <Layout.BottomTabButtonText>CADASTRE-SE{'\n'}CLIQUE AQUI</Layout.BottomTabButtonText>
            </Layout.HomeBottomTabButtonContainer>
          </Layout.TouchableContainer>
          <Layout.TouchableContainer opacity>
            <Layout.HomeBottomTabButtonContainer>
              <Layout.HomeBottomTabIcon name='barcode'/>
              <Layout.BottomTabButtonText>CONSULTA DE{'\n'}PREÇOS</Layout.BottomTabButtonText>
            </Layout.HomeBottomTabButtonContainer>
          </Layout.TouchableContainer>
        </Layout.HomeBottomTabContainer>
      </Layout.ViewContainer>
    );
  }
}

export default HomeView;
