import React from "react";
import {
  View,
  Text,
  Platform,
  LayoutAnimation,
  UIManager,
  FlatList,
  Alert,
  BackHandler,
  NetInfo,
  StyleSheet
} from "react-native";
import { List, ListItem, SearchBar } from 'react-native-elements';
import OfferTabs from "./offerTabs";
import * as Layout from "../../layout";
import * as Actions from "../../../actions";
import { connect } from "react-redux";
import { translate } from "react-i18next";
import { nullFunction, formatStringForPrice, titleCase, showSnackbar, capitalizeFirstLetter, capitalizeEachFirstLetter } from "../../../utils";
import { COLORS } from "../../../constants/styles";
import APP_DIMENSIONS from "../../../constants/appDimensions";
import { showListSharing } from "../../../actions";
import Snackbar from "react-native-snackbar";
import AsyncStorageHelper from "../../../helpers/AsyncStorageHelper";
import moment from 'moment';

const height = APP_DIMENSIONS.SCREEN_HEIGHT * 0.11;
const padding = APP_DIMENSIONS.SCREEN_HEIGHT * 0.028;

const OFFER_LIST_KEY = "OFFER_LIST";

@translate(["offers", "common"], { wait: true })
class OffersListView_CO extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      expanded: {
        stores: false,
        sorters: false,
      },
      updatingOffers: false,
      caret: "down",
      offersList: [],
      offerSearchText: "",
      backupOffersList: [],
      offersOpened: true,
      endReached: false,
      hide: true,
      fetchError: false,
      fetchStoreListError: false,
      currentGroupIndex: 0,
      iconColor: "black",
      currentSortingIndex: -1,
      selectStoreModalVisible: false,
      modalSelectedStore: null,
      selectedValues: {
        oldStore: {},
        sort: -1
      },
      recommendationList: { topSell: [], crossSell: [], upSell: [],},
      showRecommendations: false,
      activeTabIndex: 0,
      showSearchResult: false,
      tabs: []
    };
    if (Platform.OS === "android") {
      UIManager.setLayoutAnimationEnabledExperimental(true);
    }
    this.sorting = [
      this.props.t("offers:filterScreen.exhibitionList.alphabetic"),
      this.props.t("offers:filterScreen.exhibitionList.lowestPrice"),
      this.props.t("offers:filterScreen.exhibitionList.highestPrice"),
      this.props.t("offers:filterScreen.exhibitionList.expirationDate"),
    ];

    this.backHandler = BackHandler.addEventListener('hardwareBackPress', async () => {
      this.state.offersOpened ? this.props.navigation.goBack() : await this._changeView(false)
      return true;
    });
  }

  _changeLayoutAnimated = () => {
    LayoutAnimation.configureNext({
      duration: 200,
      update: {
        type: LayoutAnimation.Types.linear,
      },
    });
    this._setExpanded("stores", !this.state.expanded.stores);
  };

  _setExpanded = (key, value) => {
    this.setState({
      expanded: {
        ...this.state.expanded,
        [key]: value
      }
    })
  }

  _setSelection = (key, value) => {
    this.setState({
      selectedValues: {
        ...this.state.selectedValues,
        [key]: value
      }
    })
  }

  _recommendationKeyExtractor = (item, index) => index + "_" + item.ProductSKU;

  async componentDidMount() {
    const { currentStoreObject, nearestStore, user, preferredStoreObject } = this.props;
    let isConnected = await NetInfo.isConnected.fetch();
    await this.props.clearAllOffers();
    await this.props.clearAllRecommendations();

    this.props.fetchOffersRequest();
    this.setState({offersList: [], tabs: [], recommendationList: {topSell: [], crossSell: [], upSell: [],}});

    if(user && currentStoreObject && currentStoreObject.store_no === preferredStoreObject){
      this.props.fetchCustomerRecommendations({customerID: user.contactUuid});
      this.props.fetchAllOffers({ storeId: currentStoreObject.store_no });
      this.setState({ showRecommendations: true });
    }else{
      this.setState({ showRecommendations: false });
    }

    if (currentStoreObject === null)
      this.setState({ selectStoreModalVisible: true });

    let offers = await AsyncStorageHelper.getItem(OFFER_LIST_KEY);
    if (offers !== null && this.props.currentStoreObject !== null
      && offers.currentStore.store_no === this.props.currentStoreObject.store_no) { // ORIGINAL_COLOMBIA: idlocation
      this.props.setOffers(offers.list);
    }

    if (isConnected) {
      if (currentStoreObject !== null) {
        this.props.fetchAllOffers({ storeId: currentStoreObject.store_no }); // ORIGINAL_COLOMBIA: idlocation
        this.setState({ updatingOffers: true });
      }

      this.props.fetchStoreList();
      if (this.props.location === null) {
        this.props.getLocation(this.props);
      }
    }
    else {
      this.props.fetchOffersError();
      showSnackbar(this.props.t("common:errors.noConnection"));
    }
  }

  async componentDidUpdate(prevProps, prevState) {
    if (this.props.currentStoreObject && !prevProps.currentStoreObject) {
      this.props.clearAllOffers();
      this.setState({ offersList: [], selectStoreModalVisible: false });
      this.props.fetchAllOffers({ storeId: this.props.currentStoreObject.store_no }); // ORIGINAL_COLOMBIA: idlocation
    }

    if (!prevProps.fetchError && this.props.fetchError) {
      let isConnected = await NetInfo.isConnected.fetch();
      if (!isConnected)
        showSnackbar(this.props.t("common:errors.noConnection"));
      else {
        this.props.openModal("connectionError");
        showSnackbar(this.props.t("common:connectionErrorMessage"), Snackbar.LENGTH_SHORT);
        this.setState({ updatingOffers: false });
      }
    } else if (!prevProps.fetchStoreListError && this.props.fetchStoreListError) {
      let isConnected = await NetInfo.isConnected.fetch();
      if (!isConnected)
        showSnackbar(this.props.t("common:errors.noConnection"));
      else
        this.props.openModal("connectionError");
    }

    if ((prevProps.fetchedOfferList.length !== this.props.fetchedOfferList.length)
      && this.props.fetchedOfferList.length > 0) {
      let offers = {
        currentStore: this.props.currentStoreObject,
        list: this.props.fetchedOfferList
      }
      await AsyncStorageHelper.removeItem(OFFER_LIST_KEY);
      await AsyncStorageHelper.setItem(OFFER_LIST_KEY, offers);
      this.setState({backupOffersList: this.props.fetchedOfferList})
    }

    if (this.props.allOffersFetched && !prevProps.allOffersFetched && this.state.updatingOffers) {
      showSnackbar(this.props.t("offers:offersUpdated"), Snackbar.LENGTH_SHORT);
      this.setState({ updatingOffers: false });
    }

    if (this.props.preferredStoreObject && prevProps.preferredStoreObject===null) {
      await this.props.clearAllOffers();
      await this.props.clearAllRecommendations();
      this.props.fetchCustomerRecommendations({customerID: this.props.user.contactUuid});
      this.props.fetchAllOffers({ storeId: this.props.preferredStoreObject });

      if (this.props.currentStoreObject.store_no === this.props.preferredStoreObject)
        this.setState({tabs: [], recommendationList: {topSell: [], crossSell: [], upSell: [],}, showRecommendations: true});

    }

    if ( 
      this.props.currentStoreObject && prevProps.currentStoreObject && 
      this.props.currentStoreObject.store_no !== prevProps.currentStoreObject.store_no && 
      this.props.currentStoreObject.store_no === this.props.preferredStoreObject
    )
      this.setState({tabs: [], recommendationList: {topSell: [], crossSell: [], upSell: [],}, showRecommendations: true});

    // let tabs = [];
    let {topSell, crossSell, upSell } = this.props.recommendationList;

    if (topSell.length && !prevProps.recommendationList.topSell.length) {
      const backupTabs = this.state.tabs;
      if (!backupTabs.find(x => x.key === 'first'))
        this.setState({tabs: []}, ()=>this.setState(state=>({ 
          tabs: [...backupTabs, {key: 'first', title: this.props.t("offers:topSell")}],
          recommendationList: { ...state.recommendationList, topSell}
        })))
    }


    if (crossSell.length && !prevProps.recommendationList.crossSell.length) {
      const backupTabs = this.state.tabs;
      if (!backupTabs.find(x => x.key === 'second'))
        this.setState({tabs: []}, ()=> this.setState(state=>({ 
          tabs: [...backupTabs, {key: 'second', title: this.props.t("offers:crossSell")}],
          recommendationList: { ...state.recommendationList, crossSell}
        })))
    }

    if (upSell.length && !prevProps.recommendationList.upSell.length) {
      const backupTabs = this.state.tabs;
      if (!backupTabs.find(x => x.key === 'third'))
        this.setState({tabs: []}, ()=> this.setState(state=>({ 
          tabs: [...backupTabs, {key: 'third', title: this.props.t("offers:upSell")}],
          recommendationList: { ...state.recommendationList, upSell}
        })))
    }

    if (
      (this.props.recommendationsFetched && !prevProps.recommendationsFetched) ||
      (this.props.recommendationFetchError && !prevProps.recommendationFetchError)
    ) {
      const backupTabs = this.state.tabs;
      if (!backupTabs.find(x => x.key === 'fourth'))
        this.setState({tabs: []}, ()=> this.setState(state=>({ tabs: [...backupTabs, {key: 'fourth', title: this.props.t("offers:allOffers")} ]})))
    }
  }

  static getDerivedStateFromProps(nextProps, prevState) {

    var update = {};

    // timeout
    if (prevState.offersList.length !== nextProps.fetchedOfferList.length && prevState.offerSearchText === "") {
      update.offersList = nextProps.fetchedOfferList;
      update.endReached = false;
      update.fetchError = false;
      update.fetchStoreListError = false;
    }

    return update;
  }

  componentWillUnmount() {
    this.backHandler.remove();
  }

  _renderSorterItem = ({ item, index }) => (
    <ListItem
      title={item.toUpperCase()}
      onPress={() => {
        this._setSelection("sort", index);
        this._setExpanded("sorters", false);
      }}
    />
  );

  _renderRecomendationItem = ({ item: offer, index } ) => {
    let date = parseInt(offer.Offerend, 10);
    date = moment(date, 'YYYYMMDD').format("DD/MM/YYYY")
    if(offer.ProductRecommendation && offer.ProductRecommendation.fault || !this.state.tabs.length){
      return null;
    }
    return (
      <Layout.OfferListItem
        productName={capitalizeEachFirstLetter(offer.ProductRecommendation.ProductName)}
        productImgSrc={offer.ProductRecommendation.ProductImages ? offer.ProductRecommendation.ProductImages[0].ImageURL : "null"}
        price={''}
        embVenda={capitalizeFirstLetter(offer.ProductRecommendation.ProductSalesPomoPackingDescription)}
        sellPrice={Number(offer.ProductFinalPrice).toFixed(2).replace(".", ",")}
        //unit={offer.ProductRecommendation.offerbuyquantityunit}
        offerEndDate={this.props.t && this.props.t("offers:offerEndDate", { date: date })}
        t={this.props.t}
        i18n={this.props.i18n}
      />
    )
  };

  _renderOfferItem = ({ item: offer }, index) => {
    let datestr = offer.OfferProductToDate.match(/([0-9])\w+/)[0];
    let date = new Date(parseInt(datestr, 10));
    return (
      <Layout.OfferListItem
        productName={capitalizeEachFirstLetter(offer.ProductName)}
        productImgSrc={offer.ProductImages ? offer.ProductImages[0].ImageURL : "null"}
        price={''}
        embVenda={capitalizeFirstLetter(offer.ProductSalesPomoPackingDescription)}
        sellPrice={Number(offer.OfferPriceValue).toFixed(2).replace(".", ",")}
        unit={offer.OfferBuyQuantityUnit}
        offerEndDate={this.props.t && this.props.t("offers:offerEndDate", { date: moment(date).format("DD/MM/YYYY") })}
        t={this.props.t}
        i18n={this.props.i18n}
      />
    )
  };

  _renderRecomendationFooter = (nested) => {
    if (!this.props.recommendationListLoading && (nested === null || typeof nested === 'undefined' || nested.length === 0)) {
      return (
        <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
          <Layout.FontText style={{ color: COLORS.BLACK.BLACK, fontSize: APP_DIMENSIONS.SCREEN_WIDTH * 0.053, fontFamily: 'open-sans' }}>
            {this.props.t("offers:emptyOfferListText")}
          </Layout.FontText>
        </View>
      )
    }
    return null;
  }

  _renderOfferFooter = (nested) => {
    if (!this.props.offerListLoading && (this.state.offersList === null || typeof this.state.offersList === 'undefined' || this.state.offersList.length === 0)) {
      return (
        <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
          <Layout.FontText style={{ color: COLORS.BLACK.BLACK, fontSize: APP_DIMENSIONS.SCREEN_WIDTH * 0.053, fontFamily: 'open-sans' }}>
            {this.props.t("offers:emptyOfferListText")}
          </Layout.FontText>
        </View>
      )
    }
    return null;
  };

  _onEndReached = () => {
    this.setState({ endReached: true });
    return null;
  }

  _onRefresh = async () => {
    const { showRecommendations } = this.state;
    const { user } = this.props;
    let isConnected = await NetInfo.isConnected.fetch();
    if (isConnected) {
      if (this.props.currentStoreObject !== null) {
        this.keySearch = "";
        let update = {
          offersList: [],
          offerSearchText: "",
          backupOffersList: [],
          selectedValues: {
            ...this.state.selectedValues,
            sort: -1
          }
        }
        if (showRecommendations) {
          update.recommendationList = { topSell: [], crossSell: [], upSell: [], };
          update.tabs = [];
        }
        this.setState(update);
        if (showRecommendations) {
          await this.props.clearAllRecommendations();
          await this.props.fetchCustomerRecommendations({customerID: user.contactUuid});
        }
        await this.props.clearAllOffers();
        await this.props.fetchAllOffers({ storeId: this.props.currentStoreObject.store_no }); // ORIGINAL_COLOMBIA: idlocation
        await this._sortBySelector(this.state.selectedValues.sort);
        this.setState({ updatingOffers: true });
      }
    }
    else {
      this.props.fetchOffersError();
      showSnackbar(this.props.t("common:errors.noConnection"));
    }
  }

  _setStore = async (store) => {
    if (!this.props.fetching && (this.props.currentStoreObject === null || store.store_no !== this.props.currentStoreObject.store_no || store.idlocation !== this.props.currentStoreObject.idlocation)) {
      await this.props.setCurrentStore(store);
      if(store.store_no == this.props.preferredStoreObject){
        this.props.clearAllRecommendations();
        this.props.fetchCustomerRecommendations({ customerID: this.props.user.contactUuid});
        this.setState({
          tabs: [],
          recommendationList: { topSell: [], crossSell: [], upSell: [], },
          showRecommendations: true
        });
        this._setExpanded("stores", !this.state.expanded.stores);
      } else {
        this.props.clearAllOffers();
        this.keySearch = "";
        this.setState({
          offersList: [],
          offerSearchText: "",
          backupOffersList: [],
          selectedValues: {
            ...this.state.selectedValues,
            sort: -1
          },
          showRecommendations: false
        });
        this._sortBySelector(this.state.selectedValues.sort);
        this.props.fetchAllOffers({ storeId: this.props.currentStoreObject.store_no }); // ORIGINAL_COLOMBIA: idlocation
        this._setExpanded("stores", !this.state.expanded.stores);
      }
    }
    else {
      this._setExpanded("stores", !this.state.expanded.stores);
    }
  }

  _getStoreName = () => {
    const { currentStoreObject } = this.props;
    if (currentStoreObject !== null && typeof currentStoreObject !== 'undefined') {
      if ('name' in currentStoreObject) {                          // ORIGINAL_COLOMBIA: locationname
        return this.props.currentStoreObject.name.toUpperCase(); // ORIGINAL_COLOMBIA: locationname
      }
    }
    return "";
  }

  _searchFilterFunction = async (text) => {
    let searchBase = [];
    let willReturnToList = text === "" ? true : false;

    if (!text) {
      return this.setState({showSearchResult: false, offerSearchText: text})
    }

    searchBase = this.state.backupOffersList;

    const searchResult = searchBase.filter(item => {
      const itemData = `${item.ProductName.toUpperCase()} ${item.ProductSalesPomoPackingDescription.toUpperCase()} ${item.ProductSKU.toUpperCase()}`;
      const textData = text.toUpperCase();
      return itemData.indexOf(textData) > -1;
    });

    this.setState(()=>{
      let update = {
        endReached: !willReturnToList,
        offerSearchText: text,
        offersList: searchResult,
        showSearchResult: true,
      }
      return update;
    });
  };

  _filterByGroup = async (item, index) => {
    if (index === -1) {
      return;
    }
    else {
      var newData = null;
      if (item.code === "") newData = this.state.offersList;
      else {
        newData = this.state.offersList.filter(offer => {
          const itemData = `${offer.cod_area}`;
          const textData = item.code;
          return itemData.indexOf(textData) > -1;
        });
      }

      var willReturnToList = item.code === "" ? true : false;
      this.setState({
        offersList: newData,
        currentGroupIndex: index,
        endReached: !willReturnToList
      });
    }
  }

  _sortBySelector = (index) => {
    let newOffers = []; let newTopSell = []; let newCrossSell = []; let newUpSell = [];
    const { offersList } = this.state;
    const { topSell, crossSell, upSell } = this.state.recommendationList;
    let alphabeticallySort = (array, key, subKey = null) => array.sort((a, b)=> {
        if (subKey) {
          if (a[key] && b[key] && a[key][subKey] && b[key][subKey]) return a[key][subKey].localeCompare(b[key][subKey], 'es', { sensitivity: 'base' });
        } 
        else {
          if (a[key] && b[key]) return a[key].localeCompare(b[key], 'es', { sensitivity: 'base' });
        } 
    });
    let lowesPriceSort = (array, key) => array.sort((a, b) => {if(a[key] && b[key]) return parseFloat(a[key]) - parseFloat(b[key])});
    let highestPriceSort = (array, key) => array.sort((a, b) => parseFloat(b[key]) - parseFloat(a[key]));
    let recDateSort = (array, key) => array.sort((a, b) => {
      let date1 = parseInt(a[key], 10);
      let date2 = parseInt(b[key], 10);
      return moment(date1, 'YYYYMMDD').format("YYYYMMDD") - moment(date2, 'YYYYMMDD').format("YYYYMMDD")
    });
    switch (index) {
      case 0: {
        newOffers = alphabeticallySort(offersList, 'ProductName', null);
        newTopSell = alphabeticallySort(topSell, 'ProductRecommendation', 'ProductName');
        newCrossSell = alphabeticallySort(crossSell, 'ProductRecommendation', 'ProductName');
        newUpSell = alphabeticallySort(upSell, 'ProductRecommendation', 'ProductName');
        break;
      }
      case 1: {
        newOffers = lowesPriceSort(offersList, "OfferPriceValue");
        newTopSell = lowesPriceSort(topSell, "ProductFinalPrice");
        newCrossSell = lowesPriceSort(crossSell, "ProductFinalPrice");
        newUpSell = lowesPriceSort(upSell, "ProductFinalPrice");
        break;
      }
      case 2: {
        newOffers = highestPriceSort(offersList, "OfferPriceValue");
        newTopSell = highestPriceSort(topSell, "ProductFinalPrice");
        newCrossSell = highestPriceSort(crossSell, "ProductFinalPrice");
        newUpSell = highestPriceSort(upSell, "ProductFinalPrice");
        break;
      }
      case 3: {
        newOffers = this.state.offersList.sort(function (a, b) {
          let date1 = parseInt(a.OfferProductToDate.match(/([0-9])\w+/)[0], 10);
          let date2 = parseInt(b.OfferProductToDate.match(/([0-9])\w+/)[0], 10);
          return new Date(date1) - new Date(date2);
        });
        newTopSell = recDateSort(topSell, "Offerend");
        newCrossSell = recDateSort(crossSell, "Offerend");
        newUpSell = recDateSort(upSell, "Offerend");
        break;
      }
      default:
        newOffers = offersList;
        newTopSell = topSell;
        newCrossSell = crossSell;
        newUpSell = upSell;
        break;
    }
    this.setState({
      offersList: newOffers,
      recommendationList: { topSell: newTopSell, crossSell: newCrossSell, upSell: newUpSell},
      currentSortingIndex: index
    });
  }

  _clearFilters = async () => {
    this._setStore(this.props.nearestStore);
    await this.setState({
      selectedValues: {
        ...this.state.selectedValues,
        sort: -1
      }
    });
    this._changeView(true);
  }

  _changeView = async (applyFilter) => {

    if (this.state.offersOpened) { // vai p tela de filtros
      this.setState({
        backupOffersList: this.state.offersList,
        selectedValues: {
          ...this.state.selectedValues,
          oldStore: this.props.currentStoreObject,
        },
        expanded: {
          ...this.state.expanded,
          stores: false
        },
        offersOpened: false
      });
    }
    else if (applyFilter) { // volta p tela de lista, salvando as escolhas
      await this._sortBySelector(this.state.selectedValues.sort);
      if (this.state.currentSortingIndex === -1) { // escolheu categoria ou ordenacao
        this.setState({
          backupOffersList: this.state.offersList,
          offersOpened: true,
          iconColor: "black",
          endReached: false,
          expanded: {
            stores: false,
            sorters: false
          },
          selectedValues: {
            oldStore: {},
            sort: -1
          }
        })
      }
      else { // não fez a escolha
        this.setState({
          backupOffersList: this.state.offersList,
          offersOpened: true,
          iconColor: "#E2D07F",
          endReached: true,
          expanded: {
            stores: false,
            sorters: false
          },
          selectedValues: {
            oldStore: {},
            sort: -1
          }
        })
      }
    }
    else { // volta p tela de listas sem salvar
      await this.props.setCurrentStore(this.state.selectedValues.oldStore);
      if (this.state.currentSortingIndex === -1) { // escolheu categoria ou ordenacao
        this.setState({
          offersList: this.state.backupOffersList,
          offersOpened: true,
          endReached: false,
          expanded: {
            stores: false,
            sorters: false
          },
          selectedValues: {
            oldStore: {},
            sort: -1
          },
          iconColor: "black"
        })
      }
      else { // não fez a escolha
        this.setState({
          offersList: this.state.backupOffersList,
          offersOpened: true,
          endReached: true,
          expanded: {
            stores: false,
            sorters: false
          },
          selectedValues: {
            oldStore: {},
            sort: -1
          },
          iconColor: "#E2D07F"
        })
      }
    }
  }

  _removeDuplicatedOffersInRecomm = () => {
    return this.state.offersList.filter(val => !this.props.recommendationList.topSell.some(e => e.ProductSKU === val.ProductSKU) && !this.props.recommendationList.upSell.some(e => e.ProductSKU === val.ProductSKU) && !this.props.recommendationList.crossSell.some(e => e.ProductSKU === val.ProductSKU));
  }

  renderScene = ({ route }) => {
    switch (route.key) {
      case "first":
        return (
          <Layout.OfferList
            keyExtractor={this._recommendationKeyExtractor}
            loading={!this.state.recommendationList.topSell.length}
            data={this.state.recommendationList.topSell}
            renderItem={this._renderRecomendationItem}
            ListFooterComponent={() => this._renderRecomendationFooter(this.props.recommendationList.topSell)}
            onEndReached={this._onEndReached}
            refreshing={this.props.offerListLoading}
            onRefresh={this._onRefresh}
            t={this.props.t}
            extraData={this.state}
            contentContainerStyle={styles.card}
          />
        )
      case "second":
        return (
          <Layout.OfferList
            keyExtractor={this._recommendationKeyExtractor}
            loading={!this.state.recommendationList.crossSell.length}
            data={this.state.recommendationList.crossSell}
            renderItem={this._renderRecomendationItem}
            ListFooterComponent={() => this._renderRecomendationFooter(this.props.recommendationList.crossSell)}
            onEndReached={this._onEndReached}
            refreshing={this.props.offerListLoading}
            onRefresh={this._onRefresh}
            t={this.props.t}
            extraData={this.state}
            contentContainerStyle={styles.card}
          />
        )
      case "third":
        return (
          <Layout.OfferList
            keyExtractor={this._recommendationKeyExtractor}
            loading={!this.state.recommendationList.upSell.length}
            data={this.state.recommendationList.upSell}
            renderItem={this._renderRecomendationItem}
            ListFooterComponent={() => this._renderRecomendationFooter(this.props.recommendationList.upSell)}
            onEndReached={this._onEndReached}
            refreshing={this.props.offerListLoading}
            onRefresh={this._onRefresh}
            t={this.props.t}
            extraData={this.state}
            contentContainerStyle={styles.card}
          />
        )
      case "fourth":
        return (
          <Layout.OfferList
            keyExtractor={this._recommendationKeyExtractor}
            loading={this.props.offerListLoading}
            data={this._removeDuplicatedOffersInRecomm()}
            renderItem={this._renderOfferItem}
            ListFooterComponent={this._renderOfferFooter}
            onEndReached={this._onEndReached}
            refreshing={this.props.offerListLoading}
            onRefresh={this._onRefresh}
            t={this.props.t}
            extraData={this.state}
            contentContainerStyle={styles.card}
          />
        )
      default:
        return (
          <Layout.OfferList
            keyExtractor={this._recommendationKeyExtractor}
            loading={this.props.offerListLoading}
            data={this.state.offersList}
            renderItem={this._renderOfferItem}
            ListFooterComponent={this._renderOfferFooter}
            onEndReached={this._onEndReached}
            refreshing={this.props.offerListLoading}
            onRefresh={this._onRefresh}
            t={this.props.t}
            extraData={this.state}
            contentContainerStyle={styles.card}
          />
        )
    }
  }

  _selectStoreFromModal = async () => {
    let { t } = this.props;
    if (this.state.modalSelectedStore === null) {
      Alert.alert(
        t("common:errors.error"),
        t("common:errors.storeSelect"),
        [
          { text: 'OK' },
        ],
        { cancelable: true }
      );
    }
    else {
      await this.props.setCurrentStore(this.state.modalSelectedStore);
      this.props.clearAllOffers();
      this.keySearch = "";
      this.setState({
        offersList: [],
        offerSearchText: "",
        backupOffersList: [],
        selectedValues: {
          ...this.state.selectedValues,
          sort: -1
        }
      });
      this._sortBySelector(this.state.selectedValues.sort);
      this.props.fetchAllOffers({ storeId: this.props.currentStoreObject.cluster_no });
      this.setState({ selectStoreModalVisible: false });
    }
  }

  _onIndexChange = activeTabIndex => { this.setState({activeTabIndex}) }

  _disableFilter = () => {
    if (this.props.offerListLoading) return true;
    if (this.state.showRecommendations && !this.props.recommendationsFetched) return true
    return false;
  }

  render() {
    const { t, i18n } = this.props;
    return (
      <Layout.ViewContainer>
        <Layout.SuccessMessageModal
          t={t}
          modalVisible={this.props.modals.connectionError}
          onOk={() => this.props.closeModal('connectionError')}
          title={t('common:connectionErrorTitle')}
          contentText={t("common:connectionErrorMessage")}
          okText={'OK'}
        />
        <Layout.SelectStoreModal
          t={t}
          language={i18n.language}
          modalVisible={this.state.selectStoreModalVisible}
          closeModal={() => { this.setState({ selectStoreModalVisible: false }) }}
          onOk={this._selectStoreFromModal}
          lists={this.props.fetchedStoreList}
          onValueChange={(itemValue, itemIndex) => { this.setState({ modalSelectedStore: itemValue }) }}
          selectedValue={this.state.modalSelectedStore}
          storeListLoading={this.props.storeListLoading}
          onPressOutside={() => { this.props.currentStoreObject !== null ? this.setState({ selectStoreModalVisible: false }) : null }}
        />
        <Layout.StatusBarContainer />
        <Layout.HeaderContainer>
          <Layout.BackIcon onPress={this.state.offersOpened ? this.props.navigation.goBack : () => this._changeView(false)} />
          <Layout.HeaderLogo navigation={this.props.navigation} />
          {this.state.offersOpened &&
            <Layout.FilterButton t={t} onPress={() => this._changeView(false)} color={this.state.iconColor} disable={this._disableFilter()}/>
          }
        </Layout.HeaderContainer>
        {this.state.offersOpened ?
          <Layout.TouchableTitle
            onPress={this._changeLayoutAnimated}
            titleText={t("offers:title") + " " + this._getStoreName()}
            caretUp={this.state.expanded.stores}
          />
          : this.state.expanded.stores || this.state.expanded.sorters ?
            <Layout.TouchableTitle
              onPress={() => this.setState({ expanded: { stores: false, sorters: false } })}
              titleText={t("offers:filterScreen.filterTitle")}
              caretRight={false}
            /> :
            <Layout.Title text={t("offers:filterScreen.filterTitle")} />
        }

        <View style={{ flex: 1, bottom: 0 }}>
          <Layout.StoreList
            loading={this.props.storeListLoading}
            expanded={this.state.expanded.stores}
            hideSearchBar={!this.state.expanded.stores}
            expandedHeight={'100%'}
            hiddenHeight={0}
            component={Layout.StoreListItem}
            location={this.props.location}
            onPress={this._setStore}
            storesArray={this.props.fetchedStoreList}
            modalConnectionError={this.props.modals.connectionError}
            hasConnectionError={this.props.fetchStoreListError}
            openModal={this.props.openModal}
            closeModal={this.props.closeModal}
            t={t}
            language={i18n.language}
          />
          <Layout.OfferFilterList
            data={this.sorting}
            renderItem={this._renderSorterItem}
            showItems={this.state.expanded.sorters}
          />

          {this.state.offersOpened ?
            <View style={{ flex: 1, zIndex: 999, backgroundColor: COLORS.WHITE.WHITE_SMOKE }}>
              <Layout.CustomSearchBar
                placeholder={this.props.t("offers:searchOfferPlaceholder")}
                onChangeText={text => this._searchFilterFunction(text)}
                value={this.state.offerSearchText}
                editable={ this.state.showRecommendations && !this.props.recommendationListLoading || !this.state.showRecommendations && !this.props.offerListLoading  }
              />
              <View style={{
                flex: 1
              }}>
                {this.state.showRecommendations && !this.state.showSearchResult  ?
                  this.state.tabs.length ?
                  <OfferTabs
                    routes={this.state.tabs}
                    renderScene={this.renderScene}
                    style={this.state.offersOpened ? { flex: 1 } : { height: 0 }}
                    onIndexChange = {this._onIndexChange}
                  /> :
                  <View style={{flex: 1, backgroundColor: COLORS.WHITE.WHITE_SMOKE}}>
                    <View style={{height: APP_DIMENSIONS.SCREEN_WIDTH * 0.031}} />
                    <View style={[{flex: 1, backgroundColor: COLORS.WHITE.WHITE}, styles.card]} >
                      <Layout.ViewLoader loading/>
                    </View>
                  </View>

                  :
                  <Layout.OfferList
                    keyExtractor={this._recommendationKeyExtractor}
                    loading={this.props.offerListLoading}
                    data={this.state.offersList}
                    renderItem={this._renderOfferItem}
                    ListFooterComponent={this._renderFooter}
                    onEndReached={this._onEndReached}
                    refreshing={this.props.offerListLoading}
                    onRefresh={this._onRefresh}
                    t={this.props.t}
                    extraData={this.state}
                    contentContainerStyle={styles.card}
                  />
                }

              </View>
            </View>
            : ((this.props.offerListLoading || this.props.storeListLoading || !this.props.currentStoreObject) ? <Layout.ViewLoader loading={true} /> :
              <View style={{ flex: 1 }}>
                <View style={{ flex: 1 }}>
                  <Layout.FilterListItem
                    onPress={() => this._setExpanded("stores", true)}
                    titleText={t("offers:filterScreen.shop")}
                    selectedValue={this._getStoreName()}
                  />
                  <Layout.FilterListItem
                    onPress={() => this._setExpanded("sorters", true)}
                    titleText={t("offers:filterScreen.exhibitionOrder")}
                    selectedValue={this.state.selectedValues.sort === -2 ? t("offers:filterScreen.exhibitionList.default") :
                      this.sorting[this.state.selectedValues.sort] ? this.sorting[this.state.selectedValues.sort] :
                        this.sorting[this.state.currentSortingIndex] ? this.sorting[this.state.currentSortingIndex] : t("offers:filterScreen.exhibitionList.default")}
                  />
                  <Layout.TouchableContainer style={{ flex: 2, alignItems: 'center' }} onPress={() => this._changeView(true)} opacity>
                    <Layout.FontText style={{
                      fontFamily: 'open-sans-semibold',
                      alignSelf: 'flex-end',
                      backgroundColor: '#cc0000',
                      color: 'white',
                      fontSize: APP_DIMENSIONS.SCREEN_WIDTH * 0.041,
                      paddingVertical: APP_DIMENSIONS.SCREEN_HEIGHT * 0.005,
                      paddingHorizontal: APP_DIMENSIONS.SCREEN_WIDTH * 0.025,
                      borderRadius: 7,
                      overflow: 'hidden',
                      margin: APP_DIMENSIONS.SCREEN_WIDTH * 0.045,

                    }}>
                      {t("offers:filterScreen.saveChanges")}
                    </Layout.FontText>
                  </Layout.TouchableContainer >
                  <Layout.EmptyContainer style={{ flex: 4 }} />
                </View>
                <Layout.BottomTabContainer>
                  <Layout.TouchableContainer highlight
                    onPress={this._clearFilters}
                  >
                    <Layout.BottomTabButtonContainer style={{ backgroundColor: COLORS.RED.FIRE_ENGINE_RED }} >
                      <Layout.BottomTabIcon name='reply' type='font-awesome' />
                      <Layout.BottomTabButtonText style={{ color: COLORS.WHITE.WHITE, marginLeft: APP_DIMENSIONS.SCREEN_WIDTH * 0.025 }}>{t("offers:filterScreen.clearChanges")}</Layout.BottomTabButtonText>
                    </Layout.BottomTabButtonContainer>
                  </Layout.TouchableContainer>
                </Layout.BottomTabContainer>
              </View>
            )
          }
        </View>
      </Layout.ViewContainer>
    );
  }
}

const mapStateToProps = state => ({
  modals: {
    activateOffer: state.offers.productsList.activateOffer,
    addToShoppingList: state.offers.productsList.addToShoppingList,
    addedToShoppingListSuccess: state.offers.productsList.addedToShoppingListSuccess,
    connectionError: state.offers.productsList.connectionError,
  },
  offerListLoading: state.offers.fetchOfferList.loading,
  fetchedOfferList: state.offers.fetchOfferList.offerList,
  storeListLoading: state.storeListFetch.loading,
  fetchedStoreList: state.storeListFetch.storeList,
  fetchStoreListError: state.storeListFetch.error,
  allOffersFetched: state.offers.fetchOfferList.allOffersFetched,
  currentStoreObject: state.currentStore.object,
  preferredStoreObject: state.currentStore.preferred,
  offerListPageNumber: state.offers.fetchOfferList.offerListPageNumber,
  totalPageNumber: state.offers.fetchOfferList.totalPageNumber,
  fetching: state.offers.fetchOfferList.fetching,
  location: state.location.location,
  nearestStore: state.storeListFetch.nearestStore,
  isSorted: state.storeListFetch.isSorted,
  fetchError: state.offers.fetchOfferList.error,
  user: state.user,
  recommendationListLoading: state.offers.fetchCustomerRecommendations.loading,
  recommendationList: state.offers.fetchCustomerRecommendations.recommendationList,
  recommendationFetchError: state.offers.fetchCustomerRecommendations.error,
  recommendationsFetched: state.offers.fetchCustomerRecommendations.recommendationsFetched,

});

const mapDispatchToProps = dispatch => {
  return {
    openModal: modalKey => {
      dispatch(Actions.setModalVisible(modalKey));
    },
    closeModal: modalKey => {
      dispatch(Actions.setModalInvisible(modalKey));
    },
    stopLoading: () => {
      dispatch(Actions.stopLoading());
    },
    setLoading: () => {
      dispatch(Actions.setLoading());
    },
    fetchAllOffers: ({ storeId } = {}) => {
      dispatch(Actions.fetchOffersByStore(storeId));
    },
    fetchOffersError: () => {
      dispatch(Actions.fetchOffersError());
    },
    fetchStoreList: () => {
      dispatch(Actions.fetchStoreList()); // ORIGINAL_COLOMBIA: fetchStoreListByCountry
    },
    setCurrentStore: (store) => {
      dispatch(Actions.setCurrentStore(store))
    },
    clearAllOffers: () => {
      dispatch(Actions.clearAllOffers())
    },
    getLocation: (props) => {
      dispatch(Actions.getLocation(props));
    },
    fetchOffersRequest: () => {
      dispatch(Actions.fetchOffersRequest())
    },
    setOffers: (offers) => {
      dispatch(Actions.fetchOffersSuccess(offers))
    },
    fetchCustomerRecommendations: ({ customerID } = {}) => {
      dispatch(Actions.fetchCustomerRecommendations(customerID));
    },
    clearAllRecommendations: () => {
      dispatch(Actions.clearRecommendationsList())
    },
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(OffersListView_CO);

const styles = StyleSheet.create({
  card: {
    marginLeft: APP_DIMENSIONS.SCREEN_WIDTH * 0.03,
    marginRight: APP_DIMENSIONS.SCREEN_WIDTH * 0.03,
    borderRadius: 10,
    shadowColor: "black",
    shadowOpacity: 0.2,
    elevation: 2,
    shadowRadius: 2,
    overflow: 'hidden'
  },
});
