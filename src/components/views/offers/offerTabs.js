import * as React from 'react';
import { View, StyleSheet, Dimensions, Text } from 'react-native';
import { TabView, SceneMap, PagerPan, TabBar } from 'react-native-tab-view';
import { COLORS } from '../../../constants/styles';
import * as Layout from "../../layout";
import APP_DIMENSIONS from '../../../constants/appDimensions';

export default class OfferTabs extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      index: 0,
      routes: props.routes
    }
  }

  _renderLabel = ({ route, focused, color }) => (
    <Layout.FontText style={{ color: COLORS.RED.FIRE_ENGINE_RED, fontSize: APP_DIMENSIONS.SCREEN_WIDTH * 0.035, textAlign: 'center', fontFamily: "open-sans-bold" }}>
      {route.title}
    </Layout.FontText>
  )

  _renderTabBar = props =>
      <TabBar
        {...props}
        indicatorStyle={{
          backgroundColor: COLORS.RED.FIRE_ENGINE_RED, height: APP_DIMENSIONS.SCREEN_HEIGHT * 0.005,
        }}
        renderLabel={this._renderLabel}
        tabStyle={{ 
          height: APP_DIMENSIONS.SCREEN_HEIGHT * 0.07, width: APP_DIMENSIONS.SCREEN_WIDTH * 0.6,
         }}
        scrollEnabled
        style={{
          elevation: 0,
          backgroundColor: COLORS.WHITE.WHITE_SMOKE,
        }}
      />

  renderPager = props => (
    <PagerPan {...props} />
  );

  _onIndexChange = (index) => {
    this.setState({ index });
    this.props.onIndexChange(index);
  }

  render() {
    return (
      <TabView
        navigationState={this.state}
        renderScene={this.props.renderScene}
        renderTabBar={this._renderTabBar}
        onIndexChange={this._onIndexChange}
        initialLayout={{ width: APP_DIMENSIONS.SCREEN_WIDTH }}
        renderPager={this.renderPager}
        lazy={false}
        extraData={this.props}
        style={this.props.style}
      />
    );
  }
}

const styles = StyleSheet.create({
  scene: {
    flex: 1,
  },
});