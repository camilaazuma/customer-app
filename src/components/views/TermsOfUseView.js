import React from "react";
import {Alert} from "react-native";
import AsyncStorageHelper from "../../helpers/AsyncStorageHelper";
import { COLORS } from "../../constants/styles";
import APP_DIMENSIONS from "../../constants/appDimensions";
import styled from "styled-components/native";
import * as Layout from "../layout";
import { translate } from 'react-i18next';

const SCREEN_WIDTH = APP_DIMENSIONS.SCREEN_WIDTH;
const SCREEN_HEIGHT = APP_DIMENSIONS.SCREEN_HEIGHT;

const OuterTextContainer = styled.ScrollView`
  flex: 1;
  backgroundColor: ${COLORS.WHITE.WHITE_SMOKE};
  padding: ${SCREEN_WIDTH/17}px;
`;

const InnerTextContainer = styled.View`
  elevation: 2;
  padding: ${SCREEN_WIDTH/14}px;
  margin: 5px;
  backgroundColor: ${COLORS.WHITE.WHITE};
  shadowColor: black;
  shadowOpacity: 0.2;
`;

const TermsOfUseTitle = styled(Layout.FontText)`
  color: ${COLORS.RED.FIRE_ENGINE_RED};
  fontSize: ${SCREEN_WIDTH*0.0375};
  fontFamily: open-sans-semibold;
  letterSpacing: ${SCREEN_WIDTH*0.0016};
`;


const Separator = styled.View`
  marginTop: ${SCREEN_HEIGHT/61};
  marginBottom: ${SCREEN_HEIGHT/45.7};
  borderBottomColor: ${COLORS.GREY.LIGHT_GREY};
  borderBottomWidth: 1;
`;


const TermsOfUseText = styled(Layout.FontText)`
  fontSize: ${SCREEN_HEIGHT/56.2};
  lineHeight: ${SCREEN_HEIGHT*0.027};
  textAlign: justify;
  fontFamily: open-sans;
  letterSpacing: ${SCREEN_WIDTH*0.0016};
`;

export const TERMS_OF_USE_ACCEPTED_KEY = "TERMS_OF_USE_ACCEPTED";

@translate(['termsOfUse'], { wait: true })
class TermsOfUseView extends React.Component {

  _navigateToSignUp = () => setTimeout(()=> {this.props.navigation.navigate('SignUp')}, 1);

  _acceptTermsOfUse = async () => {
     await AsyncStorageHelper.setItem(TERMS_OF_USE_ACCEPTED_KEY, "true");
     this._navigateToSignUp();
  }

  _onNotAccepted = () => {
    let {t} = this.props;
    Alert.alert(
      t("common:attention"),
      t("termsOfUse:declineMessage"),
      [
        {text: 'OK'},
      ],
      { cancelable: true }
    );
  }

  render() {
    const { t, i18n } = this.props;
    return (
      <Layout.ViewContainer>
        <Layout.StatusBarContainer />
        <Layout.HeaderContainer style={{ paddingHorizontal: APP_DIMENSIONS.SCREEN_WIDTH*0.059 }}>
          <Layout.HeaderLogo disableHomeNav/>
        </Layout.HeaderContainer>
        <Layout.Title text={t('termsOfUse:title')}/>
        <OuterTextContainer>
          <InnerTextContainer>
            <TermsOfUseTitle>{t('termsOfUse:title2')}</TermsOfUseTitle>
            <Separator />
            <TermsOfUseText>
              {t('termsOfUse:text')}
            </TermsOfUseText>
          </InnerTextContainer>
        </OuterTextContainer>
        <Layout.BottomTabContainer>
          <Layout.TouchableContainer opacity onPress={this._onNotAccepted}>
            <Layout.BottomTabButtonContainer leftButton> 
              <Layout.BottomTabButtonText>{t('termsOfUse:decline')}</Layout.BottomTabButtonText>
            </Layout.BottomTabButtonContainer>
          </Layout.TouchableContainer>
          <Layout.TouchableContainer highlight onPress={this._acceptTermsOfUse}>
            <Layout.BottomTabButtonContainer rightButton style={{backgroundColor: COLORS.RED.FIRE_ENGINE_RED}} >
              <Layout.BottomTabButtonText style={{color: COLORS.WHITE.WHITE}}>{t('termsOfUse:accept')}</Layout.BottomTabButtonText>
              <Layout.BottomTabIcon name='arrow-right' size={APP_DIMENSIONS.SCREEN_WIDTH*0.092}/>
            </Layout.BottomTabButtonContainer>
          </Layout.TouchableContainer>
        </Layout.BottomTabContainer>
      </Layout.ViewContainer>
    );
  }
}

export default TermsOfUseView;
