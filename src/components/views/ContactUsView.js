import React from "react";
import { View, Platform, Alert, NetInfo } from "react-native";
import { COLORS } from "../../constants/styles";
import APP_DIMENSIONS from "../../constants/appDimensions";
import * as Layout from "../layout";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scrollview";
import { connect } from "react-redux";
import { translate } from "react-i18next";
import { titleCase, timeoutPromise, validateEmail, getPhoneMask, formatPhone, showSnackbar } from "../../utils";
import RNPickerSelect from "react-native-picker-select";
import { Icon } from "react-native-elements";
import firebase from "react-native-firebase";

@translate(["contactUs", "common"], { wait: true })
class ContactUsView extends React.Component {
  constructor(props) {
    super(props);
    const { user } = props;
    let phone = formatPhone(props.i18n.language, user.phone);
    this.goBack = this.props.navigation.goBack;
    this.state = {
      name: titleCase(user.fullName),
      cellPhone: phone,
      email: user.email,
      personalIdentifier: user.personalIdentifier,
      message: "",
      selectedStore: null,
      selectedState: null,
      stateList: [],
      loading: false,
      regionProperty: props.i18n.language == "es-PE" ? "province" : "region"
    };
  }

  async componentDidMount() {
    if (this.props.fetchedStoreList.length === 0) {
      await this.props.fetchStoreList();
    }
    this._setStateList();
  }

  _setStateList = () => {
    stateList = []
    this.props.fetchedStoreList.map(item => {
      if(stateList.length === 0) {
        state = {
          label: item[this.state.regionProperty].toUpperCase(),
          value: item[this.state.regionProperty],
          key: 0
        }
        stateList.push(state);
      }
      else {
        var registered = stateList.some((element) => {
          return element.value === item[this.state.regionProperty];
        })
        state = {
          label: item[this.state.regionProperty].toUpperCase(),
          value: item[this.state.regionProperty],
          key: stateList.length
        }
        if(!registered) stateList.push(state);
      }
    })
    stateList.sort((a,b) => a.label.localeCompare(b.label));
    this.setState({
      stateList: stateList
    })
    this._setStoreAndState(stateList);
  }

  _setStoreAndState = (stateList) => {
    if (this.props.currentStoreObject !== null && this.props.currentStoreObject.hasOwnProperty(this.state.regionProperty) ) {
      const currentStoreState = stateList.find(estado => estado.value.toUpperCase() === this.props.currentStoreObject[this.state.regionProperty].toUpperCase());
      if (typeof currentStoreState !== 'undefined')
        this.setState({
          selectedState: currentStoreState.value,
          selectedStore: this.props.currentStoreObject
        })
    }
    else if (this.props.user.stateName!=="" || this.props.user.stateName!==null || typeof this.props.user.stateName!=='undefined') {
      const userState = stateList.find(estado => estado.value.toUpperCase() === this.props.user.stateName.toUpperCase());
      if (typeof userState !== 'undefined')
        this.setState({ selectedState: userState.value })
    }
  }

  _onFullNameInputChange = text => this.setState({ name: text });

  _onPhoneInputChange = text => this.setState({ cellPhone: text });

  _onEmailInputChange = text => this.setState({ email: text });

  _onMessageInputChange = text => this.setState({ message: text });


  _sendMessage = async (personalIdentifier, name, cellPhone, email, store, message, goBack) => {
    let {t, i18n} = this.props;
    let isConnected = await NetInfo.isConnected.fetch();
    let storeNo = null;
    if(store){
      if (this.props.i18n.language==="es-CO") 
        storeNo = store.contact_us_id;
      else
        storeNo = store.store_no; // ORIGINAL_COLOMBIA: idlocation  
    }
    if (!isConnected)  {
      showSnackbar(t("common:errors.noConnection"));
      return;
    }
    if (
      !name ||
      name == "" ||
      !personalIdentifier ||
      personalIdentifier == "" ||
      !cellPhone ||
      cellPhone == "" ||
      !email ||
      email == "" ||
      !validateEmail(email) ||
      !message ||
      message == "" ||
      !storeNo
    ) {
      Alert.alert(
        t("common:errors.error"),
        t("contactUs:inputValidation"),
        [{ text: "OK" }],
        { cancelable: true }
      );
      return;
    }
    this.setState({loading: true});
    // TODO: Refatorar para usar as actions, reducer, http-request

    const CONTACT_US_URLS = {
      "pt-BR": "https://us-central1-br-makro-rj.cloudfunctions.net/contactUsWebsite",
      "es-CO": "https://us-central1-br-makro-rj.cloudfunctions.net/contactUsWebsiteColombia",
      "es-PE": "https://us-central1-br-makro-rj.cloudfunctions.net/contactUsWebsitePeru"
    }

    const URL_SEND_CONTACT_US_MESSAGE = CONTACT_US_URLS[this.props.i18n.language]

    const bodyParams = JSON.stringify({
      key:
        "YXMgUHLDs3ByaWFzGEZvcm5lY2ltZW50byBkZSBQcm9kdXRvcwpMb2fDrXN0aWNhFkFzc2Vzc29yaWEgZGUgSW1wcmVuc2EgT2ZlcnRhIGRlIFRlcnJlbm8gb3UgUHJvcHJ",
      name: name,
      cellPhone: cellPhone,
      email: email,
      storeId: storeNo,
      message: message,
      personalIdentifier: personalIdentifier
    })
    timeoutPromise(60000,
      fetch(`${URL_SEND_CONTACT_US_MESSAGE}`, {
        method: "POST",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json"
        },
        body: bodyParams
      })
    )
    .then(res => res.text())
    .then(result => {
      this.setState({loading: false});
      // Erro
      if (result == "Error") {
        Alert.alert(
          t("common:errors.error"),
          t("common:errors.messageSent"),
          [{ text: "OK" }],
          { cancelable: true }
        );
        return;
      } else {
        Alert.alert(
          t("common:successes.thankYou"),
          this.props.i18n.language !== "es-CO" ? t("common:successes.messageSent", {protocolNo: result}) : "",
          [{ text: "OK", onPress: () => goBack() }],
          { cancelable: true }
        );
      }
    })
    .catch(err => {
      this.setState({loading: false});
      Alert.alert(
        t("common:errors.error"),
        t("common:errors.messageSent"),
        [{ text: "OK" }],
        { cancelable: true }
      );
      console.log(err)
    })

  }

  render() {
    const { t, user, fetchedStoreList, i18n } = this.props;

    if(i18n.language!=="es-CO"){
      var storeList = fetchedStoreList.map(item =>
        ({label: titleCase(item.name), value: item, key: item.store_no})
      ).sort((a,b) => a.label.localeCompare(b.label))
    }else{
      var storeList = fetchedStoreList.map(item =>
        ({label: titleCase(item.name), value: item, key: item.store_no}) // ORIGINAL_COLOMBIA: locationname idlocation
      ).sort((a,b) => a.label.localeCompare(b.label))
    }
    if (this.state.selectedState!==null && typeof this.state.selectedState==='string' && i18n.language!=="es-CO")
      storeList = storeList.filter((item)=>item.value[this.state.regionProperty].toUpperCase()===this.state.selectedState.toUpperCase());

    return (
      <Layout.ViewContainer>
        <Layout.Loader loading={this.state.loading}  />
        <Layout.StatusBarContainer />
        <Layout.HeaderContainer>
          <Layout.BackIcon onPress={this.props.navigation.goBack} />
          <Layout.HeaderLogo navigation={this.props.navigation} />
        </Layout.HeaderContainer>
        <Layout.Title text={t("contactUs:title")} />
        <Layout.KeyboardAvoidingContainer
          behavior={Platform.OS === "ios" ? "height" : "padding"}
          keyboardVerticalOffset={APP_DIMENSIONS.SCREEN_HEIGHT * 0.137}
          style={{ justifyContent: "flex-start", flex: 1 }}
        >
          <KeyboardAwareScrollView
            contentContainerStyle={{ flexGrow: 1 }}
            keyboardShouldPersistTaps="always"
          >
            <View
              style={{
                justifyContent: "space-between",
                flex: 0.25,
                paddingTop: APP_DIMENSIONS.SCREEN_HEIGHT * 0.02,
                paddingBottom: APP_DIMENSIONS.SCREEN_HEIGHT * 0.03,
                alignContent: "center"
              }}
            >
            { this.props.i18n.language !== "es-CO" &&
              <RNPickerSelect
                items={this.state.stateList}
                // hideIcon
                value={this.state.selectedState}
                placeholder={{ label: t("contactUs:stateInputPlaceHolder"), value: null }}
                onValueChange={(itemValue, itemIndex) => { this.setState({selectedState: itemValue}) }}
              >
                <View style={{flexDirection: "row", justifyContent: "space-around", backgroundColor: "white"}}>
                  <Layout.FormTextInput value={this.state.selectedState ? this.state.selectedState.toUpperCase() : ""} placeholder={t("contactUs:stateInputPlaceHolder")}/>
                  <Icon name="caret-down" type="font-awesome" size={APP_DIMENSIONS.SCREEN_WIDTH * 0.05}
                    containerStyle={{
                    borderBottomColor: COLORS.WHITE.WHITE_SMOKE,
                    borderBottomWidth: 1 }}
                  />
                </View>
              </RNPickerSelect>
            }
              <RNPickerSelect
                items={storeList}
                // hideIcon
                value={this.state.selectedStore}
                placeholder={{ label: t("contactUs:storeInputPlaceHolder"), value: null }}
                onValueChange={(itemValue, itemIndex) => { this.setState({selectedStore: itemValue}) }}
              >
                <View style={{flexDirection: "row", justifyContent: "space-around", backgroundColor: "white"}}>
                  {this.props.i18n.language !== "es-CO" ?
                    <Layout.FormTextInput value={this.state.selectedStore ? titleCase(this.state.selectedStore.name) : ""} placeholder={t("contactUs:storeInputPlaceHolder")}/>
                    :
                    <Layout.FormTextInput value={this.state.selectedStore ? titleCase(this.state.selectedStore.name) : ""} placeholder={t("contactUs:storeInputPlaceHolder")}/> // ORIGINAL_COLOMBIA: locationname

                  }
                  <Icon name="caret-down" type="font-awesome" size={APP_DIMENSIONS.SCREEN_WIDTH * 0.05}
                  containerStyle={{
                    borderBottomColor: COLORS.WHITE.WHITE_SMOKE,
                    borderBottomWidth: 1 }}
                  />
                </View>
              </RNPickerSelect >
              <Layout.FormTextInput
                placeholder={t("contactUs:nameInputPlaceHolder")}
                inputRef={r => {
                  this.name = r;
                }}
                onSubmitEditing={event => {
                  event && this.cellPhone.focus();
                }}
                onChangeText={this._onFullNameInputChange}
                value={this.state.name}
              />
              <Layout.FormTextInput
                placeholder={t("contactUs:cellPhoneInputPlaceHolder")}
                inputRef={r => {
                  this.cellPhone = r;
                }}
                onSubmitEditing={event => {
                  event && this.email.focus();
                }}
                keyboardType={Platform.OS === "ios" ? "decimal-pad" : "numeric"}
                maskActive={true}
                type={getPhoneMask(this.props.i18n.language)}
                onChangeText={this._onPhoneInputChange}
                value={this.state.cellPhone}
                onFocus={this._didStartEditing}
                returnKeyType="done"
              />
              <Layout.FormTextInput
                placeholder={t("contactUs:emailInputPlaceHolder")}
                keyboardType={"email-address"}
                inputRef={r => {
                  this.email = r;
                }}
                onSubmitEditing={event => {
                  event && this.message.focus();
                }}
                onChangeText={this._onEmailInputChange}
                value={this.state.email}
                onFocus={this._didStartEditing}
                keyboardType={"email-address"}
                textContentType={"emailAddress"}
              />
            </View>
            <View
              style={{
                justifyContent: "flex-start",
                flex: 1,
                paddingBottom: APP_DIMENSIONS.SCREEN_HEIGHT * 0.085,
                alignContent: "center",
                borderBottomColor: COLORS.WHITE.WHITE_SMOKE,
                borderBottomWidth: 1,
                marginBottom: APP_DIMENSIONS.SCREEN_HEIGHT * 0.03
              }}
            >
              <Layout.FormTextInput
                placeholder={t("contactUs:messageInputPlaceHolder")}
                multiline
                inputRef={r => {
                  this.message = r;
                }}
                onChangeText={this._onMessageInputChange}
                returnKeyType={"done"}
                blurOnSubmit={true}
              />
            </View>
          </KeyboardAwareScrollView>
        </Layout.KeyboardAvoidingContainer>
        <Layout.BottomTabContainer>
          <Layout.TouchableContainer
            onPress={() =>
              this._sendMessage(
                this.state.personalIdentifier,
                this.state.name,
                this.state.cellPhone,
                this.state.email,
                this.state.selectedStore ? this.state.selectedStore : null,
                this.state.message,
                this.goBack
              )
            }
          >
            <Layout.BottomTabButtonContainer
              style={{ backgroundColor: COLORS.RED.FIRE_ENGINE_RED }}
            >
              <Layout.BottomTabButtonText style={{ color: COLORS.WHITE.WHITE }}>
                {t("contactUs:sendMessageButton")}
              </Layout.BottomTabButtonText>
              <Layout.BottomTabIcon name="arrow-right" />
            </Layout.BottomTabButtonContainer>
          </Layout.TouchableContainer>
        </Layout.BottomTabContainer>
      </Layout.ViewContainer>
    );
  }
}

const mapStateToProps = state => ({
  user: state.user,
  fetchedStoreList: state.storeListFetch.storeList,
  currentStoreObject: state.currentStore.object,
});

const mapDispatchToProps = dispatch => {
  return {
    fetchStoreList: () => {
      dispatch(Actions.fetchStoreList());
    }
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ContactUsView);

// export default ContactUsView;
