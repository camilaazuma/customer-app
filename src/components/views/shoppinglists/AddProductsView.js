import React from "react";
import {
  View,
  Text,
  ScrollView,
  Keyboard,
  ActivityIndicator,
  FlatList,
  Alert
} from "react-native";
import { Icon, List, ListItem } from "react-native-elements";
import { COLORS } from "../../../constants/styles";
import APP_DIMENSIONS from "../../../constants/appDimensions";
import styled, { ThemeProvider } from "styled-components/native";
import * as Layout from "../../layout";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scrollview";
import * as Actions from "../../../actions";
import { connect } from "react-redux";
import { NavigationActions } from "react-navigation";
import { translate } from 'react-i18next';

const SCREEN_HEIGHT = APP_DIMENSIONS.SCREEN_HEIGHT;
const SCREEN_WIDTH = APP_DIMENSIONS.SCREEN_WIDTH;
const params = {
  updated: "2018-11-01 19:22:02",
}

@translate(['shoppingLists', 'common'], { wait: true })
class AddProductsView extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      successMessage: false,
      categories: [],
      products: [],
      endReached: false,
      productsOpened: false,
      productsToShow: [],
      baseForSearch: [],
      shopList: [],
      selectQuantityModal: false,
      productQuantity: "",
      selectedProduct: null
    };
  }

  componentDidMount() {
    this.props.getCategories();
    this.shopList = this.props.navigation.getParam("shoppingList", "");
    this.setState({shopList: this.props.navigation.getParam("shoppingList", ""), baseForSearch: this.props.products});
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    var obj = {};
    // recebe os status dos produtos e inclui na lista de produtos
    if(prevState.products!== nextProps.products){
      obj.products = nextProps.products;
    }
    if(prevState.categories!== nextProps.categories){
      obj.categories = nextProps.categories;
    }

    return Object.keys(obj).length? obj : null;
  }

  _showProducts = (item) => {
    var filtered = this.state.products.filter((e)=>e["CATEGORY_ID"] == item);
    this.shopList.shop_list_products.map((product)=>{
      for (var i = 0; i < filtered.length; i++) {
        if(filtered[i]["ID"] == product["art_no"] ){
          if(product["deleted"] == "0"){
            filtered[i]["added"] = product["quantity"] == "0" ? false : true;
            filtered[i]["quantity"] = product["quantity"] == "0" ? "" : product["quantity"];
          }
        }
      }
    });

    this.setState({productsOpened: true, productsToShow: filtered, baseForSearch: filtered});
  }

  _addProducts = async () =>{
    let {t} = this.props;
    var item = this.state.selectedProduct;
    var qtd = this.state.productQuantity;
    var list = this.state.shopList;
    var added = false;
    if(qtd==="" || qtd===null || typeof qtd==='undefined'){
      return Alert.alert(
        t("common:errors.error"),
        t("common:errors.invalidInput"),
        [
          {text: 'OK'},
        ],
        { cancelable: true }
      );
    }
    else {
      var index = list.shop_list_products.findIndex(x => x.art_no === item.ID.toString());
      if(qtd === "0"){
        added = false;
        if(index !== -1){
          list.shop_list_products[index].deleted = "1";
        }
      }else {
        added = true;
        if(index != -1){
          list.shop_list_products[index].quantity = qtd;
          list.shop_list_products[index].deleted = "0";
        }else{
          list.shop_list_products.push({
            "art_no": item.ID.toString(),
            "deleted": "0",
            "description": item.DESCRIPTION,
            "quantity": qtd
          });
        }
        await this.props.fetchProductByBarcode(this.props.user, item.ID.toString(), this.props.currentStoreObject.store_no);
      }
      await this.props.saveShopList({user: this.props.user, updated: params.updated, shopList: list});
      this.state.productsToShow.map((i)=>{
        if(i.ID === item.ID){
          i.added = added;
          i.quantity = qtd ? qtd : "0";
        }
      });
      Keyboard.dismiss();
      Alert.alert(
        t("common:success"),
        t("common:successes.addedProduct"),
        [
          {text: 'OK', onPress: () => this.setState({productsToShow: this.state.productsToShow, shopList: list, selectQuantityModal: false})},
        ],
        { cancelable: true }
      );
    }
  }

  _changeQuantity = (item, qtd) =>{
    this.state.productsToShow.map((i)=>{
      if(i.ID == item.ID){
        i.added = false;
        i.quantity = qtd ? qtd : "";
      }
    });
    this.setState({productsToShow: this.state.productsToShow});
  }

  _hideProducts = () => {
    this.setState({productsOpened: false, baseForSearch: this.state.products});
  }

  _keyExtractor = (item, index) => index.toString();

  _renderCategoryItem = ({ item }) => (
    <ListItem
      title={item.DESCRIPTION}
      onPress={() => this._showProducts(item.ID)}
    />
  );
  _renderProductItem = ({ item }) => {
    return (
      <Layout.ShoppingListAddProductsItem
        item={item}
        code={item.ID}
        productName={item.DESCRIPTION}
        added={item.added}
        quantity={item.quantity}
        onPress={()=> this.setState({selectQuantityModal:true, selectedProduct: item})}
        t = { this.props.t }
      />
    )
  };

  _renderFooter = () => {
    if (this.state.productsToShow.length===0 && this.state.productsOpened) {
      return (
        <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
          <Layout.FontText style={{ color: COLORS.BLACK.BLACK, fontSize: APP_DIMENSIONS.SCREEN_WIDTH*0.053, fontFamily: 'open-sans' }}>
            {this.props.t("shoppingLists:addProducts.productsNotFound")}
          </Layout.FontText>
        </View>
      )
    }

    return (
      <View >
        { this.state.endReached == false ?
          <ActivityIndicator size="large"/> : null
        }
      </View>
    )
  };

  _searchFilterFunction = text => {
    var newData = this.state.baseForSearch.filter(item => {
      const itemData = `${item["DESCRIPTION"].toUpperCase()}`;
      const textData = text.toUpperCase();
      return itemData.indexOf(textData) > -1;
    });

    var willReturnToList = text== "" ? true : false;
    this.setState({
      productsOpened: true,
      productsToShow: newData,
      endReached: !willReturnToList
    });
  };

  _onPlusButtonPress = () => {
    if (this.state.productQuantity === "")
      this.setState({productQuantity: "1"})
    else
      this.setState({productQuantity: (parseInt(this.state.productQuantity)+1).toString()})
  } 

  _onMinusButtonPress = () => {
    if (this.state.productQuantity !== "" && parseInt(this.state.productQuantity)>0)
      this.setState({productQuantity: (parseInt(this.state.productQuantity)-1).toString()})      
  } 

  _onChangeProductQuantity = (value) => {
    if (/^[0-9]*$/.test(value)) {
      this.setState({productQuantity: value});
    }
  }

  render() {
    const { loading, successMessage, t } = this.props;

    if(!this.props.categoriesFetched){
      return(
        <Layout.ViewContainer>
          <Layout.Title
            text={this.props.navigation
              .getParam("shoppingList", "")
              .name.toUpperCase()+ ' - ' + t("shoppingLists:addProducts.category")}
          />
          <ActivityIndicator size="large" style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}/>
        </Layout.ViewContainer>
      )
    }
    return (
      <Layout.ViewContainer>
        <Layout.Loader loading={this.props.loadingList}/>
        <Layout.SelectProductQuantityModal
          t={t}
          modalVisible={this.state.selectQuantityModal}
          onOk={this._addProducts}
          onCancel={() => this.setState({selectQuantityModal: false, selectedProduct: null, productQuantity: ""})}
          value={this.state.productQuantity}
          onChangeText = {this._onChangeProductQuantity}   
          onPlusButtonPress={this._onPlusButtonPress}
          onMinusButtonPress={this._onMinusButtonPress}

        />

        <Layout.CreateShoppingListModal
          modalVisible={this.props.modals.createList}
          onCancel={() =>
            setTimeout(() => {
              this.props.closeModal("createList");
            }, 1)
          }
          onOk={() =>
            setTimeout(() => {
              this.props.closeModal("createList");
              this.props.navigation.navigate("ShoppingLists");
            }, 1)
          }
        />

        {this.state.productsOpened ?
          <Layout.TouchableTitle
            titleText={this.props.navigation
              .getParam("shoppingList", "")
              .name.toUpperCase()+ ' - '+ t("shoppingLists:addProducts.title").toUpperCase()}
            iconRight={false}
            caretRight={false}
            onPress={this._hideProducts}
          />
          :
          <Layout.Title
            text={this.props.navigation
              .getParam("shoppingList", "")
              .name.toUpperCase()+ ' - '+ t("shoppingLists:addProducts.category")}
          />
        }

        <Layout.CustomSearchBar
          placeholder={t("common:searchProductPlaceholder")}
          onChangeText={text => this._searchFilterFunction(text)}
        />
        <View style={{ flex: 1 }}>
          {this.state.productsOpened ?
            <FlatList
              keyExtractor={this._keyExtractor}
              data={this.state.productsToShow}
              renderItem={this._renderProductItem}
              ListFooterComponent={this._renderFooter}
              onEndReached = {()=> this.setState({endReached: true})}
            />
            :
            <FlatList
              keyExtractor={this._keyExtractor}
              data={this.state.categories}
              renderItem={this._renderCategoryItem}
              ListFooterComponent={this._renderFooter}
              onEndReached = {()=> this.setState({endReached: true})}
            />
          }
        </View>
      </Layout.ViewContainer>
    );
  }
}

const mapStateToProps = state => ({
  modals: {
    createList: state.shoppingLists.createListModal.createList
  },
  categories: state.products.fetchCategories.categoryList,
  categoriesFetched: state.products.fetchCategories.categoriesFetched,
  products: state.products.fetchProduct.allProductsList,
  user: state.user,
  currentStoreObject: state.currentStore.object,
  loadingList: state.shoppingLists.CRUDShoppingList.loading,
});

const mapDispatchToProps = dispatch => {
  return {
    openModal: modalKey => {
      dispatch(Actions.setModalVisible(modalKey));
    },
    closeModal: modalKey => {
      dispatch(Actions.setModalInvisible(modalKey));
    },
    getCategories: () => {
      dispatch(Actions.getCategories());
    },
    getAllProducts: () => {
      dispatch(Actions.getAllProducts());
    },
    saveShopList: ({user, updated, shopList}) => {
      dispatch(Actions.saveShopList(user, updated, shopList));
    },
    fetchProductByBarcode: (user, barcode, storeNo) => {
      dispatch(Actions.fetchProductByBarcode(user, barcode, storeNo))
    }
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AddProductsView);

// export default AddProductsView;
