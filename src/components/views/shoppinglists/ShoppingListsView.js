import React from "react";
import {
  View,
  Text,
  ScrollView,
  FlatList,
  StyleSheet,
  ActivityIndicator,
  Alert
} from "react-native";
import { Icon, List, ListItem  } from "react-native-elements";
import { COLORS } from "../../../constants/styles";
import APP_DIMENSIONS from "../../../constants/appDimensions";
import styled, { ThemeProvider } from "styled-components/native";
import * as Layout from "../../layout";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scrollview";
import * as Actions from "../../../actions";
import { connect } from "react-redux";
import { NavigationActions } from "react-navigation";
import { shoppingLists } from "../../../config/mockdata/shoppingLists";
import { translate } from 'react-i18next';
import uuidv1 from 'uuid/v1';
import { getDateTime } from "../../../utils";

const SCREEN_HEIGHT = APP_DIMENSIONS.SCREEN_HEIGHT;
const SCREEN_WIDTH = APP_DIMENSIONS.SCREEN_WIDTH;

const params = {
  updated: "2018-11-01 19:22:02",
}

@translate(['shoppingLists', 'common'], { wait: true })
class ShoppingListsView extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      fetchError: false,
      successMessage: false,
      fetchedLists: [],
      newShopList: {
        uuid: "",
        name: "",
        created: "",
        deleted: "0",
        shop_list_products: []
      }
    };
  }

  generateUUID() {
    return uuidv1().replace(/-/g,"");
  }

  _confirmCreateList = async () => {
    let {t} = this.props;
    if(this.state.newShopList.name === "") {
      Alert.alert(
        t("common:errors.error"),
        t("common:errors.emptyShopListName"),
        [
          {text: 'OK'},
        ],
        { cancelable: true }
      );
      return null;
    }
    await this.setState({
      newShopList: {
        ...this.state.newShopList,
        uuid: this.generateUUID(),
        created: getDateTime()
    }})
    this.props.createShopList({user: this.props.user, updated: params.updated, shopList: this.state.newShopList});
    this.props.closeModal("createList");
    this._navigateWithDetails(this.state.newShopList);
  }

  componentDidMount(){
    // TODO: substituir pelas informações do usuário logado: passaportRaw para Carousel, {passport, device_id, phone, email} para Token
      this.props.getShopList({user: this.props.user, updated: params.updated});
      this.props.getAllProducts();
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    if(prevState.fetchedLists!== nextProps.fetchedShopListList){
      var numberOfLists = nextProps.fetchedShopListList.filter(list => (list.deleted !== "1"));
      return {
        fetchedLists: nextProps.fetchedShopListList,
        hasListToShow: numberOfLists.length ? true : false,
        fetchError: false
      };
    }
    if(!prevState.fetchError && nextProps.fetchListError) {
      nextProps.openModal("connectionError");
      return {
        fetchError: true
      }
    }
    return null;
  }

  _countProducts = (shop_list_products) => {
    var products = 0;
    if (shop_list_products.length == 0) {
      return products;
    }
    for (var i = 0; i < shop_list_products.length; i++) {
      if(shop_list_products[i]["deleted"] == "1") {}
      else {
        products = products + parseInt(shop_list_products[i]["quantity"]);
      }
    }
    return products;
  }

  _onChangeText=(text) => this.setState({
    newShopList: {
      ...this.state.newShopList,
      name: text
    }})

  _navigateWithDetails = async (shoppingList) => {
    await this.props.getListDetails({productsList: shoppingList});
    this.props.navigation.navigate("ListDetail")
    this.setState({newShopList: {
      uuid: "",
      name: "",
      created: "",
      deleted: "0",
      shop_list_products: []
    }})
  }

  _onCloseConnectionError = () => {
    this.props.closeModal('connectionError');
  }

  _renderShoppingListsListItem = ({ item: shoppingList }, index) => {
    return (
      <Layout.ShoppingListsListItem
        itemTitle={shoppingList.name}
        subTitle={shoppingList.created}
        info={this._countProducts(shoppingList.shop_list_products)}
        onPress={this._navigateWithDetails.bind(this, shoppingList)}
      />
    );
  }

  render() {
    const { loadingList, loadingProduto, successMessage, t , fetchedShopListList} = this.props;

    if(!this.props.allListsFetched && !this.props.fetchListError){
      return(
        <Layout.ViewContainer>
          <Layout.Title text={t('shoppingLists:title')} />
          <ActivityIndicator size="large" style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}/>
        </Layout.ViewContainer>
      )
    } 
    return (
      <Layout.ViewContainer>
        <Layout.Loader loading={loadingList || loadingProduto} />
        <Layout.CreateShoppingListModal
          t={t}
          modalVisible={this.props.modals.createList}
          onCancel={() =>
            setTimeout(() => {
              this.props.closeModal("createList");
            }, 1)
          }
          onOk={this._confirmCreateList
          }
          nomeLista={this.state.newShopList['name']}
          onChangeText={this._onChangeText}
        />
        <Layout.SuccessMessageModal
          t={t}
          modalVisible={this.props.modals.connectionError}
          onOk={() => this._onCloseConnectionError()}
          title={t('common:connectionErrorTitle')} 
          contentText={t("common:connectionErrorMessage")} 
          okText={'OK'}     
        />
        <Layout.Title text={t('shoppingLists:title')} />
            
        { this.state.fetchedLists.length == 0 || !this.state.hasListToShow ? (
          <Layout.TouchableContainer
            onPress={() =>
              setTimeout(() => {
                this.props.fontLoader();
                this.props.openModal("createList");
              }, 1)
            }
          >
            <Layout.ContentContainer
              style={{ backgroundColor: COLORS.RED.FIRE_ENGINE_RED, marginTop: StyleSheet.hairlineWidth, }}
            >
              <Layout.BigIcon name="plus-circle" color={COLORS.WHITE.WHITE} />
              <Layout.IconTitleText>
              {t('shoppingLists:createList.createListButton')}
              </Layout.IconTitleText>
            </Layout.ContentContainer>
          </Layout.TouchableContainer>
        ) : (
          <View style={{ flex: 1 }}>
            <FlatList
              keyExtractor={(item, index) => item.uuid}
              data={this.state.fetchedLists.filter((item, index) => item.deleted !== "1")}
              renderItem={this._renderShoppingListsListItem}
              initialNumToRender={8}
              maxToRenderPerBatch={8}
              removeClippedSubviews={true}
              refreshing={true}
            />
            <Layout.BottomTabContainer>
              <Layout.TouchableContainer highlight
                onPress={() =>
                  setTimeout(() => {
                    this.props.openModal("createList");
                  }, 1)
                }
              >
                <Layout.BottomTabButtonContainer style={{backgroundColor: COLORS.RED.FIRE_ENGINE_RED}} >
                  <Layout.BottomTabIcon name='plus-circle' type='font-awesome'/>
                  <Layout.BottomTabButtonText style={{color: COLORS.WHITE.WHITE}}>{t("shoppingLists:shoppingLists.createListButton")}</Layout.BottomTabButtonText>
                </Layout.BottomTabButtonContainer>
              </Layout.TouchableContainer>
            </Layout.BottomTabContainer>
          </View>
        )}

      </Layout.ViewContainer>
    );
  }
}

const mapStateToProps = state => ({
  modals: {
    createList: state.shoppingLists.createListModal.createList,
    connectionError: state.shoppingLists.createListModal.connectionError,
  },
  fetchedShopListList: state.shoppingLists.CRUDShoppingList.shopListList,
  allListsFetched: state.shoppingLists.CRUDShoppingList.allShopListFetched,
  fetchListError: state.shoppingLists.CRUDShoppingList.error,
  fontLoaded: state.fontLoader.fontLoaded,
  loadingList: state.shoppingLists.CRUDShoppingList.loading,
  loadingProduto: state.products.fetchProduct.loading,
  user: state.user,
});

const mapDispatchToProps = dispatch => {
  return {
    openModal: modalKey => {
      dispatch(Actions.setModalVisible(modalKey));
    },
    closeModal: modalKey => {
      dispatch(Actions.setModalInvisible(modalKey));
    },
    getListDetails: ({productsList}={}) => {
      dispatch(Actions.getProductsFromList(productsList));
    },
    fontLoader: () => {
      dispatch(Actions.setFontLoadedFlag());
    },
    getShopList: ({user, updated}={}) => {
      dispatch(Actions.getShopList(user, updated));
    },
    createShopList: ({user, updated, shopList}) => {
      dispatch(Actions.saveShopList(user, updated, shopList));
    },
    getCategories: () => {
      dispatch(Actions.getCategories());
    },
    getAllProducts: () => {
      dispatch(Actions.getAllProducts());
    }
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ShoppingListsView);
