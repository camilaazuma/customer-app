import React from "react";
import {
  View,
  ScrollView,
  FlatList,
  LayoutAnimation
} from "react-native";
import { Icon, List, ListItem } from "react-native-elements";
import { COLORS } from "../../../constants/styles";
import APP_DIMENSIONS from "../../../constants/appDimensions";
import styled, { ThemeProvider } from "styled-components/native";
import * as Layout from "../../layout";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scrollview";
import * as Actions from "../../../actions";
import { connect } from "react-redux";
import { NavigationActions } from "react-navigation";
import { translate } from 'react-i18next';
import { shoppingLists } from "../../../config/mockdata/shoppingLists";
import { capitalizeFirstLetter, formatNumberForPrice } from "../../../utils";

const SCREEN_HEIGHT = APP_DIMENSIONS.SCREEN_HEIGHT;
const SCREEN_WIDTH = APP_DIMENSIONS.SCREEN_WIDTH;

@translate(['shoppingLists', 'common'], { wait: true })
class TotalPriceView extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      successMessage: false,
      expanded: false,
    };
  }

  async componentDidMount() {
    const { user, fetchedListDetails, currentStoreObject } = this.props;
    var store_no="1";
    if (currentStoreObject!==null && typeof currentStoreObject.store_no === "string")
      store_no = currentStoreObject.store_no;
    await this.props.resetAppraiseShopListResponse();
    await this.props.appraiseShopList(store_no, user.personalIdentifier, fetchedListDetails.uuid)
  }

  _getDate = () => {
    let d = new Date();
    let dateString = ("0" + d.getDate()).slice(-2) + "/" + ("0"+(d.getMonth()+1)).slice(-2) + "/" +
    d.getFullYear() + " " + ("0" + d.getHours()).slice(-2) + ":" + ("0" + d.getMinutes()).slice(-2);
    return dateString;
  };

  _getDateExpired = () => 
    this.props.appraiseShoppingListResponse === "" ? '-' : this.props.appraiseShoppingListResponse.date_expired

  _getTotalAmount = () => 
    this.props.appraiseShoppingListResponse === "" || this.props.appraiseShoppingListResponse.total_ammount === undefined ? '-' : 
    formatNumberForPrice(this.props.appraiseShoppingListResponse.total_ammount)

  _getPrice = (found, art_no) => {
    const { appraiseShoppingListResponse, t } = this.props;
    if (this.props.appraiseShoppingListResponse === "" ) {
      return "-";
    }
    else {
      if (found && typeof appraiseShoppingListResponse.shop_list_products!=='undefined'
       && appraiseShoppingListResponse.shop_list_products!==null && appraiseShoppingListResponse.shop_list_products.length!==0) {
        let price=appraiseShoppingListResponse.shop_list_products.find(product => product.art_no === art_no);
        if (typeof price!=='undefined')
          return (t("common:moneyCurrency") + formatNumberForPrice(price.totalpriceiva) );
        else
          return "-";
      }
      else  
        return "-";
    }
  }

  _setStore = async (store) =>
  {
    if  (this.props.currentStoreObject===null || store.store_no !== this.props.currentStoreObject.store_no) {
      await this.props.setCurrentStore(store);
      this.changeLayout();
    }
  }

  _getStoreName = () => {
    const {currentStoreObject} = this.props;
    return (currentStoreObject!==null &&  typeof currentStoreObject.name!=='undefined' ?  this.props.currentStoreObject.name.toUpperCase() : "" )
  }

  _renderTotalPriceListItem = ({ item: product, index } ) => {
    return (
      <Layout.TotalPriceListItem
        itemTitle={product.description}
        subTitle={product.art_no}
        info1=  {this._getPrice(product.found, product.art_no)}
        info2={product.quantity} 
      />
    );
  }

  changeLayout = () => {
    LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
    if (this.state.expanded === false)
      this.setState({
        expanded: true
      });
    else
      this.setState({
        expanded: false
      });
  };

  async componentDidUpdate(prevProps) {
    if(prevProps.currentStoreObject !== this.props.currentStoreObject) {
      const { user, fetchedListDetails, currentStoreObject } = this.props;
      var store_no="1";
      if (currentStoreObject!==null && typeof currentStoreObject.store_no === "string")
        store_no = currentStoreObject.store_no;
      await this.props.resetAppraiseShopListResponse();
      await this.props.appraiseShopList(store_no, user.personalIdentifier, fetchedListDetails.uuid)
    }
  }

  render() {
    const { loading, successMessage, t, fetchedListDetails, appraiseShoppingListResponse } = this.props;
    const filteredList = fetchedListDetails.shop_list_products.filter(product => product.deleted === '0')

    return (
      <Layout.ViewContainer>
        <Layout.Loader loading={loading} />
        <Layout.CreateShoppingListModal
          modalVisible={this.props.modals.createList}
          onCancel={() =>
            setTimeout(() => {
              this.props.closeModal("createList");
            }, 1)
          }
          onOk={() =>
            setTimeout(() => {
              this.props.closeModal("createList");
              this.props.navigation.navigate("ShoppingLists");
            }, 1)
          }
        />
        <Layout.TouchableTitle   
          onPress = {this.changeLayout}
          titleText = {"LOJA "  + (this._getStoreName().toUpperCase())}
          caretUp = { this.state.expanded }
        />
        <Layout.Title
          text={t("shoppingLists:totalPrice.title") + " - " + fetchedListDetails.name}
        />
        <View style={{ flex: 1 }}>
          <Layout.StoreList
            expanded={this.state.expanded}
            expandedHeight={'100%'}
            hiddenHeight={0}
            storesArray={this.props.fetchedStoreList}
            component={Layout.StoreListItem}
            location={this.props.location}
            onPress={this._setStore}
            modalConnectionError={this.props.modals.connectionError}
            hasConnectionError={this.props.fetchStoreListError}
            openModal={this.props.openModal}
            closeModal={this.props.closeModal}
            t={t}
          />
          <Layout.TotalPriceListFirstItem 
            itemTitle1={this.props.currentStoreObject.name}
            itemTitle2={capitalizeFirstLetter(this._getDateExpired())}
            subTitle={t("shoppingLists:totalPrice.pricedIn") + " " + this._getDate()}
            info1={t("shoppingLists:totalPrice.totalValue")}
            totalPrice={"R$" + this._getTotalAmount()}
          />
          <FlatList
            keyExtractor={(item, index) => item.art_no}
            data={filteredList}
            renderItem={this._renderTotalPriceListItem}
            initialNumToRender={5}
            maxToRenderPerBatch={5}
            windowSize={10}
            removeClippedSubviews={false}
            updateCellsBatchingPeriod={100}
          />
        </View>
      </Layout.ViewContainer>
    );
  }
}

const mapStateToProps = state => ({
  modals: {
    createList: state.shoppingLists.createListModal.createList,
    connectionError: state.priceInquiry.modal.connectionError,
  },
  fetchedListDetails: state.products.fetchProduct.productList,
  user: state.user,
  appraiseShoppingListResponse: state.shoppingLists.appraiseShoppingList.appraiseShoppingListResponse,
  loading: state.shoppingLists.appraiseShoppingList.loading,
  currentStoreObject: state.currentStore.object,
  storeListLoading: state.storeListFetch.loading,
  fetchedStoreList: state.storeListFetch.storeList,
  fetchStoreListError: state.storeListFetch.error,
  location: state.location.location,
});

const mapDispatchToProps = dispatch => {
  return {
    openModal: modalKey => {
      dispatch(Actions.setModalVisible(modalKey));
    },
    closeModal: modalKey => {
      dispatch(Actions.setModalInvisible(modalKey));
    },
    appraiseShopList: (passport, personalIdentifier, uuid) => {
      dispatch(Actions.appraiseShopList(passport, personalIdentifier, uuid));
    },
    resetAppraiseShopListResponse : () => {
      dispatch(Actions.resetAppraiseShopListResponse());
    },
    setCurrentStore: (store) => {
      dispatch(Actions.setCurrentStore(store))
    },
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(TotalPriceView);

