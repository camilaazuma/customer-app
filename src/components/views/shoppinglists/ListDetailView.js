import React from "react";
import {
  View,
  Text,
  ScrollView,
  FlatList,
  Alert,
  Platform,
  KeyboardAvoidingView
} from "react-native";
import { Icon, List, ListItem } from "react-native-elements";
import { COLORS } from "../../../constants/styles";
import APP_DIMENSIONS from "../../../constants/appDimensions";
import styled, { ThemeProvider } from "styled-components/native";
import * as Layout from "../../layout";
import * as Actions from "../../../actions";
import { connect } from "react-redux";
import { NavigationActions } from "react-navigation";
import { translate } from 'react-i18next';
import { SQLite } from 'expo';
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scrollview";

const SCREEN_HEIGHT = APP_DIMENSIONS.SCREEN_HEIGHT;
const SCREEN_WIDTH = APP_DIMENSIONS.SCREEN_WIDTH;

const params = {
  updated: "2018-11-01 19:22:02",
}

@translate(['shoppingLists', 'common'], { wait: true })
class ListDetailView extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      successMessage: false,
      db: {},
      fetchedListDetails: [],
      productListStatus: {},
      index: -1,
      fetchError: false,
      currentProductNo: ""
    };
  }

  async componentDidMount() {
    var articles = this.props.fetchedListDetails.shop_list_products.map(item =>{
      return item.art_no;
    });
    this.props.getProductListStatus({user: this.props.user, articles: articles});
    await this.props.appraiseShopList(this.props.user.passportNo, this.props.user.personalIdentifier, this.props.fetchedListDetails.uuid)
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    // recebe os status dos produtos e inclui na lista de produtos
    var obj = {};
    if(prevState.fetchedListDetails !== nextProps.fetchedListDetails){
      obj.fetchedListDetails = nextProps.fetchedListDetails;
    }
    if(prevState.productListStatus!== nextProps.productListStatus && nextProps.fetchedListDetails.shop_list_products !== undefined && nextProps.productListStatus!==undefined){
      nextProps.fetchedListDetails.shop_list_products = nextProps.fetchedListDetails.shop_list_products.map(item =>{
        if(nextProps.productListStatus.hasOwnProperty(item.art_no)){
          item.display= nextProps.productListStatus[item.art_no].display;
          item.instock= nextProps.productListStatus[item.art_no].instock;
          item.ispromo= nextProps.productListStatus[item.art_no].ispromo;
          item.promo_text= nextProps.productListStatus[item.art_no].promo_text;
          item.promo_type= nextProps.productListStatus[item.art_no].promo_type;
          item.found=true;
        }
        else {
          item.found=false;
        }
        return item;
      })
      obj.fetchedListDetails = nextProps.fetchedListDetails;
      obj.fetchError = false;
    }
    if(!prevState.fetchError && nextProps.fetchError) {
      nextProps.openModal("connectionError");
      return {
        fetchError: true
      }
    }
    return Object.keys(obj).length? obj : null;
  }

  _onChangeText=(text) => this.setState({
    fetchedListDetails: {
      ...this.state.fetchedListDetails,
      name: text
    }
  })

  _confirmNameChange = async () => {
    let { t } = this.props;
    if(this.state.fetchedListDetails.name === "") {
      Alert.alert(
        t("common:errors.error"),
        t("common:errors.emptyShopListName"),
        [
          {text: 'OK'},
        ],
        { cancelable: true }
      );
      return null;
    }
    this.props.closeModal("editList");
    await this.props.updateShopList({user: this.props.user, updated: params.updated, shopList: this.state.fetchedListDetails});
  }

  _deleteProduct = async () => {
    var products = this.state.fetchedListDetails.shop_list_products;
    product = products.find(product => product.art_no === this.state.currentProductNo);
    product.deleted = '1'
    await this.setState({
      fetchedListDetails: {
        ...this.state.fetchedListDetails,
        shop_list_products: products
      }
    })
    await this.props.updateShopList({user: this.props.user, updated: params.updated, shopList: this.state.fetchedListDetails});
    this.props.closeModal("deleteProduct");
  }

  _onChangeQuantity = async (text, productArtNo) => {
    var products = this.state.fetchedListDetails.shop_list_products;
    if (/^[0-9]*$/.test(text)) {
      products.find(product => product.art_no === productArtNo).quantity = text;
      await this.setState({
        fetchedListDetails: {
          ...this.state.fetchedListDetails,
          shop_list_products: products
        }
      })
    }
  }

  _setQuantity = async (productArtNo) =>{
    var products = this.state.fetchedListDetails.shop_list_products;
    product = products.find(product => product.art_no === productArtNo);
    if(product.quantity === "" || product.quantity === "0") {
      await this.setState({
        currentProductNo: productArtNo
      })
      this.props.openModal("deleteProduct");
    }
    else {
      await this.props.updateShopList({user: this.props.user, updated: params.updated, shopList: this.state.fetchedListDetails});
    }
  }

  _clickDeleteButton = async (productArtNo) => {
    await this.setState({
      currentProductNo: productArtNo
    })
    this.props.openModal("deleteProduct");
  }

  _deleteList = async (index) => {
    await this.setState({
      fetchedListDetails: {
        ...this.state.fetchedListDetails,
        deleted: "1"
      }
    })
    await this.props.updateShopList({user: this.props.user, updated: params.updated, shopList: this.state.fetchedListDetails});
    this.props.closeModal("editList");
    this.props.navigation.navigate("ShoppingLists")
  }

  _onChangeQuantityCallback = (productArtNo)=>(text)=>this._onChangeQuantity(text, productArtNo)

  _deleteProductCallback = (productArtNo) => () => this._clickDeleteButton(productArtNo)

  _setQuantityCallback = (productArtNo) => () => this._setQuantity(productArtNo)

  _renderShoppingListProductItem = ({ item: product, index } ) => {
    return (
      <Layout.ShoppingListProductItem
        productName={product.description}
        code={product.art_no}
        offerActive={product.ispromo}
        totalQuantity={product.quantity}
        stock={product.instock}
        found={product.found}
        onChangeQuantity={this._onChangeQuantityCallback(product.art_no)}
        deleteProduct={this._deleteProductCallback(product.art_no)}
        setQuantity={this._setQuantityCallback(product.art_no)}
        t={this.props.t}
      />
    );
  }

  render() {
    const { loading, successMessage, t } = this.props;
    const filteredList = this.state.fetchedListDetails.shop_list_products.filter((item, index) => item.deleted === "0")
    return (
      <Layout.ViewContainer>
        {/* <Layout.Loader loading={loading} /> */}
        <Layout.EditShoppingListModal
          t={t}
          modalVisible={this.props.modals.editList}
          onCancel={() =>
            setTimeout(() => {
              this.props.closeModal("editList");
            }, 1)
          }
          onOk={this._confirmNameChange}
          showOther={true}
          onOtherOption={this._deleteList}
          otherOptionText={t && t("common:delete")}
          nomeLista={this.state.fetchedListDetails.name}
          onChangeText={this._onChangeText}
        />
        <Layout.TwoButtonsModal
          t={t}
          modalVisible={this.props.modals.deleteProduct}
          onCancel={() =>
            setTimeout(() => {
              this.props.closeModal("deleteProduct");
            }, 1)
          }
          onOk={this._deleteProduct}
          okText={t && t("shoppingLists:deleteProductsModal:confirm")}
          cancelText={t && t('shoppingLists:deleteProductsModal:cancel')}
          closeModal={this.props.closeModal}
          title={t && t('shoppingLists:deleteProductsModal.title')}
          contentText={""}
          hidePicker
        />
        <Layout.SuccessMessageModal
          t={t}
          modalVisible={this.props.modals.connectionError}
          onOk={() => this.props.closeModal('connectionError')}
          title={t('common:connectionErrorTitle')} 
          contentText={t("common:connectionErrorMessage")} 
          okText={'OK'}     
        />
        <Layout.TouchableTitle
          titleText={this.state.fetchedListDetails.name.toUpperCase()}
          editIcon = {true}
          onPress={() =>
            setTimeout(() => {
              this.props.fontLoader();
              this.props.openModal("editList");
            }, 1)
          }
        />
        <KeyboardAvoidingView
          behavior={Platform.OS === "ios" ? "height" : "padding"}
          keyboardVerticalOffset={APP_DIMENSIONS.SCREEN_HEIGHT * 0.137}
          style={{ 
            flex: 1,
            backgroundColor: "transparent",
            justifyContent: "center"
          }}
        >
          <KeyboardAwareScrollView
            contentContainerStyle={{ flexGrow: 1 }}
            keyboardShouldPersistTaps="always"
          >
            <View style={{ flex: 1 }}>
              <FlatList 
                keyExtractor={(item, index) => item.art_no}
                data={filteredList}
                renderItem={this._renderShoppingListProductItem}
                initialNumToRender={5}
                maxToRenderPerBatch={5}
                windowSize={10}
                removeClippedSubviews={false}
                updateCellsBatchingPeriod={100}
                getItemLayout={(data, index) => (
                  {length: APP_DIMENSIONS.SCREEN_HEIGHT * 0.205, offset: APP_DIMENSIONS.SCREEN_HEIGHT * 0.205 * index, index}
                )}
              />
            </View>
          </KeyboardAwareScrollView>
        </KeyboardAvoidingView>
        <Layout.BottomTabContainer>
          <Layout.TouchableContainer
            opacity
            onPress={() => {
              this.props.navigation.navigate(
                "TotalPrice",
                {shoppingList: this.state.fetchedListDetails}
              );
            }}
          >
            <Layout.BottomTabButtonContainer>
              <Layout.BottomTabIcon
                type="font-awesome"
                name="dollar"
                color={COLORS.RED.FIRE_ENGINE_RED}
              />
              <Layout.BottomTabButtonText>
                {t('shoppingLists:listDetail.totalPriceButton')}
              </Layout.BottomTabButtonText>
            </Layout.BottomTabButtonContainer>
          </Layout.TouchableContainer>
          <Layout.TouchableContainer
            onPress={() => {
              this.props.navigation.navigate(
                "AddProducts",
                {shoppingList: this.state.fetchedListDetails}
              );
            }}
          >
            <Layout.BottomTabButtonContainer
              style={{ backgroundColor: COLORS.RED.FIRE_ENGINE_RED }}
            >
              <Layout.BottomTabIcon type="font-awesome" name="plus-circle" />
              <Layout.BottomTabButtonText style={{ color: COLORS.WHITE.WHITE }}>
                {t('shoppingLists:listDetail.addProductsButton')}
              </Layout.BottomTabButtonText>
            </Layout.BottomTabButtonContainer>
          </Layout.TouchableContainer>
        </Layout.BottomTabContainer>
      </Layout.ViewContainer>
    );
  }
}

const mapStateToProps = state => ({
  modals: {
    editList: state.shoppingLists.createListModal.editList,
    deleteProduct: state.shoppingLists.createListModal.deleteProduct,
    connectionError: state.shoppingLists.createListModal.connectionError,
  },
  fetchedListDetails: state.products.fetchProduct.productList,
  productListStatus: state.shoppingLists.fetchProductListStatus.productListStatus,
  user: state.user,
  fetchError: state.shoppingLists.fetchProductListStatus.getStatusError
});

const mapDispatchToProps = dispatch => {
  return {
    openModal: modalKey => {
      dispatch(Actions.setModalVisible(modalKey));
    },
    closeModal: modalKey => {
      dispatch(Actions.setModalInvisible(modalKey));
    },
    getProductListStatus: ({user, articles}={}) => {
      dispatch(Actions.getProductsAvailability(user, articles));
    },
    fontLoader: () => {
      dispatch(Actions.setFontLoadedFlag());
    },
    updateShopList: ({user, updated, shopList}) => {
      dispatch(Actions.saveShopList(user, updated, shopList));
    },
    getListDetails: ({productsList}={}) => {
      dispatch(Actions.getProductsFromList(productsList));
    },
    appraiseShopList: (passport, personalIdentifier, uuid) => {
      dispatch(Actions.appraiseShopList(passport, personalIdentifier, uuid));
    },
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ListDetailView);

// export default ListDetailView;
