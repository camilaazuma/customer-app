import React from "react";
import {
  View,
  Platform,
  Text
} from "react-native";
import { COLORS } from "../../../constants/styles";
import APP_DIMENSIONS from "../../../constants/appDimensions";
import * as Layout from "../../layout";
import { translate } from 'react-i18next';
import { MaskService } from 'react-native-masked-text';
import { connect } from "react-redux";
import * as Actions from "../../../actions";
import AsyncStorageHelper from "../../../helpers/AsyncStorageHelper";
import firebase from 'react-native-firebase';

const LOGGED_IN_KEY = "LOGGED_IN";
const LOGGED_USER_DATA = "LOGGED_USER_DATA"

@translate(['signUp', 'common'], { wait: true })
class LoginView extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      identificationInput: props.cpfOrCnpj,
      inputMask: props.navigation.getParam('inputMask', 'only-numbers'),
      showError: false,
      password: "",
      modalText: "",
      chooseValidationTypeModal: false,
      forgotPasswordValidationTypeModal: false,
      confirmationCodePurpose: null,
      error: false
    };
  }

  async componentDidUpdate(prevProps) {
    const { loginResponse, installationId, cpfOrCnpj, t } = this.props;
    if (prevProps.loginResponse === "") {
      if (loginResponse !== undefined && loginResponse.passport_no !== undefined) {
        await AsyncStorageHelper.setItem(LOGGED_IN_KEY, "true");
        userInfo = {
          passportNo: loginResponse.passport_no,
          passportRaw: loginResponse.passport_raw,
          fullName: loginResponse.cust_name,
          email: loginResponse.email,
          deviceId: installationId,
          personalIdentifier: cpfOrCnpj,
          phone: loginResponse.phone,
          cep: loginResponse.cep,
          street: loginResponse.address_st,
          addressNumber: loginResponse.address_no,
          complement: loginResponse.address_comp,
          city: loginResponse.city,
          stateName: loginResponse.state,
          contactUuid: loginResponse.contact_uuid
        }
        this.props.setUserInfo(userInfo);
        this.props.fetchPreferredStore(userInfo);
        await AsyncStorageHelper.removeItem(LOGGED_USER_DATA)
        await AsyncStorageHelper.setItem(LOGGED_USER_DATA, userInfo)

        setTimeout(() => this.props.navigation.navigate('Home'), 1);
        await firebase.analytics().logEvent('login_request_success', {
          email: loginResponse.email,
          personalIdentifier: cpfOrCnpj,
          phone: loginResponse.phone,
        });
      }
      else if (loginResponse === 100110 ||
        loginResponse === 100401 ||
        loginResponse === 100402 ||
        loginResponse === 100113) {
        if (loginResponse === 100110) {
          this.setState({ modalText: t("common:errors.userNotFound") });
        }
        else if (loginResponse === 100401) {
          this.setState({ modalText: t("common:errors.wrongPassword") });
        }
        else if (loginResponse === 100402) {
          this.setState({ modalText: t("common:errors.invalidUserStatus") });
        }
        else if (loginResponse === 100113) {
          this.setState({ modalText: t("common:errors.userBlocked") });
        }
        await firebase.analytics().logEvent('login_failed', {
          email: loginResponse.email,
          personalIdentifier: cpfOrCnpj,
          phone: loginResponse.phone,
        });
        this.props.openModal('modalMessage');
      }

    }
    if (typeof this.props.sendTokenForPasswordRegistrationResponse !== "undefined") {
      if (prevProps.sendTokenForPasswordRegistrationResponse === "" && this.props.sendTokenForPasswordRegistrationResponse.constructor === Array) {
        setTimeout(() => this.props.navigation.navigate('ConfirmationCode', { confirmationCodePurpose: this.state.confirmationCodePurpose }), 1)
      }
      if (prevProps.sendTokenForPasswordRegistrationResponse === "" && this.props.sendTokenForPasswordRegistrationResponse === 1900110) {
        this.setState({ modalText: t("common:errors.tokenError") });
        this.props.openModal('modalMessage');
      }
    }
    if (!prevProps.error && this.props.error) {
      this.setState({ error: true })
    }
  }

  componentDidMount() {
    const didFocusSubscription = this.props.navigation.addListener(
      'didFocus',
      payload => {
        this.props.resetLoginResponse();
        this.setState({
          password: "",
        })
      }
    );
  }

  _validateBeforeProceed = async () => {
    this.props.resetLoginResponse();
    if (MaskService.isValid(this.state.inputMask, this.state.identificationInput)) {
      await this.props.resetLoginResponse();
      await this.props.login(this.state.identificationInput, this.state.password)
    } else {
      await this.setState({ showError: true });
    }
  }

  _onChangePassword = (text) => this.setState({ password: text });

  _closeMessageModal = () => setTimeout(() => {
    this.props.closeModal('modalMessage');
    if (this.props.loginResponse === 100402) {
      this.setState({
        chooseValidationTypeModal: true,
        confirmationCodePurpose: 'VALIDATE_USER'
      })
    }
  }, 1)


  _chooseValidationType = validationType => async () => {
    this.setState({
      chooseValidationTypeModal: false,
      forgotPasswordValidationTypeModal: false
    });
    this.props.resetSendTokenForPasswordRegistrationResponse();
    await this.props.sendTokenForPasswordRegistration(
      MaskService.toRawValue(this.state.inputMask, this.state.identificationInput),
      validationType
    )
  }

  _onForgotPasswordPress = () => {
    this.setState({
      forgotPasswordValidationTypeModal: true,
      confirmationCodePurpose: 'CREATE_PASSWORD'
    })
  }

  _closeChooseValidationTypeModal = () => this.setState({ chooseValidationTypeModal: false })
  _closeForgotPasswordValidationTypeModal = () => this.setState({ forgotPasswordValidationTypeModal: false })

  render() {
    const { t, loginResponse, sendTokenLoading } = this.props;
    return (
      <Layout.ViewContainer>
        <Layout.Loader loading={this.props.loading || sendTokenLoading} />

        <Layout.ThreeButtonModal
          t={t}
          modalVisible={this.state.chooseValidationTypeModal}
          onOk={this._chooseValidationType("email")}
          onCancel={this._closeChooseValidationTypeModal}
          onOtherOption={this._chooseValidationType("phone")}
          title={t("signUp:login.tokenValidationTypeModal.title")}
          contentText={t("signUp:login.tokenValidationTypeModal.text")}
          okText={'Email'}
          cancelText={'Cancelar'}
          otherOptionText={'SMS'}
          hidePicker
        />

        <Layout.ThreeButtonModal
          t={t}
          modalVisible={this.state.forgotPasswordValidationTypeModal}
          onOk={this._chooseValidationType("email")}
          onCancel={this._closeForgotPasswordValidationTypeModal}
          onOtherOption={this._chooseValidationType("phone")}
          title={t("signUp:login.tokenValidationTypeModal.title")}
          contentText={t("signUp:login.tokenValidationTypeModal.newPasswordText") + "\n" +
            "Email: " + this.props.cpfInputResponse.masked_email + "\n" +
            "SMS: " + this.props.cpfInputResponse.masked_phone
          }
          okText={'Email'}
          cancelText={t("common:cancel")}
          otherOptionText={'SMS'}
          hidePicker
        />

        <Layout.SuccessMessageModal
          t={t}
          modalVisible={this.state.error}
          onOk={() => this.props.closeModal('connectionError')}
          title={t('common:connectionErrorTitle')}
          contentText={t("common:connectionErrorMessage")}
          okText={'OK'}
        />

        <Layout.SuccessMessageModal
          t={t}
          modalVisible={this.props.modalMessage}
          onOk={this._closeMessageModal}
          title={this.state.modalText}
          okText={'OK'}
        />
        <Layout.Title text={t('signUp:loginTitle')} disableUpperCase={this.props.i18n.language === 'es-CO' ? true : false} />
        <Layout.KeyboardAvoidingContainer behavior='padding' keyboardVerticalOffset={APP_DIMENSIONS.SCREEN_HEIGHT * 0.1} >
          <View style={{flex: 0.9, justifyContent: 'center'}}>
            <Layout.IconTitleText style={{ color: COLORS.BLACK.BLACK, paddingVertical: APP_DIMENSIONS.SCREEN_HEIGHT * 0.01 }}>
              {t('signUp:cpfInput.welcomeMessageTitle')}
            </Layout.IconTitleText>
            <Layout.IconTitleText style={{ color: COLORS.BLACK.BLACK, fontSize: APP_DIMENSIONS.SCREEN_WIDTH * 0.041 * APP_DIMENSIONS.SCREEN_WIDTH_SIZE_CORRECTION }}>
              {t('signUp:cpfInput.welcomeMessageBody')}
            </Layout.IconTitleText>
          </View>
          <View style={{ padding: APP_DIMENSIONS.SCREEN_WIDTH * 0.061, flex: 1 }}>
            {
              this.props.i18n.language === "es-CO" ?
                <Layout.BorderedFormInput
                  returnKeyType={'done'}
                  keyboardType={Platform.OS === 'ios' ? "decimal-pad" : 'numeric'}
                  maskActive={true}
                  type={this.state.inputMask}
                  placeholder={t('signUp:cpfInput.cpfInputPlaceHolder')}
                  value={this.state.identificationInput}
                  editable={false}
                  label={t('signUp:cpfInput.document')}
                /> :
                <Layout.FormTextInput
                  returnKeyType={'done'}
                  keyboardType={Platform.OS === 'ios' ? "decimal-pad" : 'numeric'}
                  maskActive={true}
                  type={this.state.inputMask}
                  placeholder={t('signUp:cpfInput.cpfInputPlaceHolder')}
                  value={this.state.identificationInput}
                  editable={false}
                />
            }
            {this.state.showError == true &&
              <Layout.FontText style={{
                color: COLORS.RED.FIRE_ENGINE_RED, fontFamily: "open-sans-semibold"
              }}>
                {t("common:errors.invalidPersonalIdentifier")}
              </Layout.FontText>
            }
            {
              this.props.i18n.language === "es-CO" ?
                <Layout.BorderedFormInput
                  keyboardType={"default"}
                  maskActive={false}
                  secureTextEntry={true}
                  textContentType={'password'}
                  onChangeText={this._onChangePassword}
                  placeholder={t("signUp:login.passwordInputPlaceHolder")}
                  value={this.state.password}
                  onSubmitEditing={this._validateBeforeProceed}
                  label={t("signUp:login.password")}
                /> :
                <Layout.FormTextInput
                  keyboardType={"default"}
                  maskActive={false}
                  secureTextEntry={true}
                  textContentType={'password'}
                  onChangeText={this._onChangePassword}
                  placeholder={t("signUp:login.passwordInputPlaceHolder")}
                  value={this.state.password}
                  onSubmitEditing={this._validateBeforeProceed}
                />
            }


            <Layout.TouchableContainer onPress={this._onForgotPasswordPress} style={{ flex: 0 }} opacity>
              <Layout.FontText
                style={{
                  fontFamily: 'open-sans-semibold',
                  color: COLORS.RED.FIRE_ENGINE_RED,
                  lineHeight: APP_DIMENSIONS.SCREEN_HEIGHT * 0.07,
                  fontSize: APP_DIMENSIONS.SCREEN_WIDTH * 0.045,
                  textAlign: 'left',
                }}
              >
                {t("signUp:login.forgotPassword")}
              </Layout.FontText>

            </Layout.TouchableContainer>

          </View>
        </Layout.KeyboardAvoidingContainer>
        <Layout.BottomTabContainer>
          <Layout.TouchableContainer onPress={this._validateBeforeProceed}>
            <Layout.BottomTabButtonContainer style={{ backgroundColor: COLORS.RED.FIRE_ENGINE_RED }} >
              <Layout.BottomTabButtonText style={{ color: COLORS.WHITE.WHITE }}>{t('common:next')}</Layout.BottomTabButtonText>
              <Layout.BottomTabIcon name='arrow-right' size={APP_DIMENSIONS.SCREEN_WIDTH * 0.092} />
            </Layout.BottomTabButtonContainer>
          </Layout.TouchableContainer>
        </Layout.BottomTabContainer>
      </Layout.ViewContainer>
    );
  }
}

const mapStateToProps = state => ({
  loginResponse: state.signUp.login.loginResponse,
  loading: state.signUp.login.loading,
  fetching: state.signUp.login.fetching,
  error: state.signUp.login.error,
  modalMessage: state.signUp.loginModal.modalMessage,
  installationId: state.installationId.installationId,
  cpfOrCnpj: state.signUp.cpfInput.cpfOrCnpj,

  cpfInputResponse: state.signUp.cpfInput.cpfInputResponse,

  sendTokenForPasswordRegistrationResponse: state.signUp.sendTokenForPasswordRegistration.sendTokenForPasswordRegistrationResponse,
  sendTokenLoading: state.signUp.sendTokenForPasswordRegistration.loading,
  sendTokenFetching: state.signUp.sendTokenForPasswordRegistration.fetching,
  sendTokenError: state.signUp.sendTokenForPasswordRegistration.error,
});


const mapDispatchToProps = dispatch => {
  return {
    login: (personalIdentifier, password) => {
      dispatch(Actions.login(personalIdentifier, password));
    },
    resetLoginResponse: () => {
      dispatch(Actions.resetLoginResponse());
    },
    openModal: modalKey => {
      dispatch(Actions.setModalVisible(modalKey));
    },
    closeModal: modalKey => {
      dispatch(Actions.setModalInvisible(modalKey));
    },
    setUserInfo: ({ passportNo, passportRaw, fullName, email, deviceId, personalIdentifier, phone, cep, street, addressNumber, complement, city, stateName, contactUuid }) => {
      dispatch(Actions.setUserInfo({ passportNo, passportRaw, fullName, email, deviceId, personalIdentifier, phone, cep, street, addressNumber, complement, city, stateName, contactUuid }));
    },
    fetchPreferredStore: (user) => {
      dispatch(Actions.fetchPreferredStore(user));
    },
    sendTokenForPasswordRegistration: (personalIdentifier, validationType) => {
      dispatch(Actions.sendTokenForPasswordRegistration(personalIdentifier, validationType));
    },
    resetSendTokenForPasswordRegistrationResponse: () => {
      dispatch(Actions.resetSendTokenForPasswordRegistrationResponse());
    }
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(LoginView);
