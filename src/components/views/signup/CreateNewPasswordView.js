import React from "react";
import {
  View,
  Text,
  ScrollView,
  Keyboard,
  Platform,
  TextInput
} from "react-native";
import { COLORS } from "../../../constants/styles";
import APP_DIMENSIONS from "../../../constants/appDimensions";
import * as Layout from "../../layout";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scrollview";
import { translate } from "react-i18next";
import { connect } from "react-redux";
import * as Actions from "../../../actions";
import AsyncStorageHelper from "../../../helpers/AsyncStorageHelper";
import { getPersonalIdentifierMask } from "../../../utils";

const LOGGED_IN_KEY = "LOGGED_IN";
const LOGGED_USER_DATA = "LOGGED_USER_DATA"

@translate(["signUp", "common"], { wait: true })
class CreateNewPasswordView extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      password: "",
      passwordConfirm: "",
      inputMask: getPersonalIdentifierMask(props.i18n.language, props.cpfOrCnpj),
      notEqualError: false,
      emptyError: false,
      modalText: "",
      passwordCreateSuccess: false
    };
  }

  async componentDidUpdate(prevProps) {
    const { createPasswordResponse, loginResponse, installationId, cpfOrCnpj, t } = this.props;
    if (prevProps.createPasswordResponse === "") {
      // success
      if (
        createPasswordResponse!==undefined &&
        createPasswordResponse.cust_name!==undefined
      ) {
        this.setState({ modalText: t("common:successes.newPassword") });
        this.props.openModal("modalMessage");
        this.setState({ passwordCreateSuccess: true });
      } 
      //errors
      else if (createPasswordResponse === 1900110) {
        this.setState({ modalText: t("common:errors.tokenError") });
        this.props.openModal("modalMessage");
      } 
      else if (createPasswordResponse === 100301) {
        this.setState({ modalText: t("common:errors.passwordAlreadyCreated") });
        this.props.openModal("modalMessage");
      }
      else if (createPasswordResponse === 100110) {
        this.setState({ modalText: t("common:errors.userNotFound") });
        this.props.openModal("modalMessage");
      } 
      else if (createPasswordResponse === 100113) {
        this.setState({ modalText: t("common:errors.userBlocked") });
        this.props.openModal("modalMessage");
      }
    }
    if (prevProps.loginResponse==="") {
      if (loginResponse!==undefined && loginResponse.passport_no!==undefined) {
        await AsyncStorageHelper.setItem(LOGGED_IN_KEY, "true");
        userInfo = {
          passportNo: loginResponse.passport_no,
          passportRaw: loginResponse.passport_raw,
          fullName: loginResponse.cust_name,
          email: loginResponse.email,
          deviceId: installationId,
          personalIdentifier: cpfOrCnpj,
          phone: loginResponse.phone,
          cep: loginResponse.cep,
          street: loginResponse.address_st,
          addressNumber: loginResponse.address_no,
          complement: loginResponse.address_comp,
          city: loginResponse.city,
          stateName: loginResponse.state,
          contactUuid: loginResponse.contact_uuid
        }
        this.props.setUserInfo(userInfo);
        await AsyncStorageHelper.removeItem(LOGGED_USER_DATA)
        await AsyncStorageHelper.setItem(LOGGED_USER_DATA, userInfo)
        setTimeout(()=>this.props.navigation.navigate('Home'), 1);
      }
      else if (loginResponse===100110) {
        this.setState({modalText: t("common:errors.userNotFound") });
        this.props.openModal('modalMessage');
      }
      else if (loginResponse===100401 ) {
        this.setState({modalText: t("common:errors.wrongPassword") });
        this.props.openModal('modalMessage');
      }
      else if (loginResponse===100402) {
        this.setState({modalText: t("common:errors.invalidUserStatus") });
        this.props.openModal('modalMessage');
      }
      else if (loginResponse===100113) {
        this.setState({modalText:t("common:errors.userBlocked")});
        this.props.openModal('modalMessage');
      }
    }
  }

  componentDidMount() {
    const didFocusSubscription = this.props.navigation.addListener(
      "didFocus",
      payload => {
        this.props.resetCreatePasswordResponse();
        this.setState({ passwordCreateSuccess: false });
      }
    );
  }

  _onChangePassword = text => this.setState({ password: text });

  _onChangePasswordConfirm = text => this.setState({ passwordConfirm: text });

  _didStartEditing = e => {
    this.setState({ notEqualError: false, emptyError: false });
  };

  _validateBeforeProceed = async () => {
    this.props.resetCreatePasswordResponse();
    const { cpfOrCnpj } = this.props;
    if (this.state.password !== this.state.passwordConfirm) {
      await this.setState({ notEqualError: true });
    } else if (this.state.password === "") {
      await this.setState({ emptyError: true });
    } else {
      await this.props.createNewPassword(cpfOrCnpj, this.state.password);
    }
  };

  _closeModal = () =>
    setTimeout(async () => {
      this.props.closeModal("modalMessage");
      if (this.state.passwordCreateSuccess) {
        await this.props.resetLoginResponse();
        await this.props.login(this.props.cpfOrCnpj, this.state.password);
      }
    }, 1);


  render() {
    const { t, i18n, cpfOrCnpj, loginLoading, loading } = this.props;
    return (
      <Layout.ViewContainer>
        <Layout.Loader loading={loading || loginLoading} />
        <Layout.SuccessMessageModal
          t={t}
          modalVisible={this.props.modalMessage}
          onOk={this._closeModal}
          title={this.state.modalText}
          okText={"OK"}
        />
        <Layout.Title text={t("signUp:loginTitle")} />
        <Layout.KeyboardAvoidingContainer
          behavior={Platform.OS === "ios" ? "height" : "padding"}
          keyboardVerticalOffset={APP_DIMENSIONS.SCREEN_HEIGHT * 0.137}
        >
          <Layout.IconTitleText style={{ color: COLORS.BLACK.BLACK }}>
            {t("signUp:createNewPassword.mainMessage")}
          </Layout.IconTitleText>
          <KeyboardAwareScrollView
            contentContainerStyle={{ flexGrow: 1 }}
            keyboardShouldPersistTaps="always"
          >
            <View
              style={{
                justifyContent: "space-evenly",
                flex: 1,
                paddingBottom: APP_DIMENSIONS.SCREEN_HEIGHT * 0.055,
                alignContent: "center"
              }}
            >
              <Layout.FormTextInput
                value={cpfOrCnpj}
                placeholder={t(
                  "signUp:userDataInputFields.cpfInputPlaceHolder"
                )}
                editable={false}
                maskActive={true}
                type={this.state.inputMask}
              />
              <Layout.FormTextInput
                keyboardType={"default"}
                maskActive={false}
                secureTextEntry={true}
                textContentType={"password"}
                onChangeText={this._onChangePassword}
                placeholder={t("signUp:createNewPassword.passwordInputPlaceHolder")}
                value={this.state.password}
                onFocus={this._didStartEditing}
                onSubmitEditing={event => {
                  event && this.passwordConfirm.focus();
                }}
              />
              <Layout.FormTextInput
                keyboardType={"default"}
                maskActive={false}
                secureTextEntry={true}
                textContentType={"password"}
                onChangeText={this._onChangePasswordConfirm}
                placeholder={t("signUp:createNewPassword.confirmPasswordInputPlaceHolder")}
                value={this.state.passwordConfirm}
                inputRef={r => {
                  this.passwordConfirm = r;
                }}
                onFocus={this._didStartEditing}
                onSubmitEditing={this._validateBeforeProceed}
              />
              {(this.state.notEqualError || this.state.emptyError) && (
                <Layout.FontText
                  style={{
                    color: COLORS.RED.FIRE_ENGINE_RED,
                    fontFamily: "open-sans-semibold"
                  }}
                >
                  {this.state.notEqualError
                    ? t("signUp:createNewPassword.notEqualPasswords")
                    : t("signUp:createNewPassword.blankPassword")}
                </Layout.FontText>
              )}
            </View>
          </KeyboardAwareScrollView>
        </Layout.KeyboardAvoidingContainer>
        <Layout.BottomTabContainer>
          <Layout.TouchableContainer onPress={this._validateBeforeProceed}>
            <Layout.BottomTabButtonContainer
              style={{ backgroundColor: COLORS.RED.FIRE_ENGINE_RED }}
            >
              <Layout.BottomTabButtonText style={{ color: COLORS.WHITE.WHITE }}>
                {t("common:next")}
              </Layout.BottomTabButtonText>
              <Layout.BottomTabIcon
                name="arrow-right"
                size={APP_DIMENSIONS.SCREEN_WIDTH * 0.092}
              />
            </Layout.BottomTabButtonContainer>
          </Layout.TouchableContainer>
        </Layout.BottomTabContainer>
      </Layout.ViewContainer>
    );
  }
}

const mapStateToProps = state => ({
  passportNo: state.signUp.cpfInput.cpfInputResponse.passport_no,
  createPasswordResponse: state.signUp.createNewPassword.createPasswordResponse,
  loading: state.signUp.createNewPassword.loading,
  fetching: state.signUp.createNewPassword.fetching,
  error: state.signUp.createNewPassword.error,
  modalMessage: state.signUp.createNewPasswordModal.modalMessage,
  installationId: state.installationId.installationId,
  cpfOrCnpj: state.signUp.cpfInput.cpfOrCnpj,
  loginResponse: state.signUp.login.loginResponse,
  loginLoading: state.signUp.login.loading,
});

const mapDispatchToProps = dispatch => {
  return {
    createNewPassword: (personalIdentifier, password) => {
      dispatch(Actions.createNewPassword(personalIdentifier, password));
    },
    resetCreatePasswordResponse: () => {
      dispatch(Actions.resetCreatePasswordResponse());
    },
    openModal: modalKey => {
      dispatch(Actions.setModalVisible(modalKey));
    },
    closeModal: modalKey => {
      dispatch(Actions.setModalInvisible(modalKey));
    },
    login: (personalIdentifier, password) => {
      dispatch(Actions.login(personalIdentifier, password));
    },
    resetLoginResponse: () => {
      dispatch(Actions.resetLoginResponse());
    },
    setUserInfo: ({passportNo, passportRaw, fullName, email, deviceId, personalIdentifier, phone, cep, street, addressNumber, complement, city, stateName, contactUuid}) => {
      dispatch(Actions.setUserInfo({passportNo, passportRaw, fullName, email, deviceId, personalIdentifier, phone, cep, street, addressNumber, complement, city, stateName, contactUuid}));
    },
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CreateNewPasswordView);
