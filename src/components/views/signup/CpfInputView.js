import React from "react";
import {
  View,
  Platform,
  Text,
  Keyboard
} from "react-native";
import { COLORS } from "../../../constants/styles";
import APP_DIMENSIONS from "../../../constants/appDimensions";
import * as Layout from "../../layout";
import { translate } from 'react-i18next';
import { MaskService } from 'react-native-masked-text';
import { connect } from "react-redux";
import * as Actions from "../../../actions";
import { validatePersonalIdentifier, getPersonalIdentifierMask } from "../../../utils";

let colombiaDocuments = [
  { label: "CC CE", value: "ZDOC_ID" },
  { label: "NIT", value: "ZDOC_ID_COMP" },
];
let peruDocuments = [
  { label: "DNI", value: "ZDOC_ID" },
  { label: "RUC", value: "ZDOC_ID" },
  { label: "CE", value: "ZDOC_ID" },
];

@translate(['signUp', 'common'], { wait: true })
class CpfInputView extends React.Component {

  constructor(props) {
    super(props);
    console.log(props.i18n.language);
    
    this.state = {
      identificationInput: '',
      documentTypeInput: 'ZDOC_ID',
      inputMask: 'only-numbers',
      formatError: false,
      noTypeSelectedError: false,
      chooseValidationTypeModal: false,
      error: false,
      keyboardShown: false,
      smallScreen: APP_DIMENSIONS.SCREEN_HEIGHT < 580,
      enableDocumentSelection: props.i18n.language === "es-CO" || props.i18n.language === "es-PE",
      documentTypeOptions: []
    };

    if (props.i18n.language === "es-CO"){
      this.state["documentTypeOptions"] = colombiaDocuments;
    }
    if (props.i18n.language === "es-PE"){
      this.state["documentTypeOptions"] = peruDocuments;
    }
  }

  componentDidUpdate(prevProps) {
    if (prevProps.cpfInputResponse==="" && typeof this.props.cpfInputResponse!=='undefined') {
      const { status } = this.props.cpfInputResponse;
      if (this.props.cpfInputResponse===100200) {
        setTimeout(()=>this.props.navigation.navigate('UserDataInputFields', {inputMask: this.state.inputMask}),1);
      } else if (status===100201 ) {
        this.setState({chooseValidationTypeModal: true});
      } else if (status===100202) {
        setTimeout(()=>this.props.navigation.navigate('Login', {inputMask: this.state.inputMask}),1)
      }
    }
    if (typeof this.props.sendTokenForPasswordRegistrationResponse!=="undefined"){
      if (prevProps.sendTokenForPasswordRegistrationResponse==="" && this.props.sendTokenForPasswordRegistrationResponse.constructor===Array) {
        setTimeout(()=>this.props.navigation.navigate('ConfirmationCode', {inputMask: this.state.inputMask}),1)
      }
    }

    if (prevProps.error===false && this.props.error===true)
      this.setState({error: true})

  }

  componentDidMount() {
    this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', this._keyboardDidShow);
    this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', this._keyboardDidHide);
    const didFocusSubscription = this.props.navigation.addListener(
      'didFocus',
      payload => {
        this.props.resetCpfInputResponse();
        this.props.resetSendTokenForPasswordRegistrationResponse();
      }
    );
  }

  componentWillUnmount() {
    this.keyboardDidShowListener.remove();
    this.keyboardDidHideListener.remove();
  }

  _keyboardDidShow = () => {
    this.setState({keyboardShown: true});
  };

  _keyboardDidHide = () => {
    this.setState({keyboardShown: false});
  }

  _didFinishEditing = (e) => {
    let { i18n } = this.props;
    let value = e.nativeEvent.text;
    value = value.replace(/[^a-z0-9-]/g, '');

    this.setState({inputMask: getPersonalIdentifierMask(i18n.language, value), identificationInput: value});
    this._validateBeforeProceed();
  }

  _didStartEditing = (e) => {
    this.setState({inputMask: 'only-numbers', identificationInput: '', showError: false});
  }

  _onCpfChange = text => this.setState({ identificationInput: text });

  _validateBeforeProceed = async () => {
    const { i18n } = this.props;
    const { identificationInput, documentTypeInput, inputMask } = this.state;
    const formatError = !validatePersonalIdentifier(i18n.language, identificationInput); 

    if (!documentTypeInput)
      await this.setState({noTypeSelectedError: true});
    if (formatError || !identificationInput)
      await this.setState({formatError: true});

    if ( validatePersonalIdentifier(i18n.language, identificationInput) && documentTypeInput && identificationInput) {
      await this.props.resetCpfInputResponse();
      await this.props.checkSignUpStatus( 
        MaskService.toRawValue(inputMask, identificationInput),
        documentTypeInput
      )
    }
  }

  _chooseValidationType = validationType => async () => {
    this.setState({chooseValidationTypeModal: false});
    this.props.resetSendTokenForPasswordRegistrationResponse();
    await this.props.sendTokenForPasswordRegistration(
      MaskService.toRawValue(this.state.inputMask, this.state.identificationInput), 
      validationType
    )
  }

  _closeChooseValidationTypeModal = ()=>this.setState({chooseValidationTypeModal: false})

  render() {
    const { t, sendTokenLoading, i18n } = this.props;
    const { smallScreen, keyboardShown } = this.state;
    return (
      <Layout.ViewContainer>
        <Layout.Loader loading={this.props.loading || sendTokenLoading} />
        <Layout.ThreeButtonModal 
          t={t}
          modalVisible={this.state.chooseValidationTypeModal}
          onOk={this._chooseValidationType('email')}
          onCancel={this._closeChooseValidationTypeModal}
          onOtherOption={this._chooseValidationType('phone')}
          title={t("signUp:cpfInput.tokenValidationTypeModal.title")} 
          contentText={t("signUp:cpfInput.tokenValidationTypeModal.text")}  
          okText={t("signUp:cpfInput.tokenValidationTypeModal.button2")}  
          cancelText={t("common:cancel")}  
          otherOptionText={t("signUp:cpfInput.tokenValidationTypeModal.button1")} 
          hidePicker     
        />

        <Layout.SuccessMessageModal
          t={t}
          modalVisible={this.state.error}
          onOk={() => this.setState({error: false})}
          title={t('common:connectionErrorTitle')} 
          contentText={t("common:connectionErrorMessage")} 
          okText={'OK'}     
        />
        <Layout.Title disableUpperCase={i18n.language === 'es-CO' ? true : false} text={t('signUp:loginTitle') + ' / ' + t('signUp:signUpTitle')} />
        <Layout.KeyboardAvoidingContainer behavior='padding' keyboardVerticalOffset={APP_DIMENSIONS.SCREEN_HEIGHT*0.05}>
          <View style={{flex:(keyboardShown && smallScreen ) ? 0.7 : 0.9, justifyContent: 'center'}}>
            <Layout.IconTitleText style={{ 
              color: COLORS.BLACK.BLACK,
              paddingVertical: APP_DIMENSIONS.SCREEN_HEIGHT*0.015,
              fontSize: (keyboardShown && smallScreen ) ? 
                APP_DIMENSIONS.SCREEN_WIDTH*0.045*APP_DIMENSIONS.SCREEN_WIDTH_SIZE_CORRECTION :
                APP_DIMENSIONS.SCREEN_WIDTH*0.05*APP_DIMENSIONS.SCREEN_WIDTH_SIZE_CORRECTION 

              }}>
              {t('signUp:cpfInput.welcomeMessageTitle')}
            </Layout.IconTitleText>
            <Layout.IconTitleText style={{ 
              color: COLORS.BLACK.BLACK, 
              fontSize: (keyboardShown && smallScreen ) ? 
                APP_DIMENSIONS.SCREEN_WIDTH*0.035*APP_DIMENSIONS.SCREEN_WIDTH_SIZE_CORRECTION :
                APP_DIMENSIONS.SCREEN_WIDTH*0.045*APP_DIMENSIONS.SCREEN_WIDTH_SIZE_CORRECTION 
            }}>
              {t('signUp:cpfInput.welcomeMessageBody')}
            </Layout.IconTitleText>
          </View>
          <View style={{ flex: 1 }}>
            { this.state.enableDocumentSelection &&
              <Layout.DocumentTypePicker 
                t={this.props.t} 
                items={this.state.documentTypeOptions} 
                onValueChange={(itemValue) => { this.setState({documentTypeInput: itemValue})}}
                value={this.state.documentTypeInput}
              />
            }
            {
              i18n.language === "es-CO" ? 
              <Layout.BorderedFormInput 
                returnKeyType={'done'}
                keyboardType={Platform.OS==='ios' ? "decimal-pad" : 'numeric'}
                maskActive={true}
                type={ this.state.inputMask }
                placeholder={ t('signUp:cpfInput.cpfInputPlaceHolder') }
                onEndEditing={ this._didFinishEditing }
                onChangeText = {this._onCpfChange}
                onFocus={ this._didStartEditing }
                value={this.state.identificationInput}
                maxLength={15}
                label={t('signUp:cpfInput.document')}
              />
              : 
              <Layout.FormTextInput
                returnKeyType={'done'}
                keyboardType={Platform.OS==='ios' ? "decimal-pad" : 'numeric'}
                maskActive={true}
                type={ this.state.inputMask }
                placeholder={ t('signUp:cpfInput.cpfInputPlaceHolder') }
                onEndEditing={ this._didFinishEditing }
                onChangeText = {this._onCpfChange}
                onFocus={ this._didStartEditing }
                value={this.state.identificationInput}
                maxLength={15}
              />
            }
            { this.state.noTypeSelectedError == true &&
              <Layout.FontText style={{
                color:COLORS.RED.FIRE_ENGINE_RED, fontFamily: "open-sans-semibold"
              }}>
                {t("common:errors.documentTypeNotSelected")}
              </Layout.FontText>
            }
            { this.state.formatError == true &&
              <Layout.FontText style={{
                color:COLORS.RED.FIRE_ENGINE_RED, fontFamily: "open-sans-semibold"
              }}>
                {t("common:errors.invalidPersonalIdentifier")}
              </Layout.FontText>
            }
          </View>
        </Layout.KeyboardAvoidingContainer>
        <Layout.BottomTabContainer>
          <Layout.TouchableContainer onPress={ this._validateBeforeProceed }>
            <Layout.BottomTabButtonContainer style={{backgroundColor: COLORS.RED.FIRE_ENGINE_RED}} >
              <Layout.BottomTabButtonText style={{color: COLORS.WHITE.WHITE}}>{t('common:next')}</Layout.BottomTabButtonText>
              <Layout.BottomTabIcon name='arrow-right' size={APP_DIMENSIONS.SCREEN_WIDTH*0.092}/>
            </Layout.BottomTabButtonContainer>
          </Layout.TouchableContainer>
        </Layout.BottomTabContainer>
      </Layout.ViewContainer>
    );
  }
}

const mapStateToProps = state => ({
  cpfInputResponse: state.signUp.cpfInput.cpfInputResponse,
  loading: state.signUp.cpfInput.loading,
  fetching: state.signUp.cpfInput.fetching,
  error: state.signUp.cpfInput.error,
  installationId: state.installationId.installationId,
  sendTokenForPasswordRegistrationResponse: state.signUp.sendTokenForPasswordRegistration.sendTokenForPasswordRegistrationResponse,
  sendTokenLoading: state.signUp.sendTokenForPasswordRegistration.loading,
  sendTokenFetching: state.signUp.sendTokenForPasswordRegistration.fetching,
  sendTokenError: state.signUp.sendTokenForPasswordRegistration.error,
});


const mapDispatchToProps = dispatch => {
  return {
    checkSignUpStatus: (personalIdentifier, documentName) => {
      dispatch(Actions.checkSignUpStatus(personalIdentifier, documentName));
    },
    resetCpfInputResponse: () => {
      dispatch(Actions.resetCpfInputResponse());
    },
    sendTokenForPasswordRegistration: (personalIdentifier, validationType) => {
      dispatch(Actions.sendTokenForPasswordRegistration(personalIdentifier, validationType));
    },
    resetSendTokenForPasswordRegistrationResponse: () => {
      dispatch(Actions.resetSendTokenForPasswordRegistrationResponse());
    }
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CpfInputView);