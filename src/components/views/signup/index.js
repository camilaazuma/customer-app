export {default as CpfInputView} from  "./CpfInputView";
export {default as CreateNewPasswordView} from  "./CreateNewPasswordView";
export {default as UserDataInputFieldsView} from  "./UserDataInputFieldsView";
export {default as ConfirmationCodeView} from  "./ConfirmationCodeView";
export {default as LoginView} from  "./LoginView";