import React from "react";
import { View, Text, ScrollView, Keyboard, Platform } from "react-native";
import { COLORS } from "../../../constants/styles";
import APP_DIMENSIONS from "../../../constants/appDimensions";
import * as Layout from "../../layout";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scrollview";
import { translate } from "react-i18next";
import { connect } from "react-redux";
import * as Actions from "../../../actions";
import { MaskService } from 'react-native-masked-text'
import { validateEmail, containsOnlyWhiteSpace, getPersonalIdentifierMask, getPhoneMask, validatePhone } from "../../../utils";


@translate(["signUp", "common"], { wait: true })
class UserDataInputFieldsView extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      identificationInput: props.cpfOrCnpj,
      fullName: "",
      phone: "",
      email: "",
      password: "",
      inputMask: getPersonalIdentifierMask(props.i18n.language, props.cpfOrCnpj),
      modalText: "",
      emptyError: false,
      chooseValidationTypeModal: false,
      error: false,
      emailError: false,
      phoneError: false,
      nameError: false,
      passwordError: false
    };
  }

  componentDidUpdate(prevProps) {
    const { signUpResponse, t, error } = this.props;
    if (prevProps.signUpResponse === "") {
      if (signUpResponse.cust_name !== undefined) {
        setTimeout(()=>this.props.navigation.navigate('ConfirmationCode', {password: this.state.password}),1)
      } else if (signUpResponse === 100110) {
        this.setState({ modalText: t("common:errors.userNotFound") });
        this.props.openModal("modalMessage");
      } else if (signUpResponse === 100050) {
        this.setState({ modalText: t("common:errors.serverError") });
        this.props.openModal("modalMessage");
      } else if (signUpResponse === 100113) {
        this.setState({ modalText: t("common:errors.userBlocked") });
        this.props.openModal("modalMessage");
      } else if (signUpResponse === 1900110) {
        this.setState({ modalText:  t("common:errors.tokenError") });
        this.props.openModal("modalMessage");
      } else if (signUpResponse === 100001) {
        this.setState({ modalText:  t("common:errors.error") });
        this.props.openModal("modalMessage");
      } else if (signUpResponse === 100202 || signUpResponse === 100201) {
        this.setState({ modalText:  t("common:errors.userAlreadyRegistered") });
        this.props.openModal("modalMessage");
      }
    }
    if (!prevProps.error && error) this.setState({error: true})
  }

  componentDidMount() {
    const didFocusSubscription = this.props.navigation.addListener(
      "didFocus",
      payload => {
        this.props.resetSignUpResponse();
      }
    );
  }

  _didStartEditing = e => {
    this.setState({ emptyError: false });
  };

  _validateBeforeProceed = async () => {
    this.setState({
      nameError: false,
      phoneError: false, 
      emailError:false, 
      passwordError: false,
      emptyError: false
    })
    console.log(validatePhone(this.props.i18n.language, this.state.phone));
    
    if (
      this.state.fullName === "" || containsOnlyWhiteSpace(this.state.fullName) ||
      this.state.phone === "" || !validatePhone(this.props.i18n.language, this.state.phone) || 
      this.state.email === "" || !validateEmail(this.state.email) || 
      this.state.password === ""
    ) {
      if (this.state.fullName === "" || containsOnlyWhiteSpace(this.state.fullName)) 
        this.setState({nameError: true});
      if(this.state.phone === "" || !validatePhone(this.props.i18n.language, this.state.phone))
        this.setState({phoneError: true});
      if (this.state.email === "" || !validateEmail(this.state.email) )
        this.setState({emailError: true})
      if(this.state.password === "")
        this.setState({passwordError: true})

      this.setState({emptyError: true})
    }
    else {
      this.setState({chooseValidationTypeModal: true})
    }
  };

  _sendSignUpRequest = (validationType) => async () => {
    const { passportNo, passportRaw, installationId } = this.props;
    this.setState({chooseValidationTypeModal: false})
    await this.props.resetSignUpResponse();
    await this.props.signUp(
      this.state.fullName,
      this.state.identificationInput,
      MaskService.toRawValue(getPhoneMask(this.props.i18n.language), this.state.phone),
      this.state.email,
      this.state.password,
      validationType
    );
  }

  _closeModal = () => {
    setTimeout(() => {
      this.props.closeModal("modalMessage");
      if (this.props.signUpResponse === 100201 || this.props.signUpResponse === 100202) {
        this.props.navigation.goBack();
      }
    }, 1);
  }


  _onFullNameInputChange = text => this.setState({ fullName: text });

  _onPhoneInputChange = text => this.setState({ phone: text });

  _onEmailInputChange = text => this.setState({ email: text });

  _onPasswordInputChange = text => this.setState({ password: text });

  _closeChooseValidationTypeModal = ()=>this.setState({chooseValidationTypeModal: false})

  render() {
    const { t, loading } = this.props;
    return (
      <Layout.ViewContainer>
        <Layout.Title text={t("signUp:signUpTitle")} />
        <Layout.Loader loading={loading} />
        <Layout.SuccessMessageModal 
          t={t}
          modalVisible={this.props.modalMessage}
          onOk={this._closeModal}
          title={this.state.modalText}   
          okText={'OK'}
        />
        <Layout.ThreeButtonModal 
          t={t}
          modalVisible={this.state.chooseValidationTypeModal}
          onOk={this._sendSignUpRequest("email")}
          onCancel={this._closeChooseValidationTypeModal}
          onOtherOption={this._sendSignUpRequest("phone")}
          title={t("signUp:login.tokenValidationTypeModal.title")} 
          contentText={t("signUp:login.tokenValidationTypeModal.text")}  
          okText={'Email'}  
          cancelText={t("common:cancel")}  
          otherOptionText={'SMS'} 
          hidePicker     
        />

        <Layout.SuccessMessageModal
          t={t}
          modalVisible={this.state.error}
          onOk={() => this.setState({error: false})}
          title={t('common:connectionErrorTitle')} 
          contentText={t("common:connectionErrorMessage")} 
          okText={'OK'}  
        />
        <Layout.KeyboardAvoidingContainer
          behavior={Platform.OS === "ios" ? "height" : "padding"}
          keyboardVerticalOffset={APP_DIMENSIONS.SCREEN_HEIGHT * 0.137}
          style={{ justifyContent: "flex-start", flex: 1 }}
        >
          <KeyboardAwareScrollView
            contentContainerStyle={{ flexGrow: 1 }}
            keyboardShouldPersistTaps="always"
          >
            <View
              style={{
                justifyContent: "space-between",
                flex: 1,
                paddingBottom: APP_DIMENSIONS.SCREEN_HEIGHT * 0.055,
                alignContent: "center"
              }}
            >
              <Layout.FormTextInput
                value={this.state.identificationInput}
                placeholder={t(
                  "signUp:userDataInputFields.cpfInputPlaceHolder"
                )}
                onSubmitEditing={event => {
                  event && this.fullName.focus();
                }}
                editable={false}
                maskActive={true}
                type={this.state.inputMask}
              />
              <Layout.FormTextInput
                placeholder={t(
                  "signUp:userDataInputFields.nameInputPlaceHolder"
                )}
                inputRef={r => {
                  this.fullName = r;
                }}
                onSubmitEditing={event => {
                  event && this.cellPhone.focus();
                }}
                onChangeText={this._onFullNameInputChange}
                value={this.state.fullName}
                onFocus={this._didStartEditing}
                inputError={this.state.nameError}
              />
              <Layout.FormTextInput
                placeholder={t(
                  "signUp:userDataInputFields.cellPhoneInputPlaceHolder"
                )}
                inputRef={r => {
                  this.cellPhone = r;
                }}
                onSubmitEditing={event => {
                  event && this.email.focus();
                }}
                keyboardType={Platform.OS==='ios' ? "decimal-pad" : 'numeric'}
                maskActive={true}
                type={getPhoneMask(this.props.i18n.language)}
                onChangeText={this._onPhoneInputChange}
                value={this.state.phone}
                onFocus={this._didStartEditing}
                returnKeyType='done'
                inputError={this.state.phoneError}
              />

              <Layout.FormTextInput
                placeholder={t(
                  "signUp:userDataInputFields.emailInputPlaceHolder"
                )}
                inputRef={r => {
                  this.email = r;
                }}
                onSubmitEditing={event => {
                  event && this.password.focus();
                }}
                onChangeText={this._onEmailInputChange}
                value={this.state.email}
                onFocus={this._didStartEditing}
                keyboardType={'email-address'}
                textContentType={'emailAddress'}
                inputError={this.state.emailError}
              />
              <Layout.FormTextInput
                placeholder={t(
                  "signUp:userDataInputFields.passwordInputPlaceHolder"
                )}
                inputRef={r => {
                  this.password = r;
                }}
                onChangeText={this._onPasswordInputChange}
                secureTextEntry={true}
                textContentType={"password"}
                value={this.state.password}
                onFocus={this._didStartEditing}
                inputError={this.state.passwordError}
                onSubmitEditing={this._validateBeforeProceed}
              />
              {this.state.emptyError == true && (
                <Layout.FontText
                  style={{
                    color: COLORS.RED.FIRE_ENGINE_RED,
                    fontFamily: "open-sans-semibold"
                  }}
                >
                  {t("signUp:userDataInputFields.inputValidation")}
                </Layout.FontText>
              )}
            </View>
          </KeyboardAwareScrollView>
        </Layout.KeyboardAvoidingContainer>
        <Layout.BottomTabContainer>
          <Layout.TouchableContainer
            onPress={this._validateBeforeProceed}
          >
            <Layout.BottomTabButtonContainer
              style={{ backgroundColor: COLORS.RED.FIRE_ENGINE_RED }}
            >
              <Layout.BottomTabButtonText style={{ color: COLORS.WHITE.WHITE }}>
                {t("common:next")}
              </Layout.BottomTabButtonText>
              <Layout.BottomTabIcon
                name="arrow-right"
                size={APP_DIMENSIONS.SCREEN_WIDTH * 0.092}
              />
            </Layout.BottomTabButtonContainer>
          </Layout.TouchableContainer>
        </Layout.BottomTabContainer>
      </Layout.ViewContainer>
    );
  }
}

const mapStateToProps = state => ({
  passportRaw: state.signUp.cpfInput.cpfInputResponse.passport_raw,
  passportNo: state.signUp.cpfInput.cpfInputResponse.passport_no,
  signUpResponse: state.signUp.signUp.signUpResponse,
  loading: state.signUp.signUp.loading,
  fetching: state.signUp.signUp.fetching,
  error: state.signUp.signUp.error,
  modalMessage: state.signUp.signUpModal.modalMessage,
  installationId: state.installationId.installationId,
  cpfOrCnpj: state.signUp.cpfInput.cpfOrCnpj,
});

const mapDispatchToProps = dispatch => {
  return {
    signUp: (
      full_name,
      personalIdentifier,
      phone,
      email,
      password,
      validationType
    ) => {
      dispatch(
        Actions.signUp(
          full_name,
          personalIdentifier,
          phone,
          email,
          password,
          validationType
        )
      );
    },
    resetSignUpResponse: () => {
      dispatch(Actions.resetSignUpResponse());
    },
    openModal: modalKey => {
      dispatch(Actions.setModalVisible(modalKey));
    },
    closeModal: modalKey => {
      dispatch(Actions.setModalInvisible(modalKey));
    }
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(UserDataInputFieldsView);
