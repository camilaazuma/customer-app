import React from "react";
import { View, Text } from "react-native";
import { COLORS } from "../../../constants/styles";
import APP_DIMENSIONS from "../../../constants/appDimensions";
import styled from "styled-components/native";
import * as Layout from "../../layout";
import { translate } from 'react-i18next';
import FastImage from 'react-native-fast-image-expo';
import Barcode from 'react-native-barcode-builder';
import { connect } from "react-redux";
import { FontText } from "../../layout";
import AsyncStorageHelper from "../../../helpers/AsyncStorageHelper";

const SCREEN_WIDTH = APP_DIMENSIONS.SCREEN_WIDTH;
const SCREEN_HEIGHT = APP_DIMENSIONS.SCREEN_HEIGHT;

const IDCardImage = (props) =>
  <FastImage
    style={{
      flex: 1,
      width: SCREEN_WIDTH,
      height: SCREEN_HEIGHT,
      alignSelf: `center`,
    }}
    resizeMode= {FastImage.resizeMode.contain}
    {...props}
  />


export const TERMS_OF_USE_ACCEPTED_KEY = "TERMS_OF_USE_ACCEPTED";
export const PASSPORT_BARCODE_KEY = "PASSPORT_BARCODE";

@translate(['idCard, common'], { wait: true })
class IDCardView extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            barcodeError: false,
            barcode: null
        }

        this.didFocusSubscription = this.props.navigation.addListener(
            "didFocus",
            async (payload) => {
                this._loadBarcode()
            }
        );
    }

    componentDidMount = async () => {
        await this._loadBarcode();
    }

    _loadBarcode = async () => {
        let storedBarcode = await AsyncStorageHelper.getItem(PASSPORT_BARCODE_KEY);
        if (storedBarcode)
            this.setState({barcode: storedBarcode, barcodeError: false});
        else {
            let passportNo = null;
            if(this.props.user.passportNo)
              passportNo= this.props.user.passportNo.toUpperCase();
            let userPassport = "";
            let storeNo = "";
            if (passportNo && passportNo.includes("S")) {
                [userPassport, storeNo] = passportNo.split("S");
                let barcode = this.generateBarcode(userPassport, storeNo);
                this.setState({barcode});
            }
            else {
                this.setState({barcode: "error"});
            }
        }
    }

    componentWillUnmount() {
        this.didFocusSubscription.remove();
    }

    //TODO
    generateBarcode = (userPassport, storeNo) => {
        try {
            let countryCode = "20"; // 20 is for Colombia
            let customerCheckDigit = this.generateCheckDigit(userPassport, storeNo);
            let first12digits = countryCode + storeNo + userPassport + customerCheckDigit;
            let ean13checkDigit = this.generateEAN13Digit(first12digits);
            let barcode = first12digits + ean13checkDigit;
            return barcode;
        }
        catch(e) {
            console.log(e.message);
            return "error";
        }
    }

    generateCheckDigit = (userPassport, storeNo) => {
        let originalPassport = userPassport;
        let originalStoreNo = storeNo;
        // dont know what pseq stands for, it was written in a database query
        let pseq = 0;

        if (originalPassport.length < 6)
            userPassport += pseq.toString();

        if (originalStoreNo.length === 1)
            storeNo = '0' + storeNo;

        let concatenatedString =  storeNo + userPassport + pseq%10;

        let firstDigit = 0;
        const MAX_ITERATIONS = 10000;
        let loopCounter = 0;

        while (loopCounter < MAX_ITERATIONS) {
     
            let splittedArray = concatenatedString.split("").map((digit, index)=>{
                digit = parseInt(digit);
                return (10 - index) * digit;
            })

            let sumResult = splittedArray.reduce((a, b)=>a + b, 0);

            firstDigit=sumResult%11;
            if (firstDigit!==10) break;
            loopCounter++;

            pseq = loopCounter;

            if (originalPassport.length < 6) {
                userPassport += pseq.toString();
                userPassport = userPassport.slice(1);
                pseq = 0;
            }
            concatenatedString =  storeNo + userPassport + pseq%10;

        }
        let checkDigit = firstDigit.toString() + loopCounter.toString();
        return checkDigit;
    }

    generateEAN13Digit = (first12digits) => {
        let digitArray = (""+first12digits).split('')
        let sum = digitArray.reduce((accumulator, value, index)=>{
            return index % 2 == 0 ? accumulator + 1 * value : accumulator + 3 * value
        }, 0);
        let checkDigit = ((Math.trunc(sum/10) + 1)*10 - sum)%10;
        return ""+checkDigit;
    }
    
    handleBarcodeError = () => {this.setState({barcodeError: true})}

    _navigateToScan = ()=>{this.props.navigation.navigate("Scan")} 

    render() {
        const { t, i18n } = this.props;
        return (
            <Layout.ViewContainer>
                <Layout.Title text={t("idCard:title")} />
                <View style={{ flex: 1 }}>
                    <View style={{ flex: 1, justifyContent: "center", alignItems: "center", elevation: 10 }}>
                        <IDCardImage 
                            source={require("../../../../assets/passport_card.png")}
                        />
                    </View>
                    <View style={{ flex: 1, justifyContent: "center", alignItems: "center",  }}>
                        {this.state.barcodeError && 
                            <Layout.FontText style={{fontSize: APP_DIMENSIONS.SCREEN_WIDTH*0.06, textAlign: 'center'}}>
                                {"Sin código de barras registrado"}
                            </Layout.FontText>
                        }
                        { (!this.state.barcodeError && this.state.barcode) &&
                        <Barcode value={this.state.barcode} format="EAN13" text={this.state.barcode} onError={this.handleBarcodeError} flat />
                        }
                    </View>
                    <Layout.BottomTabContainer>
                        <Layout.TouchableContainer onPress={this._navigateToScan}>
                            <Layout.BottomTabButtonContainer style={{backgroundColor: COLORS.RED.FIRE_ENGINE_RED}} >
                            <Layout.BottomTabButtonText style={{color: COLORS.WHITE.WHITE}}>{t('idCard:scanPassport')}</Layout.BottomTabButtonText>
                            </Layout.BottomTabButtonContainer>
                        </Layout.TouchableContainer>
                    </Layout.BottomTabContainer>
                </View>
            </Layout.ViewContainer>
        );
    }
}

const mapStateToProps = state => ({
    user: state.user,
});

export default connect(
    mapStateToProps,
    null
  )(IDCardView);

