import React from "react";
import * as Layout from "../../layout";
import { translate } from 'react-i18next';
import { connect } from "react-redux";
import {
    View,
    TouchableOpacity,
} from "react-native";
import { COLORS } from "../../../constants/styles";
import APP_DIMENSIONS from "../../../constants/appDimensions";
import { Camera, Permissions, Audio } from 'expo';
import { withNavigationFocus } from 'react-navigation';
import { Icon } from "react-native-elements";
import AsyncStorageHelper from "../../../helpers/AsyncStorageHelper";
import Barcode from 'react-native-barcode-builder';

const PASSPORT_BARCODE_KEY = "PASSPORT_BARCODE";


@translate(['idCard, common'], { wait: true })
class ScanView extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            hasCameraPermission: null,
            type: Camera.Constants.Type.back,
            manualInquiryInput: "",
            beepEnabled: true,
            flashMode: Camera.Constants.FlashMode.off,
            barcode: "",
            barcodeError: false,
            barcodeModal: false
        }
    }

    //TODO
    componentDidMount = async () => {
        const onWillBlurSubscription = this.props.navigation.addListener(
            "willBlur",
            payload => {
                this.setState({
                    flashMode: Camera.Constants.FlashMode.off
                });
            }
        );
        AsyncStorageHelper.removeItem(PASSPORT_BARCODE_KEY);
        const { status } = await Permissions.askAsync(Permissions.CAMERA);
        this.setState({ hasCameraPermission: status === 'granted' });
    }

    _onBarCodeScanned = async ({ type, data }) => {

        if (this.state.beepEnabled)
            await Audio.Sound.createAsync(require("../../../../assets/beep.wav"), { shouldPlay: true })
        this.setState({ barcode: data, barcodeModal: true });
        console.log("data", data);
    }

    _toggleFlash = () => {
        this.setState({
            flashMode: this.state.flashMode === Camera.Constants.FlashMode.off ? Camera.Constants.FlashMode.torch : Camera.Constants.FlashMode.off
        })
    }

    _toggleBeep = () => {
        this.setState({
            beepEnabled: this.state.beepEnabled ? false : true
        })
    }

    handleBarcodeError = (e) => { this.setState({ barcodeError: true }); console.log(e); }


    render() {
        const { t, i18n } = this.props;
        return (
            <Layout.ViewContainer>
                <Layout.ModalWrapper
                    modalVisible={this.state.barcodeModal}
                    showOk={!this.state.barcodeError}
                    showCancel
                    cancelText={"Cancelar"}
                    okText={"Confirmar"}
                    onOk={async () => {
                        this.setState({ barcodeModal: false });
                        await AsyncStorageHelper.setItem(PASSPORT_BARCODE_KEY, this.state.barcode);
                        this.props.navigation.goBack(null);
                    }}
                    onCancel={() => {
                        this.setState({ barcodeModal: false, barcodeError: false });
                    }}
                    containerStyle={{ height: APP_DIMENSIONS.SCREEN_HEIGHT * 0.4 }}
                    onPressOutside={() => {
                        this.setState({ barcodeModal: false, barcodeError: false });
                    }}
                >
                    <Layout.FontText style={{ fontSize: APP_DIMENSIONS.SCREEN_WIDTH * 0.05 }}>
                        {"Pasaporte escaneado"}
                    </Layout.FontText>
                    {(!this.state.barcodeError && this.state.barcode) &&
                        <Barcode
                            value={this.state.barcode} format="EAN13" text={this.state.barcode} onError={this.handleBarcodeError}
                            flat
                            height={APP_DIMENSIONS.SCREEN_HEIGHT * 0.17}
                            width={APP_DIMENSIONS.SCREEN_WIDTH * 0.007}
                        />
                    }
                    {
                        this.state.barcodeError &&
                        <Layout.FontText style={{ fontSize: APP_DIMENSIONS.SCREEN_WIDTH * 0.05 }}>
                            {"Formato invalido"}
                        </Layout.FontText>
                    }
                </Layout.ModalWrapper>



                <Layout.Title text={t("idCard:scanTitle")} />
                <View style={{ flex: 1 }}>
                    {this.state.hasCameraPermission ?
                        <View style={{ height: "100%" }}>
                            <View style={{ flex: 1, backgroundColor: COLORS.WHITE.WHITE }}>
                                <Camera style={{ flex: 1, }} type={this.state.type} onBarCodeScanned={
                                    this.props.isFocused && !this.state.barcodeModal ? this._onBarCodeScanned : undefined
                                }
                                    flashMode={this.state.flashMode}>
                                    <View
                                        style={{
                                            flex: 1,
                                            flexDirection: 'column',
                                        }}
                                    >
                                        <View style={{ flex: 0.2, backgroundColor: 'rgba(0,0,0,0.6)', }} />

                                        <View style={{ flex: 0.6, flexDirection: 'row' }}>
                                            <View style={{ flex: 0.1, backgroundColor: 'rgba(0,0,0,0.6)', }} />

                                            <View style={{ flex: 0.8, backgroundColor: 'transparent', alignItems: "center", justifyContent: "center" }}>
                                                <View style={{ position: "absolute", top: 0, left: 0, width: 30, height: 30, borderColor: "white", borderLeftWidth: 2, borderTopWidth: 2 }} />
                                                <View style={{ position: "absolute", top: 0, right: 0, width: 30, height: 30, borderColor: "white", borderRightWidth: 2, borderTopWidth: 2 }} />
                                                <View style={{ position: "absolute", bottom: 0, left: 0, width: 30, height: 30, borderColor: "white", borderLeftWidth: 2, borderBottomWidth: 2 }} />
                                                <View style={{ position: "absolute", bottom: 0, right: 0, width: 30, height: 30, borderColor: "white", borderRightWidth: 2, borderBottomWidth: 2 }} />
                                            </View>
                                            <View style={{ flex: 0.1, backgroundColor: 'rgba(0,0,0,0.6)' }} />
                                        </View>

                                        <View style={{ flex: 0.2, flexDirection: 'row', backgroundColor: 'rgba(0,0,0,0.6)', borderTopColor: 'transparent', marginTop: -0.2 }}>
                                            <TouchableOpacity
                                                style={{
                                                    flex: 1,
                                                    alignSelf: 'flex-end',
                                                    alignItems: 'center',
                                                }}
                                                onPress={() => {
                                                    this.setState({
                                                        type: this.state.type === Camera.Constants.Type.back
                                                            ? Camera.Constants.Type.front
                                                            : Camera.Constants.Type.back,
                                                    });
                                                }}>
                                                <Layout.FontText
                                                    style={{ fontSize: APP_DIMENSIONS.SCREEN_WIDTH * 0.043, marginBottom: 10, color: 'white', textAlign: 'center', fontFamily: 'open-sans', }}>
                                                    {' '}Flip{' '}
                                                </Layout.FontText>
                                            </TouchableOpacity>
                                            <TouchableOpacity
                                                style={{
                                                    flex: 1,
                                                    alignSelf: 'flex-end',
                                                    alignItems: 'center',
                                                }}
                                                onPress={this._toggleFlash}
                                            >
                                                <Icon
                                                    color={'white'}
                                                    name='flash'
                                                    type='font-awesome'
                                                    containerStyle={{
                                                        padding: 10
                                                    }}
                                                />
                                            </TouchableOpacity>
                                            <TouchableOpacity
                                                style={{
                                                    flex: 1,
                                                    alignSelf: 'flex-end',
                                                    alignItems: 'center',
                                                }}
                                                onPress={this._toggleBeep}
                                            >
                                                <Icon
                                                    color={'white'}
                                                    name={this.state.beepEnabled ? 'volume-high' : 'volume-off'}
                                                    type='material-community'
                                                    containerStyle={{
                                                        padding: 10
                                                    }}
                                                />
                                            </TouchableOpacity>
                                        </View>
                                    </View>
                                </Camera>
                            </View>
                            <Layout.BottomTabContainer style={{ height: APP_DIMENSIONS.SCREEN_HEIGHT * 0.068, }}>
                                <View style={{ justifyContent: 'center', alignItems: 'center', flex: 1 }}>
                                    <Layout.FontText style={{
                                        textAlign: 'center',
                                        fontFamily: 'open-sans',
                                        fontSize: APP_DIMENSIONS.SCREEN_WIDTH * 0.0375
                                    }}> {t("priceInquiry:barCodeInquiry.instructionsText")}</Layout.FontText>
                                </View>
                            </Layout.BottomTabContainer>
                        </View>
                        :
                        <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                            <Layout.FontText style={{ color: COLORS.BLACK.BLACK, fontSize: APP_DIMENSIONS.SCREEN_WIDTH * 0.053, fontFamily: 'open-sans' }}>
                                {t("priceInquiry:barCodeInquiry.noCameraPermissionText")}
                            </Layout.FontText>
                        </View>
                    }
                </View>
            </Layout.ViewContainer>
        );
    }
}

const mapStateToProps = state => ({
    user: state.user,
});

export default connect(
    mapStateToProps,
    null
)(withNavigationFocus(ScanView));

