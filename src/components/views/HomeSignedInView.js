import React from "react";
import {
  Platform,
  Linking,
  Alert,
  NetInfo,
  ScrollView,
  View
} from "react-native";
import { COLORS } from "../../constants/styles";
import * as Layout from "../layout";
import { Font, IntentLauncherAndroid } from "expo";
import { NavigationActions } from "react-navigation";
import APP_DIMENSIONS from "../../constants/appDimensions";
import { translate } from "react-i18next";
import * as Actions from "../../actions";
import { connect } from "react-redux";
import { global, nullFunction, getCustomerType, showSnackbar, isArray } from "../../utils";
import AsyncStorageHelper, { KEYS } from "../../helpers/AsyncStorageHelper";
import firebase from "react-native-firebase";
import LocationHelper from "../../helpers/LocationHelper";
import {SocialMediaBar} from "../layout/common/socialMedia";

const CAROUSEL_URL_ARRAY_KEY = "CAROUSEL_URL_ARRAY";
const STORE_LIST_KEY = "STORE_LIST";

@translate(["homeSignedIn", "common"], { wait: true })
class HomeSignedInView extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      imagesArray: [],
      locationModalVisible: false,
      selectStoreModalVisible: false,
      selectedStore: null,
      imageLoading: false,
      beforeMount: true
    };
  }

  componentDidMount = async () => {

    // send user properties to firebase
      if (this.props.currentStoreObject !== null && typeof this.props.currentStoreObject !== 'undefined') {
        firebase.analytics().setUserProperty("active_store", this.props.currentStoreObject.store_no);
      }
      firebase.analytics().setUserProperty("customer_type_descr", getCustomerType(this.props.user.personalIdentifier));

    // ask for notification permission
    firebase.messaging().requestPermission()
      .then(() => {
        firebase.messaging().registerForNotifications()
      })
      .catch(error => {
        // User has rejected permissions
      });

    // fetch store list version from firebase remote config
    if (__DEV__)
      firebase.config().enableDeveloperMode();
      
    if (this.props.fetchedStoreList.length === 0) {
      var storeList = await AsyncStorageHelper.getItem(STORE_LIST_KEY);
      try {
        await firebase.config().fetch(720);
      }
      catch (e) {
        console.log("ERROR", e)
      }
      const activated = await firebase.config().activateFetched();
      if (!activated && __DEV__) console.log('Fetched data not activated');
      const snapshot = await firebase.config().getValue('store_version');
      this.firebaseStoreVersion = snapshot.val();
      this.props.setFirebaseStoreVersion(this.firebaseStoreVersion);
      if (storeList !== null && typeof storeList.list !== 'undefined' && this.firebaseStoreVersion !== 'default' && this.firebaseStoreVersion === storeList.version) {
        this.props.fetchStoreListSuccess(storeList.list);
      }
      else if (storeList === null || this.firebaseStoreVersion === 'default' || this.firebaseStoreVersion !== storeList.version) {
        await this.props.fetchStoreList(); // ORIGINAL_COLOMBIA: fetchStoreListByCountry
      }
    }

    // check carousel cache
    var cacheCarouselArray = await AsyncStorageHelper.getItem(CAROUSEL_URL_ARRAY_KEY);

    if (typeof this.props.fetchedImageList !== 'undefined' && (cacheCarouselArray === null || this.props.fetchedImageList.length > 0))
      this._setCarouselImages(this.props.fetchedImageList);
    else {
      this._setCarouselImages(cacheCarouselArray);
    }
    // get location
    if (this.props.location === null)
      await this.props.getLocation();

    // Verifica se abriu alguma lista compartilhada
    Linking.getInitialURL()
      .then(url => {
        _openDeepLink(url, this.props, true)
      })
      .catch(err => {
        console.warn('Deeplinking error', err)
      });

    Linking.addListener('url', e => {
      _openDeepLink(e.url, this.props, false)
    });

    _openDeepLink = (url, props, openOnce) => {
      function getParameterByName(name, url) {
        if (!url) url = window.location.href;
        name = name.replace(/[\[\]]/g, '\\$&');
        var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
          results = regex.exec(url);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, ' '));
      }

      if (url && url.includes("?list=")) {
        var listId = getParameterByName('list', url);

        if (!global.openedListIds) {
          global.openedListIds = [];
        }

        if (openOnce && global.openedListIds.indexOf(listId) != -1) {
          return;
        }
        global.openedListIds.push(listId);
        props.getSharedShopList(props.user, listId, props);
      }
    };
    this.setState({ beforeMount: true })
  }

  componentDidUpdate = async (prevProps, prevState) => {
    // ordena lista de lojas por distancia
    if (!(this._isReadyToGetDistance(prevProps)) && this._isReadyToGetDistance(this.props)) {
      this.props.sortStoreList();
    }

    // Mostra modal com erro de localizacao, caso localizacao esteja desativada
    if (this.props.locationOffError && !prevProps.locationOffError && !this.props.currentStoreObject) {
      this.setState({ locationModalVisible: true });
    }
    // Mostra modal de selecionar loja caso nao consiga achar localizacao
    if (
      (this.props.locationTimeError && !prevProps.locationTimeError || this.props.locationAccessDeniedError && !prevProps.locationAccessDeniedError)
      && this.props.currentStoreObject === null
    ) {
      this.setState({ selectStoreModalVisible: true });
    }


    // caso nao consiga carregar banner, verifica cache
    if (!prevProps.fetchedImageError && this.props.fetchedImageError) {
      var cacheCarouselArray = await AsyncStorageHelper.getItem(CAROUSEL_URL_ARRAY_KEY);
      // se cache vazio, mostra banner padrao
      if (cacheCarouselArray === null)
        this._setCarouselImages([]);
      else
        this._setCarouselImages(cacheCarouselArray);

    } else if (
      Array.isArray(this.props.fetchedImageList) && Array.isArray(prevProps.fetchedImageList)
    ) {
      if (this.props.fetchedImageList.length > 0) {
        if (prevProps.fetchedImageList.length === 0) {
          await AsyncStorageHelper.setItem(CAROUSEL_URL_ARRAY_KEY, this.props.fetchedImageList);
          this._setCarouselImages(this.props.fetchedImageList);
        }
        else if (prevProps.fetchedImageList.length > 0 && this.props.fetchedImageList[0].cluster !== prevProps.fetchedImageList[0].cluster) {
          await AsyncStorageHelper.setItem(CAROUSEL_URL_ARRAY_KEY, this.props.fetchedImageList);
          this._setCarouselImages(this.props.fetchedImageList);
        }
      }
    }

    // atualiza banners ao mudar de cluster
    if (prevProps.currentStoreObject !== null && prevProps.currentStoreObject.cluster_no !== this.props.currentStoreObject.cluster_no
      || prevProps.currentStoreObject === null && this.props.currentStoreObject !== null && typeof (this.props.currentStoreObject.cluster_no) !== "undefined") {
      await this.props.fetchHomeCarousel(this.props.currentStoreObject.cluster_no);
    }

  }

  static getDerivedStateFromProps(nextProps, prevState) {
    var obj = {};
    if (prevState.beforeMount) {
      if (Array.isArray(nextProps.fetchedImageList)) {
        if (nextProps.fetchedImageList.length === 0) {
          if (nextProps.currentStoreObject !== null)
            nextProps.fetchHomeCarousel(nextProps.currentStoreObject.cluster_no);
          else
            nextProps.fetchHomeCarousel();
          obj.imageLoading = true;
        } else if (nextProps.fetchedImageList.length > 0 && nextProps.currentStoreObject !== null && typeof (nextProps.fetchedImageList[0].cluster) !== "undefined") {
          if (nextProps.currentStoreObject.cluster_no !== nextProps.fetchedImageList[0].cluster) {
            nextProps.fetchHomeCarousel(nextProps.currentStoreObject.cluster_no);
            if (nextProps.fetchedImageList[0].cluster !== null) {
              obj.imageLoading = true;
            }
          }
        }
      }
      obj.beforeMount = false;
    }

    if (nextProps.fetchedStoreList !== prevState.fetchedStoreList) {
      obj.fetchStoreList = nextProps.fetchedStoreList;
    }

    return Object.keys(obj).length ? obj : null;

  }

  _setCarouselImages = (imageList) => {
    var a = imageList.map(item => {
      return (
        <Layout.CarouselImage
          source={{ uri: item.img_url }}
          href={item.img_href}
        />
      );
    });
    this.setState({ imagesArray: [] }, () => this.setState({ imagesArray: a, imageLoading: false }));
  }

  _fallBackImage =
    <Layout.CarouselImage
      source={require('../../../assets/374.png')}
    />


  _selectStore = async () => {
    let { t } = this.props;
    if (this.state.selectedStore === null) {
      Alert.alert(
        t("common:errors.error"),
        t("common:errors.storeSelect"),
        [
          { text: 'OK' },
        ],
        { cancelable: true }
      );
    }
    else {
      this.props.setCurrentStore(this.state.selectedStore)
      this.setState({ selectStoreModalVisible: false });
    }
  }

  _activateLocation = () => {
    this.setState({ locationModalVisible: false })
    IntentLauncherAndroid.startActivityAsync(IntentLauncherAndroid.ACTION_LOCATION_SOURCE_SETTINGS);
  }

  _isReadyToGetDistance = (props) =>
    (props.fetchedStoreList.length !== 0 && props.location !== null)

  navigationAction = (routeName, action = null) =>
    NavigationActions.navigate({ routeName: routeName, action: action });

  _navigateToShopListView = () => {
    this.props.navigation.dispatch(this.navigationAction('ShoppingLists', this.navigationAction('ShoppingLists')));
  }

  render() {
    const { t, i18n } = this.props;
    return (
      <Layout.ViewContainer style={{ backgroundColor: 'transparent' }}>
        <Layout.TwoButtonsModal
          t={t}
          modalVisible={this.state.locationModalVisible}
          onOk={this._activateLocation}
          onCancel={() => {
            this.setState({ locationModalVisible: false, selectStoreModalVisible: true });
          }}
          title={t && t('common:locationModal.title')}
          contentText={t && t('common:locationModal.content')}
          okText={t && t("common:locationModal.okButton")}
          cancelText={t && t('common:locationModal.cancelButton')}
          hidePicker={true}
        />
        <Layout.SelectStoreModal
          t={t}
          modalVisible={this.state.selectStoreModalVisible}
          closeModal={this.props.closeModal}
          onOk={this._selectStore}
          lists={this.props.fetchedStoreList}
          onValueChange={(itemValue, itemIndex) => { this.setState({ selectedStore: itemValue }) }}
          selectedValue={this.state.selectedStore}
          storeListLoading={this.props.storeListLoading}
          onPressOutside={() => { this.props.currentStoreObject !== null ? this.setState({ selectStoreModalVisible: false }) : null }}
          language={i18n.language}
        />
        <Layout.StatusBarContainer />
        <Layout.HeaderContainer>
          <Layout.MenuIcon onPress={this.props.navigation.openDrawer} />
          <Layout.HeaderLogo navigation={this.props.navigation} />
        </Layout.HeaderContainer>



          <ScrollView bounces={false}>
            <Layout.CarouselContainer>
              <Layout.ImageCarousel
                images={this.state.imagesArray}
                autoplay={true}
                fallBackImage={this._fallBackImage}
                loading={this.state.imageLoading}
              />
            </Layout.CarouselContainer>
            <Layout.HomeBottomTabContainer_CO>
              <Layout.HomeMenuRowContainer>
                <Layout.TouchableContainer
                  opacity
                  onPress={() => { setTimeout(() => this.props.navigation.navigate("Offers"), 1) }}
                >
                  <Layout.HomeBottomTabButtonContainer style={{ shadowOffset: { width: 0, height: 4 } }}>
                    <Layout.HomeBottomTabIcon
                      size={APP_DIMENSIONS.SCREEN_WIDTH * 0.12}
                      name="shopping-cart"
                      color={COLORS.RED.FIRE_ENGINE_RED}
                    />
                    <Layout.BottomTabButtonText
                      style={{
                        fontSize: APP_DIMENSIONS.SCREEN_WIDTH * 0.05,
                        color: COLORS.RED.FIRE_ENGINE_RED,
                        fontFamily: "open-sans-bold"
                      }}
                    >
                      {t("homeSignedIn:offersButton")}
                    </Layout.BottomTabButtonText>
                  </Layout.HomeBottomTabButtonContainer>
                </Layout.TouchableContainer>
                <Layout.TouchableContainer
                  opacity
                  onPress={() =>
                    setTimeout(
                      async () => {
                        let isConnected = await NetInfo.isConnected.fetch();
                        if (!isConnected)
                          showSnackbar(this.props.t("common:errors.noConnection"));
                        else
                          this.props.navigation.dispatch(
                            this.navigationAction(
                              "PriceInquiry",
                              this.navigationAction("InquiryType")
                            )
                          )
                      },
                      1
                    )
                  }
                >
                  <Layout.HomeBottomTabButtonContainer style={{ shadowOffset: { width: 0, height: 4 } }} rightButton>
                    <Layout.HomeBottomTabIcon name="barcode" />
                    <Layout.BottomTabButtonText>
                      {t("homeSignedIn:priceInquiryButton")}
                    </Layout.BottomTabButtonText>
                  </Layout.HomeBottomTabButtonContainer>
                </Layout.TouchableContainer>
              </Layout.HomeMenuRowContainer>
              <Layout.HomeMenuRowContainer>
              <Layout.TouchableContainer
                  opacity
                  onPress={() =>
                    setTimeout(() => this.props.navigation.navigate("Stores"), 1)
                  }
                >
                  <Layout.HomeBottomTabButtonContainer style={{ shadowOffset: { width: 0, height: 4 } }} leftButton>
                    <Layout.HomeBottomTabIcon name="map" />
                    <Layout.BottomTabButtonText style={{ marginRight: 0 }}>
                      {t("homeSignedIn:storesButton")}
                    </Layout.BottomTabButtonText>
                  </Layout.HomeBottomTabButtonContainer>
                </Layout.TouchableContainer>


                <Layout.TouchableContainer
                  opacity
                  onPress={() =>
                    setTimeout(() => this.props.navigation.navigate("ContactUs"), 1)
                  }
                >
                  <Layout.HomeBottomTabButtonContainer style={{ shadowOffset: { width: 0, height: 4 } }} leftButton>
                    <Layout.HomeBottomTabIcon name="comments" />
                    <Layout.BottomTabButtonText style={{ marginRight: 0 }}>
                      {t("homeSignedIn:contactUs")}
                    </Layout.BottomTabButtonText>
                  </Layout.HomeBottomTabButtonContainer>
                </Layout.TouchableContainer>
              </Layout.HomeMenuRowContainer>

              <SocialMediaBar i18n={i18n}  t={t}/>
            </Layout.HomeBottomTabContainer_CO>


          </ScrollView>

      </Layout.ViewContainer>
    );
  }
}

const mapStateToProps = state => ({
  fetchedImageList: state.home.fetchHomeCarousel.carouselImageList,
  fetchedImageError: state.home.fetchHomeCarousel.error,
  fetchedStoreList: state.storeListFetch.storeList,
  storeListLoading: state.storeListFetch.loading,

  isSorted: state.storeListFetch.isSorted,
  location: state.location.location,
  locationOffError: state.location.turnedOffError,
  locationTimeError: state.location.timedOutError,
  locationAccessDeniedError: state.location.accessDeniedError,
  user: state.user,
  nearestStore: state.storeListFetch.nearestStore,
  currentStoreObject: state.currentStore.object,
});

const mapDispatchToProps = dispatch => {
  return {
    fetchHomeCarousel: (cluster_no) => {
      dispatch(Actions.fetchHomeCarousel(cluster_no));
    },
    fetchStoreList: () => {
      dispatch(Actions.fetchStoreList());
    },
    fetchStoreListByCountry: () => {
      dispatch(Actions.fetchStoreListByCountry());
    },
    fetchStoreListSuccess: (storeList) => {
      dispatch(Actions.fetchStoreListSuccess(storeList));
    },
    setFirebaseStoreVersion: (version) => {
      dispatch(Actions.setFirebaseStoreVersion(version));
    },
    getLocation: (forceAsk) => {
      dispatch(Actions.getLocation(forceAsk));
    },
    sortStoreList: () => {
      dispatch(Actions.sortStoreList());
    },
    getShopList: ({ passport, updated, personalIdentifier } = {}) => {
      dispatch(Actions.getShopList(passport, updated, personalIdentifier));
    },
    getSharedShopList: (user, shopListId, props) => {
      dispatch(Actions.getSharedShopList(user, shopListId, props));
    },
    getListDetails: ({ productsList } = {}) => {
      dispatch(Actions.getProductsFromList(productsList));
    },
    setCurrentStore: (store) => {
      dispatch(Actions.setCurrentStore(store))
    },
    fetchPreferredStore: (user) => {
      dispatch(Actions.fetchPreferredStore(user));
    },
    setPreferredStore: (store) => {
      dispatch(Actions.setPreferredStore(store))
    },
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(HomeSignedInView);
