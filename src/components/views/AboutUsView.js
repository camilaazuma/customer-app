import React from "react";
import { View, Platform } from "react-native";
import { COLORS } from "../../constants/styles";
import APP_DIMENSIONS from "../../constants/appDimensions";
import styled from "styled-components/native";
import * as Layout from "../layout";
import {SocialMediaBar} from "../layout/common/socialMedia";
import { translate } from 'react-i18next';

const SCREEN_WIDTH = APP_DIMENSIONS.SCREEN_WIDTH;
const SCREEN_HEIGHT = APP_DIMENSIONS.SCREEN_HEIGHT;

const OuterTextContainer = styled.ScrollView`
  flex: 1;
  backgroundColor: ${COLORS.WHITE.WHITE_SMOKE};
  padding: ${SCREEN_WIDTH / 17}px;
`;

const InnerTextContainer = styled.View`
  elevation: 2;
  padding: ${SCREEN_WIDTH / 14}px;
  margin: 5px;
  backgroundColor: ${COLORS.WHITE.WHITE};
  shadowColor: black;
  shadowOpacity: 0.2;
`;

const AboutUsTitle = styled(Layout.FontText)`
  color: ${COLORS.RED.FIRE_ENGINE_RED};
  fontSize: ${SCREEN_WIDTH * 0.0375};
  fontFamily: open-sans-semibold;
  letterSpacing: ${SCREEN_WIDTH * 0.0016};
`;


const Separator = styled.View`
  marginTop: ${SCREEN_HEIGHT / 61};
  marginBottom: ${SCREEN_HEIGHT / 45.7};
  borderBottomColor: ${COLORS.GREY.LIGHT_GREY};
  borderBottomWidth: 1;
`;


const AboutUsText = styled(Layout.FontText)`
  fontSize: ${SCREEN_HEIGHT / 56.2};
  lineHeight: 20;
  textAlign: justify;
  fontFamily: open-sans;
  letterSpacing: ${SCREEN_WIDTH * 0.0016};
  paddingBottom: 20;
`;

export const TERMS_OF_USE_ACCEPTED_KEY = "TERMS_OF_USE_ACCEPTED";

@translate(['AboutUs'], { wait: true })
class AboutUsView extends React.Component {

  render() {
    const { t, i18n } = this.props;
    return (
      <Layout.ViewContainer>
        <Layout.StatusBarContainer />
        <Layout.HeaderContainer>
          <Layout.BackIcon onPress={this.props.navigation.goBack} />
          <Layout.HeaderLogo navigation={this.props.navigation} />
        </Layout.HeaderContainer>
        <Layout.Title text={t("aboutUs:title")} />
        <OuterTextContainer>
          <InnerTextContainer>
            <AboutUsTitle>{t("aboutUs:subTitle")}</AboutUsTitle>
            <Separator />
            <AboutUsText>{t("aboutUs:text")}</AboutUsText>

            {i18n.language !== "es-CO" &&
              <View>
                <AboutUsTitle>{t("aboutUs:missionSubtitle")}</AboutUsTitle>
                <Separator />
                <AboutUsText>{t("aboutUs:missionText")}</AboutUsText>
                <AboutUsTitle>{t("aboutUs:visionSubtitle")}</AboutUsTitle>
                <Separator />
                <AboutUsText>{t("aboutUs:visionText")}</AboutUsText>
                <AboutUsTitle>{t("aboutUs:valueSubtitle")}</AboutUsTitle>
                <Separator />
                <AboutUsText>{t("aboutUs:valueText")}</AboutUsText>
              </View>
            }


          </InnerTextContainer>
          <View style={{paddingBottom: APP_DIMENSIONS.SCREEN_WIDTH/12}}>
            <SocialMediaBar i18n={i18n} t={t}/>
          </View>
        </OuterTextContainer>
      </Layout.ViewContainer>
    );
  }
}

export default AboutUsView;
