import React from "react";
import {
  View,
  Text,
  Image,
  TextInput,
  KeyboardAvoidingView,
  ScrollView,
  Keyboard,
  ActivityIndicator,
  Button,
  Platform
} from "react-native";
import { Icon, List, ListItem } from "react-native-elements";
import { COLORS } from "../../../constants/styles";
import styled, { ThemeProvider } from "styled-components/native";
import * as Layout from "../../layout";
import * as Actions from "../../../actions";
import { connect } from "react-redux";
import { translate } from 'react-i18next';
import APP_DIMENSIONS from "../../../constants/appDimensions";

const SCREEN_HEIGHT = APP_DIMENSIONS.SCREEN_HEIGHT;
const SCREEN_WIDTH = APP_DIMENSIONS.SCREEN_WIDTH;

@translate(['myPurchases', 'common'], { wait: true })
class PurchaseDetailView extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      successMessage: false
    };
  }

  render() {
    const { loading, successMessage, t } = this.props;
    const purchase = this.props.navigation.getParam('purchase','')
    return (
      <Layout.ViewContainer>
        {/* <Layout.Loader loading={loading} /> */}
        <Layout.Title text={t("myPurchases:title")} />
        <View style={{ flex: 1 }}>
          <Layout.TotalPriceListFirstItem 
            itemTitle1={purchase.storeName}
            itemTitle2={'20/05/2018 às 18h34'}
            subTitle={'PEDIDO #2234453453'}
            info1={t("myPurchases:purchaseDetail.viewInvoice")}
          />
          <ScrollView>
            <List containerStyle={{ marginTop: 0, borderTopWidth: 0 }}>
              {purchase.products.map((product, index) => {
                return (
                  <ListItem
                    key={index}
                    itemTitle={product.name}
                    subTitle={product.code}
                    info1 = {'R$' + product.currentPrice}                              
                    info2={product.totalQuantity} 
                    component={Layout.TotalPriceListItem}
                    // onPress={()=>{this.props.openModal(product.offerActive ? 'addToShoppingList' : 'activateOffer')}}
                  />
                );
              })}
            </List>
          </ScrollView>
        </View>
      </Layout.ViewContainer>
    );
  }
}

const mapStateToProps = state => ({
  modals: {
    createList: state.shoppingLists.createListModal.createList
  }
});

const mapDispatchToProps = dispatch => {
  return {
    openModal: modalKey => {
      dispatch(Actions.setModalVisible(modalKey));
    },
    closeModal: modalKey => {
      dispatch(Actions.setModalInvisible(modalKey));
    }
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(PurchaseDetailView);

// export default PurchaseDetailView;
