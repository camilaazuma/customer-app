import React from "react";
import {
  View,
  Text,
  Image,
  TextInput,
  KeyboardAvoidingView,
  ScrollView,
  Keyboard,
  ActivityIndicator,
  Button,
  Platform
} from "react-native";
import { Icon, List, ListItem } from "react-native-elements";
import { COLORS } from "../../../constants/styles";
import styled from "styled-components/native";
import * as Layout from "../../layout";
import * as Actions from "../../../actions";
import { connect } from "react-redux";
import { myPurchasesList } from "../../../config/mockdata/myPurchasesList";
import APP_DIMENSIONS from "../../../constants/appDimensions";
import { translate } from 'react-i18next';

const SCREEN_HEIGHT = APP_DIMENSIONS.SCREEN_HEIGHT;
const SCREEN_WIDTH = APP_DIMENSIONS.SCREEN_WIDTH;

@translate(['myPurchases', 'common'], { wait: true })
class MyPurchasesListView extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      successMessage: false
    };
  }

  render() {
    const { loading, successMessage, t } = this.props;
    return (
      <Layout.ViewContainer>
        {/* <Layout.Loader loading={loading} /> */}
        <Layout.CreateShoppingListModal
          modalVisible={this.props.modals.createList}
          onCancel={() =>
            setTimeout(() => {
              this.props.closeModal("createList");
            }, 1)
          }
          onOk={() =>
            setTimeout(() => {
              this.props.closeModal("createList");
              this.props.navigation.navigate("ShoppingLists");
            }, 1)
          }
        />
        <Layout.Title
          text={t("myPurchases:title")}
        />
        <View style={{ flex: 1 }}>
          <ScrollView>
            <List containerStyle={{ marginTop: 0, borderTopWidth: 0 }}>
              {myPurchasesList.map((purchase, index) => {
                return (
                  <ListItem
                    key={index}
                    itemTitle={purchase.date}
                    subTitle={purchase.storeName}
                    info1 = {purchase.totalPrice}                              
                    info2={purchase.productsQuantity} 
                    component={Layout.TotalPriceListItem}
                    onPress={() => { this.props.navigation.navigate("PurchaseDetail", {purchase: purchase})} }
                    // onPress={() => { this.props.navigation.navigate("ListDetail", {shoppingList: shoppingList})} }
                    // onPress={()=>{this.props.openModal(product.offerActive ? 'addToShoppingList' : 'activateOffer')}}
                  />
                );
              })}
            </List>
          </ScrollView>
        </View>
      </Layout.ViewContainer>
    );
  }
}

const mapStateToProps = state => ({
  modals: {
    createList: state.shoppingLists.createListModal.createList
  }
});

const mapDispatchToProps = dispatch => {
  return {
    openModal: modalKey => {
      dispatch(Actions.setModalVisible(modalKey));
    },
    closeModal: modalKey => {
      dispatch(Actions.setModalInvisible(modalKey));
    }
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(MyPurchasesListView);

// export default MyPurchasesListView;
