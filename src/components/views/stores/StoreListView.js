import React from "react";
import {
  View,
  Platform,
  LayoutAnimation,
  UIManager,
} from "react-native";
import { Constants } from "expo";
import { COLORS } from "../../../constants/styles";
import * as Layout from "../../layout";
import * as Actions from "../../../actions";
import { connect } from "react-redux";
import { MapView } from 'expo';
import { translate } from 'react-i18next';
import { titleCase } from "../../../utils";
import APP_DIMENSIONS from "../../../constants/appDimensions";
import { language } from "../../../config/i18n";

@translate(['stores', 'common'], { wait: true })
class StoreListView extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      mapStyle: { height: "40%"},
      mapExpanded: false,
      region: {
        latitude: -13.162073,
        longitude: -50.417475,
        latitudeDelta: 50,
        longitudeDelta: 50,
      },
      regionSet: false,
      locationError: false,
    };
    if (Platform.OS === "android") {
      UIManager.setLayoutAnimationEnabledExperimental(true);
    }
  }

  componentDidMount(){
    this.props.openModal('infoModal');
    this.props.fetchStoreList();

    if (this.props.location===null) {
      this.props.getLocation(true)
    }
  }

  static getDerivedStateFromProps(nextProps, prevState) {

    if (Constants.isDevice && nextProps.location!==null &&  prevState.regionSet===false) {
      return({
        region: {
          latitude: nextProps.location.coords.latitude,
          longitude: nextProps.location.coords.longitude,
          latitudeDelta: 0.3,
          longitudeDelta: 0.3,
        },
        regionSet: true
      })
    }else{
      if(nextProps.locationError == true && nextProps.fetchingLocation == false){
        return ({locationError: true});
      }
    }
    return null;
  }



  _changeLayout = () => {
    LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);

    if (this.state.mapExpanded === false)
      this.setState({
        mapStyle  : { height: "100%" },
        mapExpanded: true
      });
    else
      this.setState({
        mapStyle   : { height: "40%" },
        mapExpanded: false
      });
  };

  _getRegion = () => {
    if (this.props.location===null) {
      // Brazil's center
      return {
        latitude: -13.162073,
        longitude: -50.417475,
        latitudeDelta: 50,
        longitudeDelta: 50,
      }
    }
    else {
      // Device location
      return {
        latitude: this.props.location.coords.latitude,
        longitude: this.props.location.coords.longitude,
        latitudeDelta: 0.3,
        longitudeDelta: 0.3,
      }
    }
  }

  _onRegionChangeComplete = (region) => {
    this.setState({
      region
    });
  }

  _navigateToStore = (store) =>
  {
    // const { curPos, prevPos, curAng } = this.state;
    // const curRot = this.getRotation(prevPos, curPos);
    const region = {
      latitude: parseFloat(store.latitud ), // ORIGINAL_COLOMBIA: latitude
      longitude: parseFloat( store.longitud), // ORIGINAL_COLOMBIA: longitude
      latitudeDelta: 0.03,
      longitudeDelta: 0.03,
    }
    this.map.animateToRegion(
      region,
      1500
    );
    setTimeout(()=>{this.setState({region})}, 1500);
    this.refs[store.store_no].showCallout(); // ORIGINAL_COLOMBIA: idlocation
  }

  render() {
    const { t, i18n } = this.props;
    return (
      <Layout.ViewContainer>
        <Layout.StatusBarContainer />
        <Layout.HeaderContainer>
          <Layout.BackIcon onPress={this.props.navigation.goBack} />
          <Layout.HeaderLogo navigation={this.props.navigation}/>
        </Layout.HeaderContainer>
        <Layout.SuccessMessageModal
          modalVisible={this.props.modals.infoModal}
          okText = {t("stores:modal.okButton")}
          title={t("stores:modal.title")}
          contentText={t("stores:modal.content")}
          contentFontSize={APP_DIMENSIONS.SCREEN_WIDTH * 0.039}
          onOk={() => setTimeout(() => {
              this.props.closeModal('infoModal');
            }, 1)
          }
        />
        <Layout.SuccessMessageModal
          modalVisible={this.state.locationError}
          okText = {t("stores:locationErrorModal.okButton")}
          title={t("stores:locationErrorModal.title")}
          contentText={t("stores:locationErrorModal.content")}
          onOk={() => this.setState({ locationError: false }) }
        />
        <Layout.Title text={t("stores:title")} />
        <View style={{ flex: 1 }}>
          <View style={this.state.mapStyle}>
            <MapView
              ref={(el) => (this.map = el)}
              style={{ flex: 1 }}
              region = {this.state.region}
              showsUserLocation = {true}
              onRegionChangeComplete = {this._onRegionChangeComplete}
            >
              {this.props.fetchedStoreList && this.props.fetchedStoreList.map(store => (
                <MapView.Marker
                  key = {language == store.store_no } // ORIGINAL_COLOMBIA: idlocation
                  coordinate={{
                    latitude: parseFloat(store.latitud), // ORIGINAL_COLOMBIA: latitude
                    longitude: parseFloat(store.longitud) // ORIGINAL_COLOMBIA: longitude
                  }}
                  title={titleCase(store.name)}
                  description={
                    titleCase(store.address).replace(";", ",")
                  }
                  ref = {store.store_no} // ORIGINAL_COLOMBIA: idlocation
                />
              ))}
            </MapView>
          </View>
          <Layout.StoreList
            expanded={!this.state.mapExpanded}
            onPress = {this._navigateToStore}
            expandedHeight={'60%'}
            hiddenHeight={0}
            storesArray={this.props.fetchedStoreList}
            component={Layout.StoreListItem}
            location = { this.props.location }
            modalConnectionError={this.props.modals.connectionError}
            hasConnectionError={this.props.fetchStoreListError}
            openModal={this.props.openModal}
            closeModal={this.props.closeModal}
            showDeepLink
            t={t}
            language={i18n.language}
            // hideSearchBar
          />
        </View>
        <Layout.BottomTabContainer>
          <Layout.TouchableContainer onPress={this._changeLayout}>
            <Layout.BottomTabButtonContainer style={{backgroundColor: COLORS.RED.FIRE_ENGINE_RED}} >
              <Layout.BottomTabIcon
                name={ this.state.mapExpanded && 'map-marker' || 'map' }
                type='font-awesome'
              />
              <Layout.BottomTabButtonText style={{color: COLORS.WHITE.WHITE}}>
                { this.state.mapExpanded && t("stores:viewNearbyStoresButton") || t("stores:viewBiggerMapButton") }
              </Layout.BottomTabButtonText>
            </Layout.BottomTabButtonContainer>
          </Layout.TouchableContainer>
        </Layout.BottomTabContainer>
      </Layout.ViewContainer>
    );
  }
}


const mapStateToProps = state => ({
  modals: {
    infoModal: state.stores.storeList.infoModal,
    connectionError: state.stores.storeList.connectionError,
  },
  storeListLoading: state.storeListFetch.loading,
  fetchedStoreList: state.storeListFetch.storeList,
  fetchStoreListError: state.storeListFetch.error,
  location: state.location.location,
  locationError: state.location.error,
  fetchingLocation: state.location.fetching,
});

const mapDispatchToProps = dispatch => {
  return {
    openModal: modalKey => {
      dispatch(Actions.setModalVisible(modalKey));
    },
    closeModal: modalKey => {
      dispatch(Actions.setModalInvisible(modalKey));
    },
    fetchStoreList: () => {
        dispatch(Actions.fetchStoreList()); // ORIGINAL_COLOMBIA: fetchStoreListByCountry
    },
    getLocation: (forceAsk) => {
      dispatch(Actions.getLocation(forceAsk));
    }
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(StoreListView);
