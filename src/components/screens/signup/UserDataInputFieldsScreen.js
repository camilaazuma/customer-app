import React from "react";
import * as Views from "../../views";

const UserDataInputFieldsScreen = ({navigation}) =>
  <Views.SignUp.UserDataInputFieldsView navigation={navigation}/>

export default UserDataInputFieldsScreen;