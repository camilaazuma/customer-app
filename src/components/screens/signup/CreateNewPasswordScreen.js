import React from "react";
import * as Views from "../../views";

const CreateNewPasswordScreen = ({navigation}) =>
  <Views.SignUp.CreateNewPasswordView navigation={navigation}/>

export default CreateNewPasswordScreen;