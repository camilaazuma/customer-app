import React from "react";
import * as Views from "../../views";
import * as Layout from "../../layout";
import APP_DIMENSIONS from "../../../constants/appDimensions";
import { View } from "react-native";

class CpfInputScreen extends React.Component {
  static navigationOptions = ({ navigation }) => ({
    header: (
      <Layout.WhiteSafeAreaView>
        <Layout.StatusBarContainer />
        <Layout.HeaderContainer style={{ paddingHorizontal: APP_DIMENSIONS.SCREEN_WIDTH*0.059 }}>
          <Layout.HeaderLogo disableHomeNav/>
        </Layout.HeaderContainer>
      </Layout.WhiteSafeAreaView>
    ),
  });

  render() {
    return <Views.SignUp.CpfInputView navigation={this.props.navigation} />;
  }
}

export default CpfInputScreen;
