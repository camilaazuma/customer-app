import React from "react";
import * as Views from "../../views";

const ConfirmationCodeScreen = ({navigation}) =>
  <Views.SignUp.ConfirmationCodeView navigation={navigation}/> 

export default ConfirmationCodeScreen;