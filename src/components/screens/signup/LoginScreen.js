import React from "react";
import * as Views from "../../views";

const LoginScreen = ({navigation}) =>
  <Views.SignUp.LoginView navigation={navigation}/>

export default LoginScreen;