import React from "react";
import * as Views from "../views";
import * as Layout from "../layout";
import { Icon } from "react-native-elements"

class AboutUsScreen extends React.Component {
  render() {
    return <Views.AboutUsView navigation={this.props.navigation} />;
  }
}
export default AboutUsScreen;


