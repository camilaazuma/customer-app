import React from "react";
import * as Views from "../../views";
import * as Layout from "../../layout";
import * as Actions from "../../../actions";
import { connect } from "react-redux";


class ListDetailScreen extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      fetchedListDetails: [],
    };
  }


  static navigationOptions = ({ navigation}) => ({
    header: (
      <Layout.CustomHeader>
        <Layout.BackIcon
          onPress={() => setTimeout(() => navigation.navigate("ShoppingLists"), 1)}
        />
        <Layout.HeaderLogo navigation={navigation}/>
        <Layout.ShareIcon 
          onPress={() => setTimeout(() => navigation.shareList(), 1)}
        />
      </Layout.CustomHeader>
    )
  });

  render() {
    this.props.navigation.shareList = () => {
      this.props.shareList(this.props.fetchedListDetails);
    };
    return <Views.ShoppingLists.ListDetailView navigation={this.props.navigation} />;
  }
}

const mapStateToProps = state => ({
  fetchedListDetails: state.products.fetchProduct.productList,
});

const mapDispatchToProps = dispatch => {
  return {
    shareList: (list) => {
      dispatch(Actions.showListSharing(list));
    },
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ListDetailScreen);


