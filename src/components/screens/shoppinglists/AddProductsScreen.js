import React from "react";
import * as Views from "../../views";
import * as Layout from "../../layout";

class AddProductsScreen extends React.Component {
  render() {
    return <Views.ShoppingLists.AddProductsView navigation={this.props.navigation} />;
  }
}
export default AddProductsScreen;


