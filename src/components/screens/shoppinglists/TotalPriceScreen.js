import React from "react";
import * as Views from "../../views";
import * as Layout from "../../layout";

class TotalPriceScreen extends React.Component {

  static navigationOptions = ({ navigation }) => ({
    header: (
      <Layout.CustomHeader>
        <Layout.BackIcon
          onPress={() => setTimeout(() => navigation.goBack(), 1)}
        />
        <Layout.HeaderLogo navigation={navigation}/>
      </Layout.CustomHeader>
    )
  });
  render() {
    return <Views.ShoppingLists.TotalPriceView navigation={this.props.navigation} />;
  }
}
export default TotalPriceScreen;


