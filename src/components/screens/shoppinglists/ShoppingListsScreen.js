import React from "react";
import * as Views from "../../views";
import * as Layout from "../../layout";

class ShoppingListsScreen extends React.Component {
  static navigationOptions = ({ navigation }) => ({
    header: (
      <Layout.CustomHeader>
        <Layout.BackIcon
          onPress={() => setTimeout(() => navigation.navigate("Home"), 1)}
        />
        <Layout.HeaderLogo navigation={navigation}/>
      </Layout.CustomHeader>
    ),
  });
  render() {
    return <Views.ShoppingLists.ShoppingListsView navigation={this.props.navigation} />;
  }
}
export default ShoppingListsScreen;
