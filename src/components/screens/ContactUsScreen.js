import React from "react";
import * as Views from "../views";
import * as Layout from "../layout";
import { Icon } from "react-native-elements"

class ContactUsScreen extends React.Component {
  render() {
    return <Views.ContactUsView navigation={this.props.navigation} />;
  }
}
export default ContactUsScreen;


