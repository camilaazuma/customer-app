import React from "react";
import * as Views from "../../views";
import * as Layout from "../../layout";

class UserUpdateConfirmationScreen extends React.Component {
  static navigationOptions = ({ navigation }) => ({
    header: (
      <Layout.CustomHeader>
        <Layout.BackIcon
          onPress={() => setTimeout(() => navigation.navigate("Settings", {updated: false}) , 1)}
        />
        <Layout.HeaderLogo navigation={navigation}/>
      </Layout.CustomHeader>
    ),
  });
  render() {
    return <Views.Settings.UserUpdateConfirmationView navigation={this.props.navigation} />;
  }
}
export default UserUpdateConfirmationScreen;