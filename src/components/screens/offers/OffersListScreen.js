import React from "react";
import * as Views from "../../views";
import * as Layout from "../../layout";
import { Icon } from "react-native-elements"
import {language} from "../../../config/i18n";

class OffersListScreen extends React.Component {
  render() {
      return <Views.Offers.OffersListView navigation={this.props.navigation} />;
  }
}
export default OffersListScreen;
