import React from "react";
import * as Views from "../../views";
import * as Layout from "../../layout";
import { Icon } from "react-native-elements"

class MyPurchasesListScreen extends React.Component {
  render() {
    return <Views.MyPurchases.MyPurchasesListView navigation={this.props.navigation} />;
  }
}
export default MyPurchasesListScreen;


