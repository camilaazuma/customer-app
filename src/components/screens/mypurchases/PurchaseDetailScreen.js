import React from "react";
import * as Views from "../../views";
import * as Layout from "../../layout";
import { Icon } from "react-native-elements"

class PurchaseDetailScreen extends React.Component {

  render() {
    return <Views.MyPurchases.PurchaseDetailView navigation={this.props.navigation} />;
  }
}
export default PurchaseDetailScreen;


