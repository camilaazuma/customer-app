import React from "react";
import { HomeSignedInView } from "../views";

class HomeSignedInScreen extends React.Component {
  render() {
    return <HomeSignedInView navigation={this.props.navigation} />;
  }
}

export default HomeSignedInScreen;
