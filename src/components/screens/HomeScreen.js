import React from "react";
import { HomeView } from "../views";

class HomeScreen extends React.Component {
  render() {
    return <HomeView navigation={this.props.navigation} />;
  }
}

export default HomeScreen;
