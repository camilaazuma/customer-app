import React from "react";
import * as Views from "../../views";
import * as Layout from "../../layout";
import { Icon } from "react-native-elements"

class StoreListScreen extends React.Component {
  static navigationOptions = ({ navigation }) => ({
    header: (
      <Layout.CustomHeader>
        <Layout.BackIcon
          onPress={() => setTimeout(() => navigation.navigate("Home"), 1)}
        />
        <Layout.HeaderLogo navigation={navigation}/>
      </Layout.CustomHeader>
    ),
    drawerLabel: 'LOJAS',
    drawerIcon: ()=>(<Layout.DrawerItemIcon name='map' />)
  });
  render() {
    return <Views.Stores.StoreListView navigation={this.props.navigation} />;
  }
}
export default StoreListScreen;


