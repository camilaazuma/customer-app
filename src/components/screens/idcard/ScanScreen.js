import React from "react";
import * as Views from "../../views";
import * as Layout from "../../layout";

class ScanScreen extends React.Component {
  render() {
    return <Views.IDCard.ScanView navigation={this.props.navigation} />;
  }
}
export default ScanScreen;


