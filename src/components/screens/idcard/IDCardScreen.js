import React from "react";
import * as Views from "../../views";
import * as Layout from "../../layout";

class IDCardScreen extends React.Component {

  static navigationOptions = ({ navigation }) => ({
    header: (
      <Layout.CustomHeader>
        <Layout.BackIcon
          onPress={() => setTimeout(() => navigation.navigate("Home"), 1)}
        />
        <Layout.HeaderLogo navigation={navigation}/>
      </Layout.CustomHeader>
    ),
  });

  render() {
    return <Views.IDCard.IDCardView navigation={this.props.navigation} />;
  }
}
export default IDCardScreen;


