import React from "react";
import * as Views from "../../views";
import * as Layout from "../../layout";
import { Icon } from "react-native-elements"

class ProductPriceDetailScreen extends React.Component {
  render() {
    return <Views.PriceInquiry.ProductPriceDetailView navigation={this.props.navigation} />;
  }
}
export default ProductPriceDetailScreen;


