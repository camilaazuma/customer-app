import React from "react";
import * as Views from "../../views";
import * as Layout from "../../layout";
import { Icon } from "react-native-elements"
import {language} from "../../../config/i18n";

class ProductNameInquiryScreen extends React.Component {
  render() {
    if (language==="pt-BR")
      return <Views.PriceInquiry.ProductNameInquiryView navigation={this.props.navigation} />;
    else if (language==="es-CO") 
      return <Views.PriceInquiry.ProductNameInquiryView_CO navigation={this.props.navigation} />;
    return null
  }
}
export default ProductNameInquiryScreen;


