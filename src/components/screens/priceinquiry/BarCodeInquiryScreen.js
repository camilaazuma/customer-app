import React from "react";
import * as Views from "../../views";
import * as Layout from "../../layout";
import { Icon } from "react-native-elements"

class BarCodeInquiryScreen extends React.Component {
  render() {
    return <Views.PriceInquiry.BarCodeInquiryView navigation={this.props.navigation} />;
  }
}
export default BarCodeInquiryScreen;


