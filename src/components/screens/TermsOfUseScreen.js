import React from "react";
import {TermsOfUseView}  from "../views";

const TermsOfUseScreen = (props) =>
  <TermsOfUseView  navigation={props.navigation}/>

export default TermsOfUseScreen;