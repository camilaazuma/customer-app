import React from "react";
import { View, Text, FlatList, StyleSheet } from "react-native";
import * as Common from "../common";
import APP_DIMENSIONS from "../../../constants/appDimensions";
import { COLORS } from "../../../constants/styles";
import FastImage from 'react-native-fast-image-expo';
import { formatStringForPrice } from "../../../utils";

export class OfferListItem extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      image: {
        uri: "https://pcm.makro.com" +  (props.productImgSrc !== "" ? props.productImgSrc : "null"),
        priority: FastImage.priority.high,
        cache: FastImage.cacheControl.immutable
      },
      noImage: false
    };
  }

  onError = (error) => {
    this.setState({noImage: true});
  }

  render() {
    const { t, i18n, sellPrice, unit, embVenda, offerEndDate } = this.props;
    if(!this.state.noImage){
      return (
        <Common.BorderedView >
          <Common.ListItemContainer style={[{ height: APP_DIMENSIONS.SCREEN_HEIGHT * 0.255, backgroundColor: 'white'}, this.props.style]}>
            <Common.LeftContainer style={{ flex: 0.32 }}>
              <Common.ImageContainer style={{ flex: 1, backgroundColor: COLORS.WHITE.WHITE }}>
                <Common.ProductImage
                  source={this.state.image}
                  style={{width: undefined, height: undefined}}
                  resizeMode= {FastImage.resizeMode.contain}
                  onError={(e) => this.onError(e) }
                />
              </Common.ImageContainer>
            </Common.LeftContainer>
            <Common.EmptyContainer style={{ flex: 0.025 }} />
            <Common.RightContainer
              style={{
                flex:  0.65,
                flexDirection: "column",
                justifyContent: "flex-start"
              }}
            >
              <View style={{ flexDirection: "row", flex: 1.3, justifyContent: 'flex-start', alignItems: 'center' }}>
                <Common.ItemTitle
                numberOfLines={2}
                style={{
                  fontSize: APP_DIMENSIONS.SCREEN_WIDTH * 0.04,
                  lineHeight: APP_DIMENSIONS.SCREEN_HEIGHT * 0.03,
                }}
                >{this.props.productName.trim()}</Common.ItemTitle>
              </View>
              <View style={{ flexDirection: "row", flex: 1 }}>
                <Common.ItemSubTitle numberOfLines={2} style={{fontSize: APP_DIMENSIONS.SCREEN_WIDTH * 0.03 }}>{embVenda}</Common.ItemSubTitle>
              </View>
              <View style={{ flexDirection: "row", flex: 1 }}>
                <View style={{ flex: 1 }}>
                  <Common.ItemSubTitle style={{fontSize: APP_DIMENSIONS.SCREEN_WIDTH * 0.03 , textAlign: 'left'}}>{offerEndDate}</Common.ItemSubTitle>
                </View>
              </View>
              <Common.InfoContainer style={{ flexDirection: "row", flex: 1 }}>
                <View style={{ flex: 1 }}>
                  <Common.RightAlignInfo
                    style={{
                        fontFamily: "open-sans-bold",
                        fontSize: APP_DIMENSIONS.SCREEN_WIDTH * 0.042,
                        color: COLORS.RED.FIRE_ENGINE_RED,
                        textAlign: 'right'
                      }}>
                    {t("common:moneyCurrency")}
                    <Text>{formatStringForPrice(sellPrice, i18n.language)}</Text> <Text>{unit}</Text>

                  </Common.RightAlignInfo>
                </View>
              </Common.InfoContainer>
            </Common.RightContainer>
          </Common.ListItemContainer>
        </Common.BorderedView>
      );
    }else{
      return null;
    }
  }
}

export const OfferList = props => {
  if(props.loading){
    return (
      <View style={{flex: 1, backgroundColor: COLORS.WHITE.WHITE_SMOKE}}>
        <View style={{height: APP_DIMENSIONS.SCREEN_WIDTH * 0.031}} />
        <View style={[{flex: 1, backgroundColor: COLORS.WHITE.WHITE}, styles.card]} >
          <Common.ViewLoader loading={true} />
        </View>
      </View>

    );
  }else{
    return(
      <View style = {{flex: 1}}>
        <View style={{height: APP_DIMENSIONS.SCREEN_WIDTH * 0.031}} />
        <FlatList
          keyExtractor={props.keyExtractor}
          data={props.data}
          renderItem={props.renderItem}
          onEndReached={props.onEndReached}
          onEndReachedThreshold={2}
          onMomentumScrollBegin={props.onMomentumScrollBegin}
          removeClippedSubviews={false}
          initialNumToRender={12}
          maxToRenderPerBatch={12}
          ListFooterComponent={props.ListFooterComponent}
          extraData={props.extraData}
          onRefresh={props.onRefresh}
          refreshing={props.refreshing}
          contentContainerStyle = {[props.contentContainerStyle]}
        />
      </View>


    );
  }
};

const styles = StyleSheet.create({
  card: {
    marginLeft: APP_DIMENSIONS.SCREEN_WIDTH * 0.03,
    marginRight: APP_DIMENSIONS.SCREEN_WIDTH * 0.03,
    borderRadius: 10,
    shadowColor: "black",
    shadowOpacity: 0.2,
    elevation: 2,
    shadowRadius: 2,
    overflow: 'hidden'
  },
});


export const FilterListItem = props => {
  return (
    <View style={{ borderBottomColor:'black', borderBottomWidth: StyleSheet.hairlineWidth, height: APP_DIMENSIONS.SCREEN_HEIGHT*0.11 }}>
      <Common.TouchableContainer onPress={props.onPress} style={{ flex: 1 }}>
        <Common.BottomTabButtonContainer style={{ flex: 1, flexDirection: 'row' }}>
          <Common.BottomTabButtonText style={{ flex: 0.35, textAlign: 'left' }}>
            {props.titleText}
          </Common.BottomTabButtonText>
          <Common.BottomTabButtonText style={{ flex: 0.5, textAlign: 'right' }}>
            {props.selectedValue}
          </Common.BottomTabButtonText>
          <Common.BottomTabIcon type='font-awesome' name='angle-right' size={APP_DIMENSIONS.SCREEN_WIDTH*0.092} color='#FF2B6C' style={{ flex: 0.15 }}/>
        </Common.BottomTabButtonContainer>
      </Common.TouchableContainer>
    </View>
  );
};


export const OfferFilterList = props => {
  return(
    <View
      style={{
        height: props.showItems ? '100%' : 0
      }}
    >
      { (props.loading)
        ? <Common.ViewLoader loading={true} />
        :
          <FlatList
            keyExtractor={(item, index) => index.toString()}
            data={props.data}
            renderItem={props.renderItem}
            ListFooterComponent={props.ListFooterComponent}
          />
      }
    </View>
  )
}
