import React from "react";
import { View, StyleSheet, Linking } from "react-native";
import { Icon } from "react-native-elements";
import { FontText, FontTextInput } from "./text";
import APP_DIMENSIONS from "../../../constants/appDimensions";
import { COLORS } from "../../../constants/styles";
import styled from "styled-components/native";
import { nullFunction } from "../../../utils";

export const BottomTabContainer = styled.View`
  display: flex;
  flexDirection: row;
  height: ${APP_DIMENSIONS.BOTTOM_TAB.HEIGHT};
  alignItems: stretch;
  background-color: ${COLORS.WHITE.WHITE};
  justifyContent: flex-start;
  borderTopColor: ${COLORS.WHITE.WHITE_SMOKE};
  borderTopWidth: 1;
`;

export const HomeBottomTabContainer = styled(BottomTabContainer)`
  flex: 1;
  min-height: ${APP_DIMENSIONS.SCREEN_HEIGHT*0.1};
  max-height: ${APP_DIMENSIONS.SCREEN_HEIGHT*0.40}; 
  margin-top: auto;
  background-color: ${COLORS.WHITE.WHITE_SMOKE};
`;

export const HomeBottomTabContainer_CO = styled(View)`
  flex: 1;
  height: ${APP_DIMENSIONS.SCREEN_HEIGHT*0.6}; 
  background-color: ${COLORS.WHITE.WHITE_SMOKE};
  borderTopColor: ${COLORS.WHITE.WHITE_SMOKE};
  borderTopWidth: 1;
`;

export const HomeMenuRowContainer = styled(BottomTabContainer)`
  flexDirection: row;
  flex: 1;
  height: ${APP_DIMENSIONS.SCREEN_HEIGHT*0.2}; 
  background-color: ${COLORS.WHITE.WHITE_SMOKE};
`;

export const BottomTabButtonContainer = styled.View`
  flex: 1;
  flexDirection: row;
  justifyContent: center;
  alignItems: center;
  ${({ active }) => active && `
    backgroundColor: blue;
  `}
  background-color: ${COLORS.WHITE.WHITE};˜
`;

export const BottomTabButtonText = props => {
    return (
      <BottomTabButtonTextNormalCase {...props}>
        {props.children &&
          ((!props.disableUpperCase && props.children.toString().toUpperCase()) ||
            props.children.toString())}
      </BottomTabButtonTextNormalCase>
    );
  };

export const BottomTabButtonTextNormalCase = styled(FontText)`
    fontSize: ${APP_DIMENSIONS.BOTTOM_TAB.TEXT_SIZE};
    marginRight: ${APP_DIMENSIONS.SCREEN_WIDTH*0.015};
    fontFamily: open-sans-semibold;
    textAlign: center;
    letterSpacing: ${APP_DIMENSIONS.SCREEN_WIDTH*0.0016};
`;


  
export const HomeBottomTabIcon = (props) => (
    <Icon 
        type='font-awesome'
        containerStyle={{
          marginBottom: APP_DIMENSIONS.SCREEN_WIDTH*0.014
        }}
        size={props.size*APP_DIMENSIONS.SCREEN_WIDTH_SIZE_CORRECTION || APP_DIMENSIONS.SCREEN_WIDTH*0.077*APP_DIMENSIONS.SCREEN_WIDTH_SIZE_CORRECTION}
        name={props.name}
        color={props.color}
    />
);

export const BottomTabIcon = props => (
    <Icon
        type={props.type || "material-community"}
        color={COLORS.WHITE.WHITE}
        size={props.size*APP_DIMENSIONS.SCREEN_WIDTH_SIZE_CORRECTION || APP_DIMENSIONS.SCREEN_WIDTH*0.062}
        {...props}
    />
);

export const HomeBottomTabButtonContainer = styled(BottomTabButtonContainer)`
  flexDirection: column;
  marginTop: 7;
  marginBottom: 7;
  marginLeft: 7;
  marginRight:7;
  borderRadius: 10;
  shadowColor: black;
  shadowOpacity: 0.2;
  elevation: 5;
  shadowRadius: 2;
`;
