import React from "react";
import {
  View,
  SafeAreaView,
  StyleSheet
} from "react-native";
import { Icon } from "react-native-elements";
import { FontText, FontTextInput } from "./text";
import APP_DIMENSIONS from "../../../constants/appDimensions";
import { COLORS } from "../../../constants/styles";
import styled from "styled-components/native";
import { TouchableContainer } from "../common/touchable"

export const TitleContainer = styled.View`
  height: ${APP_DIMENSIONS.TITLE_HEIGHT};
  backgroundColor: ${COLORS.RED.FIRE_ENGINE_RED};
  alignSelf: stretch;
  justifyContent: center;
  alignItems: stretch;
  borderBottomColor: ${COLORS.WHITE.WHITE};
  borderBottomWidth: ${StyleSheet.hairlineWidth};
`;

export const TitleText = styled(FontText)`
  color: ${COLORS.WHITE.WHITE};
  fontSize: ${APP_DIMENSIONS.SCREEN_HEIGHT*0.021};
  textAlign: center;
  fontFamily: helvetica-neue;
  letterSpacing: ${APP_DIMENSIONS.SCREEN_WIDTH*0.0016};
`;

export const Title = props => {
  let title = "";
  try {
    title = props.disableUpperCase ? props.text : props.text.toUpperCase();
  }
  catch(e) {
    console.log(e);
  }
  return (
    <TitleContainer>
      <TitleText>{title}</TitleText>
    </TitleContainer>
  );
};

export const TouchableTitle = props => {
  var caret = "";
  if(props.hasOwnProperty("caretUp")){
    caret = props.caretUp ? "angle-up" : "angle-down";
  }
  if(props.hasOwnProperty("caretRight")){
    caret = props.caretRight ? "angle-right" : "angle-left";
  }
  if(props.hasOwnProperty("editIcon")){
    caret = "pencil"
  }

  var iconStyle;
  if(props.iconRight){
    iconStyle = {
      position: "absolute",
      right: APP_DIMENSIONS.SCREEN_WIDTH * 0.049
    };
  }else{
    iconStyle = {
      position: "absolute",
      left: APP_DIMENSIONS.SCREEN_WIDTH * 0.049
    };
  }

  return (
    <TitleContainer>
      <TouchableContainer opacity onPress={props.onPress}>
        <View
          style={{
            flex: 1,
            justifyContent: "center",
            alignItems: "center"
          }}
        >
          <View style={{flexDirection:'row', width: APP_DIMENSIONS.SCREEN_WIDTH*0.8 }}> 
            <TitleText numberOfLines={2} style={{flex: 1, flexWrap: 'wrap', textAlign: 'center'}}>
              {props.titleText }
            </TitleText>
          </View>
          <Icon
            type="font-awesome"
            name={caret}
            size={APP_DIMENSIONS.SCREEN_WIDTH * 0.073}
            color={COLORS.WHITE.WHITE}
            containerStyle={iconStyle}
          />
        </View>
      </TouchableContainer>
    </TitleContainer>
  )
}
