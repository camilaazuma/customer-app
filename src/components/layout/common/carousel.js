import React from "react";
import {
  View,
  Dimensions,
  TouchableWithoutFeedback,
  Linking,
} from "react-native";
import { COLORS } from "../../../constants/styles";
import styled from "styled-components/native";
import Carousel, { Pagination } from "react-native-snap-carousel";
import APP_DIMENSIONS from "../../../constants/appDimensions";
import * as Layout from "./";
import FastImage from 'react-native-fast-image-expo';

const SCREEN_HEIGHT = APP_DIMENSIONS.SCREEN_HEIGHT;
const SCREEN_WIDTH = APP_DIMENSIONS.SCREEN_WIDTH;
const AUTOPLAY_INTERVAL = 5000;

const StyledImage = (props) =>
  <FastImage
    style={{
      flex: 1,
      width: SCREEN_WIDTH,
      height: SCREEN_HEIGHT,
      alignSelf: `center`,
    }}
    resizeMode= {FastImage.resizeMode.contain}

    {...props}
  />


export const CarouselContainer = styled.View`

  flexDirection: row;
  backgroundColor: ${COLORS.RED.DARK_RED};
  justifyContent: center;
  alignItems: stretch;
  width: ${SCREEN_WIDTH};
  height: ${SCREEN_WIDTH*1.04957678};
  /* min-height: ${SCREEN_HEIGHT*0.593};
  max-height: ${SCREEN_HEIGHT*0.723}; */
`;

export class CarouselImage extends React.Component {

  _openURL(){
    if(this.props.href){
      Linking.canOpenURL(this.props.href).then(supported => {
        if (!supported) {
          console.log('Can\'t handle url: ' + this.props.href);
        } else {
          return Linking.openURL(this.props.href);
        }
      }).catch(err => console.error('An error occurred', err));
    }
  }

  render(){
    return(
      <TouchableWithoutFeedback onPress={()=> { this._openURL() }}>
        <StyledImage source={this.props.source} {...this.props}/>
      </TouchableWithoutFeedback>
    )
  }
}

export class ImageCarousel extends React.Component {
    constructor(props) {
      super(props);
      this.state = {
        activeSlide: 0
      };
    }

    _renderItem({ item, index }) {
      return item;
    }

    get pagination() {
      const { activeSlide } = this.state;
      return (
        <Pagination
          dotsLength={this.props.images.length}
          activeDotIndex={activeSlide}
          containerStyle={{ backgroundColor: "transparent" }}
          dotStyle={{
            width: SCREEN_WIDTH*0.017,
            height: SCREEN_WIDTH*0.017,
            borderRadius: 20,
            marginHorizontal: 1,
            borderWidth: 2,
            borderColor: COLORS.RED.FIRE_ENGINE_RED,
          }}
          dotColor={ COLORS.RED.FIRE_ENGINE_RED }
          inactiveDotColor={ COLORS.WHITE.WHITE }
          inactiveDotOpacity={1.0}
          inactiveDotScale={1.0}

        />
      );
    }

    render() {
      if (this.props.loading) {
        return (<Layout.ViewLoader loading={true} />);
      }
      if (!this.props.images || this.props.images.length === 0 ) {
        if (this.props.fallBackImage)
          return this.props.fallBackImage;
        else
          return (<Layout.ViewLoader loading={true} />);
      }

      return (

        <View style={{
          flex: 1,
          flexDirection: 'column',
          justifyContent: 'flex-end',
          alignItems: 'center',

          }}
        >
            <View
              style={{
                position: "absolute",
                top: 0,
                bottom: 0,
                left: 0,
                right: 0
              }}
            >
              <Carousel
                ref={c => {
                  this._carousel = c;
                }}
                data={this.props.images}
                renderItem={this._renderItem}
                sliderWidth={Dimensions.get("window").width}
                itemWidth={Dimensions.get("window").width}
                enableSnap={ this.props.autoplay }
                loop={ true }
                autoplay={ this.props.autoplay }
                autoplayInterval={ AUTOPLAY_INTERVAL }
                onSnapToItem={index => this.setState({ activeSlide: index })}
              />
            </View>
          <View>
            {this.pagination}
          </View>
        </View>
      );
    }
  }
