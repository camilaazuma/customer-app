import React from "react";
import { Icon } from "react-native-elements";
import APP_DIMENSIONS from "../../../constants/appDimensions";
import styled from "styled-components/native";
import { Platform, Linking } from "react-native";

const SocialMediaContainer = styled.View`
  elevation: 2;
  paddingVertical: ${APP_DIMENSIONS.SCREEN_HEIGHT*0.01}px;
  margin: 5px;
  flexDirection: row;
  justifyContent: space-around;
`;

const SocialMediaIcon = props => (
    <Icon
          type={props.type || "font-awesome"}
          color={props.color || COLORS.BLACK.BLACK}
          size={APP_DIMENSIONS.SCREEN_WIDTH*0.108}
          onPress={ props.link ? async () => { 
            let canOpenUrl = await Linking.canOpenURL(props.link);
            if (canOpenUrl) 
              Linking.openURL(props.link)
            else 
              Linking.openURL(props.fallBackLink)
          } : nullFunction }
          {...props}
      />
  )

export const SocialMediaBar = props => {
    let t = props.t;
    return(
        <SocialMediaContainer>
            <SocialMediaIcon name='facebook-square' color='#4267b2'
                link = {Platform.select({ ios: t("aboutUs:deepLinks.facebookIos"), android: t("aboutUs:deepLinks.facebookAndroid") })}
                fallBackLink = { t("aboutUs:deepLinks.facebook") }
            />
            <SocialMediaIcon name='instagram' color='#d42f8a' link={t("aboutUs:deepLinks.instagram")} />
            <SocialMediaIcon name='youtube-play' color='#FF0000' link={t("aboutUs:deepLinks.youtube")} />
            {props.i18n.language === "es-CO" &&
                <SocialMediaIcon name='twitter' color='#00aced' link={t("aboutUs:deepLinks.twitter")} />
            }
            {props.i18n.language === "pt-BR" &&
                <SocialMediaIcon name='linkedin' color='#0077b5' link={t("aboutUs:deepLinks.linkedin")} />
            }
            {props.i18n.language === "es-PE" &&
                <SocialMediaIcon name='whatsapp' color='#82c91e' link={t("aboutUs:deepLinks.whatsapp")} />
            }
    </SocialMediaContainer>
    )
}