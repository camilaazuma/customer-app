import * as React from "react";
import { Platform, StatusBar, Image, View } from "react-native";
import MaterialIcons from "react-native-vector-icons/MaterialIcons";
import SimpleLineIcons from "react-native-vector-icons/SimpleLineIcons";
import FontAwesome from "react-native-vector-icons/FontAwesome";
import Header, { HeaderButton } from "react-navigation-header-buttons";
import { COLORS } from "../../../constants/styles";
import APP_DIMENSIONS from "../../../constants/appDimensions";
import styled from "styled-components/native";
import * as Layout from "./"
import FastImage from 'react-native-fast-image-expo';
import { nullFunction } from "../../../utils";

export const HeaderContainer = styled.View`
  height: ${APP_DIMENSIONS.HEADER_HEIGHT};
  backgroundColor: ${COLORS.WHITE.WHITE};
  flexDirection: row;
  justifyContent: flex-start;
  alignItems: center;
`;

export const StatusBarContainer = (
  Platform.OS === 'ios' ? () => {StatusBar.setBarStyle('dark-content'); return null} : styled.View`height: ${StatusBar.currentHeight}; backgroundColor:  ${COLORS.RED.FIRE_ENGINE_RED};`
);

const LogoContainer = styled.View`
  flex: 1;
  flexDirection: row;
  justifyContent: flex-start;
`;

const logoRatioSize = 174/44;
const logoHeight = APP_DIMENSIONS.SCREEN_HEIGHT*0.041
const logoWidth = logoHeight*logoRatioSize;

const LogoImage = styled(FastImage)`
  height: ${logoHeight}px;
  width: ${logoWidth}px;
`;

const logoSource = require("../../../../assets/makro-logo.png");

export const HeaderLogo = (props) => {
  return (
      <LogoContainer>
        <Layout.TouchableContainer
        opacity
        onPress={()=> ((props.disableHomeNav===true) ? null : setTimeout(() => props.navigation.navigate("Home"), 1)) }
        style={{justifyContent: 'center'}}
        >
          <LogoImage source={logoSource} resizeMode={FastImage.resizeMode.contain} {...props}/>
        </Layout.TouchableContainer>
      </LogoContainer>
  );
};


export const CustomHeader = (props) => (
  <Layout.WhiteSafeAreaView>
  <Layout.StatusBarContainer />
    <HeaderContainer >
      {props.children}
    </HeaderContainer >
  </Layout.WhiteSafeAreaView>
);

const SimpleLineHeaderButton = props => (
  <HeaderButton
    {...props}
    IconComponent={SimpleLineIcons}
    iconSize={APP_DIMENSIONS.SCREEN_HEIGHT*0.0315}
    color={COLORS.GREY.NIGHT_RIDER}
  />
);

const FontAwesomeHeaderButton = props => (
  <HeaderButton
    IconComponent={FontAwesome}
    iconSize={APP_DIMENSIONS.SCREEN_HEIGHT*0.0315}
    color={COLORS.GREY.NIGHT_RIDER}
    {...props}
  />
);

const HeaderIcon = props => {

  return (
  <Header
    left={props.left}
    HeaderButtonComponent = {props.HeaderButtonComponent}
  >
    <Header.Item
      title={props.title}
      iconName={props.iconName}
      onPress={()=>{props.onPress()}}
      color={props.hasOwnProperty("iconColor") ? props.iconColor : "black"}
    />
  </Header>
)};

export const BackIcon = props => (
  <HeaderIcon
    left={true}
    HeaderButtonComponent = {FontAwesomeHeaderButton}
    title={"back"}
    iconName={"chevron-left"}
    onPress={props.onPress}
  />
);

export const MenuIcon = props => (
  <HeaderIcon
    left={true}
    HeaderButtonComponent={FontAwesomeHeaderButton}
    title="menu"
    iconName="bars"
    onPress={props.onPress}
  />
);

export const LocationIcon = props => (
  <HeaderIcon
    HeaderButtonComponent={FontAwesomeHeaderButton}
    title="location"
    iconName="map-marker"
    onPress={props.onPress}
  />
);

export const SearchIcon = props => (
  <HeaderIcon
    HeaderButtonComponent={FontAwesomeHeaderButton}
    title="search"
    iconName="search"
    onPress={props.onPress}
  />
);

export const ShareIcon = props => (
  <HeaderIcon
    HeaderButtonComponent={FontAwesomeHeaderButton}
    title="share"
    iconName="share-alt"
    onPress={props.onPress}
  />
);

export const FilterButton = props => (
  <Header
    left={props.left}
    HeaderButtonComponent = {FontAwesomeHeaderButton}
  >
    <Header.Item
      title={props.t && props.t("offers:filterScreen.filter") || ""}
      ButtonElement={
        <View style={{padding: APP_DIMENSIONS.SCREEN_WIDTH*0.031}}>
          <Layout.FontText style={{fontSize: APP_DIMENSIONS.SCREEN_WIDTH*0.035, color: props.disable ? COLORS.GREY.LIGHT_GREY : COLORS.BLACK.BLACK }}>
            {props.t && props.t("offers:filterScreen.filter")}
          </Layout.FontText>
        </View>
      }
      onPress={props.disable ? nullFunction : props.onPress}
      color={props.hasOwnProperty("iconColor") ? props.iconColor : "black"}
    />
  </Header>
);
