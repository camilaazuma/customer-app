import React from "react";
import {
  View, TouchableNativeFeedback, TouchableHighlight, TouchableOpacity, Platform
} from "react-native";
import APP_DIMENSIONS from "../../../constants/appDimensions";
import { COLORS } from "../../../constants/styles";
import styled from "styled-components/native";

const TouchableContainerAndroid = styled.TouchableNativeFeedback`
  flex: 1;
  alignItems: stretch;
`;

const TouchableStyle = {
  flex: 1,
  alignItems: 'stretch',
  justifyContent: 'center'
}
const TouchableContainerIos = (props) => {
  return props.opacity  
  ? <TouchableOpacity     
    style={TouchableStyle} 
    {...props}
  /> 
  :<TouchableHighlight 
    style={TouchableStyle} 
    { ...props }
  />
}

export const TouchableContainer = (
  Platform.OS === 'ios' ? TouchableContainerIos : TouchableContainerAndroid
);