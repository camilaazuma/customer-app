import React from 'react';
import { StatusBar, Image, BackHandler, Text, View, Platform, Dimensions, StyleSheet, NetInfo, LayoutAnimation, UIManager } from 'react-native';
import { Icon } from 'react-native-elements';
import { DrawerNavigator, DrawerItems, NavigationActions } from 'react-navigation';
import { COLORS, } from '../../../constants/styles';
import styled from 'styled-components/native';
import * as Layout from "./";
import { connect } from "react-redux";
import { translate } from 'react-i18next';
import { titleCase, nullFunction,showSnackbar } from '../../../utils'
import * as Actions from "../../../actions";
import APP_DIMENSIONS from '../../../constants/appDimensions';

let SCREEN_WIDTH = Dimensions.get('window').width,
    SCREEN_HEIGHT = Dimensions.get('window').height;

const DRAWER_DIMENSIONS = {
  HORIZONTAL_PADDING: SCREEN_WIDTH*0.061,
  HEIGHT: SCREEN_HEIGHT*0.263,
  TITLE_HEIGHT: SCREEN_HEIGHT*0.11,
  SUBTITLE_HEIGHT: SCREEN_HEIGHT*0.11,
}

const DrawerContainer = styled.View`
  flex: 1;
  flexDirection: row;
  alignItems: stretch;
  background-color: ${COLORS.GREY.NIGHT_RIDER};
`;

const DrawerItemsContainer = styled.View`
  flex: 0.85;
  background-color: ${COLORS.GREY.NIGHT_RIDER};
  justifyContent: flex-start;
  overflow:hidden;
`;

export const DrawerItemIcon = props => (
  <Icon name={props.name} color={props.color} type='font-awesome' size={SCREEN_WIDTH*0.07} />
)
// DRAWER_DIMENSIONS.SCREEN_HEIGHT;

const DrawerItemsHeaderContainer = styled.View`
  paddingHorizontal: ${DRAWER_DIMENSIONS.HORIZONTAL_PADDING};
  height: ${DRAWER_DIMENSIONS.HEIGHT};
  justifyContent: center;
  borderBottomWidth: ${StyleSheet.hairlineWidth};
  borderColor: ${COLORS.BLACK.BLACK};
  display: flex;
`;

const DrawerItemsHeaderTitleContainer = styled.View`
  height: ${DRAWER_DIMENSIONS.TITLE_HEIGHT};
  flexDirection: row;
  justifyContent: space-between;
  alignItems: center;
`;

const DrawerItemsHeaderSubTitleContainer = styled.View`
  height: ${DRAWER_DIMENSIONS.SUBTITLE_HEIGHT};
  flexDirection: row;
  alignItems: center;
`;

const DrawerItemsHeader = (props) => (
    <DrawerItemsHeaderContainer>
      <DrawerItemsHeaderTitleContainer>
        <Layout.FontText style={{ color: "white", fontSize: SCREEN_WIDTH*0.058, fontFamily: 'open-sans', flex: 0.85 }}>{props.headerTitle}</Layout.FontText>
        <Layout.TouchableContainer opacity onPress={props.cogIconOnPress} style={{flex: 0.15}}>
          <Icon name="edit" type="font-awesome" color={COLORS.WHITE.WHITE} size={SCREEN_WIDTH*0.072}/>
        </Layout.TouchableContainer>
      </DrawerItemsHeaderTitleContainer>
      <Layout.TouchableContainer opacity onPress = {props.storeOnPress} style={{}}>
        <DrawerItemsHeaderSubTitleContainer>
          <Icon
            name={props.headerSubTitleIconName}
            type={props.headerSubTitleIconType}
            color={COLORS.RED.FIRE_ENGINE_RED}
            size={SCREEN_WIDTH*0.06}
            style={{flex: 0.15}}
          />
          <Layout.FontText style={{ color: "white", fontSize: SCREEN_WIDTH*0.055, fontFamily: 'open-sans', paddingLeft: 7, flex: 0.85}}>{props.headerSubTitle}</Layout.FontText>
        </DrawerItemsHeaderSubTitleContainer>
      </Layout.TouchableContainer>
    </DrawerItemsHeaderContainer>
);

const DrawerCloseContainer = styled.View`
  flex: 0.15;
  background-color: ${COLORS.GREY.LIGHT_GREY};
  justifyContent: flex-start;
  overflow: hidden;
`;

const DrawerCloseHeaderContainer = styled.View`
  height: ${DRAWER_DIMENSIONS.HEIGHT};
  justifyContent: center;
  borderBottomWidth: ${StyleSheet.hairlineWidth};
  borderColor: ${COLORS.GREY.LIGHT_GREY};
  display: flex;
`;

const DrawerCloseItemContainer = styled.View`
  height: ${SCREEN_HEIGHT*0.092};
  borderBottomWidth: ${StyleSheet.hairlineWidth};
  borderColor: ${COLORS.GREY.LIGHT_GREY};
  justifyContent: center;
  alignItems: stretch;
  flexDirection: column;
`
const DrawerCloseTouchableContainerAndroid = styled.TouchableNativeFeedback`
  flex: 1;
`;

const DrawerCloseTouchableContainerIos = styled.TouchableHighlight`
  flex: 1;
`;

const DrawerCloseTouchableContainer = (
  Platform.OS === 'ios' ? DrawerCloseTouchableContainerIos : DrawerCloseTouchableContainerAndroid
);

const RightCaret = props => (
  <DrawerCloseItemContainer>
    {/* <View style={{flex: 1, justifyContent: 'center'}}> */}
    {props.routeName === props.activeItemKey ? (
      <Icon
        name="caret-right"
        type="font-awesome"
        color={COLORS.RED.FIRE_ENGINE_RED}
        size={SCREEN_HEIGHT * 0.162}
        containerStyle={{
          transform: [{ translateX: -SCREEN_HEIGHT* 0.0081 }],
          justifyContent: 'center',
          alignItems: 'flex-start',
          height: SCREEN_HEIGHT * 0.162
        }}
      />
    ) : null}
    {/* </View> */}
  </DrawerCloseItemContainer>
);

const DrawerCloseTouchable = props => {
  const { routes, activeItemKey } = props;
  return (
    <DrawerCloseContainer>
      <Layout.TouchableContainer opacity onPress={props.navigation && props.navigation.closeDrawer}>
        <View style={{ flex: 1 }}>
          <Layout.StatusBarContainer />
          <DrawerCloseHeaderContainer>
            <DrawerItemsHeaderTitleContainer style={{ alignItems: 'center', justifyContent: 'center' }}>
              <Icon name="x" type="feather" color={COLORS.BLACK.BLACK} size={SCREEN_HEIGHT*0.055} containerStyle={{
                transform: [{ scaleY: 0.6 }]
              }}/>
            </DrawerItemsHeaderTitleContainer>
            <DrawerItemsHeaderSubTitleContainer/>
          </DrawerCloseHeaderContainer>
          <RightCaret routeName={routes.OFFERS} activeItemKey={activeItemKey}/>
          <RightCaret routeName={routes.STORES} activeItemKey={activeItemKey}/>
          <RightCaret routeName={routes.PRICE_INQUIRY} activeItemKey={activeItemKey}/>
          <RightCaret routeName={routes.CONTACT_US} activeItemKey={activeItemKey}/>
          <RightCaret routeName={routes.ABOUT_US} activeItemKey={activeItemKey}/>
        </View>
      </Layout.TouchableContainer>
    </DrawerCloseContainer>
  )
};


const DrawerItemContainer = styled.View`
  height: ${SCREEN_HEIGHT*0.092};
  borderBottomWidth: ${StyleSheet.hairlineWidth};
  borderColor: ${COLORS.BLACK.BLACK};
  flexDirection: row;
  justifyContent: flex-start;
  alignItems: center;
  paddingLeft: ${SCREEN_WIDTH*0.024};

  ${({ routeName, activeItemKey }) => routeName === activeItemKey && `
    background: ${COLORS.RED.FIRE_ENGINE_RED};
  `}
`

const IconContainer = styled.View`
  width: ${SCREEN_WIDTH*0.13};
  marginRight: ${SCREEN_WIDTH*0.019};
`

const DrawerItemLabelText = styled(props=><Layout.FontText {...props}/>)`
  color: ${COLORS.WHITE.WHITE};
  marginLeft: 0;
  fontSize: ${SCREEN_WIDTH*0.044};
  lineHeight: ${SCREEN_WIDTH*0.06};
  fontFamily: open-sans-semibold;
  fontWeight: normal;
  letterSpacing: ${SCREEN_WIDTH*0.0016};
`
const DrawerItemLabelIconAndTextContainer = styled.View`
  flexDirection: row;
  flex: 1;
  alignItems: center;
`

const Route = props => (
  <DrawerItemContainer routeName={props.routeName} activeItemKey={props.activeItemKey}>
    <Layout.TouchableContainer opacity onPress={props.onPress}>
      <DrawerItemLabelIconAndTextContainer>
        <IconContainer>
          <DrawerItemIcon
            color={props.routeName === props.activeItemKey ? COLORS.WHITE.WHITE : COLORS.RED.FIRE_ENGINE_RED}
            name={props.iconName}
          />
        </IconContainer>
        <DrawerItemLabelText>{props.labelText.toUpperCase()}</DrawerItemLabelText>
      </DrawerItemLabelIconAndTextContainer>
    </Layout.TouchableContainer>
  </DrawerItemContainer>
);

@translate(['drawer', 'common'], { wait: true })
class DrawerMenuComponent extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      expanded: false,
      caret: "down",
      hide: true,
      fetchStoreListError: false,
    }
    if (Platform.OS === "android") {
      UIManager.setLayoutAnimationEnabledExperimental(true);
    }

    console.log(props.currentStoreObject);
    
  }

  _changeLayoutAnimated = () => {
    LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
    this._setExpanded();
  };

  _setExpanded = () => {
    this.setState( {
      expanded: !this.state.expanded
    } )
  }

  _setStore = async (store) => {
    if (this.props.currentStoreObject===null || store.store_no !== this.props.currentStoreObject.store_no || store.idlocation !== this.props.currentStoreObject.idlocation) {
      await this.props.setCurrentStore(store);
      this.props.clearAllOffers();
      this._setExpanded();
    }
    else {
      this._setExpanded();
    }
  }

  navigationAction = (routeName, action = null)=>NavigationActions.navigate({routeName: routeName, action: action });

  navigateToScreen = (screen, nestedScreen=null) => {
    return () => {
      this.setState({expanded: false})
      this.props.navigation.dispatch(this.navigationAction(screen, this.navigationAction(nestedScreen)));
    }
  }

  render() {
    const { t, i18n } = this.props;
    routes = {
      OFFERS: "Offers",
      STORES: "Stores",
      SHOPPING_LISTS: "ShoppingLists",
      MY_PURCHASES: "MyPurchases",
      PRICE_INQUIRY: "PriceInquiry",
      ID_CARD: "IDCard",
      CONTACT_US: "ContactUs",
      ABOUT_US: "AboutUs",
    }
    return (
      <DrawerContainer>
        <DrawerItemsContainer >
        <Layout.StatusBarContainer />
          <DrawerItemsHeader
            headerTitle={t("drawer:header.title") + ' ' +  titleCase(this.props.user.fullName)}
            headerSubTitle={t("drawer:header.subtitle") + ' ' + (this.props.currentStoreObject!==null ? titleCase(this.props.currentStoreObject.name) : "")}
            headerSubTitleIconName="map-marker"
            headerSubTitleIconType="font-awesome"
            cogIconOnPress={this.navigateToScreen("Settings", "Settings")}
            storeOnPress={this._changeLayoutAnimated}
          />
          { this.props.fontLoaded &&
            <View>
              <Layout.StoreList
                expanded={this.state.expanded}
                expandedHeight={'60%'}
                hiddenHeight={0}
                component={Layout.StoreListItem}
                location = { this.props.location }
                onPress = { this._setStore }
                hideSearchBar = {!false}
                modalConnectionError={false}
                hasConnectionError={false}
                openModal={this.props.openModal}
                closeModal={this.props.closeModal}
                badgeFontSize={SCREEN_WIDTH * 0.025}
                t={t}
                language={i18n.language}
              />
              <Route
                labelText={t('drawer:Offers')}
                iconName={"shopping-cart"}
                onPress={this.navigateToScreen(routes.OFFERS)}
                routeName={routes.OFFERS}
                activeItemKey={this.props.activeItemKey}
              />
              <Route
                labelText={t('drawer:Stores')}
                iconName={"map"}
                onPress={this.navigateToScreen(routes.STORES)}
                routeName={routes.STORES}
                activeItemKey={this.props.activeItemKey}
              />
              <Route
                labelText={t('drawer:PriceInquiry')}
                iconName={"barcode"}
                onPress={async ()=>{
                  //if (i18n.language!=="es-CO") {
                    let isConnected = await NetInfo.isConnected.fetch();
                    if (!isConnected)
                      showSnackbar(this.props.t("common:errors.noConnection"));
                    else
                      this.navigateToScreen(routes.PRICE_INQUIRY)();
                  //}else
                  //  alert("Em desenvolvimento");
                }}
                routeName={routes.PRICE_INQUIRY}
                activeItemKey={this.props.activeItemKey}
              />
              <Route
                labelText={t('drawer:ContactUs')}
                iconName={"comments"}
                onPress={this.navigateToScreen(routes.CONTACT_US)}
                routeName={routes.CONTACT_US}
                activeItemKey={this.props.activeItemKey}
              />
              <Route
                labelText={t('drawer:AboutUs')}
                iconName={"info-circle"}
                onPress={this.navigateToScreen(routes.ABOUT_US)}
                routeName={routes.ABOUT_US}
                activeItemKey={this.props.activeItemKey}
              />
            </View>
          }
        </DrawerItemsContainer>
        <DrawerCloseTouchable {...this.props} routes={routes} activeItemKey={this.state.expanded ? null : this.props.activeItemKey} />
      </DrawerContainer>
    );
  }
}

const mapStateToProps = state => ({
  fontLoaded: state.fontLoader.fontLoaded,
  loginResponse: state.signUp.login.loginResponse,
  nearestStore: state.storeListFetch.nearestStore,
  currentStoreObject: state.currentStore.object,
  user: state.user,
  location: state.location.location,
  fetchedStoreList: state.storeListFetch.storeList
});

const mapDispatchToProps = dispatch => {
  return {
    openModal: modalKey => {
      dispatch(Actions.setModalVisible(modalKey));
    },
    closeModal: modalKey => {
      dispatch(Actions.setModalInvisible(modalKey));
    },
    stopLoading: () => {
      dispatch(Actions.stopLoading());
    },
    setLoading: () => {
      dispatch(Actions.setLoading());
    },
    fetchAllOffers: ({storeId}={}) => {
      dispatch(Actions.fetchOffersByStore(storeId));
    },
    fetchStoreList: () => {
      dispatch(Actions.fetchStoreList());
    },
    setCurrentStore: (store) => {
      dispatch(Actions.setCurrentStore(store))
    },
    clearAllOffers: () => {
      dispatch(Actions.clearAllOffers())
    },
    getLocation: (forceAsk) => {
      dispatch(Actions.getLocation(forceAsk));
    }
  };
};

export const DrawerMenu = connect(
  mapStateToProps,
  mapDispatchToProps,
  null
)(DrawerMenuComponent);

// export  DrawerMenu;
