import { Text, TextInput } from "react-native";
import React, { Component } from "react";
import { connect } from "react-redux";
import styled from "styled-components/native";
import APP_DIMENSIONS from "../../../constants/appDimensions";
import { COLORS } from "../../../constants/styles";


class FontTextComponent extends Component {
  Loadtext() {
    if (this.props.fontLoaded) {
      return (
        <Text 
          {...this.props} 
          style={[
            {fontFamily: 'open-sans'}, 
            this.props.style,
            this.props.style && this.props.style.fontSize ?
            {fontSize: this.props.style.fontSize*APP_DIMENSIONS.SCREEN_WIDTH_SIZE_CORRECTION } : this.props.style
          ]}
        >
          {this.props.children}
        </Text>);
    }
    else
      return null;
  }
  render() {
    return this.Loadtext();
  }
}
const mapStateToProps = state => ({
  fontLoaded: state.fontLoader.fontLoaded
});

export const FontText = connect(
  mapStateToProps,
  null
)(FontTextComponent);

export const IconTitleText = styled(FontText)`
  fontSize: ${APP_DIMENSIONS.SCREEN_WIDTH*0.05*APP_DIMENSIONS.SCREEN_WIDTH_SIZE_CORRECTION};
  textAlign: center;
  color: ${COLORS.WHITE.WHITE};
  fontFamily: open-sans;
  letterSpacing: ${APP_DIMENSIONS.SCREEN_WIDTH*0.003};
`;

