import React from "react";
import { Icon } from "react-native-elements";
import APP_DIMENSIONS from "../../../constants/appDimensions";
import { COLORS } from "../../../constants/styles";

export const BigIcon = props => (
  <Icon
    type="font-awesome"
    size={APP_DIMENSIONS.SCREEN_HEIGHT * 0.123}
    color={COLORS.RED.FIRE_ENGINE_RED}
    containerStyle={{
      padding: APP_DIMENSIONS.SCREEN_WIDTH * 0.024
    }}
    {...props}
  />
);
