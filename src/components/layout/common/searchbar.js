import React from "react";
import { View } from "react-native";
import { Icon } from "react-native-elements";
import { FontTextInput } from "./input";
import APP_DIMENSIONS from "../../../constants/appDimensions";
import { COLORS } from "../../../constants/styles";

const height = APP_DIMENSIONS.SCREEN_HEIGHT * 0.11;
const padding = APP_DIMENSIONS.SCREEN_HEIGHT * 0.028;

export class CustomSearchBar extends React.Component {
    constructor(props) {
      super(props);
    }

    render(){
      return (
        <View
          style={{
            height: height,
            padding: padding,
            backgroundColor: COLORS.WHITE.WHITE_SMOKE,
            justifyContent: "center",
            alignItems: "stretch"
          }}
        >
          <View
            style={{
              flex: 1,
              flexDirection: "row",
              backgroundColor: COLORS.WHITE.WHITE,
              justifyContent: "flex-start",
              alignItems: "center"
            }}
          >
            <Icon
              name="search"
              type="font-awesome"
              color={COLORS.GREY.DARK_GRAY}
              size={APP_DIMENSIONS.SCREEN_HEIGHT * 0.024}
              containerStyle={{ padding: APP_DIMENSIONS.SCREEN_HEIGHT * 0.015 }}
            />
            <FontTextInput
              underlineColorAndroid="#00000000"
              placeholder={this.props.placeholder}
              placeholderTextColor={COLORS.GREY.DARK_GRAY}
              onChange={this.props.onChange}
              onChangeText = {this.props.onChangeText}
              onEndEditing = {this.props.onEndEditing}
              value={this.props.value}
              editable={this.props.editable}
              style={{
                width: "100%",
                fontFamily: "open-sans",
                fontSize: APP_DIMENSIONS.SCREEN_WIDTH * 0.043
              }}
            />
          </View>
        </View>
      )
    }
  };
