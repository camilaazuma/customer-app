import React from "react";
import { View, TextInput, Platform } from "react-native";
import { Button } from 'react-native-elements';
import Icon from "react-native-vector-icons/FontAwesome";
import { connect } from "react-redux";
import APP_DIMENSIONS from "../../../constants/appDimensions";
import { COLORS } from "../../../constants/styles";
import { TextInputMask } from 'react-native-masked-text'
import * as Layout from "./";
import { FontText } from "./text";

class FontTextInputComponent extends React.Component {

  LoadInputext() {
    if (this.props.fontLoaded) {
      if (this.props.maskActive === true) {
        return <TextInputMask {...this.props} refInput={(r) => { this.props.inputRef && this.props.inputRef(r) }} />
      }
      else
        return <TextInput {...this.props} ref={(r) => { this.props.inputRef && this.props.inputRef(r) }} />
    }
    else
      return null;
  }
  render() {
    return this.LoadInputext();
  }
}

const mapStateToProps = state => ({
  fontLoaded: state.fontLoader.fontLoaded
});

export const FontTextInput = connect(
  mapStateToProps,
  null
)(FontTextInputComponent);

class CustomTextInput extends React.Component {
  constructor(props) {
    super(props);
    this.state = { placeholder: props.text.length == 0 };
  }

  handleChange = (ev) => {
    this.setState({ placeholder: ev.nativeEvent.text.length == 0 });
    this.props.onChange && this.props.onChange(ev);
  }

  render() {
    const { placeholderStyle, style, onChange, ...rest } = this.props;

    return (
      <FontTextInput
        {...rest}
        onChange={this.handleChange}
        style={this.state.placeholder ? [style, placeholderStyle] : style}
      />
    );
  }
}

export const FormTextInput = props => (
  <View
    style={{
      borderBottomColor: props.multiline
        ? "#00000000"
        : props.inputError ?
          COLORS.RED.FIRE_ENGINE_RED
          : COLORS.WHITE.WHITE_SMOKE,
      borderBottomWidth: props.inputError ? 2 : 1
    }}
  >
    <FontTextInput
      text=""
      underlineColorAndroid={"#00000000"}
      style={{
        height: props.multiline ? null : APP_DIMENSIONS.SCREEN_HEIGHT * 0.05,
        borderBottomColor: COLORS.GREY.LIGHT_GREY,
        width: APP_DIMENSIONS.SCREEN_WIDTH * 0.833,
        fontFamily: "open-sans-semibold",
        fontSize: APP_DIMENSIONS.SCREEN_WIDTH * 0.041 * APP_DIMENSIONS.SCREEN_WIDTH_SIZE_CORRECTION,
        letterSpacing: APP_DIMENSIONS.SCREEN_WIDTH * 0.0016 * APP_DIMENSIONS.SCREEN_WIDTH_SIZE_CORRECTION,
        ...props.style
      }}
      placeholderTextColor={COLORS.BLACK.BLACK}
      inputRef={props.inputRef}
      onSubmitEditing={props.onSubmitEditing}
      {...props}
    />
  </View>
)

export const BorderedFormInput = (props) => (
  <View>
    <View style={{ paddingVertical: APP_DIMENSIONS.SCREEN_HEIGHT * 0.006 }}>
      <FontText style={{
        fontSize: APP_DIMENSIONS.SCREEN_WIDTH * 0.035,
        fontFamily: "open-sans-bold",
      }}>{props.label}</FontText>
    </View>
    <View
      style={{
        borderWidth: 1, borderColor: COLORS.GREY.DARK_GRAY,
        flexDirection: 'row', justifyContent: 'space-between',
        height: APP_DIMENSIONS.SCREEN_HEIGHT * 0.053, alignItems: "center",
        paddingHorizontal: APP_DIMENSIONS.SCREEN_WIDTH * 0.03,
      }}
    >
      <FontTextInput 
        style={{ fontSize: APP_DIMENSIONS.SCREEN_WIDTH * 0.035, width: APP_DIMENSIONS.SCREEN_WIDTH * 0.8, ...props.style }}
        text=""
        underlineColorAndroid={"#00000000"}
        placeholderTextColor={COLORS.BLACK.BLACK}
        inputRef={props.inputRef}
        onSubmitEditing={props.onSubmitEditing}
        {...props}
      />
    </View>
  </View>
)

