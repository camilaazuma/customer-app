import React from "react";
import {
  View,
  Text,
  FlatList,
  Platform,
  Linking,
  ActivityIndicator,
  KeyboardAvoidingView
} from "react-native";
import { Icon } from "react-native-elements";
import { COLORS } from "../../../constants/styles";
import { Constants, Location, Permissions } from 'expo';
import styled from "styled-components/native";
import APP_DIMENSIONS from "../../../constants/appDimensions";
import * as Layout from "./";
import geolib from "geolib";
import { titleCase, nullFunction, capitalizeFirstLetter } from "../../../utils";
import LocationHelper from "../../../helpers/LocationHelper";
import AsyncStorageHelper from "../../../helpers/AsyncStorageHelper";
import { connect } from "react-redux";
import * as Actions from "../../../actions";
import FastImage from 'react-native-fast-image-expo';

export const BorderedView = styled.View`
  border-bottom-color: ${COLORS.GREY.LIGHT_GREY};
  border-top-color: ${COLORS.GREY.LIGHT_GREY};
  border-bottom-width: 1;
`;

export const ListItemContainer = styled.View`
  background-color: ${COLORS.WHITE.WHITE};
  padding: ${APP_DIMENSIONS.SCREEN_WIDTH * 0.031}px;
  flex-direction: row;
  justify-content: flex-start;
  align-items: center;
  height: ${APP_DIMENSIONS.SCREEN_HEIGHT * 0.205};
`;

export const ProductListItemContainer = styled.View`
  background-color: ${COLORS.WHITE.WHITE};
  padding: ${APP_DIMENSIONS.SCREEN_WIDTH * 0.031}px;
  flex-direction: row;
  justify-content: flex-start;
  align-items: center;
  border-bottom-color: ${COLORS.GREY.LIGHT_GREY};
  border-top-color: ${COLORS.GREY.LIGHT_GREY};
  border-bottom-width: 1;
  height: ${APP_DIMENSIONS.SCREEN_HEIGHT * 0.15};
`;

export const LeftContainer = styled.View`
  justify-content: center;
  align-items: stretch;
`;

export const EmptyContainer = styled.View``;

export const ImageContainer = styled.View`
  align-items: stretch;
  background-color: ${COLORS.WHITE.WHITE_SMOKE};
`;

export const ProductImage = styled(FastImage)`
  position: absolute;
  top: 7;
  bottom: 7;
  left: 7;
  right: 7;
  background-color: white;
`;

export const RightContainer = styled.View``;

export const InfoContainer = styled.View`
  justify-content: flex-start;
  align-items: flex-start;
`;

export const InfoText = props => <Layout.FontText {...props} numberOfLines={props.numberOfLines || 1}  />;

export const ItemTitle = props => (
  <View style={{ flex: 1, justifyContent: "center"}}>
    <InfoText
      numberOfLines={props.numberOfLines}
      style={{
        fontWeight: "bold",
        fontSize: APP_DIMENSIONS.SCREEN_WIDTH * 0.046,
        lineHeight: APP_DIMENSIONS.SCREEN_HEIGHT * 0.04,
        textAlign: "left",
        fontFamily:'open-sans-semibold',
        ...props.style
      }}

    >
      {""}
      {props.children}
      {""}
    </InfoText>
  </View>
);

export const RightAlignInfo = props => (
  <View style={{ flex: 1, justifyContent: "center"}}>
    <InfoText
      style={{
        textAlign: "right",
        fontSize: APP_DIMENSIONS.SCREEN_WIDTH * 0.034,
        fontFamily:'open-sans-italic',
        letterSpacing: APP_DIMENSIONS.SCREEN_WIDTH * 0.0016,
        ...props.style
      }}
      {...props}
    >
      {props.children}
    </InfoText>
    { props.secondLine &&
      <InfoText
        style={{
          textAlign: "right",
          fontSize: APP_DIMENSIONS.SCREEN_WIDTH * 0.034,
          fontFamily:'open-sans-italic',
          letterSpacing: APP_DIMENSIONS.SCREEN_WIDTH * 0.0016,
          ...props.style
        }}
        {...props}
      >
        {props.secondLine}
      </InfoText>
    }
  </View>
);

export const ItemSubTitle = props => (
  <View style={{ flex: 1, justifyContent: "center" }}>
    <InfoText
      numberOfLines={props.numberOfLines}
      style={{
        textAlign: "left",
        fontSize: APP_DIMENSIONS.SCREEN_WIDTH * 0.037,
        color: COLORS.GREY.NIGHT_RIDER,
        fontFamily: 'open-sans-semibold',
        letterSpacing: APP_DIMENSIONS.SCREEN_WIDTH * 0.0016,
        ...props.style
      }}
    >
      {props.children}
    </InfoText>
  </View>
);

export const ColoredBgInfo = props => (
  <View
    style={{
      backgroundColor: props.backgroundColor, // COLORS.GREY.LIGHT_GREY,
      borderRadius: APP_DIMENSIONS.SCREEN_WIDTH * 0.048,
      height: APP_DIMENSIONS.SCREEN_WIDTH * 0.072,
      paddingHorizontal: APP_DIMENSIONS.SCREEN_WIDTH * 0.031,
      justifyContent: "center"
    }}
  >
    <Layout.FontText
      style={{
        color: props.fontColor, //COLORS.BLACK.BLACK,
        fontSize: props.fontSize || APP_DIMENSIONS.SCREEN_WIDTH * 0.03
      }}
    >
      {props.children}
    </Layout.FontText>
  </View>
);

export const DeepLinkIcon = styled.Image`
  resize-mode: contain;
  position: absolute;
  top: 5;
  bottom: 5;
  left: 5;
  right: 5;
  background-color: white;
  height: undefined;
  width: undefined;
`;

export class StoreListItem extends React.PureComponent {

  _formatBusinessHoursString = text => {
    try {
      var hours = text.toLowerCase();
      const willCapitalizeStrings = [
        'domingo', 'segunda', 'terça', 'quarta', 'quinta', 'sexta', 'sábado', 'domingo', 'feriado',
        'lunes', 'martes', 'miercoles', 'jueves', 'viernes', 'festivo'
      ]
      const sundayStrings = ['domingos', 'dom.', ';'];
      let searchIndex = -1;
      let found = sundayStrings.some((string, index, array)=>{
        searchIndex = hours.indexOf(string);
        return (searchIndex > -1)
      })
      if (found) hours = (hours.substring(0, searchIndex) + '\n' + hours.substring(searchIndex)).replace(';', '');

      willCapitalizeStrings.map((string)=>{
        hours = hours.replace(string, capitalizeFirstLetter(string))
      })

      return hours;
    }
    catch(e) {
      return text;
    }
  };

  render () {
    return (
      <BorderedView>
        <Layout.TouchableContainer opacity onPress={this.props.onPress} >
          <ListItemContainer style={{ height: APP_DIMENSIONS.SCREEN_HEIGHT * 0.169 }}>
            <LeftContainer style={{ flex: 0.75 }}>
              <ItemTitle style={{ fontSize: APP_DIMENSIONS.SCREEN_WIDTH * 0.064, lineHeight: APP_DIMENSIONS.SCREEN_HEIGHT * 0.04 }}>{this.props.itemTitle}</ItemTitle>
              <ItemSubTitle>{this.props.subTitle}</ItemSubTitle>
              <Layout.FontText
                fontColor={COLORS.WHITE.NIGHT_RIDER}
                style={{ paddingLeft: 6, fontSize: APP_DIMENSIONS.SCREEN_WIDTH * 0.026, lineHeight: APP_DIMENSIONS.SCREEN_HEIGHT * 0.017, fontFamily: 'open-sans-semibold', }}
              >
                {this._formatBusinessHoursString(this.props.text)}
              </Layout.FontText>
              <Layout.FontText
                fontColor={COLORS.WHITE.NIGHT_RIDER}
                style={{ paddingLeft: 6, fontSize: APP_DIMENSIONS.SCREEN_WIDTH * 0.026, lineHeight: APP_DIMENSIONS.SCREEN_HEIGHT * 0.017, fontFamily: 'open-sans-semibold', }}
              >
                {this.props.t("common:telephone") + ": " + this.props.phone}
              </Layout.FontText>
            </LeftContainer>
            <RightContainer
              style={{
                flex: 0.25,
                flexDirection: "column",
                justifyContent: "center",
                alignItems: "center"
              }}
            >
              <ColoredBgInfo
                backgroundColor={COLORS.RED.FIRE_ENGINE_RED}
                fontColor={COLORS.WHITE.WHITE}
                fontSize={this.props.badgeFontSize}
              >
                {this.props.info ? this.props.info : "-- km"}
              </ColoredBgInfo>
              { this.props.showDeepLink &&
              <View style={{flex: 1, flexDirection: 'row'}}>
                <View style={{flex: 1,  justifyContent: 'center', alignItems: 'stretch'}}>
                  <Layout.TouchableContainer onPress={this.props.wazeDeepLink}>
                    <ImageContainer style={{ flex: 1, backgroundColor: COLORS.WHITE.WHITE }}>
                      <DeepLinkIcon
                        source={require("../../../../assets/waze.png")}
                      />
                    </ImageContainer>
                  </Layout.TouchableContainer>
                </View>
                <View style={{flex: 1,  justifyContent: 'center', alignItems: 'stretch'}}>
                  <Layout.TouchableContainer onPress={this.props.mapsDeepLink}>
                    <ImageContainer style={{ flex: 1, backgroundColor: COLORS.WHITE.WHITE }}>
                      <DeepLinkIcon
                        source={require("../../../../assets/route-icon.png")}
                      />
                    </ImageContainer>
                  </Layout.TouchableContainer>
                </View>
              </View>}
            </RightContainer>
          </ListItemContainer>
        </Layout.TouchableContainer>
      </BorderedView>
    )
  }
}


export class ShoppingListsListItem extends React.PureComponent {
  render () {
    return (
      <Layout.TouchableContainer opacity onPress={this.props.onPress}>
        <ListItemContainer style={{ height: APP_DIMENSIONS.SCREEN_HEIGHT * 0.149 }}>
          <LeftContainer style={{ flex: 0.7 }}>
            <ItemTitle style={{ fontSize: APP_DIMENSIONS.SCREEN_WIDTH * 0.064, lineHeight: APP_DIMENSIONS.SCREEN_HEIGHT * 0.036 }}>{this.props.itemTitle}</ItemTitle>
            <ItemSubTitle>{this.props.subTitle}</ItemSubTitle>
          </LeftContainer>
          <RightContainer
            style={{
              flex: 0.3,
              flexDirection: "column",
              justifyContent: "center",
              alignItems: "flex-end"
            }}
          >
            <ColoredBgInfo
              backgroundColor={COLORS.RED.FIRE_ENGINE_RED}
              fontColor={COLORS.WHITE.WHITE}
            >
              {this.props.info}
            </ColoredBgInfo>
          </RightContainer>
        </ListItemContainer>
      </Layout.TouchableContainer>
    )
  }
}


export class ShoppingListProductItem extends React.PureComponent {
  render() {
    let { t } = this.props;
    return (
      <ListItemContainer>
        <RightContainer
          style={{
            flex: 1,
            flexDirection: "column",
            justifyContent: "flex-start"
          }}
        >
          <InfoContainer style={{ flex: 1.5 }}>
            <View style={{ flexDirection: "row", flex: 1 }}>
              <View style={{ flex: 0.87, justifyContent: "center" }}>
                <ItemTitle>{this.props.productName}</ItemTitle>
              </View>
              <View
                style={{
                  flex: 0.13,
                  justifyContent: "center",
                }}
              >
                {this.props.stock || this.props.found ? null : (
                  <Icon
                    name="warning"
                    type="font-awesome"
                    color={COLORS.RED.FIRE_ENGINE_RED}
                    size={APP_DIMENSIONS.SCREEN_WIDTH * 0.049}
                  />
                )}
                {this.props.offerActive ? (
                  <View
                    style={{
                      flexDirection: "row",
                      flexWrap: "wrap",
                      alignItems: "center"
                    }}
                  >
                    <Layout.FontText
                      style={{
                        color: COLORS.RED.FIRE_ENGINE_RED,
                        fontSize: APP_DIMENSIONS.SCREEN_WIDTH * 0.025,
                        fontFamily: 'open-sans'
                      }}
                    >
                      {t && (t("shoppingLists:shoppingLists.offer") + " ") }
                    </Layout.FontText>
                    <Icon
                      name="certificate"
                      type="font-awesome"
                      color={COLORS.RED.FIRE_ENGINE_RED}
                      size={APP_DIMENSIONS.SCREEN_WIDTH * 0.049}
                    />
                  </View>
                ) : null}
              </View>
            </View>
            <View style={{ flex: 1 }}>
              <View style={{ flex: 1, justifyContent: "center", flexWrap: "wrap" }}>
                <ItemSubTitle style={{ color: COLORS.BLACK.BLACK }}>
                  <Text>{this.props.code} | </Text>
                  <Text
                    style={{
                      color: this.props.stock
                        ? COLORS.BLACK.BLACK
                        : COLORS.RED.FIRE_ENGINE_RED
                    }}
                  >
                    {this.props.found ? (this.props.stock ? t("shoppingLists:shoppingLists.inStock") : t("shoppingLists:shoppingLists.outOfStock")) : t("shoppingLists:shoppingLists.notFound") }
                  </Text>
                </ItemSubTitle>
              </View>
            </View>
          </InfoContainer>
          <View
            style={{flexDirection: "row", flex: 1, alignItems: 'center'}}
          >
            <View style={{ flex: 0.24 }}>
              <Text>{t("shoppingLists:shoppingLists.quantity")+ ": "}</Text>
            </View>
            <View style={{flex: 0.23}}>
              <Layout.FormTextInput
                keyboardType={Platform.OS==='ios' ? "decimal-pad" : 'numeric'}
                returnKeyType='done'
                value={this.props.totalQuantity.toString()}
                onChangeText={this.props.onChangeQuantity}
                onBlur={this.props.setQuantity}
                style = {{textAlign: 'center', backgroundColor: '#C0C0C0'}}
              />
            </View>
            <EmptyContainer style={{ flex: 0.4 }} />
            <View style={{flex: 0.13}}>
              <Layout.TouchableContainer onPress={this.props.deleteProduct}>
                <Layout.BottomTabButtonContainer>
                  <Icon
                    name="trash-o"
                    type="font-awesome"
                    color={COLORS.RED.FIRE_ENGINE_RED}
                    size={APP_DIMENSIONS.SCREEN_WIDTH * 0.058}
                    containerStyle={{
                      flex: 1,
                      alignItems: 'center',
                      justifyContent: 'center'
                    }}
                  />
                </Layout.BottomTabButtonContainer>
              </Layout.TouchableContainer>
            </View>
          </View>
        </RightContainer>
      </ListItemContainer>
    );
  }
}

export class TotalPriceListItem extends React.PureComponent {
  render() {
    return (
      <Layout.TouchableContainer opacity onPress={this.props.onPress}>
        <ListItemContainer style={{ height: APP_DIMENSIONS.SCREEN_HEIGHT * 0.12 }}>
          <LeftContainer style={{ flex: 0.8 }}>
            <ItemTitle>{this.props.itemTitle}</ItemTitle>
            <ItemSubTitle>{this.props.subTitle}</ItemSubTitle>
          </LeftContainer>
          <RightContainer
            style={{
              flex: 0.2,
              flexDirection: "column",
              justifyContent: "center",
              alignItems: "flex-end"
            }}
          >
            <RightAlignInfo style={{ color: COLORS.RED.FIRE_ENGINE_RED, textAlign: 'right', fontSize: APP_DIMENSIONS.SCREEN_WIDTH * 0.035 }} numberOfLines={2}>
              {this.props.info1}
            </RightAlignInfo>
            <ColoredBgInfo
              backgroundColor={COLORS.GREY.LIGHT_GREY}
              fontColor={COLORS.BLACK.BLACK}
            >
              {this.props.info2}
            </ColoredBgInfo>
          </RightContainer>
        </ListItemContainer>
      </Layout.TouchableContainer>
    )
  }
}

export const TotalPriceListFirstItem = props => (
  <View style={{ height: APP_DIMENSIONS.SCREEN_HEIGHT * 0.15 }}>
    <Layout.TouchableContainer opacity onPress={props.onPress}>
      <ListItemContainer style={{ height: APP_DIMENSIONS.SCREEN_HEIGHT * 0.15 }} >
        <LeftContainer style={{ flex: 0.65 }}>
          {/* <ItemTitle>{props.itemTitle1}</ItemTitle> */}
          <ItemTitle>{props.itemTitle2}</ItemTitle>
          <ItemSubTitle>{props.subTitle}</ItemSubTitle>
        </LeftContainer>
        <RightContainer
          style={{
            flex: 0.35,
            flexDirection: "column",
            justifyContent: "center",
            alignItems: "flex-end"
          }}
        >
          <RightAlignInfo
            style={{ color: COLORS.BLACK.BLACK, fontFamily: "open-sans" }}
          >
            {props.info1}
          </RightAlignInfo>
          <ColoredBgInfo
            backgroundColor={COLORS.RED.FIRE_ENGINE_RED}
            fontColor={COLORS.WHITE.WHITE}
          >
            {props.totalPrice}
          </ColoredBgInfo>
        </RightContainer>
      </ListItemContainer>
    </Layout.TouchableContainer>
  </View>
);


export class ShoppingListAddProductsItem extends React.PureComponent {
  render() {
    var color = this.props.added ? COLORS.GREEN.GREEN : COLORS.GREY.LIGHT_GREY;
    var quantity = typeof this.props.quantity !== 'undefined' ? parseInt(this.props.quantity) : 0;
    let { t } = this.props;
    return (
      <Layout.TouchableContainer opacity onPress={this.props.onPress}>
        <ProductListItemContainer style={{backgroundColor: this.props.added ? COLORS.BLUE.ALICE_BLUE : COLORS.WHITE.WHITE}}>
          <LeftContainer style={{ flex: 0.1 }}>
            <Icon
              name="check"
              type="font-awesome"
              color={color}
              size={APP_DIMENSIONS.SCREEN_WIDTH * 0.049}
            />
          </LeftContainer>
          <RightContainer
            style={{
              flex: 0.9,
              flexDirection: "column",
              justifyContent: "flex-start"
            }}
          >
            <View style={{ flex: 1 }}>
              <View style={{ flexDirection: "row", flex: 1 }}>
                <View style={{ flex: 1, justifyContent: "center" }}>
                  <InfoText numberOfLines={2}
                    style={{
                      fontWeight: "bold",
                      fontSize: APP_DIMENSIONS.SCREEN_WIDTH * 0.046,
                      textAlign: "left",
                      fontFamily:'open-sans-semibold',
                    }}>{this.props.productName}</InfoText>
                </View>
              </View>
            </View>

            <View style={{ flex: 1 }}>
              <View style={{ flexDirection: "row", flex: 1 }}>
                <View style={{ flex: 0.6, justifyContent: "center" }}>
                  <InfoText style={{
                      textAlign: "left",
                      fontSize: APP_DIMENSIONS.SCREEN_WIDTH * 0.037,
                      color: COLORS.GREY.NIGHT_RIDER,
                      fontFamily: 'open-sans-semibold',
                    }}>ART {this.props.code}</InfoText>

                </View>
                <View style={{ flex: 0.4, justifyContent: "center" }}>
                  <View
                    style={{flexDirection: "row", flex: 1, alignItems: 'center'}}
                  >
                    <View style={{ flex: 1,  }}>
                      <Layout.FontText style={{fontFamily: 'open-sans'}}>{t("shoppingLists:shoppingLists.quantity")+ ": "}</Layout.FontText>
                    </View>
                    <View style={{flex: 1, }}>
                      <Layout.FontText style = {{textAlign: 'center', backgroundColor: '#C0C0C0'}}> {quantity}</Layout.FontText>
                    </View>
                  </View>
                </View>
              </View>
            </View>
          </RightContainer>
        </ProductListItemContainer>
      </Layout.TouchableContainer>
    );
  }
}

export class ProductNamePriceInquiryListItem_CO extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      image: { uri: props.productImgSrc }
    };
  }
  onError = (error) => {this.setState({ image: require('../../../../assets/noimage_es.png')})}

  render() {
    const {t} = this.props;
    return (
      <Layout.TouchableContainer opacity onPress={this.props.onPress}>
        <ListItemContainer>
          <LeftContainer style={{ flex: 0.32 }}>
            <ImageContainer style={{ flex: 1 }}>
              <ProductImage
                source={this.state.image}
                style={{width: undefined, height: undefined}}
                resizeMode= {FastImage.resizeMode.contain}
                onError={(e) => this.onError(e) }
              />
            </ImageContainer>
          </LeftContainer>
          <EmptyContainer style={{ flex: 0.025 }} />
          <RightContainer
            style={{
              flex: 0.65,
              flexDirection: "column",
              justifyContent: "flex-start"
            }}
          >
            <View style={{ flexDirection: "row", flex: 1 }}>
              <ItemTitle numberOfLines={2}>{this.props.productName}</ItemTitle>
            </View>
            <View style={{ flexDirection: "row", flex: 1 }}>
              <ItemSubTitle numberOfLines={2} style={{fontSize: APP_DIMENSIONS.SCREEN_WIDTH * 0.03 }}>{this.props.productDescription}</ItemSubTitle>
            </View>
          </RightContainer>
        </ListItemContainer>
      </Layout.TouchableContainer>
    );
  }
}

export class ProductNamePriceInquiryListItem extends React.PureComponent {
  render() {
    return (
      <Layout.BorderedView>
        <Layout.TouchableContainer opacity onPress={this.props.onPress}>
          <ListItemContainer style={{  height: APP_DIMENSIONS.SCREEN_HEIGHT * 0.1 }}>
            <RightContainer
              style={{
                flex: 1,
                flexDirection: "column",
                justifyContent: "flex-start"
              }}
            >
              <InfoContainer style={{ flexDirection: "column", flex: 1, justifyContent: 'center',  }}>
                <ItemTitle>{this.props.productName}</ItemTitle>
              </InfoContainer>
            </RightContainer>
          </ListItemContainer>
        </Layout.TouchableContainer>
      </Layout.BorderedView>
    );
  }
}

const STORE_LIST_KEY = "STORE_LIST";

class StoreListComponent extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      arrayholder: [],
      fetchStoreListError: false,
      language: props.language
    };
  }

  async componentDidMount() {
    if (this.state.data.length === 0) {
      if (this._isReadyToGetDistance(this.props))
        this.props.sortStoreList();
      this.setState({ data: this.props.fetchedStoreList, arrayholder: this.props.fetchedStoreList, fetchStoreListError: false});
    }

    if (this._shouldSetCurrentStoreFromCRM()) this._setCurrentStoreFromCRM();
    if (this._shouldSetNearestStoreAsCurrentStore()) await this.props.setCurrentStore(this.props.nearestStore);

  }

  async componentDidUpdate(prevProps, prevState) {
    if (!(this._isReadyToGetDistance(prevProps)) && this._isReadyToGetDistance(this.props)) {
      this.props.sortStoreList();
    }

    if (!prevProps.preferredStore) {
      if (this._shouldSetCurrentStoreFromCRM()) await this._setCurrentStoreFromCRM();
    }

    if (!(prevProps.isSorted) && this.props.isSorted ) {
      this.setState({ data: this.props.fetchedStoreList, arrayholder: this.props.fetchedStoreList, fetchStoreListError: false});
      if (this._shouldSetNearestStoreAsCurrentStore()) await this.props.setCurrentStore(this.props.nearestStore);
    }

    if (prevProps.fetchedStoreList.constructor === Array && prevProps.fetchedStoreList.length === 0 && this.props.fetchedStoreList.length > 0) {
      // salva lista de lojas em async storage
      let storeList = {};
      let firebaseStoreVersion = this.props.firebaseStoreVersion
      storeList.version = firebaseStoreVersion === "default" ? 0 : firebaseStoreVersion;
      storeList.list = this.props.fetchedStoreList;

      if (firebaseStoreVersion!==null)
        await AsyncStorageHelper.setItem(STORE_LIST_KEY, storeList);

      if (this._shouldSetCurrentStoreFromCRM()) await this._setCurrentStoreFromCRM();

      this.setState({ data: this.props.fetchedStoreList, arrayholder: this.props.fetchedStoreList, fetchStoreListError: false});
    }
  }

  static getDerivedStateFromProps(nextProps, prevState){
    if(!prevState.fetchStoreListError && nextProps.hasConnectionError) {
      nextProps.openModal("connectionError");
      return {
        fetchStoreListError: true
      }
    }
    return null;
  }

  _shouldSetCurrentStoreFromCRM = () => {
    const { user, preferredStore, currentStoreObject } = this.props;
    if (user.contactUuid) { // confere se usuario estava cadastrado no crm
      if (preferredStore && currentStoreObject === null) { // confere se puxou loja favorita e se ainda nao foi escolhida loja
        return true;
      }
    }
    return false;
  }

  _setCurrentStoreFromCRM = async () => {
    let preferredStoreObject = this.props.fetchedStoreList.find(store=>store.store_no === this.props.preferredStore);
    if (preferredStoreObject) await this.props.setCurrentStore(preferredStoreObject);
  }

  _shouldSetNearestStoreAsCurrentStore = () => (this.props.isSorted && !this.props.currentStoreObject && !this.props.user.contactUuid ) 

  _isReadyToGetDistance = (props) =>
    (props.fetchedStoreList.length !== 0 && props.location!==null)

  _searchFilterFunction = text => {
    const newData = this.state.arrayholder.filter(item => {
      const itemData =  `${item.name.toUpperCase()}`; //locationname

      const textData = text.toUpperCase();

      return itemData.indexOf(textData) > -1;
    });
    this.setState({ data: newData });
  };

  _onChangeText = text => this._searchFilterFunction(text);

  renderedAllItems = false;

  _openURL(link, fallbackLink = null){
    Linking.canOpenURL(link).then(supported => {
      if (!supported) {
        console.log('Can\'t handle url: ' + link);
        if (fallbackLink)
          return this._openURL(fallbackLink);
      } else {
        return Linking.openURL(link);
      }
    }).catch(err =>  console.error('An error occurred', err));
  }

  _wazeDeepLink = (latitude, longitude) => () => this._openURL(`https://www.waze.com/ul?ll=${latitude},${longitude}&navigate=yes&zoom=17`)

  _mapsDeepLink = (latitude, longitude) => () => {
    this._openURL( 
      (Platform.OS === 'ios' ? 'comgooglemaps' : 'https') + `://www.google.com/maps/search/?api=1&query=${latitude},${longitude}`, 
      Platform.OS === 'ios' ? `http://maps.apple.com/?q=Makro&ll=${latitude},${longitude}` : null
    )
  }

  _renderStoreListItem = ({ item: store }, index) => {
    if(store.store_no === this.state.data[this.state.data.length-1].store_no ||
    store.idlocation === this.state.data[this.state.data.length-1].idlocation) {
      this.renderedAllItems = true;
    }
    else {
      this.renderedAllItems = false;
    }

    return (
      <StoreListItem
        onPress = {()=>this.props.onPress(store)}
        key={store.store_no}  // ORIGINAL_COLOMBIA: idlocation
        itemTitle={titleCase(store.name)} // ORIGINAL_COLOMBIA: locationname
        subTitle={titleCase(store.address).replace(";", ",")} // ORIGINAL_COLOMBIA: addressstreetname
        text={store.hours}
        phone={titleCase(store.phone)} // ORIGINAL_COLOMBIA: phonenumber
        info={store.distance!==undefined && store.distance!==null ?
          store.distance + " km" : ""
        }
        badgeFontSize={this.props.badgeFontSize}
        showDeepLink={this.props.showDeepLink}
        wazeDeepLink={this._wazeDeepLink(store.latitud, store.longitud)} // ORIGINAL_COLOMBIA: latitude longitude
        mapsDeepLink={this._mapsDeepLink(store.latitud, store.longitud)} // ORIGINAL_COLOMBIA: latitude longitude
        t={this.props.t}
      />
    );
  }


  _renderFooter = () =>
  {
    const { t } = this.props
    if (this.renderedAllItems)  return null;
    else if (this.state.data===null) {
      return (
        <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
          <Layout.FontText style={{ color: COLORS.BLACK.BLACK, fontSize: APP_DIMENSIONS.SCREEN_WIDTH*0.053, fontFamily: 'open-sans' }}>
            { t && t("common:emptyStoreListText")}
          </Layout.FontText>
        </View>
      )
    }
    else {
    return (
    <View>
      <ActivityIndicator size="large"/>
    </View>
  )}};

  render() {
    const { t } = this.props;
    return (
      <View
        style={{
          height: this.props.expanded
            ? this.props.expandedHeight
            : this.props.hiddenHeight
        }}
      >
      <Layout.SuccessMessageModal
          t={this.props.t}
          modalVisible={this.props.modalConnectionError}
          onOk={() => this.props.closeModal('connectionError')}
          title={t('common:connectionErrorTitle')}
          contentText={t("common:connectionErrorMessage")}
          okText={'OK'}
        />
      {!this.props.hideSearchBar &&
        <KeyboardAvoidingView>
              <Layout.CustomSearchBar
                placeholder={t && t("common:searchStorePlaceholder")}
                onChangeText ={this._onChangeText}
              />
        </KeyboardAvoidingView>
      }

        { (this.props.storeListLoading && this.props.expanded)
          ? <Layout.ViewLoader loading={true} />
          :
            <FlatList
              keyExtractor={(item, index) => {return item.store_no}} // ORIGINAL_COLOMBIA: idlocation
              data={this.state.data}
              renderItem={this._renderStoreListItem}
              initialNumToRender={8}
              maxToRenderPerBatch={8}
              removeClippedSubviews={true}
              refreshing={true}
              ListFooterComponent={this._renderFooter}
              style={{backgroundColor: COLORS.WHITE.WHITE}}
            />
        }
      </View>
    );
  }
}

const mapStateToProps = state => ({
  storeListLoading: state.storeListFetch.loading,
  fetchedStoreList: state.storeListFetch.storeList,
  isSorted: state.storeListFetch.isSorted,
  firebaseStoreVersion: state.storeListFetch.firebaseStoreVersion,
  location: state.location.location,
  nearestStore: state.storeListFetch.nearestStore,
  currentStoreObject: state.currentStore.object,
  preferredStore: state.currentStore.preferred,
  user: state.user,
});

const mapDispatchToProps = dispatch => {
  return {
    sortStoreList: () => {
      dispatch(Actions.sortStoreList());
    },
    setCurrentStore: (store) => {
      dispatch(Actions.setCurrentStore(store))
    },
    fetchStoreList: () => {
      dispatch(Actions.fetchStoreList());
    },
  };
};

export const StoreList = connect(
  mapStateToProps,
  mapDispatchToProps
)(StoreListComponent);
