import React from "react";
import { Platform, View, Modal, Button, Dimensions, Picker, TextInput, StyleSheet, TouchableWithoutFeedback } from "react-native";
import { Icon } from "react-native-elements";
import { COLORS } from "../../../constants/styles";
import styled from "styled-components/native";
import {connect} from "react-redux"
import * as Actions from "../../../actions"
import * as Layout from "../common";
import APP_DIMENSIONS from "../../../constants/appDimensions";
import { FontText } from "./text";
import { withNavigationFocus } from 'react-navigation';
import { nullFunction } from "../../../utils";


const SCREEN_HEIGHT = APP_DIMENSIONS.SCREEN_HEIGHT;
const SCREEN_WIDTH = APP_DIMENSIONS.SCREEN_WIDTH;

export const ModalBackground = styled.View`
    flex: 1;
    alignItems: center;
    flexDirection: column;
    justifyContent: space-around;
    backgroundColor: #00000040;
`;

export const ModalContainer = styled.View`
    backgroundColor: ${COLORS.WHITE.WHITE};
    minHeight: ${SCREEN_HEIGHT*APP_DIMENSIONS.SCREEN_HEIGHT_SIZE_CORRECTION*0.285};
    width: ${SCREEN_WIDTH*0.93};
    borderRadius: 5;
    display: flex;
    alignItems: center;
    justifyContent: center;
`;

export const ModalContentContainer = styled.View`
  padding: ${SCREEN_WIDTH*0.045}px;
`;

export const ModalContentTitleText = styled(FontText)`
  fontSize: ${SCREEN_WIDTH*0.045};
  textAlign: center;
  fontFamily: 'open-sans';
  letterSpacing: ${SCREEN_WIDTH*0.003};
`;

export const ModalContentText = styled.Text`
  fontSize: ${SCREEN_WIDTH*0.043};
  textAlign: center;
`;


class ModalWrapper extends React.Component {

  onOk = () => {
    this.props.onOk();
  };

  onCancel = () => {
    this.props.onCancel();
  };

  onOtherOption = () => {
    this.props.onOtherOption();
  };

  okButton = () => this.props.showOk ? (
    <Layout.TouchableContainer
      opacity
      onPress={this.onOk}
    >
    <Layout.BottomTabButtonContainer
      style={{
        borderRightWidth: 0,
        borderLeftWidth: this.props.showCancel ? StyleSheet.hairlineWidth : 0,
        borderLeftColor: COLORS.GREY.LIGHT_GREY,
        backgroundColor: 'transparent'
       }}
    >
      <Layout.BottomTabButtonText>
        {this.props.okText}
      </Layout.BottomTabButtonText>
    </Layout.BottomTabButtonContainer>
  </Layout.TouchableContainer>
  ) : null;

  cancelButton = () => this.props.showCancel ? (
    <Layout.TouchableContainer
      opacity
      onPress={this.onCancel}
    >
    <Layout.BottomTabButtonContainer
      style={{
        borderRightWidth: this.props.showOk ? StyleSheet.hairlineWidth : 0,
        borderLeftWidth: 0,
        borderRightColor: COLORS.GREY.LIGHT_GREY,
        backgroundColor: 'transparent'
      }}
    >
      <Layout.BottomTabButtonText>
        {this.props.cancelText}
      </Layout.BottomTabButtonText>
    </Layout.BottomTabButtonContainer>
  </Layout.TouchableContainer>
  ) : null;

  otherButton = () => this.props.showOther ? (
    <Layout.TouchableContainer
      opacity
      onPress={this.onOtherOption}
    >
    <Layout.BottomTabButtonContainer
      style={{
        borderLeftWidth: this.props.showOk ? StyleSheet.hairlineWidth : 0,
        borderRightWidth: 0,
        borderLeftColor: COLORS.GREY.LIGHT_GREY,
        backgroundColor: 'transparent'
      }}
    >
      <Layout.BottomTabButtonText>
        {this.props.otherOptionText}
      </Layout.BottomTabButtonText>
    </Layout.BottomTabButtonContainer>
  </Layout.TouchableContainer>
  ) : null;

  render() {
    const { modalVisible, isFocused, onPressOutside } = this.props;
    return ( isFocused ?
      <Modal
        transparent={true}
        animationType={"fade"}
        visible={modalVisible}
        onRequestClose={() => {
          console.log("close modal");
        }}
      >
        <TouchableWithoutFeedback onPress={onPressOutside ? onPressOutside : nullFunction}>
          <ModalBackground >
            <TouchableWithoutFeedback onPress={nullFunction}>
              <ModalContainer style={this.props.containerStyle} >
                <View style={{flex: 1, alignItems: 'center', justifyContent: 'center', }}>
                  {this.props.children}
                </View>
                { !this.props.hideBottomTab &&
                  <Layout.BottomTabContainer style={{ backgroundColor:  'transparent', borderTopColor: COLORS.GREY.LIGHT_GREY }}>
                    {this.cancelButton()}
                    {this.okButton()}
                    {this.otherButton()}
                  </Layout.BottomTabContainer>
                }
              </ModalContainer>
            </TouchableWithoutFeedback>
          </ModalBackground>
        </TouchableWithoutFeedback>
      </Modal>
      : null
    );
  }
}

export default withNavigationFocus(ModalWrapper);

export const SuccessMessageModal = props => {
  const {t} = props;
  return (
    <Layout.ModalWrapper
      modalVisible={props.modalVisible}
      showOk
      okText={props.okText || t && t("common:next")}
      onOk={props.onOk}
      containerStyle={props.contentText && {height: APP_DIMENSIONS.SCREEN_HEIGHT*0.342}}
      onPressOutside={props.onOk}
    >
      <Layout.ModalContentContainer>
         <View style={{flex: 1, justifyContent: 'space-around'}}>
          <Layout.ModalContentTitleText>
            {props.title}
          </Layout.ModalContentTitleText>

        { props.contentText &&
          <Layout.ModalContentTitleText style={{
            fontSize: props.contentFontSize || APP_DIMENSIONS.SCREEN_WIDTH*0.043,
            letterSpacing: APP_DIMENSIONS.SCREEN_WIDTH*0.0016 }}>
            {props.contentText}
          </Layout.ModalContentTitleText>
        }
        </View>
      </Layout.ModalContentContainer>
    </Layout.ModalWrapper>
  );
};

export const TwoButtonsModal = (props) => {
  const {t} = props;
  return (
    <Layout.ModalWrapper
      modalVisible={props.modalVisible}
      showOk
      okText={props.okText}
      showCancel
      cancelText={props.cancelText}
      onOk={props.onOk}
      onCancel={props.onCancel}
      containerStyle={{height: APP_DIMENSIONS.SCREEN_HEIGHT*0.342}}
      onPressOutside={props.onCancel}
    >
      <Layout.ModalContentContainer   >
        <View style={{flex: 1, justifyContent: 'space-around'}}>
          <Layout.ModalContentTitleText>
            {props.title}
          </Layout.ModalContentTitleText>
          <Layout.ModalContentTitleText style={{
            fontSize: APP_DIMENSIONS.SCREEN_WIDTH*0.038,
            letterSpacing: APP_DIMENSIONS.SCREEN_WIDTH*0.0016 }}>
            {props.contentText}
          </Layout.ModalContentTitleText>
          {props.hidePicker ? null : <Layout.ShoppingListPicker />}

        </View>
      </Layout.ModalContentContainer>
    </Layout.ModalWrapper>
  );
};

export const ThreeButtonModal = (props) => {
  const {t} = props;
  return (
    <Layout.ModalWrapper
      modalVisible={props.modalVisible}
      showOk
      okText={props.okText}
      showCancel
      cancelText={props.cancelText}
      showOther
      otherOptionText={props.otherOptionText}
      onOk={props.onOk}
      onCancel={props.onCancel}
      onOtherOption={props.onOtherOption}
      containerStyle={{height: APP_DIMENSIONS.SCREEN_HEIGHT*0.342}}
      onPressOutside={props.onCancel}
    >
      <Layout.ModalContentContainer   >
        <View style={{flex: 1, justifyContent: 'space-around'}}>
          <Layout.ModalContentTitleText>
            {props.title}
          </Layout.ModalContentTitleText>
          <Layout.ModalContentTitleText style={{
            fontSize: APP_DIMENSIONS.SCREEN_WIDTH*0.038,
            letterSpacing: APP_DIMENSIONS.SCREEN_WIDTH*0.0016 }}>
            {props.contentText}
          </Layout.ModalContentTitleText>
          {props.hidePicker ? null : <Layout.ShoppingListPicker />}

        </View>
      </Layout.ModalContentContainer>
    </Layout.ModalWrapper>
  );
};

export const AddToShoppingListModal = (props) => {
  const {t} = props;
  return (
    <Layout.ModalWrapper
      modalVisible={props.modalVisible}
      showOk
      okText={t && t("common:add")}
      showCancel
      cancelText={t && t('common:close')}
      onOk={props.onOk}
      onCancel={props.onCancel}
      containerStyle={{height: APP_DIMENSIONS.SCREEN_HEIGHT*0.3}}
      onPressOutside={props.onCancel}
    >
      <Layout.ModalContentContainer>
        <View style={{flex: 1, justifyContent: 'space-around'}}>
          <Layout.ModalContentTitleText>
            {t && t('offers:modals.addToShoppingList.title')}
          </Layout.ModalContentTitleText>
          <Layout.ShoppingListPicker
           lists={props.lists}
           onValueChange={props.onValueChange}
           selectedValue={props.selectedValue}
          />
        </View>
      </Layout.ModalContentContainer>
    </Layout.ModalWrapper>
  );
};

export const SelectStoreModal = (props) => {
  const {t} = props;
  return (
    <Layout.ModalWrapper
      modalVisible={props.modalVisible}
      showOk
      okText={t && t("stores:setStoreModal.okButton")}
      onOk={props.onOk}
      containerStyle={{height: APP_DIMENSIONS.SCREEN_HEIGHT*0.33*(APP_DIMENSIONS.SCREEN_HEIGHT_SIZE_CORRECTION)}}
      onPressOutside={props.onPressOutside}
    >
      <Layout.ModalContentContainer>
        <View style={{flex: 1, justifyContent: 'space-around'}}>
          <Layout.ModalContentTitleText>
            {t && t('stores:setStoreModal.title')}
          </Layout.ModalContentTitleText>
          <Layout.StorePicker
            t={t}
            lists={props.lists}
            onValueChange={props.onValueChange}
            selectedValue={props.selectedValue}
            storeListLoading={props.storeListLoading}
            language={props.language}
          />
        </View>
      </Layout.ModalContentContainer>
    </Layout.ModalWrapper>
  );
};

export const SelectProductQuantityModal = (props) => {
  const {t} = props;
  return (
    <Layout.ModalWrapper
      modalVisible={props.modalVisible}
      showOk
      okText={t && t("common:add")}
      showCancel
      cancelText={t && t('common:close')}
      onOk={props.onOk}
      onCancel={props.onCancel}
      containerStyle={{height: APP_DIMENSIONS.SCREEN_HEIGHT*0.3}}
      onPressOutside={props.onCancel}
    >
      <Layout.ModalContentContainer>
        <View style={{flex: 1, justifyContent: 'space-around'}}>
          <Layout.ModalContentTitleText>
            {t && t("shoppingLists:shoppingLists.quantitySelect")}
          </Layout.ModalContentTitleText>
          <View style={{flex: 1, flexDirection: 'row', justifyContent: 'center', alignItems: 'center'}}>
            <View style={{ flex: 0.5 }}>
              <Layout.FontText style ={{fontSize: SCREEN_WIDTH*0.041, fontFamily: 'open-sans'}}>
                {t && t("shoppingLists:shoppingLists.quantity") + ": "}
              </Layout.FontText>

            </View>

            <View style={{ flex: 0.5, flexDirection: 'row' }}>
              <Layout.TouchableContainer onPress={props.onMinusButtonPress}>
                <Icon
                  name='minus-circle'
                  type='font-awesome'
                  containerStyle={{
                    marginRight: 3
                  }}
                />
              </Layout.TouchableContainer>
              <Layout.FormTextInput
                style={{ backgroundColor: COLORS.GREY.LIGHT_GREY, width: SCREEN_WIDTH*0.14, textAlign: 'center' }}
                onChangeText={props.onChangeText}
                value={props.value}
                keyboardType={Platform.OS==='ios' ? "decimal-pad" : 'numeric'}
              />
              <Layout.TouchableContainer onPress={props.onPlusButtonPress}>
                <Icon
                  name='plus-circle'
                  type='font-awesome'
                  containerStyle={{
                    marginLeft: 3
                  }}
                />
              </Layout.TouchableContainer>
            </View>
          </View>
        </View>
      </Layout.ModalContentContainer>
    </Layout.ModalWrapper>
  );
};

export const CreateShoppingListModal = (props) => {
  const {t} = props;
  return (
    <Layout.ModalWrapper
      modalVisible={props.modalVisible}
      showOk
      okText={t && t("common:add")}
      showCancel
      cancelText={t && t('common:close')}
      onOk={props.onOk}
      onCancel={props.onCancel}
      containerStyle={{height: APP_DIMENSIONS.SCREEN_HEIGHT*0.3}}
      onPressOutside={props.onCancel}
    >
      <Layout.ModalContentContainer>
        <View style={{ flex: 1, justifyContent: 'space-around' }}>
          <Layout.ModalContentTitleText>
            {t && t('shoppingLists:createListModal.title')}
          </Layout.ModalContentTitleText>
          <Layout.FormTextInput placeholder={t && t('shoppingLists:createListModal.inputPlaceholder')} textAlign={'center'} value={props.nomeLista} onChangeText={props.onChangeText}/>
        </View>
      </Layout.ModalContentContainer>
    </Layout.ModalWrapper>
  );
};

export const EditShoppingListModal = (props) => {
  const {t} = props;
  return (
    <Layout.ModalWrapper
      modalVisible={props.modalVisible}
      showOk
      okText={t && t("common:save")}
      showCancel
      cancelText={t && t('common:close')}
      onOk={props.onOk}
      onCancel={props.onCancel}
      showOther={props.showOther}
      otherOptionText={props.otherOptionText}
      onOtherOption={props.onOtherOption}
      containerStyle={{height: APP_DIMENSIONS.SCREEN_HEIGHT*0.3}}
      onPressOutside={props.onCancel}
    >
      <Layout.ModalContentContainer>
        <View style={{ flex: 1, justifyContent: 'space-around' }}>
          <Layout.ModalContentTitleText>
            {t && t('shoppingLists:editListModal.title')}
          </Layout.ModalContentTitleText>
          <Layout.FormTextInput placeholder={t && t('shoppingLists:editListModal.inputPlaceholder')} textAlign={'center'} value={props.nomeLista} onChangeText={props.onChangeText}/>
        </View>
      </Layout.ModalContentContainer>
    </Layout.ModalWrapper>
  );
};

export const ConfirmDeleteProductModal = (props) => {
  const {t} = props;
  return (
    <Layout.ModalWrapper
      modalVisible={props.modalVisible}
      showOk
      okText={t && t("common:add")}
      showCancel
      cancelText={t && t('common:close')}
      onOk={props.onOk}
      onCancel={props.onCancel}
      containerStyle={{height: APP_DIMENSIONS.SCREEN_HEIGHT*0.3}}
      onPressOutside={props.onCancel}
    >
      <Layout.ModalContentContainer>
        <View style={{ flex: 1, justifyContent: 'space-around' }}>
          <Layout.ModalContentTitleText>
            {t && t('shoppingLists:editListModal.title')}
          </Layout.ModalContentTitleText>
          <Layout.FormTextInput placeholder={t && t('shoppingLists:editListModal.inputPlaceholder')} textAlign={'center'} value={props.nomeLista} onChangeText={props.onChangeText}/>
        </View>
      </Layout.ModalContentContainer>
    </Layout.ModalWrapper>
  );
};

export const ManualInquiryModal = (props) => {
  const {t} = props;
  return (
    <Layout.ModalWrapper
      modalVisible={props.modalVisible}
      showOk
      okText={t('priceInquiry:barCodeInquiry.modal.search')}
      showCancel
      cancelText={t && t('common:close')}
      onOk={props.onOk}
      onCancel={props.onCancel}
      containerStyle={{height: APP_DIMENSIONS.SCREEN_HEIGHT*APP_DIMENSIONS.SCREEN_HEIGHT_SIZE_CORRECTION*0.3}}
      onPressOutside={props.onCancel}
    >
      <Layout.ModalContentContainer>
        <View style={{ flex: 1, justifyContent: 'space-around' }}>
          <Layout.ModalContentTitleText style={{
            fontSize: APP_DIMENSIONS.SCREEN_WIDTH*0.044,
            letterSpacing: APP_DIMENSIONS.SCREEN_WIDTH*0.0016 }}>
            {t && t('priceInquiry:barCodeInquiry.modal.manualInquiryText')}
          </Layout.ModalContentTitleText>
          <Layout.FormTextInput
            placeholder={t && t('priceInquiry:barCodeInquiry.modal.placeholder')}
            textAlign={'center'} value={props.inputValue}
            onChangeText={props.onChangeText}
            keyboardType={Platform.OS==='ios' ? "decimal-pad" : 'numeric'}
            returnKeyType='done'
          />
        </View>
      </Layout.ModalContentContainer>
    </Layout.ModalWrapper>
  );
};
