import React from "react";
import { View, Picker, Platform, ActivityIndicator, Text } from "react-native";
import { Icon, CheckBox } from "react-native-elements";
import APP_DIMENSIONS from "../../../constants/appDimensions";
import { COLORS } from "../../../constants/styles";
import * as Layout from "../common";
import RNPickerSelect from 'react-native-picker-select';
import { FontText } from "./text";
import { TouchableContainer } from "./touchable";
import { connect } from "react-redux";

export const ShoppingListPicker = props => {
  const lists = props.lists.filter(item => item.deleted === '0').map(item =>
    ({ label: item.name, value: item, key: item.uuid })
  )
  return (
    <View style={{
      borderBottomColor: COLORS.GREY.LIGHT_GREY,
      borderBottomWidth: 1,
    }}>
      <RNPickerSelect
        onValueChange={props.onValueChange}
        items={lists}
        hideIcon
        value={props.selectedValue}
        placeholder={{ label: 'Selecione uma lista', value: null }}
      />
    </View>
  )
};

export const StorePicker = props => {
  let lists = [];
  if (props.language === "pt-BR") {
    lists = props.lists.map(item =>
      ({ label: item.name.toUpperCase(), value: item, key: item.store_no })
    )
  } else if (props.language === "es-CO") {
    lists = props.lists.map(item =>
      ({ label: item.name.toUpperCase(), value: item, key: item.store_no }) // ORIGINAL_COLOMBIA: locationname idlocation
    )
  }
  lists = lists.sort((a, b) => a.label.localeCompare(b.label));
  const { t } = props;
  if (props.storeListLoading)
    return (
      <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
        <ActivityIndicator />
        <FontText style={{ fontFamily: "open-sans", textAlign: "center", fontSize: APP_DIMENSIONS.SCREEN_WIDTH * 0.033 }}> {t && t("stores:setStoreModal.loadingStores")} </FontText>
      </View>
    )
  return (
    <RNPickerSelect
      onValueChange={props.onValueChange}
      textInputProps={{ fontSize: APP_DIMENSIONS.SCREEN_WIDTH * APP_DIMENSIONS.SCREEN_WIDTH_SIZE_CORRECTION * 0.045 }}
      items={lists}
      hideIcon
      value={props.selectedValue}
      placeholder={{ label: t && t('stores:setStoreModal.placeholder'), value: null }}
      style={{
        inputIOSContainer: {
          borderBottomColor: COLORS.GREY.DARK_GRAY,
          borderBottomWidth: 1,
        }
      }}
    />
  )
};


export class DocumentTypePickerComponent extends React.Component {
  state = {
    items: this.props.items,
    selectedIndex: 0,
    selectedValue: this.props.items[0].value,
    modalVisible: false
  }

  onValueChange = (value, index) => () => {
    this.props.onValueChange(value);
    this.setState({selectedIndex: index, selectedValue: value}, ()=>console.log(this.state));
    this.hideModal();
  } 
  showModal = () => this.setState({modalVisible: true});
  hideModal = () => this.setState({modalVisible: false});

  render() {
    const { t } = this.props;
    const selectedItem = this.props.items.find(item => item.value === this.props.value);
    const label = selectedItem ? selectedItem.label : "";

    if (this.props.storeListLoading)
      return (
        <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
          <ActivityIndicator />
        </View>
      )
    return (
      <View>
        <Layout.ModalWrapper
          modalVisible={this.state.modalVisible}
          hideBottomTab
          onPressOutside={this.hideModal}
          containerStyle={{
            alignItems: 'stretch',
            paddingVertical: APP_DIMENSIONS.SCREEN_HEIGHT*0.02*APP_DIMENSIONS.SCREEN_HEIGHT_SIZE_CORRECTION,
            paddingHorizontal: APP_DIMENSIONS.SCREEN_WIDTH*0.05*APP_DIMENSIONS.SCREEN_WIDTH_SIZE_CORRECTION,
            flex: 0.2
          }}
        >
          <View style={{ flex: 0.2, flexDirection: 'row', justifyContent: 'flex-start', alignSelf: 'stretch', alignItems: 'center' }}>
            <FontText style={{
              fontSize: APP_DIMENSIONS.SCREEN_WIDTH * 0.05,
              fontFamily: "open-sans-bold", textAlign: 'left'
            }}>{t("signUp:cpfInput.documentTypePlaceHolder")}</FontText>
          </View>
          <View style={{ flex: 0.8,  justifyContent: 'center', alignSelf: 'stretch', alignItems: 'center' }}>
            { 
              this.state.items.map((item, index) => 
                <CheckBox
                  key = {index}
                  title={`${item.label}`}
                  checked={index===this.state.selectedIndex}
                  checkedIcon='dot-circle-o'
                  uncheckedIcon='circle-o'
                  checkedColor={COLORS.RED.FIRE_ENGINE_RED}
                  uncheckedColor={COLORS.GREY.DARK_GRAY}
                  size = {APP_DIMENSIONS.SCREEN_WIDTH*0.07*APP_DIMENSIONS.SCREEN_WIDTH_SIZE_CORRECTION}
                  containerStyle={{ 
                    backgroundColor: COLORS.WHITE.WHITE,
                    borderWidth: 0,
                    alignSelf: 'stretch',
                    paddingVertical: APP_DIMENSIONS.SCREEN_HEIGHT*0.02*APP_DIMENSIONS.SCREEN_HEIGHT_SIZE_CORRECTION
                  }}
                  onPress={this.onValueChange(item.value, index)}
                  textStyle={{
                    fontFamily: this.props.fontLoaded ? 'open-sans' : null,
                    fontSize: APP_DIMENSIONS.SCREEN_WIDTH*0.04*APP_DIMENSIONS.SCREEN_WIDTH_SIZE_CORRECTION
                  }}
                />
              )
            }
          </View>
        </Layout.ModalWrapper>
        <View style={{ paddingVertical: APP_DIMENSIONS.SCREEN_HEIGHT * 0.006 }}>
          <FontText style={{
            fontSize: APP_DIMENSIONS.SCREEN_WIDTH * 0.035,
            fontFamily: "open-sans-bold",
          }}>{t("signUp:cpfInput.documentTypePlaceHolder")}</FontText>
        </View>
        <TouchableContainer opacity style={{flex : null}} onPress={this.showModal}>
          <View
            style={{
              borderWidth: 1, borderColor: COLORS.GREY.DARK_GRAY,
              flexDirection: 'row', justifyContent: 'space-between',
              height: APP_DIMENSIONS.SCREEN_HEIGHT * 0.053, alignItems: "center",
              paddingRight: APP_DIMENSIONS.SCREEN_WIDTH * 0.03,
              paddingLeft: APP_DIMENSIONS.SCREEN_WIDTH * 0.015,
            }}
          >
            <FontText style={{ fontSize: APP_DIMENSIONS.SCREEN_WIDTH * 0.035 }}> {this.state.items[this.state.selectedIndex].label}</FontText>
            <Icon name="chevron-down" type="font-awesome" size={APP_DIMENSIONS.SCREEN_WIDTH * 0.05}
            />
          </View>
        </TouchableContainer>
      </View>

    )
  }
};

const mapStateToProps = state => ({
  fontLoaded: state.fontLoader.fontLoaded
});

export const DocumentTypePicker = connect(
  mapStateToProps,
  null
)(DocumentTypePickerComponent);
