import React from "react";
import { Text, View, Modal, ActivityIndicator } from "react-native";
import { Icon } from "react-native-elements";
import { COLORS } from "../../../constants/styles";
import styled from "styled-components/native";
import * as Layout from "./"
import { withNavigationFocus } from 'react-navigation';

export const LoaderComponent = props => {
  const { loading, ...attributes } = props;

  return ( props.isFocused ?
    <Modal
      transparent={true}
      animationType={"none"}
      visible={loading}
      onRequestClose={() => {
        console.log("close modal");
      }}
    >
      <Layout.ModalBackground>
        <Layout.ModalContainer style={{ height: 100, width: 100, minHeight: 100 }}>
          <ActivityIndicator animating={loading} />
        </Layout.ModalContainer>
      </Layout.ModalBackground>
    </Modal> : null
  );
};

export const Loader = withNavigationFocus(LoaderComponent);

export const ViewLoader = props => {
  const { loading, ...attributes } = props;

  return (
    <View style={{position: 'absolute', height: loading ? null : 0, top: '50%', left: '50%'}}>
      <View><ActivityIndicator animating={loading} /></View>
    </View>
  );
};
