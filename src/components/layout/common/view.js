import React from "react";
import {
  View,
  SafeAreaView,
  KeyboardAvoidingView
} from "react-native";
import styled from "styled-components/native";
import APP_DIMENSIONS from "../../../constants/appDimensions";
import { COLORS } from "../../../constants/styles";

export const WhiteSafeAreaView = styled.SafeAreaView`
  backgroundColor: ${COLORS.WHITE.WHITE};
`;

export const ViewContainer = styled(WhiteSafeAreaView)`
  flex: 1;
`;

export const KeyboardAvoidingContainer = styled.KeyboardAvoidingView`
  flex: 1;
  backgroundColor: transparent;
  padding: ${APP_DIMENSIONS.SCREEN_WIDTH*0.061}px;
  justifyContent: center;
  alignItems: center;
`;

export const ContentContainer = styled.View`
  flex: 1;
  backgroundColor: ${COLORS.WHITE.WHITE};
  padding: ${APP_DIMENSIONS.SCREEN_WIDTH*0.061}px;
  justifyContent: center;
  alignItems: center;
`;
