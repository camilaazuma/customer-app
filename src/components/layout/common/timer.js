import React from "react";
import { Text } from "react-native"

export default class Timer extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            active: true,
            timeLeft: this.props.seconds || 0
        }
    }

    componentDidMount() {
        this.start();
    }

    componentWillUnmount() {
        clearInterval(this.timerID);
    }

    componentDidUpdate(prevProps) {
        if (prevProps.seconds !== this.props.seconds && this.state.active===false) {
            this.setState({timeLeft: this.props.seconds});
            this.start();
        }
    }

    start = () => {
        this.setState({
            active: true
        })
        this.timerID = setInterval(this.decrement, 1000)
    }

    decrement = () => {
        if (this.state.timeLeft!==0)
            this.setState({
                timeLeft: this.state.timeLeft - 1
            })
        else {
            this.setState({ active: false })
            if (this.props.onTimeIsOver) {
                this.props.onTimeIsOver();
            }
        }
    }

    convertSecondsToMMSS = (seconds) => {
        let sec_num = parseInt(seconds, 10);
        let minutes = Math.floor(sec_num/60);
        let secs = sec_num - (minutes*60);
        if (minutes < 10) {minutes = "0"+minutes;}
        if (secs < 10) {secs = "0"+secs;}
        return minutes+':'+secs;
    }  

    render () {
        return (
            <Text> {this.convertSecondsToMMSS(this.state.timeLeft)} </Text>
        )
    }
}