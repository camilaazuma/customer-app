import React from "react";
import {
  View,
} from "react-native";
import { COLORS } from "../../../constants/styles";
import APP_DIMENSIONS from "../../../constants/appDimensions";
import * as Layout from "../../layout";
import { titleCase, capitalizeFirstLetter, getSubtraction, formatStringForPrice } from "../../../utils";
import { language } from "../../../config/i18n";

// @translate(['priceInquiry', 'common'], { wait: true })
export class PriceLabel extends React.Component {
  constructor(props) {
    super(props)
  }

  productTypes = {
    PACK_VIRTUAL: "packvirtual",
    NORMAL: "normal",
    EMBALAGEM: "embalagem",
    MEDIDA: "medida"
  }

  getSellUnit = (index, unitPrice) => {
    const { sell_unit } = this.props.product;
    const { t } = this.props
    if(sell_unit===null) return "";
    if(!sell_unit.includes("_")) return sell_unit;
    const unitArray = sell_unit.split("_");
    if (unitPrice) {
      const unit = unitArray[0];
      if(unit==="quilo") return (t && t('priceInquiry:productPriceDetail.sellKiloUnit', {unit: unit}))
      else return (t && t('priceInquiry:productPriceDetail.sellUnit', {unit: unit}))
    }
    else
      return unitArray[index];
  }

  hasPromoText = () => {
    const { promo_text } = this.props.product;
    return (promo_text !== null && !(promo_text.trim()===""))
  }

  getPromoText = () => {
    const { promo_text } = this.props.product;
    const { t } = this.props;
    let p_text = this.hasPromoText() ? promo_text : t('priceInquiry:productPriceDetail.prodTagOfferPromoText');
    let discXVol =  t('priceInquiry:productPriceDetail.prodTagOfferDiscXVolShort');
    if (p_text.startsWith(discXVol)) {
      p_text = p_text.replace(discXVol, t('priceInquiry:productPriceDetail.prodTagOfferDiscXVol'));
    }
    return capitalizeFirstLetter(p_text);
  }

  render(){
    const productTypes = this.productTypes;
    const { product, t } = this.props;

    if (product && product.descr)
      switch(this.props.product.display) {
        case productTypes.PACK_VIRTUAL:
          return (
            <PackVirtualLabel
              product={product}
              getPromoText={this.getPromoText}
              getSellUnit={this.getSellUnit}
              t={t}
            />
          )
        case productTypes.NORMAL:
          return (
            <NormalLabel
              product={product}
              getPromoText={this.getPromoText}
              getSellUnit={this.getSellUnit}
              t={t}
            />
          )
        case productTypes.EMBALAGEM:
          return (
            <EmbalagemLabel
              product={product}
              getPromoText={this.getPromoText}
              getSellUnit={this.getSellUnit}
              t={t}
            />
          )
        case productTypes.MEDIDA:
          return (
            <MedidaLabel
              product={product}
              getPromoText={this.getPromoText}
              getSellUnit={this.getSellUnit}
              t={t}
            />
          )
        default:
          return (
            <NormalLabel
              product={product}
              getPromoText={this.getPromoText}
              getSellUnit={this.getSellUnit}
              t={t}
            />
          )
      }
    return null;
  }

}

export const PackVirtualLabel = (props) => {
  const { t, product } = props;
  return(
    <View style={{ flex: 1, backgroundColor: COLORS.YELLOW.PARIS_DAISY, }}>
      <View style={{
        flex: 0.25,
        borderTopColor: COLORS.GREY.LIGHT_GREY,
        borderTopWidth: 1,
        paddingHorizontal: APP_DIMENSIONS.SCREEN_WIDTH*0.05,
        paddingVertical: APP_DIMENSIONS.SCREEN_WIDTH*0.01
      }} >
        <Layout.ItemTitle>{titleCase(product.descr)}</Layout.ItemTitle>
        <Layout.ItemSubTitle>{product.art_no} | { product.instock === "1" ? t('shoppingLists:shoppingLists.inStock') : t('shoppingLists:shoppingLists.outOfStock') }</Layout.ItemSubTitle>
      </View>

      <View
        style={{
          flex: 0.75,
          flexDirection: "row",
          borderTopColor: COLORS.GREY.LIGHT_GREY,
          borderTopWidth: 1,
        }}
      >
        <View style={{ flex: 1, justifyContent: 'space-between', alignItems: 'center' }} >
          <Layout.ItemSubTitle style={{fontFamily: 'open-sans'}}>{props.getPromoText()}</Layout.ItemSubTitle>
          <Layout.ItemSubTitle style={{fontSize: APP_DIMENSIONS.SCREEN_WIDTH*0.073, }}>{product.iart_threshold}</Layout.ItemSubTitle>
          <Layout.ItemSubTitle style={{fontFamily: 'open-sans'}} numberOfLines={2}>{props.getSellUnit(1, false) + "\n(ATACADO)"}</Layout.ItemSubTitle>
        </View>
        <View style={{ flex: 1, backgroundColor: "black", alignItems: 'center' }} >
          <Layout.ItemSubTitle numberOfLines={2} style={{color:COLORS.YELLOW.PARIS_DAISY, fontFamily: 'open-sans', textAlign: 'center'}}>
            {t('priceInquiry:productPriceDetail.price', {price: props.getSellUnit(0, false)})}
          </Layout.ItemSubTitle>
          <Layout.ItemSubTitle style={{color:COLORS.YELLOW.PARIS_DAISY, fontSize: APP_DIMENSIONS.SCREEN_WIDTH*0.073}}>
            {t('priceInquiry:productPriceDetail.price', {price: formatStringForPrice(product.pr2)})}
          </Layout.ItemSubTitle>
          <Layout.ItemSubTitle>{''}</Layout.ItemSubTitle>
        </View>
        <View style={{ flex: 1,  }}>
          <View style={{ flex: 1, justifyContent: 'space-between', alignItems: 'center'}} >
            <Layout.ItemSubTitle style={{fontFamily: 'open-sans', textAlign: 'center',}} numberOfLines={2}>{props.getSellUnit(0, true)}</Layout.ItemSubTitle>
            <Layout.ItemSubTitle style={{fontSize: APP_DIMENSIONS.SCREEN_WIDTH*0.073}}>
              {t('priceInquiry:productPriceDetail.price', {price: formatStringForPrice(product.pr1)})}
            </Layout.ItemSubTitle>
          </View>
          <View style={{ flex: 1, borderTopColor: COLORS.GREY.LIGHT_GREY, borderTopWidth: 1, justifyContent: 'space-between', alignItems: 'center'}} >
            <Layout.ItemSubTitle style={{fontFamily: 'open-sans', textAlign: 'center', fontSize: APP_DIMENSIONS.SCREEN_HEIGHT * 0.015}} numberOfLines={3}>
              {t && t('priceInquiry:productPriceDetail.wholesaleSaving', {unit:props.getSellUnit(0, false)})}
            </Layout.ItemSubTitle>
            <Layout.ItemSubTitle style={{fontSize: APP_DIMENSIONS.SCREEN_WIDTH*0.073}}>
              {t('priceInquiry:productPriceDetail.price', {price: formatStringForPrice(getSubtraction(product.pr1, product.pr2).toString())}) }
            </Layout.ItemSubTitle>
          </View>
        </View>
      </View>
    </View>
  )
}

export const NormalLabel = (props) => {
  const { t, product } = props;
  return(
    <View style={{ flex: 1, backgroundColor: COLORS.YELLOW.PARIS_DAISY, }}>
      <View style={{
        flex: 0.25,
        borderTopColor: COLORS.GREY.LIGHT_GREY,
        borderTopWidth: 1,
        paddingHorizontal: APP_DIMENSIONS.SCREEN_WIDTH*0.05,
        paddingVertical: APP_DIMENSIONS.SCREEN_WIDTH*0.01
      }} >
        <Layout.ItemTitle>{titleCase(product.descr)}</Layout.ItemTitle>
        <Layout.ItemSubTitle>{product.art_no} | { product.instock === "1" ? t('shoppingLists:shoppingLists.inStock') : t('shoppingLists:shoppingLists.outOfStock') }</Layout.ItemSubTitle>
      </View>

      <View
        style={{
          flex: 0.75,
          flexDirection: "row",
          borderTopColor: COLORS.GREY.LIGHT_GREY,
          borderTopWidth: 1,
        }}
      >
        <View style={{ flex: 1, justifyContent: 'space-between', alignItems: 'center', borderRightColor: COLORS.GREY.LIGHT_GREY, borderRightWidth: 1, }} >
          <Layout.ItemSubTitle style={{fontFamily: 'open-sans'}}>
            { product.sell_unit.toUpperCase() === "QUILO" && (t('priceInquiry:productPriceDetail.pricePer') + " ")}  {capitalizeFirstLetter(product.sell_unit) }
          </Layout.ItemSubTitle>
          <Layout.ItemSubTitle style={{fontSize: APP_DIMENSIONS.SCREEN_WIDTH*0.073, }}>
            {t('priceInquiry:productPriceDetail.price', {price: formatStringForPrice(product.pr1)})}
          </Layout.ItemSubTitle>
        </View>
        <View style={{ flex: 1,  }}>
          <View style={{ flex: 1, justifyContent: 'space-between', alignItems: 'center'}} >
            { (props.getPromoText() !== null && props.getPromoText().toUpperCase() === "OPORTUNIDADE") ?
              <Layout.ProductImage style={{ flex: 1, width: null, height: null, backgroundColor: COLORS.YELLOW.PARIS_DAISY }} source={require("../../../../assets/signopesos.png")}/>
              :
              <Layout.ItemSubTitle style={{fontFamily: 'open-sans', textAlign: 'center'}}>
                {props.getPromoText().toUpperCase() ==="EM OFERTA" ? "" : props.getPromoText()}
              </Layout.ItemSubTitle>
            }
          </View>
          <View style={{ flex: 1, borderTopColor: COLORS.GREY.LIGHT_GREY, borderTopWidth: 1, justifyContent: 'space-between', alignItems: 'center'}} >
            <Layout.ItemSubTitle style={{fontFamily: 'open-sans', textAlign: 'center'}}>
              {product.sell_unit.toUpperCase() === "QUILO" && "Makro"}
            </Layout.ItemSubTitle>
          </View>
        </View>
      </View>
    </View>
  )
}


export const EmbalagemLabel = (props) => {
  const { t, product } = props;
  return(
    <View style={{ flex: 1, backgroundColor: COLORS.YELLOW.PARIS_DAISY, }}>
      <View style={{
        flex: 0.25,
        borderTopColor: COLORS.GREY.LIGHT_GREY,
        borderTopWidth: 1,
        paddingHorizontal: APP_DIMENSIONS.SCREEN_WIDTH*0.05,
        paddingVertical: APP_DIMENSIONS.SCREEN_WIDTH*0.01
      }} >
        <Layout.ItemTitle>{titleCase(product.descr)}</Layout.ItemTitle>
        <Layout.ItemSubTitle>{product.art_no} | { product.instock === "1" ? t('shoppingLists:shoppingLists.inStock') : t('shoppingLists:shoppingLists.outOfStock') }</Layout.ItemSubTitle>
      </View>

      <View
        style={{
          flex: 0.75,
          flexDirection: "row",
          borderTopColor: COLORS.GREY.LIGHT_GREY,
          borderTopWidth: 1,
        }}
      >
        <View style={{ flex: 1, justifyContent: 'space-between', alignItems: 'center', borderRightColor: COLORS.GREY.LIGHT_GREY, borderRightWidth: 1, }} >
          <Layout.ItemSubTitle style={{fontFamily: 'open-sans'}}>
            {t('priceInquiry:productPriceDetail.totalUnits', {unit: product.sell_unit, iartThreshold: product.iart_threshold})}
          </Layout.ItemSubTitle>
          <Layout.ItemSubTitle style={{fontSize: APP_DIMENSIONS.SCREEN_WIDTH*0.073, }}>
            {t('priceInquiry:productPriceDetail.price', {price: formatStringForPrice(product.pr1)})}
          </Layout.ItemSubTitle>
        </View>
        <View style={{ flex: 1,  }}>
          <View style={{ flex: 1, justifyContent: 'space-between', alignItems: 'center'}} >
            { (props.getPromoText() !== null && props.getPromoText().toUpperCase() === "OPORTUNIDADE") ?
              <Layout.ProductImage style={{ flex: 1, width: null, height: null, backgroundColor: COLORS.YELLOW.PARIS_DAISY }} source={require("../../../../assets/signopesos.png")}/>
              :
              <Layout.ItemSubTitle style={{fontFamily: 'open-sans', textAlign: 'center'}}>
                {props.getPromoText().toUpperCase() ==="EM OFERTA" ? "" : props.getPromoText()}
              </Layout.ItemSubTitle>
            }
          </View>
          <View style={{ flex: 1, borderTopColor: COLORS.GREY.LIGHT_GREY, borderTopWidth: 1, justifyContent: 'space-between', alignItems: 'center'}} >
            <Layout.ItemSubTitle style={{fontFamily: 'open-sans', textAlign: 'center'}} numberOfLines={2}>
              {t('priceInquiry:productPriceDetail.closedPackingUnit', {unit: product.sell_unit})}
            </Layout.ItemSubTitle>
            <Layout.ItemSubTitle style={{fontFamily: 'open-sans', textAlign: 'center', fontSize: APP_DIMENSIONS.SCREEN_WIDTH*0.073}} >
              {t('priceInquiry:productPriceDetail.price', {price: formatStringForPrice(product.prc1mea)})}
            </Layout.ItemSubTitle>
          </View>
        </View>
      </View>
    </View>
  )
}

export const MedidaLabel = (props) => {
  const { t, product } = props;
  return(
    <View style={{ flex: 1, backgroundColor: COLORS.YELLOW.PARIS_DAISY, }}>
      <View style={{
        flex: 0.25,
        borderTopColor: COLORS.GREY.LIGHT_GREY,
        borderTopWidth: 1,
        paddingHorizontal: APP_DIMENSIONS.SCREEN_WIDTH*0.05,
        paddingVertical: APP_DIMENSIONS.SCREEN_WIDTH*0.01
      }} >
        <Layout.ItemTitle>{titleCase(product.descr)}</Layout.ItemTitle>
        <Layout.ItemSubTitle>{product.art_no} | { product.instock === "1" ? t('shoppingLists:shoppingLists.inStock') : t('shoppingLists:shoppingLists.outOfStock') }</Layout.ItemSubTitle>
      </View>

      <View
        style={{
          flex: 0.75,
          flexDirection: "row",
          borderTopColor: COLORS.GREY.LIGHT_GREY,
          borderTopWidth: 1,
        }}
      >
        <View style={{ flex: 1, justifyContent: 'space-between', alignItems: 'center', borderRightColor: COLORS.GREY.LIGHT_GREY, borderRightWidth: 1, }} >
          <Layout.ItemSubTitle style={{fontFamily: 'open-sans'}}>
            { props.getSellUnit(0, false) + " C/ " + product.iart_threshold + props.getSellUnit(1, false) }
          </Layout.ItemSubTitle>
          <Layout.ItemSubTitle style={{fontSize: APP_DIMENSIONS.SCREEN_WIDTH*0.073, }}>
            {t('priceInquiry:productPriceDetail.price', {price: formatStringForPrice(product.pr1)})}
          </Layout.ItemSubTitle>
        </View>
        <View style={{ flex: 1,  }}>
          <View style={{ flex: 1, justifyContent: 'space-between', alignItems: 'center'}} >
            { (props.getPromoText() !== null && props.getPromoText().toUpperCase() === "OPORTUNIDADE") ?
              <Layout.ProductImage style={{ flex: 1, width: null, height: null, backgroundColor: COLORS.YELLOW.PARIS_DAISY }} source={require("../../../../assets/signopesos.png")}/>
              :
              <Layout.ItemSubTitle style={{fontFamily: 'open-sans', textAlign: 'center'}}>
                {props.getPromoText().toUpperCase() ==="EM OFERTA" ? "" : props.getPromoText()}
              </Layout.ItemSubTitle>
            }
          </View>
          <View style={{ flex: 1, borderTopColor: COLORS.GREY.LIGHT_GREY, borderTopWidth: 1, justifyContent: 'space-between', alignItems: 'center'}} >
            <Layout.ItemSubTitle style={{fontFamily: 'open-sans', textAlign: 'center'}} numberOfLines={2}>
              {t('priceInquiry:productPriceDetail.pricePer') + " " + props.getSellUnit(2, false) }
            </Layout.ItemSubTitle>
            <Layout.ItemSubTitle style={{fontFamily: 'open-sans', textAlign: 'center', fontSize: APP_DIMENSIONS.SCREEN_WIDTH*0.073}} >
              {t('priceInquiry:productPriceDetail.price', {price: formatStringForPrice(product.prc1mea)})}
            </Layout.ItemSubTitle>
          </View>
        </View>
      </View>
    </View>
  )
}

export class PriceLabel_CO extends React.Component {
  constructor(props) {
    super(props);
  }

  isPromo = () => {
    const {ProductOffer} = this.props.product;
    return (ProductOffer !== null && ProductOffer.length > 0)
  }

  getPromoText = () => {
    const { t } = this.props;
    let p_text = this.isPromo() ? this.props.product.ProductOffer[0].OfferDescription : t('priceInquiry:productPriceDetail.prodTagOfferPromoText');
    let discXVol =  t('priceInquiry:productPriceDetail.prodTagOfferDiscXVolShort');
    if (p_text.startsWith(discXVol)) {
      p_text = p_text.replace(discXVol, t('priceInquiry:productPriceDetail.prodTagOfferDiscXVol'));
    }
    return capitalizeFirstLetter(p_text);
  }

  hasWholesalePrice = () =>{
    const ProductPrice = this.props.product.ProductPrice[0];
    return (ProductPrice.WholesalePriceValue != "" && ProductPrice.WholesalePriceTypeText != "" && ProductPrice.WholesalePriceQtyValue != "")
  }

  render(){
    const { product, t } = this.props;

    return(
      <NormalLabel_CO
        product={product}
        getPromoText={this.getPromoText}
        isPromo={this.isPromo()}
        t={t}
      />
    );
  }

}

export const NormalLabel_CO = (props) => {
  const { product, t } = props;

  if (product && product.ProductName){
    return(
      <View style={{ flex: 1, backgroundColor: COLORS.YELLOW.PARIS_DAISY, }}>
        <View style={{
          flex: 0.25,
          borderTopColor: COLORS.GREY.LIGHT_GREY,
          borderTopWidth: 1,
          paddingHorizontal: APP_DIMENSIONS.SCREEN_WIDTH*0.05,
          paddingVertical: APP_DIMENSIONS.SCREEN_WIDTH*0.01
        }} >
          <Layout.ItemTitle>{titleCase(product.ProductName)}</Layout.ItemTitle>
          <Layout.ItemSubTitle>{product.ProductSKU} | { product.ProductStock[0].StockQuantityValue >= 1 ? t('shoppingLists:shoppingLists.inStock') : t('shoppingLists:shoppingLists.outOfStock') }</Layout.ItemSubTitle>
        </View>

        <View
          style={{
            flex: 0.75,
            flexDirection: "row",
            borderTopColor: COLORS.GREY.LIGHT_GREY,
            borderTopWidth: 1,
          }}
        >
          <View style={{ flex: 1, justifyContent: 'space-between', alignItems: 'center', borderRightColor: COLORS.GREY.LIGHT_GREY, borderRightWidth: 1, }} >
            <Layout.ItemSubTitle style={{fontFamily: 'open-sans'}} >
              {capitalizeFirstLetter(product.ProductSalesPomoPackingDescription) }
            </Layout.ItemSubTitle>
            <Layout.ItemSubTitle style={{fontSize: APP_DIMENSIONS.SCREEN_WIDTH*0.04, }}>
              {t('priceInquiry:productPriceDetail.price', {price: formatStringForPrice(props.isPromo? product.ProductOffer[0].OfferPriceValue : product.ProductPrice[0].UnitPriceValue, language)})}
            </Layout.ItemSubTitle>
          </View>
          <View style={{ flex: 1,  }}>
            <View style={{ flex: 1, justifyContent: 'space-between', alignItems: 'center'}} >
              { (props.getPromoText() !== null && props.getPromoText().toUpperCase() === "OPORTUNIDADE") ?
                <Layout.ProductImage style={{ flex: 1, width: null, height: null, backgroundColor: COLORS.YELLOW.PARIS_DAISY }} source={require("../../../../assets/signopesos.png")}/>
                :
                <Layout.ItemSubTitle style={{fontFamily: 'open-sans', textAlign: 'center'}} numberOfLines={2}>
                  {props.getPromoText().toUpperCase() ==="EN OFERTA" ? "" : props.getPromoText()}
                </Layout.ItemSubTitle>
              }
            </View>
            <View style={{ flex: 1, borderTopColor: COLORS.GREY.LIGHT_GREY, borderTopWidth: 1, justifyContent: 'space-between', alignItems: 'center'}} >
              <Layout.ItemSubTitle style={{fontFamily: 'open-sans', textAlign: 'center'}}>
              { product.ProductSalesPomoPackingDescription ? product.ProductSalesPomoPackingDescription.toUpperCase() === "QUILO" && "Makro" : "Makro"}
              </Layout.ItemSubTitle>
            </View>
          </View>
        </View>
      </View>
    );
  }

  return null;
}
export const PackVirtualLabel_CO = (props) => {
  const { t, product } = props;
  return(
    <View style={{ flex: 1, backgroundColor: COLORS.YELLOW.PARIS_DAISY, }}>
      <View style={{
        flex: 0.25,
        borderTopColor: COLORS.GREY.LIGHT_GREY,
        borderTopWidth: 1,
        paddingHorizontal: APP_DIMENSIONS.SCREEN_WIDTH*0.05,
        paddingVertical: APP_DIMENSIONS.SCREEN_WIDTH*0.01
      }} >
        <Layout.ItemTitle>{titleCase(product.descr)}</Layout.ItemTitle>
        <Layout.ItemSubTitle>{product.art_no} | { product.instock === "1" ? t('shoppingLists:shoppingLists.inStock') : t('shoppingLists:shoppingLists.outOfStock') }</Layout.ItemSubTitle>
      </View>

      <View
        style={{
          flex: 0.75,
          flexDirection: "row",
          borderTopColor: COLORS.GREY.LIGHT_GREY,
          borderTopWidth: 1,
        }}
      >
        <View style={{ flex: 1, justifyContent: 'space-between', alignItems: 'center' }} >
          <Layout.ItemSubTitle style={{fontFamily: 'open-sans'}}>{props.getPromoText()}</Layout.ItemSubTitle>
          <Layout.ItemSubTitle style={{fontSize: APP_DIMENSIONS.SCREEN_WIDTH*0.073, }}>{product.iart_threshold}</Layout.ItemSubTitle>
          <Layout.ItemSubTitle style={{fontFamily: 'open-sans'}} numberOfLines={2}>{props.getSellUnit(1, false) + "\n(ATACADO)"}</Layout.ItemSubTitle>
        </View>
        <View style={{ flex: 1, backgroundColor: "black", alignItems: 'center' }} >
          <Layout.ItemSubTitle numberOfLines={2} style={{color:COLORS.YELLOW.PARIS_DAISY, fontFamily: 'open-sans', textAlign: 'center'}}>
            {t('priceInquiry:productPriceDetail.price', {price: props.getSellUnit(0, false)})}
          </Layout.ItemSubTitle>
          <Layout.ItemSubTitle style={{color:COLORS.YELLOW.PARIS_DAISY, fontSize: APP_DIMENSIONS.SCREEN_WIDTH*0.073}}>
            {t('priceInquiry:productPriceDetail.price', {price: formatStringForPrice(product.pr2, language)})}
          </Layout.ItemSubTitle>
          <Layout.ItemSubTitle>{''}</Layout.ItemSubTitle>
        </View>
        <View style={{ flex: 1,  }}>
          <View style={{ flex: 1, justifyContent: 'space-between', alignItems: 'center'}} >
            <Layout.ItemSubTitle style={{fontFamily: 'open-sans', textAlign: 'center',}} numberOfLines={2}>{props.getSellUnit(0, true)}</Layout.ItemSubTitle>
            <Layout.ItemSubTitle style={{fontSize: APP_DIMENSIONS.SCREEN_WIDTH*0.073}}>
              {t('priceInquiry:productPriceDetail.price', {price: formatStringForPrice(product.pr1, language)})}
            </Layout.ItemSubTitle>
          </View>
          <View style={{ flex: 1, borderTopColor: COLORS.GREY.LIGHT_GREY, borderTopWidth: 1, justifyContent: 'space-between', alignItems: 'center'}} >
            <Layout.ItemSubTitle style={{fontFamily: 'open-sans', textAlign: 'center', fontSize: APP_DIMENSIONS.SCREEN_HEIGHT * 0.015}} numberOfLines={3}>
              {t && t('priceInquiry:productPriceDetail.wholesaleSaving', {unit:props.getSellUnit(0, false)})}
            </Layout.ItemSubTitle>
            <Layout.ItemSubTitle style={{fontSize: APP_DIMENSIONS.SCREEN_WIDTH*0.073}}>
              {t('priceInquiry:productPriceDetail.price', {price: formatStringForPrice(getSubtraction(product.pr1, product.pr2).toString(), language)}) }
            </Layout.ItemSubTitle>
          </View>
        </View>
      </View>
    </View>
  )
}
