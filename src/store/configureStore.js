import { createStore, applyMiddleware, compose } from "redux";
import { connect } from "react-redux";
import thunk from "redux-thunk";
import { createLogger } from "redux-logger";
import rootReducer from "../reducers/index";
import {
    reduxifyNavigator,
    createReactNavigationReduxMiddleware,
    createNavigationReducer,
  } from 'react-navigation-redux-helpers';
import { RootStack } from "../config/routes";

// const middleware = createReactNavigationReduxMiddleware(
//   "root",
//   state => state.nav
// );

// const App = reduxifyNavigator(RootStack, "root");

// const mapStateToProps = (state) => ({
//     state: state.nav,
// });

// const AppWithNavigationState = connect(mapStateToProps)(App);

export default function configureStore() {
    const logger = createLogger();
    const enhancer = compose(
        applyMiddleware (
            thunk, 
            // logger
        )
    );
    const store = createStore(
        rootReducer,
        undefined,
        enhancer
    );
    return store;
}