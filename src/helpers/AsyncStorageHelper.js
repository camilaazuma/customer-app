import { AsyncStorage } from "react-native";

export default {
  async setItem(key, value) {
    try {
      return await AsyncStorage.setItem(key, JSON.stringify(value));
    } catch (error) {
      // console.error('AsyncStorage#setItem error: ' + error.message);
    }
  },
  async getItem(key) {
    return await AsyncStorage.getItem(key).then(result => {
      if (result) {
        try {
          result = JSON.parse(result);
        } catch (e) {
          // console.error('AsyncStorage#getItem error deserializing JSON for key: ' + key, e.message);
        }
      }
      return result;
    });
  },
  async removeItem(key) {
    return await AsyncStorage.removeItem(key);
  }
};

export const KEYS = {
  AUTH_TOKEN_KEY: "AUTH_TOKEN",
  AUTH_SAP_TOKEN_KEY: "AUTH_SAP_TOKEN",
  SET_STORE: "SET_STORE", // current selected store
  STORE_LIST_KEY: "STORE_LIST" ,
  CAROUSEL_URL_ARRAY_KEY: "CAROUSEL_URL_ARRAY",
  PASSPORT_BARCODE_KEY: "PASSPORT_BARCODE",
  OFFER_LIST_KEY: "OFFER_LIST",
  USER_SCAN_COUNT_KEY: "USER_SCAN_COUNT",
  USER_SCAN_DATE_KEY: "USER_SCAN_DATE",
  LOGGED_IN_KEY: "LOGGED_IN",
  LOGGED_USER_DATA: "LOGGED_USER_DATA",
  TERMS_OF_USE_ACCEPTED_KEY: "TERMS_OF_USE_ACCEPTED",
  USER_LOCATION_PERMISSION_ACCEPTED_KEY: "USER_LOCATION_PERMISSION_ACCEPTED_KEY",
  SET_PREFERRED_STORE: "SET_PREFERRED_STORE"
}