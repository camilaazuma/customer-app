import { Platform, Alert } from "react-native";
import { Constants, Location, Permissions } from 'expo';
import geolib from "geolib";
import { timeoutPromise } from "../utils";
import AsyncStorageHelper from "./AsyncStorageHelper";

const USER_LOCATION_PERMISSION_ACCEPTED_KEY = "USER_LOCATION_PERMISSION_ACCEPTED_KEY";

export default {
  // location will not work on snack emulator
  checkIfAndroidSnackEmulator() {
    if (Platform.OS === "android" && !Constants.isDevice) {
      Alert.alert(
        "Erro",
        "Localização não disponível em emulador Snack. Utilize um dispositivo!",
        [{ text: "OK" }],
        { cancelable: true }
      );
      return true;
    } else {
      return false;
    }
  },
  async getLocationAsync (forceAsk) {
    if (!this.checkIfAndroidSnackEmulator()) {
      await this.askLocationPermission(forceAsk)
      let location = await timeoutPromise(15000, Location.getCurrentPositionAsync({enableHighAccuracy: true}));
      return location;
    }
  },

  async askLocationPermission(forceAsk) {
    if (!this.checkIfAndroidSnackEmulator()) {
      const permission = await AsyncStorageHelper.getItem(USER_LOCATION_PERMISSION_ACCEPTED_KEY);
      if (permission===false && !forceAsk) {
        return; 
      }
      let { status } = await Permissions.askAsync(Permissions.LOCATION);
      if (status !== 'granted') {
        await AsyncStorageHelper.setItem(USER_LOCATION_PERMISSION_ACCEPTED_KEY, false);
        return;
      }
      await AsyncStorageHelper.setItem(USER_LOCATION_PERMISSION_ACCEPTED_KEY, true);
    }
  },
  getDistance(latitudeA, longitudeA, latitudeB, longitudeB) {
    return (
      geolib.getDistance(
        {latitude: latitudeA, longitude: longitudeA},
        {latitude: latitudeB, longitude: longitudeB},
        100
      ) / 1000
    )
  }
};
