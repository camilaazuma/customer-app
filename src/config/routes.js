import React from "react";
import {
  createStackNavigator,
  createDrawerNavigator,
  createSwitchNavigator
} from "react-navigation";
import {
  TermsOfUseScreen,
  HomeScreen,
  HomeSignedInScreen,
  SignUp,
  Offers,
  Settings,
  ShoppingLists,
  MyPurchases,
  Stores,
  ContactUsScreen,
  AboutUsScreen,
  PriceInquiry,
  IDCard
} from "../components/screens";
import {
  genericNavigatorConfig,
  drawerNavigatorConfig,
  signedInDrawerNavigatorConfig,
  appNavigatorConfig,
  generateGenericNavigationOptions
} from "./routes.config";
import { NetInfo, Alert, AppState } from "react-native";
import { Font } from "expo";
import * as Actions from "../actions";
import { connect } from "react-redux";
import { I18nextProvider, translate } from "react-i18next";
import i18n, {language} from "./i18n";
import AsyncStorageHelper from "../helpers/AsyncStorageHelper";
import { TERMS_OF_USE_ACCEPTED_KEY } from "../components/views/TermsOfUseView";
import Snackbar from "react-native-snackbar";
import firebase from "react-native-firebase";
import { showSnackbar, getCustomerType } from "../utils";

const LOGGED_IN_KEY = "LOGGED_IN";
const LOGGED_USER_DATA = "LOGGED_USER_DATA";
const SET_STORE = "SET_STORE";
const SET_PREFERRED_STORE = "SET_PREFERRED_STORE";

const SignUpNavigator = createStackNavigator(
  {
    CpfInput: {
      screen: SignUp.CpfInputScreen
    },
    CreateNewPassword: {
      screen: SignUp.CreateNewPasswordScreen
    },
    UserDataInputFields: {
      screen: SignUp.UserDataInputFieldsScreen
    },
    Login: {
      screen: SignUp.LoginScreen
    },
    ConfirmationCode: {
      screen: SignUp.ConfirmationCodeScreen
    }
  },
  {
    ...genericNavigatorConfig,
    navigationOptions: generateGenericNavigationOptions({
      disableHomeNav: true
    })
  }
);

const ShoppingListsNavigator = createStackNavigator(
  {
    ShoppingLists: {
      screen: ShoppingLists.ShoppingListsScreen
    },
    ListDetail: {
      screen: ShoppingLists.ListDetailScreen
    },
    TotalPrice: {
      screen: ShoppingLists.TotalPriceScreen
    },
    AddProducts: {
      screen: ShoppingLists.AddProductsScreen
    }
  },
  {
    ...genericNavigatorConfig,
    navigationOptions: generateGenericNavigationOptions()
  }
);

const MyPurchasesNavigator = createStackNavigator(
  {
    MyPurchasesList: {
      screen: MyPurchases.MyPurchasesListScreen
    },
    PurchaseDetail: {
      screen: MyPurchases.PurchaseDetailScreen
    }
  },
  {
    ...genericNavigatorConfig,
    navigationOptions: generateGenericNavigationOptions()
  }
);

const IDCardNavigator = createStackNavigator(
  {
    IDCard: {
      screen: IDCard.IDCardScreen
    },
    Scan: {
      screen: IDCard.ScanScreen
    }
  },
  {
    ...genericNavigatorConfig,
    navigationOptions: generateGenericNavigationOptions()
  }
);

const PriceInquiryNavigator = createStackNavigator(
  {
    InquiryType: {
      screen: PriceInquiry.InquiryTypeScreen
    },
    ProductNameInquiry: {
      screen: PriceInquiry.ProductNameInquiryScreen
    },
    BarCodeInquiry: {
      screen: PriceInquiry.BarCodeInquiryScreen
    },
    ProductPriceDetail: {
      screen: PriceInquiry.ProductPriceDetailScreen
    }
  },
  {
    ...genericNavigatorConfig,
    navigationOptions: generateGenericNavigationOptions()
  }
);

const SettingsNavigator = createStackNavigator(
  {
    Settings: {
      screen: Settings.SettingsScreen
    },
    UserUpdateConfirmation: {
      screen: Settings.UserUpdateConfirmationScreen
    }
  },
  {
    ...genericNavigatorConfig,
    navigationOptions: generateGenericNavigationOptions()
  }
);

const DrawerMenuSignedInNavigator = createDrawerNavigator(
  {
    Home: {
      screen: HomeSignedInScreen
    },
    Offers: {
      screen: Offers.OffersListScreen
    },
    Stores: {
      screen: Stores.StoreListScreen
    },
    ShoppingLists: {
      screen: ShoppingListsNavigator
    },
    MyPurchases: {
      screen: MyPurchasesNavigator
    },
    ContactUs: {
      screen: ContactUsScreen
    },
    IDCard: {
      screen: IDCardNavigator
    },
    AboutUs: {
      screen: AboutUsScreen
    },
    Settings: {
      screen: SettingsNavigator
    },
    PriceInquiry: {
      screen: PriceInquiryNavigator
    }
  },
  signedInDrawerNavigatorConfig
);

const createTermsOfUseAlreadyAcceptedNavigator = function(alreadyLoggedIn = false) {
  return createSwitchNavigator(
    {
      SignUp: SignUpNavigator,
      HomeSignedIn: DrawerMenuSignedInNavigator,
    },
    {
      headerMode: "none",
      initialRouteName: alreadyLoggedIn ? "HomeSignedIn" : "SignUp"
    }
  );
}

const createTermsOfUseNavigator = function(alreadyLoggedIn = false, termsOfUseAlreadyAccepted = false) {
  return createSwitchNavigator(
    {
      TermsOfUse: { screen: TermsOfUseScreen },
      TermsOfUseAlreadyAccepted: createTermsOfUseAlreadyAcceptedNavigator(alreadyLoggedIn),
    },
    {
      initialRouteName: termsOfUseAlreadyAccepted
        ? "TermsOfUseAlreadyAccepted"
        : "TermsOfUse"
    }
  );
}

class RootStackComponent extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loggedIn: false,
      checkedLoggedIn: false,
      termsOfUseAccepted: false,
      checkedTermsOfUseAccepted: false,
    };
  }

  appState = AppState.currentState

  async componentDidMount() {
    await Font.loadAsync({
      "open-sans": require("../../assets/fonts/OpenSans-Regular.ttf"),
      "open-sans-bold": require("../../assets/fonts/OpenSans-Bold.ttf"),
      "open-sans-semibold": require("../../assets/fonts/OpenSans-SemiBold.ttf"),
      "open-sans-italic": require("../../assets/fonts/OpenSans-Italic.ttf"),
      "open-sans-semibold-italic": require("../../assets/fonts/OpenSans-SemiBoldItalic.ttf"),
      "helvetica-neue": require("../../assets/fonts/HelveticaNeueLTStd-Bd2.ttf")
    });
    this.props.fontLoader();
    this.props.getInstallationId();
    this.setState({
      termsOfUseAccepted: await this._termsOfUseAlreadyAccepted(),
      checkedTermsOfUseAccepted: true,
      loggedIn: await this._alreadyLoggedIn(),
      checkedLoggedIn: true
    });
    NetInfo.addEventListener(
      'connectionChange',
      this.handleConnectivityChange
    );
    AppState.addEventListener('change', this._handleAppStateChange);
  }

  componentWillUnmount(){
    AppState.removeEventListener('change', this._handleAppStateChange);
  }

  shouldComponentUpdate(nextProps, nextState) {
    if (
      this.props.currentStoreObject!==nextProps.currentStoreObject ||
      this.props.user!==nextProps.user
    )
      return false;
    return true;
  }

  _handleAppStateChange = (nextAppState) => {
    if (
      this.appState === 'active' &&
      nextAppState.match(/inactive|background/)
    ) {

      if (language==="pt-BR") {
        firebase.analytics().setUserProperty("customer_type_descr", getCustomerType(this.props.user.personalIdentifier));
      }
      if (this.props.currentStoreObject!==null && typeof this.props.currentStoreObject!=='undefined') {
        firebase.analytics().setUserProperty("active_store", this.props.currentStoreObject.store_no);  // ORIGINAL_COLOMBIA: idlocation
      }

    }
    this.appState = nextAppState;
  };

  handleConnectivityChange = (connectionInfo) => {
    if (connectionInfo.type==="none" || connectionInfo.type==="unknown") {
      showSnackbar(i18n.getFixedT()("common:errors.noConnection"));
    }
  }

  _termsOfUseAlreadyAccepted = async () =>
    await AsyncStorageHelper.getItem(TERMS_OF_USE_ACCEPTED_KEY);

  _alreadyLoggedIn = async () => {
    var logged = await AsyncStorageHelper.getItem(LOGGED_IN_KEY);
    if(logged) {
      var userData = await AsyncStorageHelper.getItem(LOGGED_USER_DATA);
      if(userData && userData.hasOwnProperty('passportNo')) {
        // usuário logado
        userInfo = {
          passportNo: userData.passportNo,
          passportRaw: userData.passportRaw,
          fullName: userData.fullName,
          email: userData.email,
          deviceId: userData.deviceId,
          personalIdentifier: userData.personalIdentifier,
          phone: userData.phone,
          cep: userData.cep,
          street: userData.street,
          addressNumber: userData.addressNumber,
          complement: userData.complement,
          city: userData.city,
          stateName: userData.stateName,
          contactUuid: userData.contactUuid,
        }
        this.props.setUserInfo(userInfo)
        if (language==="pt-BR") {
          firebase.analytics().setUserProperty("customer_type_descr", getCustomerType(userInfo.personalIdentifier));
        }
        var storeData = await AsyncStorageHelper.getItem(SET_STORE);
        var preferredStore = await AsyncStorageHelper.getItem(SET_PREFERRED_STORE);
        if(storeData && storeData.hasOwnProperty('name')) {
          this.props.setCurrentStore(storeData);
          firebase.analytics().setUserProperty("active_store", storeData.store_no); // idlocation
        }

        if(preferredStore) this.props.setPreferredStore(preferredStore);
        this.props.fetchPreferredStore(userInfo);
      }
    }
    return logged;
  }

  render() {
    const { termsOfUseAccepted, checkedTermsOfUseAccepted, loggedIn, checkedLoggedIn } = this.state;
    if (!checkedLoggedIn || !checkedTermsOfUseAccepted) {
      return null;
    }
    const AppNavigator = createTermsOfUseNavigator(loggedIn, termsOfUseAccepted);
    return <AppNavigator />;
  }
}

const mapStateToProps = state => ({
  currentStoreObject: state.currentStore.object,
  user: state.user
});

const mapDispatchToProps = dispatch => {
  return {
    fontLoader: () => {
      dispatch(Actions.setFontLoadedFlag());
    },
    getInstallationId: () => {
      dispatch(Actions.getInstallationId());
    },
    setUserInfo: ({passportNo, passportRaw, fullName, email, deviceId, personalIdentifier, phone, cep, street, addressNumber, complement, city, stateName, contactUuid}) => {
      dispatch(Actions.setUserInfo({passportNo, passportRaw, fullName, email, deviceId, personalIdentifier, phone, cep, street, addressNumber, complement, city, stateName, contactUuid}));
    },
    setCurrentStore: (store) => {
      dispatch(Actions.setCurrentStore(store))
    },
    fetchPreferredStore: (user) => {
      dispatch(Actions.fetchPreferredStore(user));
    },
    setPreferredStore: (store) => {
      dispatch(Actions.setPreferredStore(store))
    },
  };
};

const ConnectedRootStack = connect(
  mapStateToProps,
  mapDispatchToProps
)(RootStackComponent);

// Wrapping a stack with translation hoc asserts we trigger new render on language change
// the hoc is set to only trigger rerender on languageChanged
const I18nWrappedRootStack = () => {
  return <ConnectedRootStack screenProps={{ t: i18n.getFixedT() }} />;
};

const ReloadAppOnLanguageChange = translate("common", {
  bindI18n: "languageChanged",
  bindStore: false
})(I18nWrappedRootStack);

export default (RootStack = () => (
  <I18nextProvider i18n={i18n}>
    <ReloadAppOnLanguageChange />
  </I18nextProvider>
));
