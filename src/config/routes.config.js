import React from "react";
import { Platform } from "react-native";
import APP_DIMENSIONS from "../constants/appDimensions";
import * as Layout from "../components/layout";


export const genericNavigatorConfig = {
  mode: Platform.OS === "ios" ? "float" : "screen",
};

export const generateGenericNavigationOptions  = ({disableHomeNav}=false) => ({navigation}) => ({
  header: (
    <Layout.CustomHeader>
      <Layout.BackIcon onPress={()=>navigation.goBack(null)} />
      <Layout.HeaderLogo navigation={navigation} disableHomeNav={disableHomeNav} />
    </Layout.CustomHeader>
  )
})

export const signedInDrawerNavigatorConfig = {
  contentComponent: props => <Layout.DrawerMenu {...props}/>,
  drawerWidth: APP_DIMENSIONS.SCREEN_WIDTH,
};

export const appNavigatorConfig = {
  initialRouteName: "TermsOfUse" 
  // headerMode: "none"
};
