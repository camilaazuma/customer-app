import i18n from 'i18next';
import { DangerZone } from 'expo';
const { Localization } = DangerZone;
import VersionNumber from 'react-native-version-number';
import { getLanguage } from '../utils.js';

// creating a language detection plugin using expo
// http://i18next.com/docs/ownplugin/#languagedetector

export const fallback = "pt-BR";

export const language = getLanguage(VersionNumber.bundleIdentifier);
 
export const supportedLocales = {
    "pt-BR": {
        name: "Português-BR",
        translationFileLoader: () => require("../../assets/i18n/pt-BR/translation.json"),
 
        // en is default locale in Moment
        // momentLocaleLoader: () => Promise.resolve(),
    },
    "es-CO": {
        name: "Español-CO",
        translationFileLoader: () => require("../../assets/i18n/es-CO/translation.json"),
        // momentLocaleLoader: () => import('moment/locale/ar'),
    },
    "es-PE": {
        name: "Español-PE",
        translationFileLoader: () => require("../../assets/i18n/es-PE/translation.json"),
    },
};

export const defaultNamespace = "common";
 
export const namespaces = [
  "common",
  "drawer",
  "termsOfUse",
  "signUp",
  "homeSignedIn",
  "offers",
  "shoppingLists",
  "stores",
  "myPurchases",
  "contactUs",
  "aboutUs",
  "idCard",
  "settings",
  "priceInquiry"
];

const translationLoader = {
  type: 'backend',
  init: () => {},
  read: function(language, namespace, callback) {
      let resource, error = null;

      try {
          resource = supportedLocales[language]
              .translationFileLoader()[namespace];
      } catch (_error) { error = _error; }

      callback(error, resource);
  },
};


const languageDetector = {
  type: 'languageDetector',
  async: true, // async detection
  detect: (cb) => {
    return Localization.getCurrentLocaleAsync()
      .then(lng => { cb(lng); })
  },
  init: () => {},
  cacheUserLanguage: () => {}
}

i18n
  // .use(languageDetector)
  .use(translationLoader)
  .init({
    fallbackLng: fallback,
    ns: namespaces,
    defaultNS: 'common',
    interpolation: {
      escapeValue: false // not needed for react
    },
    lng: language
  })


export default i18n;