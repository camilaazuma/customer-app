export default productExample = {
    "art_no":"690393",
    "store_no":"72",
    "descr":"rabo suino salg paineira 800g",
    "origen":"brz",
    "pr1":"15.190",
    "prc1vat":"15.190",
    "prc1mea":"18.990",
    "pr2":"0.000",
    "prc2vat":"0.000",
    "prc2mea":"0.000",
    "display":"normal",
    "julian":"18317",
    "iart_threshold":"1",
    "art_grp_no":"71",
    "art_grp_sub_no":"1",
    "dept_no":"30",
    "sell_unit":"unidade",
    "ispromo":"0",
    "instock":"1",
    "iscombo":null,
    "promo_text":"oportunidade",
    "promo_pv":"0",
    "promo_type":null
 }