import { products } from "./offersList"

export const shoppingLists = [
      {
        "index": 1,
        "listName": "Lista Teste 1",
        "lastUpdated": "20/05/2018", 
        "numberOfItems": 40,
        "products": products
      }, 
      {
        "index": 2,
        "listName": "Lista Teste 2",
        "lastUpdated": "10/02/2018", 
        "numberOfItems": 23,
        "products": products
      }, 
      {
        "index": 3,
        "listName": "Lista Teste 3",
        "lastUpdated": "15/05/2018", 
        "numberOfItems": 12,
        "products": products
      }, 
      {
        "index": 4,
        "listName": "Lista Teste 4",
        "lastUpdated": "22/01/2018", 
        "numberOfItems": 4,
        "products": products
      }, 
      {
        "index": 5,
        "listName": "Lista Teste 5",
        "lastUpdated": "20/05/2018", 
        "numberOfItems": 40,
        "products": products
      }, 
      {
        "index": 6,
        "listName": "Lista Teste 6",
        "lastUpdated": "20/05/2018", 
        "numberOfItems": 40,
        "products": products
      }, 
      {
        "index": 7,          
        "listName": "Lista Teste 7",
        "lastUpdated": "20/05/2018", 
        "numberOfItems": 40,
        "products": products
      }, 

]

  