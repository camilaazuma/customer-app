
export const products = [
    {
      "productCategory": "Higiene",
      "productType": "Sabonete",
      "code": "ART 23453",
      "offerActive": false,
      "id": 347586,
      "name":
        "Sabonete Líquido Antibacteriano PROTEX Complete 12 Refil 200ml",
      "type": "simpleProduct",
      "sku": "1078799",
      "currentPrice": 8.49,
      "sellPrice": 8.49,
      "nominalPrice": false,
      "shortDescription":
        "</b><br><b>Refil Sabonete Líquido Protex Complete 12 200ml</b><br><br><b>Características e Benefícios: </b><br>Elimina 99,9% das bactérias, pele protegida por 12 horas, fórmula exclusiva com pH balanceado, ajuda a limpar e remover as impurezas da pele.</b><br><br><b>Ingredientes Ativos:</b> <br>Triclocarban.</b><br><br><b>Instruções de Uso: </b> <br> Aplique um pouco do sabonete líquido sobre uma esponja. Faça uma suave massagem sobre o corpo e rosto, desfrutando da cremosa espuma e deliciosa fragrância. Enxágue em seguida e, se desejar, repita o processo.",
      "alcoholic": false,
      "stock": true,
      "validOnStore": true,
      "thumbPath": "/img/uploads/1/556/526556x50x50.jpg",
      "priceType": "P",
      "totalQuantity": 3,
      "urlDetails":
        "/produto/347586/sabonete-liquido-antibacteriano-protex-complete-12-refil-200ml",
      "mainShelfId": 2258,
      "shelfList": [
        { "active": true, "id": 2258, "name": "Sabonetes", "parentId": 127 },
        { "active": true, "id": 127, "name": "Perfumaria" }
      ],
      "mapOfImages": {
        "0": {
          "BIG": "/img/uploads/1/556/526556.jpg",
          "SMALL": "/img/uploads/1/556/526556x80x80.jpg",
          "MEDIUM": "/img/uploads/1/556/526556x200x200.jpg",
          "ALLOWZOOM": true
        }
      },
      "priceProgressiveMap": {},
      "commercialStructure": "/31/1440/",
      "attributes": {},
      "simpleProduct": true,
      "kit": false,
      "homogeneousKit": false,
      "heterogeneousKit": false
    },
    {
      "productCategory": "Higiene",
      "productType": "Lâmina de barbear",
      "code": "ART 21453",
      "offerActive": false,
      "id": 159050,
      "name": "Aparelho de Barbear GILLETTE Mach 3 Regular",
      "type": "simpleProduct",
      "sku": "3762936",
      "currentPrice": 18.75,
      "sellPrice": 18.75,
      "nominalPrice": false,
      "shortDescription":
        "\nA Gillette Mach 3 proporciona um barbear mais rente e com menos irritação, ainda quando se barbear a contrapelo.\n<br>\n<br>\n<b>Características</b>\n<br>\nA Gillette Mach 3 possui avançadas tecnologias de lâmina tripla e banda lubrificante Lubrastrip, que melhoram o deslizamento, com a finalidade de que os caveleiros possam desfrutar de um barbear mais confortável e rente, com menos irritação ainda se barbeando a contrapelo.\n<br>\n<br>\n<b>Composição</b>\n<br>\n<ul>\n<li>Resinas Termoplásticas, \n<br>\n<li>Aluminio, \n<br>\n<li>Aço Inoxidável revestido com platina, \n<br>\n<li>Cromo, \n<br>\n<li>Politetrafluoroetileno,\n<br>\n<li>Aluminio\n</ul>\n<br>\n<br>\n<b>Instruções de uso</b>\n<br>\nProducto não perecível. Não guardar os cartuchos de Gillette Sensor Excel em locais com temperatura acima de 48°C. Aplique o creme de barbear e deslize o aparelho sobre seu rosto.",
      "alcoholic": false,
      "stock": false,
      "validOnStore": true,
      "thumbPath": "/img/uploads/1/919/446919x50x50.jpg",
      "priceType": "P",
      "totalQuantity": 5,
      "urlDetails":
        "/produto/159050/aparelho-de-barbear-gillette-mach-3-regular",
      "mainShelfId": 5221,
      "shelfList": [
        {
          "active": true,
          "id": 2256,
          "name": "Kits para aparelhos de barba",
          "parentId": 127
        },
        { "active": true, "id": 5221, "name": "Barba", "parentId": 127 },
        { "active": true, "id": 127, "name": "Perfumaria" }
      ],
      "mapOfImages": {
        "0": {
          "BIG": "/img/uploads/1/919/446919.jpg",
          "SMALL": "/img/uploads/1/919/446919x80x80.jpg",
          "MEDIUM": "/img/uploads/1/919/446919x200x200.jpg",
          "ALLOWZOOM": false
        }
      },
      "priceProgressiveMap": {},
      "commercialStructure": "/31/1406/",
      "attributes": {},
      "simpleProduct": true,
      "kit": false,
      "homogeneousKit": false,
      "heterogeneousKit": false
    },
    {
      "productCategory": "Higiene",
      "productType": "Sabonete",
      "code": "ART 23455",
      "offerActive": true,      
      "id": 343133,
      "name": "Sabonete Líquido NIVEA Creme Care 250ml",
      "type": "simpleProduct",
      "sku": "1071846",
      "currentPrice": 7.69,
      "sellPrice": 7.69,
      "nominalPrice": false,
      "alcoholic": false,
      "stock": true,
      "validOnStore": true,
      "thumbPath": "/img/uploads/1/460/488460x50x50.jpg",
      "priceType": "P",
      "totalQuantity": 12,
      "urlDetails": "/produto/343133/sabonete-liquido-nivea-creme-care-250ml",
      "mainShelfId": 2258,
      "shelfList": [
        { "active": true, "id": 2258, "name": "Sabonetes", "parentId": 127 },
        { "active": true, "id": 127, "name": "Perfumaria" }
      ],
      "mapOfImages": {
        "0": {
          "BIG": "/img/uploads/1/460/488460.jpg",
          "SMALL": "/img/uploads/1/460/488460x80x80.jpg",
          "MEDIUM": "/img/uploads/1/460/488460x200x200.jpg",
          "ALLOWZOOM": false
        }
      },
      "priceProgressiveMap": {},
      "commercialStructure": "/31/1440/",
      "attributes": {},
      "simpleProduct": true,
      "kit": false,
      "homogeneousKit": false,
      "heterogeneousKit": false
    },
    {
      "productCategory": "Higiene",
      "productType": "Desodorante",
      "code": "ART 23053",
      "offerActive": false,
      "id": 343362,
      "name": "Desodorante Roll NIVEA Protect & Care 50ml",
      "type": "simpleProduct",
      "sku": "1071845",
      "currentPrice": 9.55,
      "sellPrice": 9.55,
      "nominalPrice": false,
      "alcoholic": false,
      "stock": true,
      "validOnStore": true,
      "thumbPath": "/img/uploads/1/943/486943x50x50.jpg",
      "priceType": "P",
      "totalQuantity": 0,
      "urlDetails":
        "/produto/343362/desodorante-roll-nivea-protect---care-50ml",
      "mainShelfId": 2272,
      "shelfList": [
        {
          "active": true,
          "id": 2272,
          "name": "Desodorantes feminino",
          "parentId": 127
        },
        { "active": true, "id": 127, "name": "Perfumaria" }
      ],
      "mapOfImages": {
        "0": {
          "BIG": "/img/uploads/1/943/486943.jpg",
          "SMALL": "/img/uploads/1/943/486943x80x80.jpg",
          "MEDIUM": "/img/uploads/1/943/486943x200x200.jpg",
          "ALLOWZOOM": false
        }
      },
      "priceProgressiveMap": {},
      "commercialStructure": "/31/116/",
      "attributes": {},
      "simpleProduct": true,
      "kit": false,
      "homogeneousKit": false,
      "heterogeneousKit": false
    },
    {
      "offerActive": false,
      "id": 68327,
      "name": "Talco para os Pés TENYS PÉ Baruel Woman 100g",
      "type": "simpleProduct",
      "sku": "1022988",
      "currentPrice": 6.19,
      "sellPrice": 6.19,
      "nominalPrice": false,
      "shortDescription":
        "<b>Composição:</b><br> Extrato de calêndula, amido de mandioca, carbonato de magnésio, dióxido de silício, ácido benzóico, ácido bónico, estearato de zinco e fragrância.",
      "alcoholic": false,
      "stock": true,
      "validOnStore": true,
      "thumbPath": "/img/uploads/1/612/468612x50x50.jpg",
      "priceType": "P",
      "totalQuantity": 0,
      "urlDetails":
        "/produto/68327/talco-para-os-pes-tenys-pe-baruel-woman-100g",
      "mainShelfId": 2278,
      "shelfList": [
        { "active": true, "id": 2278, "name": "Talcos", "parentId": 127 },
        { "active": true, "id": 127, "name": "Perfumaria" }
      ],
      "mapOfImages": {
        "0": {
          "BIG": "/img/uploads/1/612/468612.jpg",
          "SMALL": "/img/uploads/1/612/468612x80x80.jpg",
          "MEDIUM": "/img/uploads/1/612/468612x200x200.jpg",
          "ALLOWZOOM": false
        }
      },
      "priceProgressiveMap": {},
      "commercialStructure": "/31/116/",
      "attributes": {},
      "simpleProduct": true,
      "kit": false,
      "homogeneousKit": false,
      "heterogeneousKit": false
    },
    {
      "offerActive": false,
      "id": 174511,
      "name": "Sabonete Antibacteriano PROTEX Limpeza Profunda 90g",
      "type": "simpleProduct",
      "sku": "4524458",
      "currentPrice": 1.99,
      "sellPrice": 1.99,
      "nominalPrice": false,
      "shortDescription":
        "</b><br><b> Sabonete PROTEX Limpeza Profunda 90G.<b><br><br><b>Características e Benefícios: </b><br> Protex Limpeza Profunda ajuda a eliminar as bactérias e remover dos poros as impurezas da e oleosidade que podem causar cravos e espinhas. Contém extratos de algas marinhas, conhecido pelos seus benefícios de limpeza profunda e remoção de impurezas.</b><br><br><b>Ingredientes Ativos:</b> <br> Triclocarban.</b><br><br><b>Instruções de Uso: </b><br> Molhar o corpo ou a região desejada, ensaboar normalmente e enxaguar.",
      "alcoholic": false,
      "stock": true,
      "validOnStore": true,
      "thumbPath": "/img/uploads/1/544/526544x50x50.jpg",
      "priceType": "P",
      "totalQuantity": 0,
      "urlDetails":
        "/produto/174511/sabonete-antibacteriano-protex-limpeza-profunda-90g",
      "mainShelfId": 2258,
      "shelfList": [
        { "active": true, "id": 2258, "name": "Sabonetes", "parentId": 127 },
        { "active": true, "id": 127, "name": "Perfumaria" }
      ],
      "mapOfImages": {
        "0": {
          "BIG": "/img/uploads/1/544/526544.jpg",
          "SMALL": "/img/uploads/1/544/526544x80x80.jpg",
          "MEDIUM": "/img/uploads/1/544/526544x200x200.jpg",
          "ALLOWZOOM": true
        },
        "1": {
          "BIG": "/img/uploads/1/513/526513.jpg",
          "SMALL": "/img/uploads/1/513/526513x80x80.jpg",
          "MEDIUM": "/img/uploads/1/513/526513x200x200.jpg",
          "ALLOWZOOM": true
        }
      },
      "priceProgressiveMap": {},
      "commercialStructure": "/31/1440/",
      "attributes": {},
      "simpleProduct": true,
      "kit": false,
      "homogeneousKit": false,
      "heterogeneousKit": false
    },
    {
      "offerActive": false,
      "id": 164032,
      "name": "Loção Pós Barba NIVEA For Men 100ml",
      "type": "simpleProduct",
      "sku": "0675710",
      "currentPrice": 27.75,
      "sellPrice": 27.75,
      "nominalPrice": false,
      "shortDescription":
        "<b>Composição.</b><br> Água, álcool denat. glicerina, C12-15 benzoato de alquila, triglicerídio cáprico/caprílico, pantenol, fragrância, palmitato de isopropila, acetato de tocoferol, bisabolol, extrato de slit sea, extrato de oyster shell, esqualano, cetomacrogol-10, crosspolímero de aquil acrilatos C10-30/acrilatos de sódio, EDTA dissódico e propilenoglicol.",
      "alcoholic": false,
      "stock": true,
      "validOnStore": true,
      "thumbPath": "/img/uploads/1/363/489363x50x50.jpg",
      "priceType": "P",
      "totalQuantity": 0,
      "urlDetails": "/produto/164032/locao-pos-barba-nivea-for-men-100ml",
      "mainShelfId": 2273,
      "shelfList": [
        {
          "active": true,
          "id": 2273,
          "name": "Cremes pré e pós-barba",
          "parentId": 127
        },
        { "active": true, "id": 127, "name": "Perfumaria" }
      ],
      "mapOfImages": {
        "0": {
          "BIG": "/img/uploads/1/363/489363.jpg",
          "SMALL": "/img/uploads/1/363/489363x80x80.jpg",
          "MEDIUM": "/img/uploads/1/363/489363x200x200.jpg",
          "ALLOWZOOM": false
        }
      },
      "priceProgressiveMap": {},
      "commercialStructure": "/31/1760/",
      "attributes": {},
      "simpleProduct": true,
      "kit": false,
      "homogeneousKit": false,
      "heterogeneousKit": false
    },
    {
      "offerActive": false,
      "id": 113273,
      "name": "Desodorante Aerosol NIVEA For Men Sensitive 94g",
      "type": "simpleProduct",
      "sku": "3965610",
      "currentPrice": 10.9,
      "sellPrice": 10.9,
      "nominalPrice": false,
      "shortDescription":
        "<b>Composição:</b><br>Butane, isobutane, propane, cyclomethicone, aluminum chlorohydrate, dimethicone, disteardimonium hectorite, octydodecanol, peresa gratissima, tocopheryl acetate, fragrance.",
      "alcoholic": false,
      "stock": true,
      "validOnStore": true,
      "thumbPath": "/img/uploads/1/200/516200x50x50.jpg",
      "priceType": "P",
      "totalQuantity": 0,
      "urlDetails":
        "/produto/113273/desodorante-aerosol-nivea-for-men-sensitive-94g",
      "mainShelfId": 2275,
      "shelfList": [
        {
          "active": true,
          "id": 2275,
          "name": "Desodorantes masculino",
          "parentId": 127
        },
        { "active": true, "id": 127, "name": "Perfumaria" }
      ],
      "mapOfImages": {
        "0": {
          "BIG": "/img/uploads/1/200/516200.jpg",
          "SMALL": "/img/uploads/1/200/516200x80x80.jpg",
          "MEDIUM": "/img/uploads/1/200/516200x200x200.jpg",
          "ALLOWZOOM": false
        }
      },
      "priceProgressiveMap": {},
      "commercialStructure": "/31/116/",
      "attributes": {},
      "simpleProduct": true,
      "kit": false,
      "homogeneousKit": false,
      "heterogeneousKit": false
    },
    {
      "offerActive": false,
      "id": 158229,
      "name": "Sabonete Líquido Infantil GRANADO Neutro 250ml",
      "type": "simpleProduct",
      "sku": "3992319",
      "currentPrice": 17.5,
      "sellPrice": 17.5,
      "nominalPrice": false,
      "shortDescription":
        "<b>Composição:</b><br>Sodium lauryl sulfate, tea-cocoyl glutamate, decyl glucoside, cocamidopropyl betaine, cocamide dea, peg-150 distearate, glycerin, methylchloroisothiazolinone, methylisothiazolinone, tris (tetramethylhydroxypiperidinol), citrate, sodium benzotriazolyl butylphenol sulfonate, CI 19140, fragrance, citric acid, aqua.",
      "alcoholic": false,
      "stock": true,
      "validOnStore": true,
      "thumbPath": "/img/uploads/1/390/445390x50x50.jpg",
      "priceType": "P",
      "totalQuantity": 0,
      "urlDetails":
        "/produto/158229/sabonete-liquido-infantil-granado-neutro-250ml",
      "mainShelfId": 2258,
      "shelfList": [
        { "active": true, "id": 2258, "name": "Sabonetes", "parentId": 127 },
        { "active": true, "id": 127, "name": "Perfumaria" }
      ],
      "mapOfImages": {
        "0": {
          "BIG": "/img/uploads/1/390/445390.jpg",
          "SMALL": "/img/uploads/1/390/445390x80x80.jpg",
          "MEDIUM": "/img/uploads/1/390/445390x200x200.jpg",
          "ALLOWZOOM": false
        },
        "1": {
          "BIG": "/img/uploads/1/567/404567.jpg",
          "SMALL": "/img/uploads/1/567/404567x80x80.jpg",
          "MEDIUM": "/img/uploads/1/567/404567x200x200.jpg",
          "ALLOWZOOM": false
        }
      },
      "priceProgressiveMap": {},
      "commercialStructure": "/31/1706/",
      "attributes": {},
      "simpleProduct": true,
      "kit": false,
      "homogeneousKit": false,
      "heterogeneousKit": false
    },
    {
      "offerActive": false,
      "id": 34308,
      "name": "Sabonete de Glicerina PHEBO Frescor da Manhã 90g",
      "type": "simpleProduct",
      "sku": "4419464",
      "currentPrice": 3.35,
      "sellPrice": 3.35,
      "nominalPrice": false,
      "alcoholic": false,
      "stock": true,
      "validOnStore": true,
      "thumbPath": "/img/uploads/1/76/282076x50x50.jpg",
      "priceType": "P",
      "totalQuantity": 0,
      "urlDetails":
        "/produto/34308/sabonete-de-glicerina-phebo-frescor-da-manha-90g",
      "mainShelfId": 2258,
      "shelfList": [
        { "active": true, "id": 2258, "name": "Sabonetes", "parentId": 127 },
        { "active": true, "id": 127, "name": "Perfumaria" }
      ],
      "mapOfImages": {
        "0": {
          "BIG": "/img/uploads/1/76/282076.jpg",
          "SMALL": "/img/uploads/1/76/282076x80x80.jpg",
          "MEDIUM": "/img/uploads/1/76/282076x200x200.jpg",
          "ALLOWZOOM": false
        }
      },
      "priceProgressiveMap": {},
      "commercialStructure": "/31/1440/",
      "attributes": {},
      "simpleProduct": true,
      "kit": false,
      "homogeneousKit": false,
      "heterogeneousKit": false
    },
    {
      "offerActive": false,
      "id": 112093,
      "name": "Creme de Tratamento OX Oils Nutrição Intensa Pote 300g",
      "type": "simpleProduct",
      "sku": "6053932",
      "currentPrice": 20.9,
      "sellPrice": 20.9,
      "nominalPrice": false,
      "alcoholic": false,
      "stock": true,
      "validOnStore": true,
      "thumbPath": "/img/uploads/1/72/476072x50x50.jpg",
      "priceType": "P",
      "totalQuantity": 0,
      "urlDetails":
        "/produto/112093/creme-de-tratamento-ox-oils-nutricao-intensa-pote-300g",
      "mainShelfId": 2249,
      "shelfList": [
        {
          "active": true,
          "id": 2247,
          "name": "Modeladores",
          "parentId": 127
        },
        {
          "active": true,
          "id": 2249,
          "name": "Tratamentos e reparações ",
          "parentId": 127
        },
        { "active": true, "id": 127, "name": "Perfumaria" }
      ],
      "mapOfImages": {
        "0": {
          "BIG": "/img/uploads/1/72/476072.jpg",
          "SMALL": "/img/uploads/1/72/476072x80x80.jpg",
          "MEDIUM": "/img/uploads/1/72/476072x200x200.jpg",
          "ALLOWZOOM": false
        }
      },
      "priceProgressiveMap": {},
      "commercialStructure": "/31/1436/",
      "attributes": {},
      "simpleProduct": true,
      "kit": false,
      "homogeneousKit": false,
      "heterogeneousKit": false
    },
    {
      "offerActive": false,
      "id": 13369,
      "name": "Absorvente INTIMUS GEL Soft sem Abas Pacote com 8 Unidades",
      "type": "simpleProduct",
      "sku": "0568425",
      "currentPrice": 4.25,
      "sellPrice": 4.25,
      "nominalPrice": false,
      "shortDescription":
        "<b>Composição:</b><br> Filme de polietileno, fibras de celulose, polímero superabsorvente, polipropileno, adesivos termoplásticos e papel siliconado.",
      "alcoholic": false,
      "stock": true,
      "validOnStore": true,
      "thumbPath": "/img/uploads/1/843/450843x50x50.jpg",
      "priceType": "P",
      "totalQuantity": 0,
      "urlDetails":
        "/produto/13369/absorvente-intimus-gel-soft-sem-abas-pacote-com-8-unidades",
      "mainShelfId": 2287,
      "shelfList": [
        {
          "active": true,
          "id": 2287,
          "name": "Absorventes",
          "parentId": 127
        },
        { "active": true, "id": 127, "name": "Perfumaria" }
      ],
      "mapOfImages": {
        "0": {
          "BIG": "/img/uploads/1/843/450843.jpg",
          "SMALL": "/img/uploads/1/843/450843x80x80.jpg",
          "MEDIUM": "/img/uploads/1/843/450843x200x200.jpg",
          "ALLOWZOOM": false
        }
      },
      "priceProgressiveMap": {},
      "commercialStructure": "/43/1604/",
      "attributes": {},
      "simpleProduct": true,
      "kit": false,
      "homogeneousKit": false,
      "heterogeneousKit": false
    }
  ]