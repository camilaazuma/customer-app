import React from "react";
import { Provider } from 'react-redux'
import RootStack from "./src/config/routes";
import { NetInfo, Alert, Image } from "react-native";
import configureStore from "./src/store/configureStore";
import { SQLite, FileSystem, Asset } from 'expo';
import firebase from "react-native-firebase";
import { cacheImages } from './src/utils';
import { Permissions } from 'expo-permissions';
import i18n from "./src/config/i18n";
Asset;

const store = configureStore()

export default class App extends React.Component {

  async makeSQLiteDirAsync() {
    const dbDummy = SQLite.openDatabase('dummy.db');
   
    try {
      await dbDummy.transaction(tx => tx.executeSql(''));
    } catch(e) {
      if (this.state.debugEnabled) console.log('error while executing SQL in dummy DB');
    }
  }

  async loadDB() {

    await this.makeSQLiteDirAsync();
    //workaround: .db was not being recognized
    await FileSystem.downloadAsync(Asset.fromModule(require("./assets/sql.mp4")).uri, FileSystem.documentDirectory + 'SQLite/sql.db');

    this.db = SQLite.openDatabase('sql.db');

  }

  async componentDidMount(){
    this.loadDB();
    await cacheImages([require('./assets/374.png'), require('./assets/makro-logo.png')]);

    // Set default values for remote config
    firebase.config().setDefaults({
      store_version: "default",
    });
  }

  render() {
    return (
      <Provider store={store}>
        <RootStack />
      </Provider>
    );
  }
}
