fastlane documentation
================
# Installation

Make sure you have the latest version of the Xcode command line tools installed:

```
xcode-select --install
```

Install _fastlane_ using
```
[sudo] gem install fastlane -NV
```
or alternatively using `brew cask install fastlane`

# Available Actions
## iOS
### ios new_version
```
fastlane ios new_version
```

### ios reset_makro_config
```
fastlane ios reset_makro_config
```

### ios prepare_dev
```
fastlane ios prepare_dev
```

### ios generate_beta
```
fastlane ios generate_beta
```

### ios beta
```
fastlane ios beta
```


----

This README.md is auto-generated and will be re-generated every time [fastlane](https://fastlane.tools) is run.
More information about fastlane can be found on [fastlane.tools](https://fastlane.tools).
The documentation of fastlane can be found on [docs.fastlane.tools](https://docs.fastlane.tools).
